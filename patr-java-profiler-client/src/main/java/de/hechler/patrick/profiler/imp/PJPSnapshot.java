// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.imp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PJPSnapshot {
	
	public final long         time;
	private final PJPThread[] threads;
	
	public PJPSnapshot(long time, List<PJPThread> threads) {
		this.time = time;
		this.threads = threads.toArray(new PJPThread[threads.size()]);
		Arrays.sort(this.threads);
	}
	
	public int threadCount() {
		return threads.length;
	}
	
	public PJPThread thread(int i) {
		return threads[i];
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.threads);
		result = prime * result + (int) ( this.time ^ ( this.time >>> 32 ) );
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( this == obj ) {
			return true;
		}
		if ( !( obj instanceof PJPSnapshot ) ) {
			return false;
		}
		PJPSnapshot other = (PJPSnapshot) obj;
		if ( !Arrays.equals(this.threads, other.threads) ) {
			return false;
		}
		if ( this.time != other.time ) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("Snapshot [");
		b.append(this.threads.length);
		b.append(" threads]");
		return b.toString();
	}
	
	public void append(Appendable a) throws IOException {
		a.append("PJPSnapshot:\n");
		for (PJPThread t : threads) {
			t.append(a);
		}
	}
	
}
