// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.term;

import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_BC_PREFIX_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_C_BLACK_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_C_CYAN_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_C_RGB_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_END;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_FC_PREFIX_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_RESET_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_SEP;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.FC_CYAN;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.FC_GREEN;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.FC_MAGENTA;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.FC_RED;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.FC_RGB_GRAY;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.RESET;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_NEXT_LINE;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_START_OF_DISPLAY;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.ERASE_COMPLETE_DISPLAY;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.ERASE_END_OF_DISPLAY;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.ERASE_END_OF_LINE;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.cursorLine;

import java.awt.Point;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import de.hechler.patrick.profiler.client.Version;
import de.hechler.patrick.profiler.client.ui.PJPUserInterface;
import de.hechler.patrick.profiler.client.ui.utils.ExecutingNode;
import de.hechler.patrick.profiler.client.ui.utils.Node;
import de.hechler.patrick.profiler.client.ui.utils.NodeMerger;
import de.hechler.patrick.profiler.imp.PJPExecutingStack;
import de.hechler.patrick.profiler.imp.PJPSnapshot;
import de.hechler.patrick.profiler.imp.PJPStackFrame;
import de.hechler.patrick.profiler.imp.PJPThread;

public class PJPTerm extends PJPAbstractTerm implements PJPUserInterface {
	
	protected Point worldPos    = new Point();
	protected Point worldBounds = new Point(Integer.MAX_VALUE / 2, 1);
	
	private final TermNode root = new TermNode(null, 0, 0, null);
	
	private TermNode selected = root;
	
	private final NodeMerger<TermNode> nmerge = new NodeMerger<TermNode>() {
		
		@Override
		protected TermNode newNode(TermNode parent, Object child) {
			return new TermNode(parent, parent.indent + 4, calcOwnLen(child), child);
		}
		
		@Override
		protected void remove(TermNode removedNode, List<TermNode> path, @SuppressWarnings("unused") int childIndex) {
			for (TermNode tm : path) { if ( !tm.visibleChildren ) break; tm.length -= removedNode.length; }
			worldBounds.y = root.length;
		}
		
		@Override
		protected void insert(TermNode insertedNode, List<TermNode> path, @SuppressWarnings("unused") int childIndex) {
			for (TermNode tm : path) { if ( !tm.visibleChildren ) break; tm.length += insertedNode.length; }
			worldBounds.y = root.length;
		}
		
		@Override
		protected void change(TermNode changedNode, @SuppressWarnings("unused") List<TermNode> path,
			@SuppressWarnings("unused") int childIndex) {
			int newOwnLen = calcOwnLen(changedNode.value);
			if ( newOwnLen != changedNode.ownLength ) {
				int diff = newOwnLen - changedNode.ownLength;
				// new = old + diff
				// new - old = diff
				changeSize(changedNode, diff);
				changedNode.ownLength = newOwnLen;
			}
		}
		
	};
	
	private String   remoteAdress;
	private String   remoteInfo;
	private String[] remoteInfos;
	private String   error;
	
	public PJPTerm() throws IOException {
		super();
	}
	
	public PJPTerm(String tty) throws IOException {
		super(tty);
	}
	
	public PJPTerm(OutputStreamWriter out, WritebackReader in) {
		super(out, in);
	}
	
	@Override
	protected void initTerm(String tty) throws IOException {
		super.initTerm(tty);
		out.write(ERASE_COMPLETE_DISPLAY);
	}
	
	public PJPTerm init(String remote, Version remotePjpVersion, int remoteJvmVersion) {
		this.remoteAdress = remote;
		String pjpShortVersion = remotePjpVersion.withMaxLength(32);
		this.remoteInfo = "  patrjprof " + pjpShortVersion + ", jvm " + remoteJvmVersion;
		this.remoteInfos = new String[]{ "patrjprof " + pjpShortVersion, "jvm " + remoteJvmVersion };
		return this;
	}
	
	public PJPTerm init(String remote) {
		this.remoteAdress = remote;
		this.remoteInfo = null;
		return this;
	}
	
	@Override
	public void close() throws Exception {
		this.in.close();
		this.out.close();
	}
	
	@Override
	public void state(int state) {
		String apppend;
		switch ( state ) {
		case STATE_RUNNING:
			apppend = null;
			break;
		case STATE_FINAL_SNAPSHOT:
			apppend = "final state";
			break;
		case STATE_TERMINATED_NO_SN:
			apppend = "terminated";
			break;
		case STATE_CONNECTION_LOST:
			apppend = "connection lost";
			break;
		default:
			throw new IllegalArgumentException("illegal state: " + state);
		}
		String[] vals = apppend == null ? remoteInfos : Arrays.copyOf(remoteInfos, remoteInfos.length + 1);
		if ( apppend != null ) {
			vals[vals.length - 1] = apppend;
		}
		StringJoiner sj = new StringJoiner(", ", "  ", "");
		for (String val : vals) {
			sj.add(val);
		}
		remoteInfo = sj.toString();
	}
	
	@Override
	public synchronized void update(PJPSnapshot show) {
		if ( show == null ) {
			root.value = null;
			root.ownLength = root.length = 0;
			root.children = null;
			root.visibleChildren = false;
			worldPos.x = worldPos.y = 0;
			worldBounds.x = worldBounds.y = 1;
			return;
		}
		if ( root.value == null ) {
			root.value = show;
			root.ownLength = calcOwnLen(show);
			genChildren(root);
			worldPos.x = worldPos.y = 0;
			worldBounds.x = Integer.MAX_VALUE / 2;
			worldBounds.y = root.length;
			return;
		}
		nmerge.merge(root, show, new ArrayList<>(), 0);
	}
	
	private void genChildren(TermNode tn) {
		Object value = tn.value;
		int cc = Node.extractChildCount(value);
		tn.children = new ArrayList<>(cc);
		int newLen = tn.ownLength;
		int ci = tn.indent + 4;
		for (int i = 0; i < cc; i++) {
			Object cv = Node.generateChild(value, i);
			int ol = calcOwnLen(cv);
			newLen += ol;
			tn.children.add(new TermNode(tn, ci, ol, cv));
		}
		tn.visibleChildren = true;
		changeSize(tn, newLen - tn.length);
		if ( newLen != tn.length ) {
			throw new AssertionError("matematical error");
		}
	}
	
	private void addChildrenLength(TermNode tn) {
		int cc = tn.children.size();
		int newLen = tn.ownLength;
		for (int i = 0; i < cc; i++) {
			TermNode cv = (TermNode) tn.children.get(i);
			newLen += cv.length;
		}
		tn.visibleChildren = true;
		changeSize(tn, newLen - tn.length);
		if ( tn.length != newLen ) {
			throw new AssertionError("matematical error");
		}
	}
	
	@Override
	public void error(Throwable t) {
		error = t.toString();
	}
	
	/**
	 * flag which indicates that the terminal currently reads a command<br>
	 * used so that if the world is written during this time the last line is not modified
	 */
	protected boolean inReadAndExec;
	
	@Override
	protected void readAndExecCmd(StringBuilder cmd) throws IOException {
		Point max = maxDisplayPos();
		int line = this.headerLines() + 1;
		int maxLine = max.y - 1;
		int remainingY = this.worldBounds.y - this.worldPos.y;
		int stepsLine = line + remainingY + 1;
		this.out.write(cursorLine(Math.min(stepsLine, maxLine)));
		showWorldDownBorder(this.out, max, this.worldPos.x);
		try {
			inReadAndExec = true;
			super.readAndExecCmd(cmd);
		} finally {
			inReadAndExec = false;
		}
	}
	
	@Override
	protected void showWorld(Appendable b, Point max) throws IOException {
		int line = headerLines();
		if ( line >= max.y || max.x < 3 ) {
			b.append("the terminal is too small (I need more than " + line + " lines and at lest 3 columns)");
			return;
		}
		Point pos = new Point(this.worldPos);
		final int wpx = pos.x;
		b.append(
			CSI + CSI_RESET_C + CSI_SEP + CSI_BC_PREFIX_C + CSI_C_CYAN_C + CSI_SEP + CSI_FC_PREFIX_C + CSI_C_BLACK_C + CSI_END //
				+ "\u250C"//
				+ repeat("\u2500", Math.min(max.x - 2, ( this.worldBounds.x - wpx )))//
				+ "\u2510" + RESET + CURSOR_NEXT_LINE);
		line++;
		for (; line < max.y - 1; line++, pos.y++, pos.x = wpx) {
			if ( pos.y >= this.worldBounds.y ) {
				break;
			}
			appendLine(b, pos, max.x);
			b.append(ERASE_END_OF_LINE + CURSOR_NEXT_LINE);
		}
		showWorldDownBorder(b, max, wpx);
		if ( inReadAndExec ) {
			line++;
			for (; line < max.y; line++) {
				b.append(CURSOR_NEXT_LINE + ERASE_END_OF_LINE);
			}
		} else {
			b.append(ERASE_END_OF_DISPLAY);
		}
	}
	
	protected void appendLine(Appendable b, Point pos, int lastColumn) throws IOException {
		b.append(CSI + CSI_RESET_C + CSI_SEP + CSI_BC_PREFIX_C + CSI_C_CYAN_C + CSI_SEP + CSI_FC_PREFIX_C + CSI_C_BLACK_C
			+ CSI_END + "\u2502" + RESET);
		int len = lastColumn - 2;
		if ( pos.x > this.worldBounds.x - len ) {
			len = this.worldBounds.x - pos.x;
		}
		appendContent(b, pos, len);
		b.append(CSI + CSI_RESET_C + CSI_SEP//
			+ CSI_BC_PREFIX_C + CSI_C_CYAN_C + CSI_SEP//
			+ CSI_FC_PREFIX_C + CSI_C_BLACK_C + CSI_END//
			+ "\u2502"//
			+ ERASE_END_OF_LINE //
			+ RESET);
	}
	
	protected void showWorldDownBorder(Appendable b, Point max, final int worldPosX) throws IOException {
		b.append(CSI + CSI_RESET_C + CSI_SEP//
			+ CSI_BC_PREFIX_C + CSI_C_CYAN_C + CSI_SEP//
			+ CSI_FC_PREFIX_C + CSI_C_BLACK_C + CSI_END //
			+ "\u2514"//
			+ repeat("\u2500", Math.min(max.x - 2, ( this.worldBounds.x - worldPosX )))//
			+ "\u2518"//
			+ RESET//
			+ ERASE_END_OF_LINE//
		);
	}
	
	private String repeat(String str, int rep) {
		StringBuilder sb = new StringBuilder(str.length() * rep);
		while ( --rep >= 0 ) {
			sb.append(str);
		}
		return sb.toString();
	}
	
	@Override
	protected void actCtrlPos1() throws IOException {
		worldPos.x = 0;
		worldPos.y = 0;
	}
	
	@Override
	protected void actCtrlEnd() throws IOException {
		worldPos.y = worldBounds.y - usableWorldLines();
	}
	
	@Override
	protected void actPos1() throws IOException {
		worldPos.x = 0;
	}
	
	@Override
	protected void actEnd() throws IOException {
	}
	
	@Override
	protected void actPageUp() throws IOException {
		worldPos.y = Math.max(0, worldPos.y - usableWorldLines());
	}
	
	@Override
	protected void actPageDown() throws IOException {
		int uwl = usableWorldLines();
		worldPos.y = Math.max(0, Math.min(worldBounds.y - uwl, worldPos.y + uwl));
	}
	
	@Override
	protected void actW() throws IOException {
		worldPos.y = Math.max(0, worldPos.y - 1);
	}
	
	@Override
	protected void actA() throws IOException {
		worldPos.x = Math.max(0, worldPos.x - 1);
	}
	
	@Override
	protected void actS() throws IOException {
		worldPos.y = Math.max(0, Math.min(worldBounds.y - usableWorldLines(), worldPos.y + 1));
	}
	
	@Override
	protected void actD() throws IOException {
		worldPos.x = Math.max(0, Math.min(worldBounds.x - usableWorldColumns(), worldPos.x + 1));
	}
	
	@Override
	protected synchronized void actArrowUp() throws IOException {
		TermNode top = selected;
		if ( selected.parent != null ) {
			TermNode parent = selected.parent;
			int child = NodeMerger.findChild(parent.children, top.value);
			if ( child > 0 ) {
				TermNode newSelected = (TermNode) parent.children.get(child - 1);
				selected = newSelected;
				while ( newSelected.visibleChildren && !newSelected.children.isEmpty() ) {
					newSelected = (TermNode) newSelected.children.get(newSelected.children.size() - 1);
					selected = newSelected;
				}
			} else {
				selected = selected.parent;
			}
		}
		show(selected);
	}
	
	@Override
	protected synchronized void actArrowDown() throws IOException {
		TermNode top = selected;
		if ( top.visibleChildren && !top.children.isEmpty() ) {
			selected = (TermNode) top.children.get(0);
			show(selected);
			return;
		}
		while ( top.parent != null ) {
			TermNode parent = top.parent;
			int child = NodeMerger.findChild(parent.children, top.value);
			if ( child + 1 < parent.children.size() ) {
				selected = (TermNode) parent.children.get(child + 1);
				break;
			}
			top = parent;
		}
		show(selected);
	}
	
	@Override
	protected synchronized void actArrowLeft() throws IOException {
		TermNode top = selected;
		if ( top.visibleChildren && !top.children.isEmpty() ) {
			top.visibleChildren = false;
			int diff = top.length - top.ownLength;
			changeSize(top, -diff);
			if ( top.length != top.ownLength ) {
				throw new AssertionError("mathematical error: " + top.length + " : " + top.ownLength);
			}
		} else if ( top != root ) {
			top.visibleChildren = false;
			selected = selected.parent;
		}
		show(selected);
	}
	
	@Override
	protected synchronized void actArrowRight() throws IOException {
		TermNode top = selected;
		if ( top.visibleChildren && !top.children.isEmpty() ) {
			selected = (TermNode) top.children.get(0);
			show(selected);
			return;
		}
		if ( top.children == null ) {
			if ( Node.isLeaf(top) ) {
				show(selected);
				return;
			}
			genChildren(top);
			show(selected);
			return;
		}
		if ( !top.visibleChildren ) {
			addChildrenLength(top);
		} else if ( !top.children.isEmpty() ) {
			selected = (TermNode) top.children.get(0);
		}
		show(selected);
	}
	
	private void changeSize(TermNode top, int diff) throws AssertionError {
		TermNode tn = top;
		do {
			tn.length += diff;
			tn = tn.parent;
		} while ( tn != null );
		worldBounds.y = root.length;
	}
	
	protected void show(TermNode tn) {
		int offY = 0;
		for (TermNode ttn = tn; ttn.parent != null; ttn = ttn.parent) {
			offY += ttn.parent.ownLength;
			for (int i = 0;; i++) {
				TermNode s = (TermNode) ttn.parent.children.get(i);
				if ( s == ttn ) {
					break;
				}
				offY += s.length;
			}
		}
		int uwl = usableWorldLines();
		int uwc = usableWorldColumns();
		int offX = tn.indent;
		if ( worldPos.y > offY - ( uwl > 16 ? uwl / 4 : 4 ) ) {
			if ( uwl > 16 ) {
				worldPos.y = Math.max(0, offY - uwl / 4);
			} else {
				worldPos.y = offY;
			}
		} else if ( worldPos.y + uwl - ( uwl > 16 ? uwl / 4 : 4 ) < offY ) {
			if ( uwl > 16 ) {
				worldPos.y = offY - uwl + uwl / 4;
			} else {
				worldPos.y = offY - uwl + tn.ownLength;
			}
		}
		if ( worldPos.x + uwc / 16 > offX ) {
			worldPos.x = uwc > 32 ? Math.max(0, offX - uwc / 16) : offX;
		} else if ( worldPos.x + ( uwc / 4 ) < offX ) {
			worldPos.x = uwc > 32 ? Math.max(0, offX - uwc / 4) : offX;
		}
		worldPos.x = correctCoordinate(worldPos.x, worldBounds.x - uwc - 1);
		worldPos.y = correctCoordinate(worldPos.y, worldBounds.y - uwl - 1);
	}
	
	/**
	 * corrects the given coordinate<br>
	 * if {@code position} is outside of the range {@code 0..max} the nearest value inside of it is returned<br>
	 * if {@code max} is less than {@code 0} the returned value is {@code 0}
	 * 
	 * @param position the position to correct
	 * @param max      the maximum position
	 * 
	 * @return the corrected position
	 */
	private static int correctCoordinate(int position, int max) {
		if ( position > max ) {
			position = max;
		}
		if ( position < 0 ) {
			position = 0;
		}
		return position;
	}
	
	protected String help0;
	protected String help1;
	protected String help2;
	
	private String[] helps = new String[]{//@formatter:off
		"help: [PAGE] show the given help page",
		"exit: [EXIT_NUM] exit with num",
		"quit: exit with 0 (or 1 if invalid used)",
		"clear: clear complete display",
	};//@formatter:on
	
	@Override
	protected void execCommand(String[] args) throws IOException {
		if ( "help".equals(args[0]) ) {
			int index;
			if ( args.length == 1 ) {
				index = 0;
			} else if ( args.length != 2 ) {
				addUnknownInfo(UIMP_ILLEGAL_USAGE, "Usage: help [HELP_PAGE]");
				return;
			} else {
				try {
					int page = Integer.parseInt(args[1]) - 1;
					if ( page < 0 ) {
						addUnknownInfo(UIMP_ILLEGAL_USAGE, "PAGE <= 0 (" + page + ")");
						return;
					}
					index = page * 3;
					if ( index / 3 != page || index >= helps.length ) {
						addUnknownInfo(UIMP_ILLEGAL_USAGE, "PAGE > " + ( helps.length + 2 ) / 3);
						return;
					}
				} catch ( @SuppressWarnings("unused") NumberFormatException e ) {
					if ( "help".equals(args[1]) ) {
						addUnknownInfo(UIMP_USER_REQUEST, "Usage: help [HELP_PAGE]");
					} else {
						addUnknownInfo(UIMP_ILLEGAL_USAGE, "invalid page: '" + args[1] + '\'');
					}
					return;
				}
			}
			help0 = helps[index];
			if ( index < helps.length - 1 ) {
				help1 = helps[index + 1];
			}
			if ( index < helps.length - 2 ) {
				help2 = helps[index + 2];
			}
		} else
		
		{
			super.execCommand(args);
		}
	}
	
	@Override
	protected int read(char[] buf, int off, int len) throws IOException {
		Point mdp = this.displayBounds;
		if ( mdp != null && inReadAndExec && mdp.x > 1 && mdp.y > 2 ) {
			mdp = new Point(mdp.x, mdp.y - 1);
		}
		for (int cnt = 1; !in.ready(); cnt++) {
			if ( ( cnt & 0xF ) != 0 ) {
				try {
					Thread.sleep(10L);
				} catch ( InterruptedException e ) {
					InterruptedIOException iioe = new InterruptedIOException();
					iioe.initCause(e);
					throw iioe;
				}
			} else {
				if ( mdp != null && mdp.x > 1 && mdp.y > 1 ) {
					super.showWorld(mdp);
					// clear help only on user activity
				}
			}
		}
		return super.read(buf, off, len);
	}
	
	@Override
	protected void clearUnknownInfo() {
		super.clearUnknownInfo();
		this.help0 = null;
		this.help1 = null;
		this.help2 = null;
	}
	
	private int usableWorldLines() {
		return maxDisplayPos().y - headerLines() - 2 - 1;
	}
	
	private int usableWorldColumns() {
		return maxDisplayPos().x - 2 - 1;
	}
	
	protected int headerLines() {
		return 5;
	}
	
	@Override
	protected void showHeaderLines(Appendable b, Point max) throws IOException {
		String line0 = "PatrJProf: " + remoteAdress;
		b.append(CURSOR_START_OF_DISPLAY);
		append(b, max, line0);
		b.append(ERASE_END_OF_LINE);
		String line0End = "use the arrow keys to select          ";
		String line1End = "use WASD to navigate without selecting";
		String line2End = "use Ctrl+D to terminate               ";
		String line3End = "use :<COMMAND>\\n to execute command   ";
		String line4End = "(see :help)                         ";
		showEnd(b, max, line0, line0End);
		b.append(CURSOR_NEXT_LINE + ERASE_END_OF_LINE);
		String line1 = error == null ? remoteInfo : error;
		if ( line1 != null ) append(b, max, line1);
		showEnd(b, max, line1, line1End);
		if ( error != null ) showMid(b, max, line1, remoteInfo, line1End);
		b.append(CURSOR_NEXT_LINE + ERASE_END_OF_LINE);
		if ( help0 != null ) append(b, max, help0);
		if ( unknownInfo != null ) showMid(b, max, help0, unknownInfo);
		showEnd(b, max, help0, unknownInfo, line2End);
		b.append(CURSOR_NEXT_LINE + ERASE_END_OF_LINE);
		if ( help1 != null ) append(b, max, help1);
		showEnd(b, max, help1, line3End);
		b.append(CURSOR_NEXT_LINE + ERASE_END_OF_LINE);
		if ( help2 != null ) append(b, max, help2);
		showEnd(b, max, help2, line4End);
		b.append(CURSOR_NEXT_LINE + ERASE_END_OF_LINE);
	}
	
	protected Appendable append(Appendable b, Point max, String h1) throws IOException {
		return b.append(h1.length() <= max.x ? h1 : h1.substring(0, max.x));
	}
	
	private static int calcOwnLen(Object obj) {
		if ( obj instanceof PJPStackFrame ) {
			if ( ( (PJPStackFrame) obj ).subFrames() != 0 ) return 3;
			return 2;
		}
		if ( obj instanceof ExecutingNode ) {
			ExecutingNode en = (ExecutingNode) obj;
			if ( en.shown.subFrames() != 0 || en.index + 1 < en.stack.frameCount() ) return 3;
			return 2;
		}
		if ( obj instanceof PJPExecutingStack ) return 1;
		if ( obj instanceof PJPThread ) return 1;
		if ( obj instanceof PJPSnapshot ) return 1;
		throw new AssertionError(obj == null ? "null" : obj.getClass().getName());
	}
	
	protected void appendContent(Appendable b, Point pos, int chars) throws IOException {
		TermNode tn = root;
		if ( tn.length < pos.y ) {
			b.append(repeat(" ", chars));
			return;
		}
		int remain;
		for (int off = 0;;) {
			if ( off + tn.ownLength > pos.y ) {
				remain = pos.y - off;
				break;
			}
			off += tn.ownLength;
			for (int i = 0;; i++) {
				TermNode ctn = (TermNode) tn.children.get(i);
				if ( off + ctn.length > pos.y ) {
					tn = ctn;
					break;
				}
				off += ctn.length;
			}
		}
		int indent = remain != 0 ? tn.indent + 2 : tn.indent;
		int off = pos.x;
		int length = chars;
		if ( off + length <= indent ) {
			b.append(repeat(" ", length));
			return;
		}
		if ( off < indent ) {
			int diff = indent - off;
			b.append(repeat(" ", diff));
			length -= diff;
			off = 0;
		} else {
			off -= indent;
		}
		// off is now relative to indent
		String str;
		if ( calcOwnLen(tn.value) != tn.ownLength ) {
			throw new AssertionError(tn);
		}
		if ( tn.value instanceof PJPStackFrame || tn.value instanceof ExecutingNode ) {
			PJPStackFrame sf = tn.value instanceof PJPStackFrame ? (PJPStackFrame) tn.value : ( (ExecutingNode) tn.value ).shown;
			switch ( remain ) {
			case 0:
				str = sf.className + '.' + sf.methodName + sf.methodDescriptor;
				break;
			case 1:
				StringBuilder sb = new StringBuilder();
				sb.append("executed ");
				PJPStackFrame.appendTime(sb, sf.executionTime);
				if ( sf.directExec() != sf.executionTime ) {
					sb.append(", directly executed ");
					PJPStackFrame.appendTime(sb, sf.directExec());
				}
				sb.append(", called ").append(sf.callCount).append(" times");
				str = sb.toString();
				break;
			case 2:
				if ( calcOwnLen(tn.value) != 3 ) {
					throw new AssertionError(tn);
				}
				str = "called " + sf.subFrames() + " methods";
				break;
			default:
				throw new AssertionError(remain);
			}
		} else if ( tn.value instanceof PJPExecutingStack ) {
			PJPExecutingStack es = (PJPExecutingStack) tn.value;
			if ( remain != 0 ) {
				throw new AssertionError(remain);
			}
			str = "Executing Stack (depth of " + es.frameCount() + ')';
		} else if ( tn.value instanceof PJPThread ) {
			PJPThread t = (PJPThread) tn.value;
			if ( remain != 0 ) {
				throw new AssertionError(remain);
			}
			StringBuilder sb = new StringBuilder();
			sb.append("Thread ").append(t.name);
			sb.append(", executed ");
			PJPStackFrame.appendTime(sb, t.execTime());
			str = sb.toString();
		} else if ( tn.value instanceof PJPSnapshot ) {
			PJPSnapshot sn = (PJPSnapshot) tn.value;
			if ( remain != 0 ) {
				throw new AssertionError(remain);
			}
			str = "Snapshot (" + sn.threadCount() + " threads)";
		} else {
			throw new AssertionError(tn.value == null ? "null" : tn.value.getClass().getName());
		}
		if ( remain == 0 ) {
			if ( selected == tn ) {
				b.append(FC_GREEN);
			} else {
				Object val = tn.value;
				if ( val instanceof PJPThread ) {
					b.append(FC_CYAN);
				} else if ( val instanceof PJPExecutingStack ) {
					b.append(FC_MAGENTA);
				} else if ( val instanceof ExecutingNode ) {
					b.append(FC_MAGENTA);
				} else if ( val instanceof PJPStackFrame ) {
					PJPStackFrame sf = (PJPStackFrame) val;
					if ( sf.executionTime >= 10_000_000_000L ) {
						b.append(FC_RED);
					} else if ( sf.executionTime >= 1_000_000_000L ) {// orange
						b.append(CSI + CSI_FC_PREFIX_C + CSI_C_RGB_C + "255" + CSI_SEP + "200" + CSI_SEP + "0" + CSI_END);
					} else if ( sf.executionTime >= 10_000_000L ) {
						
					} else {
						b.append(FC_RGB_GRAY);
					}
				}
			}
		}
		if ( str.length() < off ) {
			b.append(repeat(" ", length));
		} else if ( str.length() - off >= length ) {
			b.append(str.substring(off, off + length));
		} else {
			b.append(str.substring(off)).append(repeat(" ", length - ( str.length() - off )));
		}
	}
	
}
