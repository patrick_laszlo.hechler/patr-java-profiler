// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.utils;

import de.hechler.patrick.profiler.imp.PJPExecutingStack;
import de.hechler.patrick.profiler.imp.PJPStackFrame;

public class ExecutingNode {
	
	public final PJPExecutingStack stack;
	public final PJPStackFrame     shown;
	public final int               index;
	
	public ExecutingNode(PJPExecutingStack stack, PJPStackFrame shown, int index) {
		this.stack = stack;
		this.shown = shown;
		this.index = index;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		PJPStackFrame.appendTime(b, this.shown.executionTime).append(" : ");
		b.append(this.shown.className).append('.').append(this.shown.methodName).append(this.shown.methodDescriptor);
		return b.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		result = prime * result + ( ( shown == null ) ? 0 : shown.hashCode() );
		result = prime * result + ( ( stack == null ) ? 0 : stack.hashCode() );
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( this == obj ) { return true; }
		if ( !( obj instanceof ExecutingNode ) ) { return false; }
		ExecutingNode other = (ExecutingNode) obj;
		if ( index != other.index ) { return false; }
		if ( shown == null ) {
			if ( other.shown != null ) { return false; }
		} else if ( !shown.equals(other.shown) ) { return false; }
		if ( stack == null ) {
			if ( other.stack != null ) { return false; }
		} else if ( !stack.equals(other.stack) ) { return false; }
		return true;
	}
	
}
