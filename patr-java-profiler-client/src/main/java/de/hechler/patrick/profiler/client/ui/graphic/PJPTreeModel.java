// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.graphic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import de.hechler.patrick.profiler.client.ui.utils.Node;
import de.hechler.patrick.profiler.client.ui.utils.NodeMerger;
import de.hechler.patrick.profiler.imp.PJPSnapshot;

public class PJPTreeModel implements TreeModel {
	
	private Node                    root;
	private PJPSnapshot             show;
	private List<TreeModelListener> listen = new ArrayList<>();
	private boolean                 noLeafes;
	// 5;0,5*6
	
	public PJPTreeModel() {}
	
	public PJPTreeModel(boolean noLeafes) {
		this.noLeafes = noLeafes;
	}
	
	public PJPTreeModel(PJPSnapshot show) {
		this.show(show);
	}
	
	
	public synchronized void rootSuffix(String suffix) {
		this.root.suffix = suffix;
		fireChanged(event(root, Collections.emptyList(), 0));
	}
	
	public synchronized void show(PJPSnapshot show) {
		SwingUtilities.invokeLater(() -> {
			if ( this.show == show ) {
				return;
			} else if ( this.show == null ) {
				this.show = show;
				this.root = new Node(show);
				fireInserted(new TreeModelEvent(root, (Object[]) null));
				return;
			} else if ( show == null ) {
				Node rem = this.root;
				this.show = null;
				this.root = null;
				fireRemoved(new TreeModelEvent(rem, (Object[]) null));
				return;
			}
			this.show = show;
			nmerger.merge(root, show, new ArrayList<>(), 0);
		});
	}
	
	private final NodeMerger<Node> nmerger = new NodeMerger<Node>() {
		
		@Override
		protected Node newNode(@SuppressWarnings("unused") Node parent, Object child) {
			return new Node(child);
		}
		
		@Override
		protected void remove(Node removedNode, List<Node> path, int childIndex) {
			fireRemoved(event(removedNode, path, childIndex));
		}
		
		@Override
		protected void insert(Node removedNode, List<Node> path, int childIndex) {
			fireInserted(event(removedNode, path, childIndex));
		}
		
		@Override
		protected void change(Node removedNode, List<Node> path, int childIndex) {
			fireChanged(event(removedNode, path, childIndex));
		}
		
	};
	
	private TreeModelEvent event(Node ocn, List<Node> path, int index) {
		if ( ocn == this.root ) {
			return new TreeModelEvent(ocn, (Object[]) null);
		}
		Object[] arr = path.toArray();
		return new TreeModelEvent(ocn, arr, new int[]{ index }, new Object[]{ ocn });
	}
	
	private void fireChanged(TreeModelEvent tme) {
		for (int i = 0, s = listen.size(); i < s; i++) {
			listen.get(i).treeNodesChanged(tme);
		}
	}
	
	private void fireInserted(TreeModelEvent tme) {
		for (int i = 0, s = listen.size(); i < s; i++) {
			listen.get(i).treeNodesInserted(tme);
		}
	}
	
	private void fireRemoved(TreeModelEvent tme) {
		for (int i = 0, s = listen.size(); i < s; i++) {
			listen.get(i).treeNodesRemoved(tme);
		}
	}
	
	
	public PJPSnapshot show() {
		return this.show;
	}
	
	@Override
	public Object getRoot() {
		return this.root;
	}
	
	@Override
	public Object getChild(Object parent, int index) {
		Node p = (Node) parent;
		if ( p.children == null ) {
			getChildCount(parent);
		}
		return p.children.get(index);
		
	}
	
	@Override
	public int getChildCount(Object parent) {
		Node p = (Node) parent;
		if ( p.children == null ) {
			int cc = Node.extractChildCount(p.value);
			p.children = new ArrayList<>(cc);
			for (int i = 0; i < cc; i++) {
				p.children.add(new Node(Node.generateChild(p.value, i)));
			}
		}
		return p.children.size();
	}
	
	@Override
	public boolean isLeaf(Object node) {
		if ( noLeafes ) return false;
		Node n = (Node) node;
		if ( n.children == null ) {
			return Node.isLeaf(n.value);
		}
		return n.children.size() == 0;
	}
	
	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		if ( path.getPathCount() == 0 || !( newValue instanceof PJPSnapshot ) ) {
			show((PJPSnapshot) newValue);
			return;
		}
		throw new UnsupportedOperationException("I only support replacing the root value with a new snapshot");
	}
	
	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if ( !( parent instanceof Node && child instanceof Node ) ) return -1;
		Node p = (Node) parent;
		Node c = (Node) child;
		if ( p.children == null ) return -1;
		for (int i = 0; i < p.children.size(); i++) {
			if ( p.children.get(i) == c ) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public void addTreeModelListener(TreeModelListener l) {
		if ( l != null ) listen.add(l);
	}
	
	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		if ( l != null ) listen.remove(l);
	}
	
}
