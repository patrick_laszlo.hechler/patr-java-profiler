// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.imp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PJPThread implements Comparable<PJPThread> {
	
	public final long              id;
	public final String            name;
	public final PJPExecutingStack executing;
	private final PJPStackFrame[]  stacks;
	
	public PJPThread(long id, String name, PJPExecutingStack executing, List<PJPStackFrame> stacks) {
		this.id = id;
		this.name = name;
		this.executing = (PJPExecutingStack) executing;
		this.stacks = stacks.toArray(new PJPStackFrame[stacks.size()]);
		Arrays.sort(this.stacks);
	}
	
	public int finishedCount() {
		return stacks.length;
	}
	
	public PJPStackFrame finishedStack(int i) {
		return stacks[i];
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.executing == null ) ? 0 : this.executing.hashCode() );
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		result = prime * result + Arrays.hashCode(this.stacks);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( this == obj ) {
			return true;
		}
		if ( !( obj instanceof PJPThread ) ) {
			return false;
		}
		PJPThread other = (PJPThread) obj;
		if ( this.executing == null ) {
			if ( other.executing != null ) {
				return false;
			}
		} else if ( !this.executing.equals(other.executing) ) {
			return false;
		}
		if ( this.name == null ) {
			if ( other.name != null ) {
				return false;
			}
		} else if ( !this.name.equals(other.name) ) {
			return false;
		}
		if ( !Arrays.equals(this.stacks, other.stacks) ) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(this.name);
		b.append(": ");
		PJPStackFrame.appendTime(b, execTime());
		if ( this.stacks.length != 0 ) {
			b.append(", finished ").append(this.stacks.length);
		}
		if ( this.executing.frameCount() != 0 ) {
			b.append(", executing");
		}
		return b.toString();
	}
	
	public void append(Appendable a) throws IOException {
		a.append(" Thread ").append(this.name).append(":\n");
		if ( this.executing.frameCount() != 0 ) {
			a.append(" currently executing:\n");
			this.executing.append(a);
		}
		if ( stacks.length != 0 ) {
			a.append(" finished executing: ").append(Integer.toString(stacks.length)).append('\n');
			for (int i = 0; i < stacks.length; i++) {
				if ( stacks[i] == null ) {
					a.append("  null\n");
					continue;
				}
				stacks[i].append("  ", a);
			}
		}
	}
	
	@Override
	public int compareTo(PJPThread o) {
		return Long.compare(o.execTime(), execTime());
	}
	
	public long execTime() {
		long et = 0L;
		if ( executing.frameCount() != 0 ) {
			PJPStackFrame f = executing.frame(0);
			if ( f != null ) {
				et = f.executionTime;
			}
		}
		for (int i = 0; i < stacks.length; i++) {
			et += stacks[i].executionTime;
		}
		return et;
	}
	
}
