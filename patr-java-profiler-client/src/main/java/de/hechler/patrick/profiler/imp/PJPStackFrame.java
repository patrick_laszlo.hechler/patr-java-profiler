// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.imp;

import java.io.IOError;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PJPStackFrame implements Comparable<PJPStackFrame> {
	
	public final String           className;
	public final String           methodName;
	public final String           methodDescriptor;
	public final long             callCount;
	public final long             executionTime;
	public final long             wastedTime;
	private final PJPStackFrame[] called;
	
	public PJPStackFrame(String className, String methodName, String methodDescriptor, long callCount, long time, long wastedTime,
		List<PJPStackFrame> called) {
		this.className = className;
		this.methodName = methodName;
		this.methodDescriptor = methodDescriptor;
		this.callCount = callCount;
		this.executionTime = time;
		this.wastedTime = wastedTime;
		this.called = called.toArray(new PJPStackFrame[called.size()]);
		Arrays.sort(this.called);
	}
	
	public int subFrames() {
		return this.called.length;
	}
	
	public PJPStackFrame subFrame(int i) {
		return this.called[i];
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) ( this.callCount ^ ( this.callCount >>> 32 ) );
		result = prime * result + Arrays.hashCode(this.called);
		result = prime * result + ( ( this.className == null ) ? 0 : this.className.hashCode() );
		result = prime * result + ( ( this.methodDescriptor == null ) ? 0 : this.methodDescriptor.hashCode() );
		result = prime * result + ( ( this.methodName == null ) ? 0 : this.methodName.hashCode() );
		result = prime * result + (int) ( this.executionTime ^ ( this.executionTime >>> 32 ) );
		result = prime * result + (int) ( this.wastedTime ^ ( this.wastedTime >>> 32 ) );
		return result;
	}
	
	public boolean sameMethod(PJPStackFrame ofs) {
		return className.equals(ofs.className) //
			&& methodName.equals(ofs.methodName) //
			&& methodDescriptor.equals(ofs.methodDescriptor);
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( this == obj ) {
			return true;
		}
		if ( !( obj instanceof PJPStackFrame ) ) {
			return false;
		}
		PJPStackFrame other = (PJPStackFrame) obj;
		if ( this.callCount != other.callCount ) {
			return false;
		}
		if ( !Arrays.equals(this.called, other.called) ) {
			return false;
		}
		if ( this.className == null ) {
			if ( other.className != null ) {
				return false;
			}
		} else if ( !this.className.equals(other.className) ) {
			return false;
		}
		if ( this.methodDescriptor == null ) {
			if ( other.methodDescriptor != null ) {
				return false;
			}
		} else if ( !this.methodDescriptor.equals(other.methodDescriptor) ) {
			return false;
		}
		if ( this.methodName == null ) {
			if ( other.methodName != null ) {
				return false;
			}
		} else if ( !this.methodName.equals(other.methodName) ) {
			return false;
		}
		if ( this.executionTime != other.executionTime ) {
			return false;
		}
		if ( this.wastedTime != other.wastedTime ) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		appendTime(b, this.executionTime).append(", called ").append(this.callCount).append(" times: ");
		b.append(this.className).append('.').append(this.methodName).append(this.methodDescriptor);
		return b.toString();
	}
	
	@Override
	public int compareTo(PJPStackFrame o) {
		long a = this.executionTime;
		long b = o.executionTime;
		if ( a != b ) {
			if ( a < b ) return 1;
			return -1;
		}
		return 0;
	}
	
	public void append(String indent, Appendable a) throws IOException {
		appendMyInfo(indent, a);
		appendSubFrames(indent.concat("  "), a);
	}
	
	public void appendMyInfo(String indent, Appendable a) throws IOException {
		a.append(indent).append(className).append('.').append(methodName).append(methodDescriptor).append('\n');
		a.append(indent).append(" called ").append(Long.toString(callCount)).append(" times\n");
		a.append(indent).append(" executed ");
		appendTime(a, Long.toString(this.executionTime)).append('\n');
		long directExec = directExec();
		a.append(indent).append(" directly executed ");
		appendTime(a, Long.toString(directExec)).append('\n');
		a.append(indent).append(" wastedTime ");
		appendTime(a, Long.toString(this.wastedTime)).append('\n');
	}

	public long directExec() { long directExec = this.executionTime;
	for (int i = 0; i < called.length; i++) {
		directExec -= called[i].executionTime;
	} return directExec; }
	
	private static Appendable appendTime(Appendable a, String nanos) throws IOException {
		if ( nanos.length() <= 9 ) {
			a.append("0.");
		} else {
			a.append(nanos, 0, nanos.length() - 9).append('.');
		}
		if ( nanos.length() < 9 ) {
			for (int i = nanos.length(); i < 9; i++) a.append('0');
			return a.append(nanos).append(" seconds");
		} else {
			return a.append(nanos, nanos.length() - 9, nanos.length()).append(" seconds");
		}
	}
	
	@SuppressWarnings("unused")
	private static Appendable appendTimeDyn(Appendable a, String nanos) throws IOException {
		if ( nanos.length() < 6 ) {
			return a.append(nanos).append(" nanoseconds");
		}
		String suffix;
		int off;
		if ( nanos.length() < 9 ) {
			off = 6;
			suffix = " milliseconds";
		} else {
			off = 9;
			suffix = " seconds";
		}
		if ( nanos.length() == off ) {
			a.append("0.");
		} else {
			a.append(nanos, 0, nanos.length() - off).append('.');
		}
		a.append(nanos, nanos.length() - off, nanos.length()).append(suffix);
		return a;
	}
	
	public static StringBuilder appendTime(StringBuilder a, long n) {
		try {
			return (StringBuilder) appendTime(a, Long.toString(n));
		} catch ( IOException e ) {
			throw new IOError(e);
		}
	}
	
	public void appendSubFrames(String indent, Appendable a) throws IOException {
		for (int i = 0; i < called.length; i++) {
			if ( called[i] == null ) {
				a.append(indent).append("null\n");
				continue;
			}
			called[i].append(indent, a);
		}
	}
	
}
