// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui;

import de.hechler.patrick.profiler.imp.PJPSnapshot;

public interface PJPUserInterface {
	
	/**
	 * closes the user interface, making any further method call declared in this interface undefined behavior (even
	 * close)
	 * <p>
	 * the user interface should free now all resources it previously allocated
	 * 
	 * @throws Exception if an error occurs, for example if the remaining unwritten data could not be flushed
	 */
	void close() throws Exception;
	
	/**
	 * the profiled process is still running (the default state)
	 */
	int STATE_RUNNING          = 0;
	/**
	 * the profiled process sent a final snapshot directly before termination
	 */
	int STATE_FINAL_SNAPSHOT   = 1;
	/**
	 * the profiled process did <b>not</b> sent a final snapshot directly before termination, but did notify the client
	 * about the termination
	 */
	int STATE_TERMINATED_NO_SN = 2;
	/**
	 * the connection to the server is lost/broken without the server notifying the client
	 */
	int STATE_CONNECTION_LOST  = 3;
	
	/**
	 * changes the state of the profiled process
	 * 
	 * @param state the new state of the profiled process
	 */
	void state(int state);
	
	/**
	 * updates so that the new snapshot is shown
	 * 
	 * @param show the new snapshot
	 */
	void update(PJPSnapshot show);
	
	/**
	 * tells the user interface that a fatal error occurred
	 * <p>
	 * this implicitly also sets the {@link #state(int) state} to {@link #STATE_CONNECTION_LOST connection lost}
	 * 
	 * @param t the error that occurred
	 */
	void error(Throwable t);
	
}
