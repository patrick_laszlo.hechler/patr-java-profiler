// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.imp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PJPExecutingStack {
	
	private final PJPStackFrame[] frames;
	
	public PJPExecutingStack(List<PJPStackFrame> frames) {
		this.frames = frames.toArray(new PJPStackFrame[frames.size()]);
	}
	
	public int frameCount() {
		return frames.length;
	}
	
	public PJPStackFrame frame(int i) {
		return frames[i];
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.frames);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( this == obj ) {
			return true;
		}
		if ( !( obj instanceof PJPExecutingStack ) ) {
			return false;
		}
		PJPExecutingStack other = (PJPExecutingStack) obj;
		if ( !Arrays.equals(this.frames, other.frames) ) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("ExecutingStack: ").append(this.frames.length).append(" frames: ");
		PJPStackFrame.appendTime(b, this.frames[0].executionTime);
		return b.toString();
	}
	
	public void append(Appendable a) throws IOException {
		String indent = "  ";
		for (int i = 0; i < frames.length; i++) {
			if ( frames[i] == null ) {
				a.append(indent).append("null\n");
				continue;
			}
			frames[i].appendMyInfo(indent, a);
			indent = indent.concat("  ");
		}
		for (int i = frames.length; --i >= 0;) {
			if ( frames[i] == null ) {
				continue;
			}
			frames[i].appendSubFrames(indent, a);
			indent = indent.substring(2);
		}
	}
	
}
