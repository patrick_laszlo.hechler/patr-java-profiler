// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.term;

import de.hechler.patrick.profiler.client.ui.utils.Node;

public class TermNode extends Node {
	
	public final TermNode parent;
	public final int      indent;
	public int            ownLength;
	public int            length;
	public boolean        visibleChildren;
	
	public TermNode(TermNode parent, int indent, int length, Object value) {
		super(value);
		this.parent = parent;
		this.indent = indent;
		this.ownLength = this.length = length;
	}
	
}
