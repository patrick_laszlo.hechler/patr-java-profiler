// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client;

import static de.hechler.patrick.profiler.transmit.Constants.BYE_1;
import static de.hechler.patrick.profiler.transmit.Constants.HELLO_4;
import static de.hechler.patrick.profiler.transmit.Constants.HELLO_RESPONSE_1;
import static de.hechler.patrick.profiler.transmit.Constants.JVM_VERSION_1;
import static de.hechler.patrick.profiler.transmit.Constants.JVM_VERSION_STR_1;
import static de.hechler.patrick.profiler.transmit.Constants.PID_1;
import static de.hechler.patrick.profiler.transmit.Constants.QUIT_1;
import static de.hechler.patrick.profiler.transmit.Constants.REPORT_1;
import static de.hechler.patrick.profiler.transmit.Constants.SEND_REPORT_1;
import static de.hechler.patrick.profiler.transmit.Constants.START_1;
import static de.hechler.patrick.profiler.transmit.Constants.VERSION_1;
import static de.hechler.patrick.profiler.transmit.Constants.VERSION_STR_1;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.lang.reflect.InvocationTargetException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Optional;

import de.hechler.patrick.profiler.imp.Importer;
import de.hechler.patrick.profiler.imp.PJPSnapshot;
import de.hechler.patrick.profiler.transmit.Constants;

public class ClientImpl implements AutoCloseable {
	
	private final SocketChannel sc;
	private final ByteBuffer    b;
	private final Importer      imp;
	
	// only set when server and client JVM version >= 9
	// only works when sever and client are on the same machine
	private long    pid;
	private Instant startTime;
	
	public ClientImpl(SocketChannel sc) {
		this.sc = sc;
		this.b = ByteBuffer.allocateDirect(8192);
		this.b.order(ByteOrder.LITTLE_ENDIAN);
		// DO NOT REORDER: this has to be the last statement
		this.imp = new ClientImporter();
	}
	
	public void connect() throws IOException {
		connect(false);
	}
	
	public void connect(boolean noStart) throws IOException {
		b.putInt(0, HELLO_4);
		( (Buffer) b ).limit(4);
		fullWrite(sc, b);
		( (Buffer) b ).limit(5);
		readSome(sc, b);
		if ( b.get(4) != HELLO_RESPONSE_1 ) {
			throw new StreamCorruptedException("invalid response to connect");
		}
		if ( Constants.jvmVersion() >= 9 ) {
			( (Buffer) b ).limit(6);
			b.put(5, PID_1);
			fullWrite(sc, b);
			( (Buffer) b ).limit(26);
			readSome(sc, b);
			long pid = b.getLong(6);
			long startSecond = b.getLong(14);
			long startNano = b.getInt(22);
			if ( pid != 0L || startSecond != 0L || startNano != 0 ) {
				if ( exists(pid, startSecond, startNano) ) {
					// it is possible to report false positives:
					// both machines start at the same time a process with the same PID
					this.pid = pid;
					this.startTime = Instant.ofEpochSecond(startSecond, startNano);
				}
			}
			if ( startTime == null ) {
				System.err.println("could not find the profiled process (no porblem as long as it answers me)");
				System.err.println("  pid: " + pid);
				System.err.println("  starts: " + startSecond);
				System.err.println("  startn: " + startNano);
			}
		}
		// allow to suppress this, so that the user can connect multiple clients
		// - (if the server supports it)
		if ( !noStart ) {
			// notify the server that I have done my setup
			start();
		}
	}
	
	public void start() throws IOException, StreamCorruptedException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		b.put(0, START_1);
		fullWrite(sc, b);
		( (Buffer) b ).position(0);
		readSome(sc, b);
		if ( b.get(0) != START_1 ) {
			throw new StreamCorruptedException("invalid response to START from the server");
		}
	}
	
	public String jvmVersionStr() throws IOException {
		return serverJvmVersionStr(sc, b);
	}
	
	public int jvmVersion() throws IOException {
		return serverJvmVersion(sc, b);
	}
	
	public String versionStr() throws IOException {
		return serverVersionStr(sc, b);
	}
	
	public Version version() throws IOException {
		return serverVersion(sc, b);
	}
	
	public Instant startTime() throws IOException {
		return startTime;
	}
	
	public long pid() throws IOException {
		return pid;
	}
	
	public Optional<PJPSnapshot> tryReadLastSnapshot() throws IOException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		int val = sc.read(b);
		if ( val == 0 ) {
			return null;
		} else if ( val == -1 ) {
			throw new EOFException("reached EOF");
		}
		if ( b.get(0) != SEND_REPORT_1 ) {
			if ( b.get(0) == QUIT_1 ) {
				return Optional.empty();
			}
			throw new StreamCorruptedException("the server did not send " + SEND_REPORT_1);
		}
		PJPSnapshot sn = imp.imp();
		try {
			( (Buffer) b ).position(0);
			( (Buffer) b ).limit(1);
			readSome(sc, b);
			if ( b.get(0) != QUIT_1 ) {
				System.err
					.println("the server send something other than QUIT_1 after a non-requested SEND_REPORT_1: " + b.get(0));
			}
		} catch ( @SuppressWarnings("unused") EOFException e ) {
			// ignore EOF when the only valid thing is QUIT
		} catch ( IOException e ) {
			System.err.println("error while expecting QUIT_1: " + e);
		}
		return Optional.of(sn);
	}
	
	public PJPSnapshot snapshot() throws IOException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		b.put(0, REPORT_1);
		fullWrite(sc, b);
		( (Buffer) b ).limit(2);
		readSome(sc, b);
		if ( b.get(1) != SEND_REPORT_1 ) {
			if ( b.get(1) == QUIT_1 ) {
				return null;
			}
			throw new StreamCorruptedException("the server did not send " + SEND_REPORT_1 + " as a response to " + REPORT_1);
		}
		return imp.imp();
	}
	
	private String serverVersionStr(SocketChannel sc, ByteBuffer b) throws IOException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		b.put(0, VERSION_STR_1);
		fullWrite(sc, b);
		collectStream(sc, b);
		byte[] bs = new byte[b.limit()];
		b.get(bs);
		return new String(bs, StandardCharsets.UTF_8);
	}
	
	private Version serverVersion(SocketChannel sc, ByteBuffer b) throws IOException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		b.put(0, VERSION_1);
		fullWrite(sc, b);
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(13);
		readSome(sc, b);
		int major = b.getInt(0);
		int minor = b.getInt(4);
		int bugfix = b.getInt(8);
		switch ( b.get(12) ) {
		case 0:
			return new Version(major, minor, bugfix, false);
		case 1:
			return new Version(major, minor, bugfix, true);
		case 2:
			ByteArrayOutputStream baos = new ByteArrayOutputStream(64);
			delegateStream(sc, baos);
			return new Version(major, minor, bugfix, new String(baos.toByteArray(), StandardCharsets.UTF_8));
		default:
			throw new StreamCorruptedException("illegal version type: " + b.get(12));
		}
	}
	
	private String serverJvmVersionStr(SocketChannel sc, ByteBuffer b) throws IOException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		b.put(0, JVM_VERSION_STR_1);
		fullWrite(sc, b);
		collectStream(sc, b);
		byte[] bs = new byte[b.limit()];
		b.get(bs);
		return new String(bs, StandardCharsets.UTF_8);
	}
	
	private int serverJvmVersion(SocketChannel sc, ByteBuffer b) throws IOException {
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(1);
		b.put(0, JVM_VERSION_1);
		fullWrite(sc, b);
		( (Buffer) b ).position(0);
		( (Buffer) b ).limit(4);
		readSome(sc, b);
		return b.getInt(0);
	}
	
	private static boolean exists(long pid, long startS, long startN) {
		try {
			Class<?> phCls = Class.forName("java.lang.ProcessHandle");
			Optional<?> optPH = (Optional<?>) phCls.getMethod("of", Long.TYPE).invoke(null, pid);
			if ( optPH.isPresent() ) {
				Object ph = optPH.get();
				Object i = phCls.getMethod("info").invoke(ph);
				Optional<?> start = (Optional<?>) Class.forName("java.lang.ProcessHandle$Info").getMethod("startInstant")
					.invoke(i);
				if ( start.isPresent() ) {
					Instant instant = (Instant) start.get();
					if ( instant.getEpochSecond() == startS && instant.getNano() == startN ) {
						return true;
					}
				}
			}
			return false;
		} catch ( @SuppressWarnings("unused") IllegalAccessException | InvocationTargetException | NoSuchMethodException
			| SecurityException | ClassNotFoundException e ) {
			return false;
		}
	}
	
	private void collectStream(SocketChannel sc, ByteBuffer b) throws IOException {
		( (Buffer) b ).position(0);
		while ( true ) {
			int pos = b.position();
			( (Buffer) b ).limit(pos + 4);
			readSome(sc, b);
			( (Buffer) b ).position(pos);
			int len = b.getInt(pos);
			if ( len == 0 ) break;
			while ( b.capacity() - pos - 4 < len ) {
				ByteBuffer tmp = ByteBuffer.allocate(len);
				( (Buffer) b ).position(pos - 4);
				( (Buffer) b ).flip();
				tmp.put(b);
				b = tmp;
			}
			( (Buffer) b ).limit(b.position() + len);
			readSome(sc, b);
		}
		( (Buffer) b ).flip();
	}
	
	private void delegateStream(SocketChannel sc, OutputStream out) throws IOException {
		ByteBuffer buf = ByteBuffer.allocate(1024);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.position(0);
		while ( true ) {
			int pos = buf.position();
			( (Buffer) buf ).limit(pos + 4);
			readSome(sc, buf);
			( (Buffer) buf ).position(pos);
			int len = buf.getInt(pos);
			if ( len == 0 ) break;
			if ( buf.capacity() - pos - 4 < len ) {
				out.write(buf.array(), 0, pos);
				if ( buf.capacity() < len ) {
					buf = ByteBuffer.allocate(len);
				} else {
					( (Buffer) buf ).position(0);
				}
			}
			( (Buffer) buf ).limit(buf.position() + len);
			readSome(sc, buf);
		}
		out.write(buf.array(), 0, buf.position());
	}
	
	private static void fullWrite(SocketChannel sc, ByteBuffer bb) throws IOException {
		while ( bb.hasRemaining() ) {
			sc.write(bb);
		}
	}
	
	private void readSome(SocketChannel sc, ByteBuffer bb) throws IOException {
		for (int cnt = 1;; cnt++) {
			int val = sc.read(bb);
			if ( val > 0 ) {
				return;
			}
			if ( val == -1 ) throw new EOFException();
			if ( cnt % 100 == 0 ) {
				if ( startTime != null ) {
					if ( exists(pid, startTime.getEpochSecond(), startTime.getNano()) ) {
						System.err.println("the server is still running, but it seens to not respond");
					} else {
						throw new EOFException("the server process terminated");
					}
				} else {
					System.err.println("there seems to be no response from the server");
				}
			}
			if ( cnt % 1000 == 0 ) {
				throw new EOFException("reached 10 second timeout (" + bb.remaining() + " bytes remaining)");
			}
			try {
				Thread.sleep(10);
			} catch ( InterruptedException e ) {
				throw new InterruptedIOException(e.toString());
			}
		}
	}
	
	public class ClientImporter extends Importer {
		
		public ClientImporter() {
			super(ClientImpl.this.sc, ClientImpl.this.b);
		}
		
		@Override
		protected void read() throws IOException {
			( (Buffer) buf ).position(0);
			( (Buffer) buf ).limit(buf.capacity());
			ClientImpl.this.readSome((SocketChannel) in, buf);
			( (Buffer) buf ).flip();
		}
		
	}
	
	@Override
	public void close() throws IOException {
		if ( sc.isOpen() ) {
			( (Buffer) b ).position(0);
			( (Buffer) b ).limit(1);
			b.put(0, BYE_1);
			fullWrite(sc, b);
			sc.close();
		}
	}
	
}
