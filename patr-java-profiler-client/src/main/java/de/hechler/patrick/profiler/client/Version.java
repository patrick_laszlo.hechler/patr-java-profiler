// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client;

import java.util.Objects;

public class Version {
	
	public final int     major;
	public final int     minor;
	public final int     bugfix;
	public final boolean snapshot;
	public final String  branch;
	
	public Version(int major, int minor, int bugfix, boolean snapshot) {
		this.major = major;
		this.minor = minor;
		this.bugfix = bugfix;
		this.snapshot = snapshot;
		this.branch = null;
	}
	
	public Version(int major, int minor, int bugfix, String branch) {
		this.major = major;
		this.minor = minor;
		this.bugfix = bugfix;
		this.snapshot = true;
		this.branch = Objects.requireNonNull(branch);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.bugfix;
		result = prime * result + this.major;
		result = prime * result + this.minor;
		result = prime * result + ( this.snapshot ? 1231 : 1237 );
		result = prime * result + ( ( this.branch == null ) ? 0 : this.branch.hashCode() );
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( this == obj ) {
			return true;
		}
		if ( !( obj instanceof Version ) ) {
			return false;
		}
		Version other = (Version) obj;
		if ( this.bugfix != other.bugfix ) {
			return false;
		}
		if ( this.major != other.major ) {
			return false;
		}
		if ( this.minor != other.minor ) {
			return false;
		}
		if ( this.snapshot != other.snapshot ) {
			return false;
		}
		if ( this.branch == null ) {
			if ( other.branch != null ) {
				return false;
			}
		} else if ( !this.branch.equals(other.branch) ) {
			return false;
		}
		return true;
	}
	
	/**
	 * returns the version represented by the given string
	 * <p>
	 * omitted numbers are interpreted as {@code 0} ({@code fromString("1.0.0").equals(fromString("1.."))} will be true)
	 * 
	 * @param version the version
	 * 
	 * @return the version represented by the given string
	 */
	public static Version fromString(String version) {
		int i0 = version.indexOf('.');
		int i1 = version.indexOf('.', i0 + 1);
		int i2 = version.indexOf('-', i1 + 1);
		if ( i2 == -1 ) i2 = version.length();
		if ( i1 == -1 ) throw new IllegalArgumentException(version);
		try {
			int major = i0 == 1 ? 0 : Integer.parseInt(version.substring(0, i0));
			int minor = i0 + 1 == i1 ? 0 : Integer.parseInt(version.substring(i0, i1));
			int bugfix = i1 + 1 == i2 ? 0 : Integer.parseInt(version.substring(i1, i2));
			if ( i2 == version.length() ) {
				return new Version(major, minor, bugfix, false);
			}
			if ( !version.endsWith("-SNAPSHOT") ) {
				throw new IllegalArgumentException(version);
			}
			if ( i2 + "-SNAPSHOT".length() == version.length() ) {
				return new Version(major, minor, bugfix, true);
			}
			return new Version(major, minor, bugfix, version.substring(i2, version.length() - "-SNAPSHOT".length()));
		} catch ( @SuppressWarnings("unused") NumberFormatException e ) {
			throw new IllegalArgumentException(version);
		}
	}
	
	public String withMaxLength(int maxLength) {
		String result = toString();
		if ( result.length() <= maxLength ) return result;
		return result.substring(0, maxLength);
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(this.major);
		b.append('.');
		b.append(this.minor);
		b.append('.');
		b.append(this.bugfix);
		if ( this.snapshot ) {
			if ( this.branch != null ) {
				b.append('-').append(this.branch);
			}
			b.append("-SNAPSHOT");
		}
		return b.toString();
	}
	
}
