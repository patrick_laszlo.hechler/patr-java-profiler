// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.imp;

import java.io.EOFException;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Importer {
	
	protected final ReadableByteChannel in;
	protected final ByteBuffer          buf;
	
	public Importer(ReadableByteChannel in, ByteBuffer buf) {
		this.in = in;
		this.buf = buf;
	}
	
	public PJPSnapshot imp() throws IOException {
		List<PJPThread> threads = new ArrayList<>();
		long time = readInt64();
		while ( true ) {
			String name = readOptString();
			if ( name == null ) return new PJPSnapshot(time, threads);
			long id = readInt64();
			PJPExecutingStack exec = new PJPExecutingStack(readFrames(time));
			List<PJPStackFrame> finished = readFrames(0);
			threads.add(new PJPThread(id, name, exec, finished));
		}
	}
	
	protected List<PJPStackFrame> readFrames(long subtract) throws IOException {
		int fss = readInt32();
		List<PJPStackFrame> result = new ArrayList<>(fss);
		while ( --fss >= 0 ) {
			result.add(readFrame(subtract));
		}
		return result;
	}
	
	protected PJPStackFrame readFrame(long startTime) throws IOException {
		String className = readOptString();
		if ( className == null ) return null;
		String methodName = readString();
		String methodDescriptor = readString();
		long callCount = readInt64();
		long execTime = startTime == 0 ? readInt64() : startTime - readInt64();
		long wastedTime = readInt64();
		List<PJPStackFrame> called = readFrames(0L);
		return new PJPStackFrame(className, methodName, methodDescriptor, callCount, execTime, wastedTime, called);
	}
	
	protected String readString() throws IOException {
		String str = readOptString();
		if ( str == null ) throw new StreamCorruptedException("missing no opt string");
		return str;
	}
	
	protected String readOptString() throws IOException {
		int len = readInt32();
		if ( len == -1 ) return null;
		if ( len < 0 ) throw new StreamCorruptedException(
			"negative sized string: " + len + " : 0x" + Integer.toHexString(len).toUpperCase());
		byte[] bs = new byte[len];
		int off = 0;
		while ( bs.length > off ) {
			len = bs.length - off;
			int lim = buf.remaining();
			if ( len > lim ) {
				len = lim;
				if ( lim == 0 ) {
					read();
					continue;
				}
			}
			buf.get(bs, off, len);
			off += len;
		}
		return new String(bs, StandardCharsets.UTF_8);
	}
	
	protected long readInt64() throws IOException {
		if ( buf.remaining() >= 8 ) {
			return buf.getLong();
		}
		long value = 0;
		for (int s = 0; s < 64; s += 8) {
			if ( buf.hasRemaining() ) {
				value |= ( buf.get() & 0xFFL ) << s;
				continue;
			}
			read();
			value |= ( buf.get() & 0xFF ) << s;
		}
		return value;
	}
	
	protected int readInt32() throws IOException {
		if ( buf.remaining() >= 4 ) {
			return buf.getInt();
		}
		int value = 0;
		for (int s = 0; s < 32; s += 8) {
			if ( buf.hasRemaining() ) {
				value |= ( buf.get() & 0xFF ) << s;
				continue;
			}
			read();
			value |= ( buf.get() & 0xFF ) << s;
		}
		return value;
	}
	
	protected void read() throws IOException {
		if ( buf.position() != buf.limit() ) {
			throw new AssertionError();
		}
		((Buffer)buf).position(0);
		((Buffer)buf).limit(buf.capacity());
		while ( true ) {
			int r = in.read(buf);
			if ( r <= 0 ) {
				if ( r == 0 ) continue;
				throw new EOFException();
			}
			((Buffer)buf).flip();
			return;
		}
	}
	
}
