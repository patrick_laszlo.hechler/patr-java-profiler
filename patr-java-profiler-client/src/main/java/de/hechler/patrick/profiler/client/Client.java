// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client;

import static de.hechler.patrick.profiler.transmit.Constants.echoResource;
import static de.hechler.patrick.profiler.transmit.Constants.jvmVersion;

import java.awt.GraphicsEnvironment;
import java.io.EOFException;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ProtocolFamily;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;
import java.util.Scanner;

import javax.swing.SwingUtilities;

import de.hechler.patrick.profiler.client.ui.PJPUserInterface;
import de.hechler.patrick.profiler.client.ui.graphic.PJPGui;
import de.hechler.patrick.profiler.client.ui.term.AnsiCodes;
import de.hechler.patrick.profiler.client.ui.term.PJPTerm;
import de.hechler.patrick.profiler.imp.Importer;
import de.hechler.patrick.profiler.imp.PJPSnapshot;

public class Client {
	
	public static final String TTY_SUPPORT_PROP = "patrjprof.client.tty";
	
	private static final String RES_VERSION = "/de/hechler/patrick/profiler/client/VERSION";
	private static final String RES_LICENSE = "/de/hechler/patrick/profiler/client/LICENSE";
	
	private static final int TTY_NONE       = 0;
	private static final int TTY_ONLY_COLOR = 1;
	private static final int TTY_SIMPLE     = 2;
	private static final int TTY_FULL       = 3;
	
	// only works when profiled JVM version >= 9 and same machine
	private final int ttySupport;
	
	private Client() {
		String value = System.getProperty(TTY_SUPPORT_PROP);
		if ( value != null ) {
			switch ( value.toLowerCase() ) {
			case "0":
			case "none":
			case "no-color":
				ttySupport = TTY_NONE;
				break;
			case "1":
			case "very-simple":
			case "only-color":
				ttySupport = TTY_ONLY_COLOR;
				break;
			case "2":
			case "simple":
			case "color":
				ttySupport = TTY_SIMPLE;
				break;
			case "3":
			case "full":
			case "extended":
			case "courser":
				ttySupport = TTY_FULL;
				break;
			default:
				System.err.println("unknown " + TTY_SUPPORT_PROP + " value: '" + value + "'");
				ttySupport = 0;
			}
		} else if ( System.console() != null ) {
			ttySupport = TTY_SIMPLE;
		} else {
			ttySupport = TTY_NONE;
		}
	}
	
	public static void main(String[] args) throws Exception {
		try {
			new Client().execute(args);
		} catch ( Exception e ) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void execute(String[] args) throws Exception {
		if ( args.length == 0 ) {
			args = new String[]{ "--help" };
		}
		int port = -1;
		String host = null;
		SocketChannel sc = null;
		SocketAddress sa = null;
		String file = null;
		int terminal = 0;
		String tty = null;
		boolean onlyValidate = false;
		for (int i = 0; i < args.length; i++) {
			if ( "--help".equalsIgnoreCase(args[i]) ) {
				help(System.out);
				System.exit(1);
			} else if ( "--version".equalsIgnoreCase(args[i]) ) {
				if ( ttySupport >= TTY_SIMPLE ) {
					System.out.print(AnsiCodes.ITALIC + "Patr-Java-Profiler Client" + AnsiCodes.NO_ITALIC + ": Version ");
				} else {
					System.out.print("Patr-Java-Profiler Client: Version ");
				}
				echoResource(RES_VERSION);
				System.out.println();
				System.exit(1);
			} else if ( "--license".equalsIgnoreCase(args[i]) ) {
				echoResource(RES_LICENSE);
				System.exit(1);
			} else if ( isValueArg("--socket", args[i], '=') || isValueArg("--unix", args[i], '=') ) {
				if ( jvmVersion() < 16 ) {
					System.err.println("to open a connection with a socket file your JVM version must be at least 16");
					System.err.println("  your JVM version is " + jvmVersion());
					System.exit(1);
				}
				String value = args[i];
				int in = value.indexOf('=');
				if ( in != -1 ) {
					value = value.substring(in + 1);
				} else if ( ++i == args.length ) {
					fail("missing argument after " + args[i - 1], args);
				} else {
					value = args[i];
				}
				if ( sc != null || file != null || port != -1 || host != null ) {
					fail("multiple targets are not allowed", args);
				}
				sa = (SocketAddress) Class.forName("java.net.UnixDomainSocketAddress").getMethod("of", String.class).invoke(null,
					value);
//				UnixDomainSocketAddress udsa = UnixDomainSocketAddress.of(value);
				sc = (SocketChannel) SocketChannel.class.getMethod("open", ProtocolFamily.class).invoke(null,
					StandardProtocolFamily.valueOf("UNIX"));
			} else if ( isValueArg("--port", args[i], '=') ) {
				String value = args[i];
				int in = value.indexOf('=');
				if ( in != -1 ) {
					value = value.substring(in + 1);
				} else if ( ++i == args.length ) {
					fail("missing argument after " + args[i - 1], args);
				} else {
					value = args[i];
				}
				if ( sc != null || file != null || port != -1 ) {
					fail("multiple targets are not allowed", args);
				}
				port = Integer.valueOf(value);
			} else if ( isValueArg("--host", args[i], '=') ) {
				String value = args[i];
				int in = value.indexOf('=');
				if ( in != -1 ) {
					value = value.substring(in + 1);
				} else if ( ++i == args.length ) {
					fail("missing argument after " + args[i - 1], args);
				} else {
					value = args[i];
				}
				if ( sc != null || file != null || host != null ) {
					fail("multiple targets are not allowed", args);
				}
				host = value;
			} else if ( isValueArg("--file", args[i], '=') || isValueArg("--validate", args[i], '=') ) {
				if ( args[i].startsWith("--validate") ) {
					onlyValidate = true;
				}
				String value = args[i];
				int in = value.indexOf('=');
				if ( in != -1 ) {
					value = value.substring(in + 1);
				} else if ( ++i == args.length ) {
					fail("missing argument after " + args[i - 1], args);
				} else {
					value = args[i];
				}
				if ( sc != null || file != null || port != -1 || host != null ) {
					fail("multiple targets are not allowed", args);
				}
				file = value;
			} else if ( "--remote".equals(args[i]) ) {
				if ( ( i += 2 ) >= args.length ) {
					System.err.println("missing argument after " + args[i - 1]);
					System.exit(1);
				}
				if ( sc != null || file != null || port != -1 || host != null ) {
					fail("multiple targets are not allowed", args);
				}
				host = args[i - 1];
				port = Integer.parseInt(args[i]);
			} else if ( "--term".equals(args[i]) ) {
				if ( terminal != 0 ) {
					fail("multiple client specifications are not allowed", args);
				}
				terminal = 1;
			} else if ( isValueArg("--tty", args[i], '=') ) {
				String value = args[i];
				int in = value.indexOf('=');
				if ( in != -1 ) {
					value = value.substring(in + 1);
				} else if ( ++i == args.length ) {
					fail("missing argument after " + args[i - 1], args);
				} else {
					value = args[i];
				}
				if ( terminal == -1 ) {
					fail("--graphic is not allowed when --tty is set", args);
				}
				if ( tty != null ) {
					fail("--tty is specified multiple times", args);
				}
				tty = value;
			} else if ( "--graphic".equals(args[i]) ) {
				if ( terminal != 0 ) {
					fail("multiple client specifications are not allowed", args);
				}
				if ( tty != null ) {
					fail("--graphic is not allowed when --tty is set", args);
				}
				terminal = -1;
			} else {
				fail("illegal argument " + args[i], args);
			}
		}
		if ( terminal == 0 ) {
			if ( tty != null ) {
				terminal = 1;
			} else if ( GraphicsEnvironment.isHeadless() ) {
				terminal = 1;
			} else {
				terminal = -1;
			}
		}
		if ( file != null ) {
			ReadableByteChannel rbc = FileChannel.open(Paths.get(file), StandardOpenOption.READ);
			ByteBuffer buf = ByteBuffer.allocateDirect(1 << 13);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			((ByteBuffer)buf).limit(0);
			Importer i = new Importer(rbc, buf);
			PJPSnapshot imp = i.imp();
			if (onlyValidate) {
				return;
			}
			if ( terminal > 0 ) {
				PJPTerm ui = new PJPTerm(tty);
				try {
					ui.update(imp);
					ui.init(file);
					ui.exec();
				} catch ( Exception e ) {
					PipedInputStream in = new PipedInputStream();
					PipedOutputStream out = new PipedOutputStream();
					out.connect(in);
					try ( PrintStream ps = new PrintStream(out, false, "UTF-8") ) {
						new Thread(() -> {
							try ( Scanner scan = new Scanner(in) ) {
								while ( scan.hasNextLine() ) {
									System.out.print(scan.nextLine() + "\r\n");
								}
							}
						}).start();
						e.printStackTrace(ps);
					}
				}
				return;
			}
			PJPGui ui = new PJPGui();
			ui.update(imp);
			ui.init(file).makeVisible();
			return;
		}
		if ( sc == null ) {
			if ( port == -1 ) {
				if ( host != null ) {
					fail("no port specified", args);
				}
				fail("no connection target/file specified", args);
			}
			if ( host == null ) {
				sa = new InetSocketAddress(InetAddress.getLoopbackAddress(), port);
			} else {
				sa = new InetSocketAddress(host, port);
			}
			sc = SocketChannel.open();
		}
		sc.connect(sa);
		handle(sc, terminal, tty);
	}
	
	private static boolean isValueArg(String expect, String actual, char assign) {
		if ( !actual.startsWith(expect) ) {
			return false;
		}
		return actual.length() == expect.length() || actual.charAt(expect.length()) == assign;
	}
	
	private void fail(String msg, String[] args) {
		System.err.println(msg);
		System.err.println("arguments:");
		for (String arg : args) {
			System.err.println("  " + arg);
		}
		help(System.err);
		System.exit(1);
	}
	
	private void help(PrintStream out) {
		if ( ttySupport >= TTY_SIMPLE ) {
			out.println(AnsiCodes.ITALIC + "Patr-Java-Profiler Client" + AnsiCodes.NO_ITALIC + ": Usage");
		} else {
			out.println("Patr-Java-Profiler Client: Usage");
		}
		out.println("  --help");
		out.println("    print this message to stdout and exit");
		out.println("  --version");
		out.println("    print the license to version and exit");
		out.println("  --license");
		out.println("    print the license to stdout and exit");
		out.println("  --socket <SOCKET_FILE>");
		out.println("  --unix <SOCKET_FILE>");
		out.println("    connect to the socket");
		out.println("    only available if the JVM version is >= 16 (JVM version: " + jvmVersion() + ")");
		out.println("    not allowed to be used with --port or --remote");
		out.println("  --file <FILE>");
		out.println("    extract the information from the output file");
		out.println("  --validate <FILE>");
		out.println("    extract the information from the output file, but don'T show it the user");
		out.println("    exit with zero if the file is valid");
		out.println("    if the file is not valid print an error message and exit with a non-zero value");
		out.println("  --host <HOST>");
		out.println("    connect to host");
		out.println("    if set --port must also be used");
		out.println("  --port <PORT>");
		out.println("    use port when connecting");
		out.println("    not allowed to be used with --socket or --remote");
		out.println("  --remote <HOST> <PORT>");
		out.println("    connect to host at port");
		out.println("    same as --host <HOST> --port <PORT>");
		out.println("  --term");
		out.println("    use a terminal based client");
		out.println("  --tty <TTY>");
		out.println("    specifies the terminal which should be used");
		out.println("    not allowed to be used togeter with --graphic");
		out.println("  --graphic");
		out.println("    use a graphical client");
	}
	
	private void handle(SocketChannel sc, int terminal, String tty) throws IOException {
		ClientImpl c = new ClientImpl(sc);
		try {
			c.connect(true);
			PJPUserInterface ui;
			if ( terminal > 0 ) {
				PJPTerm tui = new PJPTerm(tty);
				tui.update(c.snapshot());
				tui.init(sc.getRemoteAddress().toString(), c.version(), c.jvmVersion());
				ui = tui;
			} else {
				PJPGui gui = new PJPGui();
				gui.update(c.snapshot());
				gui.init(sc.getRemoteAddress().toString(), c.version(), c.jvmVersion());
				gui.makeVisible();
				ui = gui;
			}
			c.start();
			sc.configureBlocking(false);
			Thread t = new Thread(() -> handle(sc, c, ui));
			t.setDaemon(true);
			t.setName("PatrJProf: Daemon");
			t.start();
			if ( ui instanceof PJPTerm ) {
				( (PJPTerm) ui ).exec();
			}
		} catch ( Throwable t ) {
			c.close();
			t.printStackTrace();
		}
	}
	
	private static void handle(SocketChannel sc, ClientImpl c, PJPUserInterface ui) {
		try {
			long start = System.currentTimeMillis();
			while ( true ) {
				long next = start + 1_000;
				if ( next > System.currentTimeMillis() ) {
					start = next;
				} else {
					long wait = next - System.currentTimeMillis();
					System.err.println("server/client seems to be overloaded (additional delay of " + wait + " ms)");
					start = System.currentTimeMillis();
				}
				do {
					Optional<PJPSnapshot> ls = c.tryReadLastSnapshot();
					if ( ls == null ) {
						Thread.sleep(10L);
					} else if ( ls.isPresent() ) {
						ui.state(PJPUserInterface.STATE_FINAL_SNAPSHOT);
						ui.update(ls.get());
						return;
					} else {
						ui.state(PJPUserInterface.STATE_TERMINATED_NO_SN);
						return;
					}
				} while ( System.currentTimeMillis() < next );
				final PJPSnapshot s;
				try {
					s = c.snapshot();
				} catch ( @SuppressWarnings("unused") EOFException e ) {
					ui.state(PJPUserInterface.STATE_CONNECTION_LOST);
					return;
				}
				if ( s != null ) {
					ui.update(s);
				} else {
					ui.state(PJPUserInterface.STATE_TERMINATED_NO_SN);
				}
			}
		} catch ( Exception e0 ) {
			if ( sc.isOpen() ) {
				try {
					Optional<PJPSnapshot> ls = c.tryReadLastSnapshot();
					if ( ls != null ) {
						if ( ls.isPresent() ) {
							SwingUtilities.invokeLater(() -> {
								ui.state(PJPUserInterface.STATE_FINAL_SNAPSHOT);
								ui.update(ls.get());
							});
							return;
						} else {
							SwingUtilities.invokeLater(() -> {
								ui.state(PJPUserInterface.STATE_TERMINATED_NO_SN);
							});
							return;
						}
					}
				} catch ( @SuppressWarnings("unused") IOException err ) {}
			}
			ui.error(e0);
			e0.printStackTrace();
		}
	}
	
}
