// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.utils;

import java.util.List;

import de.hechler.patrick.profiler.imp.PJPExecutingStack;
import de.hechler.patrick.profiler.imp.PJPSnapshot;
import de.hechler.patrick.profiler.imp.PJPStackFrame;
import de.hechler.patrick.profiler.imp.PJPThread;

public class Node {
	
	public String     suffix;
	public Object     value;
	public List<Node> children;
	
	public Node(Object value) {
		this.value = value;
	}
	
	public Object value() {
		return value;
	}
	
	@Override
	public String toString() {
		if ( this.suffix != null ) return this.value.toString().concat(this.suffix);
		return this.value.toString();
	}
	
	//static helper methods
	
	public static Object generateChild(Object parent, int index) {
		if ( parent instanceof PJPSnapshot ) {
			return ( (PJPSnapshot) parent ).thread(index);
		} else if ( parent instanceof PJPThread ) {
			PJPExecutingStack exe = ( (PJPThread) parent ).executing;
			if ( exe.frameCount() == 0 ) {
				return ( (PJPThread) parent ).finishedStack(index);
			}
			if ( index == 0 ) {
				return exe;
			}
			return ( (PJPThread) parent ).finishedStack(index - 1);
		} else if ( parent instanceof PJPExecutingStack ) {
			if ( index == 0 ) {
				PJPExecutingStack p = (PJPExecutingStack) parent;
				return new ExecutingNode(p, p.frame(0), 0);
			}
			throw new IndexOutOfBoundsException("index " + index + " out of bounds for length 1");
		} else if ( parent instanceof ExecutingNode ) {
			ExecutingNode p = (ExecutingNode) parent;
			int newIndex = p.index + 1;
			if ( newIndex == p.stack.frameCount() ) {
				return p.shown.subFrame(index);
			}
			if ( index == 0 ) {
				return new ExecutingNode(p.stack, p.stack.frame(newIndex), newIndex);
			}
			return p.shown.subFrame(index - 1);
		} else if ( parent instanceof PJPStackFrame ) {
			return ( (PJPStackFrame) parent ).subFrame(index);
		} else if ( parent == null ) {
			throw new NullPointerException("parent");
		}
		throw new IllegalArgumentException("illegal node class: " + parent.getClass());
	}
	
	public static int extractChildCount(Object parent) {
		if ( parent instanceof PJPSnapshot ) {
			return ( (PJPSnapshot) parent ).threadCount();
		} else if ( parent instanceof PJPThread ) {
			PJPExecutingStack exe = ( (PJPThread) parent ).executing;
			if ( exe.frameCount() == 0 ) {
				return ( (PJPThread) parent ).finishedCount();
			}
			return 1 + ( (PJPThread) parent ).finishedCount();
		} else if ( parent instanceof PJPExecutingStack ) {
			return 1;
		} else if ( parent instanceof ExecutingNode ) {
			ExecutingNode p = (ExecutingNode) parent;
			int newIndex = p.index + 1;
			if ( newIndex == p.stack.frameCount() ) {
				return p.shown.subFrames();
			}
			return p.shown.subFrames() + 1;
		} else if ( parent instanceof PJPStackFrame ) {
			return ( (PJPStackFrame) parent ).subFrames();
		} else if ( parent == null ) {
			throw new NullPointerException("parent");
		}
		throw new IllegalArgumentException("illegal node class: " + parent.getClass());
	}
	
	public static boolean isLeaf(Object node) {
		if ( node == null ) return true;
		if ( node instanceof PJPStackFrame ) {
			return ( (PJPStackFrame) node ).subFrames() == 0;
		}
		if ( node instanceof ExecutingNode ) {
			ExecutingNode en = (ExecutingNode) node;
			return en.index + 1 == en.stack.frameCount() && en.shown.subFrames() == 0;
		}
		return false;
	}
	
	public static int generateIndexOfChild(Object parent, Object child) {
		if ( parent instanceof PJPSnapshot ) {
			PJPSnapshot p = (PJPSnapshot) parent;
			int tc = p.threadCount();
			for (int i = 0; i < tc; i++) {
				if ( p.thread(i) != child ) continue;
				return i;
			}
		} else if ( parent instanceof PJPThread ) {
			PJPThread p = (PJPThread) parent;
			PJPExecutingStack exe = p.executing;
			int off = 0;
			if ( exe.frameCount() != 0 ) {
				if ( child == exe ) return 0;
				off = 1;
			}
			int c = p.finishedCount();
			for (int i = 0; i < c; i++) {
				if ( p.finishedStack(i) != child ) continue;
				return off + i;
			}
		} else if ( parent instanceof PJPExecutingStack ) {
			if ( child instanceof ExecutingNode ) {
				ExecutingNode c = (ExecutingNode) child;
				if ( c.stack == parent && c.index == 0 ) {
					return 0;
				}
			}
		} else if ( parent instanceof ExecutingNode ) {
			ExecutingNode p = (ExecutingNode) parent;
			if ( child instanceof ExecutingNode ) {
				ExecutingNode c = (ExecutingNode) child;
				if ( p.stack == c.stack && p.index + 1 == c.index ) {
					return 0;
				}
			} else {
				int newIndex = p.index + 1;
				int off = 1;
				if ( newIndex == p.stack.frameCount() ) {
					off = 0;
				}
				int c = p.shown.subFrames();
				for (int i = 0; i < c; i++) {
					if ( p.shown.subFrame(i) != child ) continue;
					return off + i;
				}
			}
		} else if ( parent instanceof PJPStackFrame ) {
			PJPStackFrame p = (PJPStackFrame) parent;
			int c = p.subFrames();
			for (int i = 0; i < c; i++) {
				if ( p.subFrame(i) != child ) continue;
				return i;
			}
		}
		return -1;
	}
	
}
