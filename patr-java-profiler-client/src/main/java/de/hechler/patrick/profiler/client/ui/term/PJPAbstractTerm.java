// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.term;

import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.CSI_SEP;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.C_ESC;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.FC_RED;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.NO_REVERSE_FB;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.RESET;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.REVERSE_FB;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_GET;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_SET_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_SET_COLUM_C;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_START_OF_DISPLAY;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.CURSOR_START_OF_LINE;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.ERASE_COMPLETE_DISPLAY;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.ERASE_END_OF_LINE;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.HIDE_CURSOR;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.SHOW_CURSOR;
import static de.hechler.patrick.profiler.client.ui.term.AnsiCodes.Extended.cursorColumn;

import java.awt.Point;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class PJPAbstractTerm {
	
	public static final String TTY = "/dev/tty";
	
	protected final OutputStreamWriter out;
	protected final WritebackReader    in;
	protected Point                    displayBounds;
	
	public PJPAbstractTerm() throws IOException {
		this(TTY);
	}
	
	public PJPAbstractTerm(String tty) throws IOException {
		if ( tty == null ) tty = TTY;
		if ( !Files.exists(Paths.get(tty)) ) {
			throw new FileNotFoundException(tty);
		}
		this.out = new OutputStreamWriter(new FileOutputStream(tty), StandardCharsets.UTF_8);
		this.in = new WritebackReader(new InputStreamReader(new FileInputStream(tty), StandardCharsets.UTF_8));
		this.displayBounds = new Point(1, 1);
		initTerm(tty);
	}
	
	protected PJPAbstractTerm(OutputStreamWriter out, WritebackReader in) {
		this.out = out;
		this.in = in;
		this.displayBounds = new Point(1, 1);
	}
	
	protected void initTerm(String tty) throws IOException {
		try {
			Process p = Runtime.getRuntime().exec(new String[]{ "/bin/stty", "-F", tty, "-g" });
			checkResult(p);
			byte[] saved = readAllBytes(p.getInputStream());
//			p = Runtime.getRuntime().exec(new String[]{ "/bin/stty", "-F", TTY, "-icanon", "-echo" });
			p = Runtime.getRuntime().exec(new String[]{ "/bin/stty", "-F", tty, "raw", "-echo" });
			checkResult(p);
			String[] arr = new String(saved, StandardCharsets.UTF_8).trim().split("\\s+");
			String[] cleanup = new String[arr.length + 3];
			cleanup[0] = "/bin/stty";
			cleanup[1] = "-F";
			cleanup[2] = tty;
			System.arraycopy(arr, 0, cleanup, 3, arr.length);
			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				try {
					Runtime.getRuntime().exec(cleanup).waitFor(5, TimeUnit.SECONDS);
				} catch ( IOException | InterruptedException e ) {
					e.printStackTrace();
				}
				try {
					this.out.write(SHOW_CURSOR + RESET);
				} catch ( IOException e ) {
					e.printStackTrace();
				}
				try {
					this.out.close();
					this.in.close();
				} catch ( IOException e ) {
					e.printStackTrace();
				}
			}));
			this.out.write(HIDE_CURSOR);
		} catch ( InterruptedException e ) {
			e.printStackTrace();
		}
	}
	
	protected void checkResult(Process p) throws IOException, InterruptedException {
		int e = p.waitFor();
		if ( e == 0 ) return;
		this.out.write(ERASE_COMPLETE_DISPLAY + CURSOR_START_OF_DISPLAY + "failed to execute the process:");
		this.out.write("exit number: " + e);
		this.out.write("stderr:");
		this.out.write(new String(readAllBytes(p.getErrorStream()), StandardCharsets.UTF_8));
		this.out.write("stdout:");
		this.out.write(new String(readAllBytes(p.getInputStream()), StandardCharsets.UTF_8));
		this.out.flush();
		System.exit(e);
	}
	
	public static byte[] readAllBytes(InputStream in) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		transfer(in, baos);
		return baos.toByteArray();
	}
	
	public static void transfer(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[2048];
		while ( true ) {
			int r = in.read(buf, 0, 2048);
			if ( r == -1 ) break;
			if ( r == 0 ) {
				r = in.read();
				if ( r == -1 ) break;
				buf[0] = (byte) r;
				r = in.read(buf, 1, 2047);
				if ( r == -1 ) {
					out.write(buf, 0, 1);
					break;
				}
				r++;
			}
			out.write(buf, 0, r);
		}
	}
	
	protected Point maxDisplayPos() {
		Point mdp = this.displayBounds;
		try {
			if ( mdp == null ) {
				this.displayBounds = mdp = new Point();
				return updateMaxPos();
			} else if ( mdp.x <= 1 || mdp.y <= 1 ) {
				return updateMaxPos();
			} else return mdp;
		} catch ( @SuppressWarnings("unused") IOException e ) {
			return mdp == null ? new Point(1, 1) : mdp;
		}
	}
	
	protected Point updateMaxPos() throws IOException {
		this.out.write(CSI + "9999" + CSI_SEP + "9999" + CURSOR_SET_C + CURSOR_GET);
		this.out.flush();
		return readCursorPoint(this.displayBounds);
	}
	
	protected Point cursorPoint(Point pos) throws IOException {
		this.out.write(CURSOR_GET);
		this.out.flush();
		return readCursorPoint(pos);
	}
	
	protected boolean supportDeviceStatusReport;
	
	protected Point readCursorPoint(Point pos) throws IOException {
		long start = System.currentTimeMillis();
		char[] b = new char[16];
		int r = read(b, 0, b.length);
		if ( r < 0 ) {
			this.out.write("\r\nreached EOF\r\n");
			this.out.flush();
			System.exit(1);
		}
		int i = 0;
		while ( true ) {
			for (; i < r; i++) {
				if ( b[i] != C_ESC ) {
					continue;
				}
				if ( i + 1 == r ) break;
				if ( b[i + 1] != '[' ) {
					continue;
				}
				int m;
				int line = -1;
				for (m = i + 2; m < r; m++) {
					if ( b[m] < '0' || b[m] > '9' ) {
						if ( b[m] == ';' ) {
							line = Integer.parseInt(String.valueOf(b, i + 2, m - ( i + 2 )));
						}
						break;
					}
				}
				if ( line == -1 ) {
					i = m;
					continue;
				}
				int column = -1;
				int l;
				for (l = m + 1; l < r; l++) {
					if ( b[l] < '0' || b[l] > '9' ) {
						if ( b[l] == 'R' ) {
							column = Integer.parseInt(String.valueOf(b, m + 1, l - ( m + 1 )));
						}
						break;
					}
				}
				if ( column == -1 ) {
					i = l;
					continue;
				}
				if ( i != 0 ) this.in.writeBufStart(b, 0, i);
				if ( l + 1 != r ) this.in.writeBufStart(b, l, r - l);
				if ( pos == null ) return new Point(column, line);
				pos.x = column;
				pos.y = line;
				supportDeviceStatusReport = true;
				return pos;
			}
			long current = System.currentTimeMillis();
			if ( current - start >= 2500L && !supportDeviceStatusReport ) {
				this.out.write(RESET + FC_RED + "the console does not seem to support the device status report command: [");
				for (i = 0; i < r; i++) {
					if ( i != 0 ) this.out.write(", ");
					this.out.write((int) b[i] + " : '" + b[i] + '\'');
				}
				this.out.write("]\r\n" + RESET);
				this.out.flush();
				System.exit(1);
			}
			if ( r == b.length ) {
				b = Arrays.copyOf(b, b.length << 1);
			}
			if ( !this.in.ready() ) {
				try {
					Thread.sleep(100L);
				} catch ( InterruptedException e ) {
					e.printStackTrace();
				}
			}
			r += read(b, i, b.length - i);
		}
	}
	
	protected int read(char[] buf, int off, int len) throws IOException {
		return this.in.read(buf, off, len);
	}
	
	protected void showWorld() throws IOException {
		showWorld(updateMaxPos());
	}
	
	protected void showWorld(Point max) throws IOException {
		StringBuilder sb = new StringBuilder();
		showHeaderLines(sb, max);
		showWorld(sb, max);
		this.out.write(sb.toString());
		this.out.flush();
	}
	
	protected void showStart(Appendable b, Point max, String line, String mid, String end) throws IOException {
		int maxEnd = max.x;
		if ( mid != null ) {
			int midPos = ( maxEnd - mid.length() ) >> 1;
			if ( 0 < midPos ) {
				maxEnd = midPos;
			}
		}
		if ( end != null ) {
			if ( max.x > end.length() ) {
				maxEnd = Math.min(maxEnd, max.x - end.length());
			}
		}
		if ( line.length() < maxEnd ) {
			b.append(CURSOR_START_OF_LINE + line);
		}
	}
	
	protected void showMid(Appendable b, Point max, String line, String mid) throws IOException {
		showMid(b, max, line, mid, null);
	}
	
	protected void showMid(Appendable b, Point max, String line, String mid, String end) throws IOException {
		if ( line == null ) line = "";
		int pos = ( max.x - mid.length() ) >> 1;
		int maxEnd = max.x;
		int minPos = line.length();
		if ( end != null ) {
			if ( max.x - minPos > end.length() ) {
				maxEnd -= end.length();
			}
		}
		if ( minPos < pos && pos + mid.length() < maxEnd ) {
			b.append(CSI + pos + CURSOR_SET_COLUM_C + mid);
		}
	}
	
	protected void showEnd(Appendable b, Point max, String line, String end) throws IOException {
		showEnd(b, max, line, null, end);
	}
	
	protected void showEnd(Appendable b, Point max, String line, String mid, String end) throws IOException {
		if ( line == null ) line = "";
		int minPos = line.length();
		if ( mid != null ) {
			int pos = ( max.x - mid.length() ) >> 1;
			if ( line.length() < pos ) {
				minPos = pos + mid.length();
			}
		}
		if ( max.x - minPos > end.length() ) {
			b.append(cursorColumn(max.x - end.length() + 1) + end);
		}
	}
	
	protected String unknownInfo;
	
	public void exec() throws IOException {
		char[] b = new char[64];
		int off = 0;
		while ( true ) {
			showWorld();
			int r = read(b, off, b.length - off);
			if ( r < 0 ) break;
			r += off;
			clearUnknownInfo();
			for (int i = 0; i < r;) {
				i = act(b, r, i);
				if ( i < 0 ) {
					off = -i;
					if ( off == b.length ) {
						b = Arrays.copyOf(b, b.length << 1);
					}
					break;
				}
			}
		}
	}
	
	protected void clearUnknownInfo() {
		this.unknownInfo = null;
	}
	
	private int act(char[] b, int r, int i) throws IOException {
		switch ( b[i] ) {
		case C_ESC:
			return actESC(b, r, i);
		case '\u0004':
			actCtrlD();
			break;
		case '\u0003':
			actCtrlC();
			break;
		case 'w':
			actW();
			break;
		case 'a':
			actA();
			break;
		case 's':
			actS();
			break;
		case 'd':
			actD();
			break;
		case ':':
			return actColon(b, r, i);
		default:
			return actUnknown(b, i, r);
		}
		return i + 1;
	}
	
	protected int actUnknown(char[] b, int i, @SuppressWarnings("unused") int r) throws IOException {
		String add = unknownInfo(b[i]);
		addUnknownInfo(UIMP_UNKNOWN_INPUT, add);
		return i + 1;
	}
	
	public static final int UIMP_UNKNOWN_INPUT = 100;
	public static final int UIMP_EXCEPTION     = 200;
	public static final int UIMP_ILLEGAL_USAGE = 300;
	public static final int UIMP_USER_REQUEST  = 400;
	
	public static final int MIN_UIMP = UIMP_EXCEPTION;
	
	protected void addUnknownInfo(int importance, String add) {
		if ( importance < MIN_UIMP ) return;
		if ( this.unknownInfo == null ) {
			this.unknownInfo = add;
		} else {
			this.unknownInfo += ", " + add;
		}
	}
	
	protected String unknownInfo(char c) {
		if ( c >= ' ' && c < 127 ) {
			return "0x" + Integer.toHexString(c).toUpperCase() + " : '" + c + '\'';
		}
		return "0x" + Integer.toHexString(c).toUpperCase();
	}
	
	private int actColon(char[] b, int reat, int index) throws IOException {
		int i = index;
		int r = reat;
		while ( ++i < r ) {
			if ( b[i] == '\n' || b[i] == '\r' ) {
				execCommand(String.valueOf(b, index + 1, i));
				return i + 1;
			} else if ( b[i] == '\u0004' ) {
				actCtrlD();
				return i + 1;
			} else if ( b[i] == '\u0003' ) {
				return i + 1;
			}
		}
		StringBuilder cmd = new StringBuilder();
		cmd.append(b, index + 1, reat - index - 1);
		readAndExecCmd(cmd);
		return reat;
	}
	
	protected void readAndExecCmd(StringBuilder cmd) throws IOException {
		char[] b = new char[16];
		int i;
		int r;
		this.out.write(CSI + "9999" + CURSOR_SET_C + ":" + cmd + ERASE_END_OF_LINE + REVERSE_FB + ' ' + NO_REVERSE_FB);
		int pos = cmd.length();
		int bigIndex = previus.size();
		while ( true ) {
			this.out.flush();
			r = read(b, 0, b.length);
			if ( r < 0 ) {
				execCommand(cmd.toString());
				return;
			}
			int ai = 0;
			for (i = 0; i < r; i++) {
				if ( b[i] == '\n' || b[i] == '\r' ) {
					execCommand(cmd.toString());
				} else if ( b[i] == '\u007F' ) {
					insert(cmd, pos, b, ai, i);
					pos += i - ai;
					if ( pos != 0 ) {
						cmd.replace(pos - 1, pos, "");
						pos--;
					}
					ai = i + 1;
					continue;
				} else if ( b[i] == C_ESC ) {
					insert(cmd, pos, b, ai, i);
					pos += i - ai;
					if ( i + 2 < r && b[i + 1] == '[' ) {
						switch ( b[i + 2] ) {
						case 'C':
							pos = Math.min(cmd.length(), pos + 1);
							break;
						case 'D':
							pos = Math.max(0, pos - 1);
							break;
						case 'F':
							pos = cmd.length();
							break;
						case 'H':
							pos = 0;
							break;
						case 'A':
							if ( bigIndex > 0 ) {
								cmd.replace(0, cmd.length(), this.previus.get(--bigIndex));
								pos = cmd.length();
							}
							break;
						case 'B':
							if ( bigIndex < this.previus.size() - 2 ) {
								cmd.replace(0, cmd.length(), this.previus.get(++bigIndex));
							} else {
								cmd.replace(0, cmd.length(), "");
							}
							pos = cmd.length();
							break;
						case '5':
							if ( i + 3 < r && b[i + 3] == '~' ) {
								if ( !this.previus.isEmpty() ) {
									cmd.replace(0, cmd.length(), this.previus.get(bigIndex = 0));
									pos = cmd.length();
								}
							} else {
								ai = i = i + 1;
							}
							continue;
						case '6':
							if ( i + 3 < r && b[i + 3] == '~' ) {
								int s = this.previus.size();
								if ( s != 0 ) {
									cmd.replace(0, cmd.length(), this.previus.get(bigIndex = s - 1));
									pos = cmd.length();
								}
							} else {
								ai = i = i + 1;
							}
							continue;
						case '3':
							if ( i + 3 < r && b[i + 3] == '~' ) {
								if ( pos != cmd.length() ) {
									cmd.replace(pos, pos + 1, "");
								}
								ai = i = i + 4;
							} else {
								ai = i = i + 1;
							}
							continue;
						default:
							ai = i = i + 1;
							continue;
						}
						ai = i = i + 3;
					}
					continue;
				} else if ( b[i] == '\u0004' ) {
					actCtrlD();
				} else if ( b[i] != '\u0003' ) {
					continue;
				}
				if ( i + 1 != r ) {
					this.in.writeBufStart(b, i, r - i - 1);
				}
				return;
			}
			if ( ai != r ) {
				insert(cmd, pos, b, ai, i);
				pos += r - ai;
			}
			if ( cmd.length() == pos ) {
				this.out.write(
					CSI + "9999" + CSI_SEP + "2" + CURSOR_SET_C + cmd + REVERSE_FB + ' ' + NO_REVERSE_FB + ERASE_END_OF_LINE);
			} else {
				this.out.write(CSI + "9999" + CSI_SEP + "2" + CURSOR_SET_C + cmd.substring(0, pos) + REVERSE_FB + cmd.charAt(pos)
					+ NO_REVERSE_FB + cmd.substring(pos + 1) + ERASE_END_OF_LINE);
			}
		}
	}
	
	private void insert(StringBuilder cmd, int insertPos, char[] buf, int startBufIndex, int endBufIndex) {
		if ( insertPos == cmd.length() ) {
			cmd.append(buf, startBufIndex, endBufIndex - startBufIndex);
		} else {
			cmd.insert(insertPos, buf, startBufIndex, endBufIndex - startBufIndex);
		}
	}
	
	protected int actESC(char[] b, int r, int i) throws IOException {
		if ( i + 2 < r && b[i + 1] == '[' ) {
			switch ( b[i + 2] ) {
			case 'A':
				actArrowUp();
				break;
			case 'B':
				actArrowDown();
				break;
			case 'C':
				actArrowRight();
				break;
			case 'D':
				actArrowLeft();
				break;
			case 'F':
				actEnd();
				break;
			case 'H':
				actPos1();
				break;
			case '1':
				return actPotCtrlPos(b, i, r);
			case '5':
				return actPotPage(b, i, r, true);
			case '6':
				return actPotPage(b, i, r, false);
			default:
				return actUnknown(b, i, r);
			}
			return i + 3;
		}
		if ( i + 2 >= r && this.in.ready() ) {
			System.arraycopy(b, i, b, 0, r - i);
			return -( r - i );
		}
		return actUnknown(b, i, r);
	}
	
	protected int actPotCtrlPos(char[] b, int i, int r) throws IOException {
		if ( i + 5 >= r && this.in.ready() ) {
			System.arraycopy(b, i, b, 0, r - i);
			return -( r - i );
		}
		if ( b[i + 3] != ';' || b[i + 4] != '5' ) {
			return actUnknown(b, r, i);
		}
		switch ( b[i + 5] ) {
		case 'H':
			actCtrlPos1();
			break;
		case 'F':
			actCtrlEnd();
			break;
		default:
			return actUnknown(b, r, i);
		}
		return i + 6;
	}
	
	protected int actPotPage(char[] b, int i, int r, boolean up) throws IOException {
		if ( i + 3 >= r && this.in.ready() ) {
			System.arraycopy(b, i, b, 0, r - i);
			return -( r - i );
		}
		if ( b[i + 3] != '~' ) {
			return actUnknown(b, r, i);
		}
		if ( up ) {
			actPageUp();
		} else {
			actPageDown();
		}
		return i + 4;
	}
	
	
	protected List<String> previus = new ArrayList<>();
	
	protected void execCommand(String cmd) {
		boolean added = false;
		if ( !cmd.trim().isEmpty() ) {
			added = true;
			this.previus.add(cmd);
		}
		String[] args = parseCommand(cmd);
		if ( args == null || args.length == 0 ) return;
		if ( !added ) {
			this.previus.add(cmd);
		}
		try {
			execCommand(args);
		} catch ( RuntimeException | IOException re ) {
			re.printStackTrace();
			addUnknownInfo(UIMP_EXCEPTION, re.toString());
		}
	}
	
	private static final Pattern CMD = Pattern
		.compile("(\\s+)|([^\\s'\"$\\\\]+)|(\\$[^\\s'\"$\\\\]+)|(\\\\[$'\"])|('[^']*')|(\"[^\"]\")");
	
	protected String[] parseCommand(String cmd) {
		Matcher matcher = CMD.matcher(cmd);
		int last = 0;
		List<String> args = new ArrayList<>();
		StringBuilder sb = null;
		while ( matcher.find() ) {
			if ( last != matcher.start() ) {
				addUnknownInfo(UIMP_ILLEGAL_USAGE, "invalid command: '" + cmd + "'");
				return null;
			}
			last = matcher.end();
			String ws = matcher.group(1);
			String word = matcher.group(2);
			String dollar = matcher.group(3);
			String escape = matcher.group(4);
			String singleQuoted = matcher.group(5);
			String doubleQuoted = matcher.group(6);
			if ( ws != null ) {
				if ( sb != null ) {
					args.add(sb.toString());
					sb = null;
				}
				continue;
			}
			if ( sb == null ) sb = new StringBuilder();
			if ( word != null ) sb.append(word);
			else if ( dollar != null ) {
				String ref = dollar.substring(1);
				String deref = deref(ref);
				if ( deref == null ) {
					addUnknownInfo(UIMP_ILLEGAL_USAGE, "unknown refered value: " + ref);
					return null;
				}
				sb.append(deref);
			} else if ( escape != null ) sb.append(escape.substring(1));
			else if ( singleQuoted != null ) sb.append(singleQuoted.substring(1, singleQuoted.length() - 2));
			else sb.append(doubleQuoted.substring(1, doubleQuoted.length() - 2));
		}
		if ( last != cmd.length() ) {
			addUnknownInfo(UIMP_ILLEGAL_USAGE, "invalid command: '" + cmd + "'");
			return null;
		}
		if ( sb != null ) {
			args.add(sb.toString());
		}
		return args.toArray(new String[args.size()]);
	}
	
	private String deref(String ref) {
		switch ( ref ) {
		case "display.columns":
			return Integer.toString(maxDisplayPos().x);
		case "display.lines":
			return Integer.toString(maxDisplayPos().y);
		default:
			if ( ref.startsWith("env.") ) {
				return System.getenv(ref.substring(4));
			}
			if ( ref.startsWith("prop.") ) {
				return System.getProperty(ref.substring(5));
			}
			return System.getProperty(ref);
		}
	}
	
	protected void execCommand(String[] args) throws IOException {
		switch ( args[0] ) {
		case "exit":
			try {
				System.exit(args.length == 1 ? 0 : Integer.parseInt(args[1]));
			} catch ( @SuppressWarnings("unused") NumberFormatException e ) {
				if ( "help".equalsIgnoreCase(args[1].trim()) || "--help".equalsIgnoreCase(args[1].trim()) ) {
					addUnknownInfo(UIMP_USER_REQUEST, "Usage: exit [EXIT_NUMBER]");
					return;
				}
				this.out.write(FC_RED + "could not parse EXIT_NUMBER: '" + args[1] + '\'');
				System.exit(1);
			}
		case "quit":
			System.exit(args.length == 1 ? 0 : 1);
		case "clear":
			this.out.write(ERASE_COMPLETE_DISPLAY);
			this.out.flush();
			break;
		default:
			addUnknownInfo(UIMP_ILLEGAL_USAGE, "unknown command '" + args[0] + '\'');
		}
	}
	
	protected void actCtrlD() throws IOException {
		System.exit(0);
	}
	
	protected void actCtrlC() throws IOException {
		this.out.write(ERASE_COMPLETE_DISPLAY);
		this.out.flush();
	}
	
	protected abstract void actCtrlPos1() throws IOException;
	protected abstract void actCtrlEnd() throws IOException;
	
	protected abstract void actPos1() throws IOException;
	protected abstract void actEnd() throws IOException;
	
	protected abstract void actPageUp() throws IOException;
	protected abstract void actPageDown() throws IOException;
	
	protected abstract void actW() throws IOException;
	protected abstract void actA() throws IOException;
	protected abstract void actS() throws IOException;
	protected abstract void actD() throws IOException;
	
	protected abstract void actArrowUp() throws IOException;
	protected abstract void actArrowDown() throws IOException;
	protected abstract void actArrowLeft() throws IOException;
	protected abstract void actArrowRight() throws IOException;
	
	
	protected abstract void showHeaderLines(Appendable b, Point max) throws IOException;
	protected abstract void showWorld(Appendable b, Point max) throws IOException;
	
}
