// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.term;

/**
 * this class defines constants usable to configure the format of the written text and some helper methods
 */
public class AnsiCodes {
	
	private AnsiCodes() {}
	
	/**
	 * the escape character used to start a sequence
	 */
	public static final char   C_ESC = '\u001B';
	/**
	 * the escape string used to start a sequence
	 * 
	 * @see #C_ESC
	 */
	public static final String ESC   = "\u001B";
	
	/**
	 * the start of a CSI sequence
	 */
	public static final String CSI     = ESC + "[";
	/**
	 * the separator during a CSI sequence
	 */
	public static final String CSI_SEP = ";";
	/**
	 * the default termination character of a CSI sequence
	 */
	public static final String CSI_END = "m";
	
	public static final String CSI_RESET_C             = "0";
	public static final String CSI_BOLD_C              = "1";
	public static final String CSI_LESS_INTENSE_C      = "2";
	public static final String CSI_ITALIC_C            = "3";
	public static final String CSI_UNDERLINE_C         = "4";
	public static final String CSI_REVERSE_FB_C        = "7";
	public static final String CSI_STRIKE_THROUGH_C    = "9";
	public static final String CSI_DEF_INTENSE_C       = "22";
	public static final String CSI_NO_ITALIC_C         = "23";
	public static final String CSI_NO_UNDERLINE_C      = "24";
	public static final String CSI_NO_REVERSE_FB_C     = "27";
	public static final String CSI_NO_STRIKE_THROUGH_C = "29";
	public static final String CSI_DEF_FC_C            = "39";
	public static final String CSI_DEF_BC_C            = "49";
	
	public static final String CSI_FC_PREFIX_C = "3";
	public static final String CSI_BC_PREFIX_C = "4";
	
	public static final String CSI_C_BLACK_C   = "0";
	public static final String CSI_C_RED_C     = "1";
	public static final String CSI_C_GREEN_C   = "2";
	public static final String CSI_C_YELLOW_C  = "3";
	public static final String CSI_C_BLUE_C    = "4";
	public static final String CSI_C_MAGENTA_C = "5";
	public static final String CSI_C_CYAN_C    = "6";
	public static final String CSI_C_WHITE_C   = "7";
	
	public static final String CSI_C_RGB_C = "8" + CSI_SEP + "2" + CSI_SEP;
	
	public static final String CSI_C_RGB_GRAY_C       = CSI_C_RGB_C + "128" + CSI_SEP + "128" + CSI_SEP + "128";
	public static final String CSI_C_RGB_LIGHT_GRAY_C = CSI_C_RGB_C + "192" + CSI_SEP + "192" + CSI_SEP + "192";
	public static final String CSI_C_RGB_DARK_GRAY_C  = CSI_C_RGB_C + "64" + CSI_SEP + "64" + CSI_SEP + "64";
	
	public static final String RESET             = CSI + CSI_RESET_C + CSI_END;
	public static final String BOLD              = CSI + CSI_BOLD_C + CSI_END;
	public static final String LESS_INTENSE      = CSI + CSI_LESS_INTENSE_C + CSI_END;
	public static final String ITALIC            = CSI + CSI_ITALIC_C + CSI_END;
	public static final String UNDERLINE         = CSI + CSI_UNDERLINE_C + CSI_END;
	public static final String REVERSE_FB        = CSI + CSI_REVERSE_FB_C + CSI_END;
	public static final String STRIKE_THROUGH    = CSI + CSI_STRIKE_THROUGH_C + CSI_END;
	public static final String DEF_INTENSE       = CSI + CSI_DEF_INTENSE_C + CSI_END;
	public static final String NO_ITALIC         = CSI + CSI_NO_ITALIC_C + CSI_END;
	public static final String NO_UNDERLINE      = CSI + CSI_NO_UNDERLINE_C + CSI_END;
	public static final String NO_REVERSE_FB     = CSI + CSI_NO_REVERSE_FB_C + CSI_END;
	public static final String NO_STRIKE_THROUGH = CSI + CSI_NO_STRIKE_THROUGH_C + CSI_END;
	public static final String DEF_FC            = CSI + CSI_DEF_FC_C + CSI_END;
	public static final String DEF_BC            = CSI + CSI_DEF_BC_C + CSI_END;
	public static final String FC_BLACK          = CSI + CSI_FC_PREFIX_C + CSI_C_BLACK_C + CSI_END;
	public static final String FC_RED            = CSI + CSI_FC_PREFIX_C + CSI_C_RED_C + CSI_END;
	public static final String FC_GREEN          = CSI + CSI_FC_PREFIX_C + CSI_C_GREEN_C + CSI_END;
	public static final String FC_YELLOW         = CSI + CSI_FC_PREFIX_C + CSI_C_YELLOW_C + CSI_END;
	public static final String FC_BLUE           = CSI + CSI_FC_PREFIX_C + CSI_C_BLUE_C + CSI_END;
	public static final String FC_MAGENTA        = CSI + CSI_FC_PREFIX_C + CSI_C_MAGENTA_C + CSI_END;
	public static final String FC_CYAN           = CSI + CSI_FC_PREFIX_C + CSI_C_CYAN_C + CSI_END;
	public static final String FC_WHITE          = CSI + CSI_FC_PREFIX_C + CSI_C_WHITE_C + CSI_END;
	public static final String FC_RGB_GRAY       = CSI + CSI_FC_PREFIX_C + CSI_C_RGB_GRAY_C + CSI_END;
	public static final String FC_RGB_LIGHT_GRAY = CSI + CSI_FC_PREFIX_C + CSI_C_RGB_LIGHT_GRAY_C + CSI_END;
	public static final String FC_RGB_DARK_GRAY  = CSI + CSI_FC_PREFIX_C + CSI_C_RGB_DARK_GRAY_C + CSI_END;
	public static final String BC_BLACK          = CSI + CSI_BC_PREFIX_C + CSI_C_BLACK_C + CSI_END;
	public static final String BC_RED            = CSI + CSI_BC_PREFIX_C + CSI_C_RED_C + CSI_END;
	public static final String BC_GREEN          = CSI + CSI_BC_PREFIX_C + CSI_C_GREEN_C + CSI_END;
	public static final String BC_YELLOW         = CSI + CSI_BC_PREFIX_C + CSI_C_YELLOW_C + CSI_END;
	public static final String BC_BLUE           = CSI + CSI_BC_PREFIX_C + CSI_C_BLUE_C + CSI_END;
	public static final String BC_MAGENTA        = CSI + CSI_BC_PREFIX_C + CSI_C_MAGENTA_C + CSI_END;
	public static final String BC_CYAN           = CSI + CSI_BC_PREFIX_C + CSI_C_CYAN_C + CSI_END;
	public static final String BC_WHITE          = CSI + CSI_BC_PREFIX_C + CSI_C_WHITE_C + CSI_END;
	public static final String BC_RGB_GRAY       = CSI + CSI_BC_PREFIX_C + CSI_C_RGB_GRAY_C + CSI_END;
	public static final String BC_RGB_LIGHT_GRAY = CSI + CSI_BC_PREFIX_C + CSI_C_RGB_LIGHT_GRAY_C + CSI_END;
	public static final String BC_RGB_DARK_GRAY  = CSI + CSI_BC_PREFIX_C + CSI_C_RGB_DARK_GRAY_C + CSI_END;
	
	public static String bc(int r, int g, int b) {
		return CSI + CSI_BC_PREFIX_C + CSI_C_RGB_C + CSI_SEP + r + CSI_SEP + g + CSI_SEP + b + CSI_END;
	}
	
	public static String fc(int r, int g, int b) {
		return CSI + CSI_FC_PREFIX_C + CSI_C_RGB_C + CSI_SEP + r + CSI_SEP + g + CSI_SEP + b + CSI_END;
	}
	
	/**
	 * extended ANSI codes (and help methods) which are not always supported (usually only when there is a tty)
	 */
	public static class Extended {
		
		public static final String BELL = "\u0007";
		
		public static final String CURSOR_UP_C        = "A";
		public static final String CURSOR_DOWN_C      = "B";
		public static final String CURSOR_FORWARD_C   = "C";
		public static final String CURSOR_BACK_C      = "D";
		public static final String CURSOR_NEXT_LINE_C = "E";
		public static final String CURSOR_PREV_LINE_C = "F";
		public static final String CURSOR_SET_COLUM_C = "G";
		public static final String CURSOR_SET_C       = "H";
		
		public static final String ERASE_IN_DISPLAY_C = "J";
		public static final String ERASE_IN_LINE_C    = "K";
		
		public static final String CURSOR_UP               = CSI + CURSOR_UP_C;
		public static final String CURSOR_DOWN             = CSI + CURSOR_DOWN_C;
		public static final String CURSOR_FORWARD          = CSI + CURSOR_FORWARD_C;
		public static final String CURSOR_BACK             = CSI + CURSOR_BACK_C;
		public static final String CURSOR_NEXT_LINE        = CSI + CURSOR_NEXT_LINE_C;
		public static final String CURSOR_PREV_LINE        = CSI + CURSOR_PREV_LINE_C;
		public static final String CURSOR_START_OF_LINE    = CSI + CURSOR_SET_COLUM_C;
		public static final String CURSOR_START_OF_DISPLAY = CSI + CURSOR_SET_C;
		
		public static final String ERASE_END_OF_DISPLAY   = CSI + ERASE_IN_DISPLAY_C;
		public static final String ERASE_START_OF_DISPLAY = CSI + "1" + ERASE_IN_DISPLAY_C;
		public static final String ERASE_COMPLETE_DISPLAY = CSI + "2" + ERASE_IN_DISPLAY_C;
		
		public static final String ERASE_END_OF_LINE   = CSI + ERASE_IN_LINE_C;
		public static final String ERASE_START_OF_LINE = CSI + "1" + ERASE_IN_LINE_C;
		public static final String ERASE_COMPLETE_LINE = CSI + "2" + ERASE_IN_LINE_C;
		
		public static final String CURSOR_GET = CSI + "6n";
		
		public static final String SHOW_CURSOR = CSI + "?25h";
		public static final String HIDE_CURSOR = CSI + "?25l";
		
		public static final String END_OF_STRING = ESC + "\\";
		
		public static final String TITLE_START = ESC + "]0;";
		public static final String TITLE_END   = END_OF_STRING;
		
		public static String cursorHoriontal(int steps) {
			if ( steps > 0 ) {
				return CSI + steps + CURSOR_FORWARD_C;
			} else if ( steps < 0 ) {
				return CSI + -steps + CURSOR_BACK_C;
			} else {
				return "";
			}
		}
		
		public static String cursorVertical(int steps) {
			if ( steps > 0 ) {
				return CSI + steps + CURSOR_UP_C;
			} else if ( steps < 0 ) {
				return CSI + -steps + CURSOR_DOWN_C;
			} else {
				return "";
			}
		}
		
		public static String cursorLines(int lines) {
			if ( lines > 0 ) {
				return CSI + lines + CURSOR_NEXT_LINE_C;
			} else if ( lines < 0 ) {
				return CSI + -lines + CURSOR_PREV_LINE_C;
			} else {
				return "";
			}
		}
		
		public static String cursor(int line, int column) {
			return CSI + line + CSI_SEP + column + CURSOR_SET_C;
		}
		
		public static String cursorLine(int line) {
			return CSI + line + CURSOR_SET_C;
		}
		
		public static String cursorColumn(int column) {
			return CSI + column + CURSOR_SET_COLUM_C;
		}
		
	}
	
}
