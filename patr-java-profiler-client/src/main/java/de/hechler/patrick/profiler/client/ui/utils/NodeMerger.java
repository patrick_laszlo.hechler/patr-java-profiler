// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.utils;

import java.util.ArrayList;
import java.util.List;

import de.hechler.patrick.profiler.imp.PJPExecutingStack;
import de.hechler.patrick.profiler.imp.PJPStackFrame;
import de.hechler.patrick.profiler.imp.PJPThread;

public abstract class NodeMerger< N extends Node > {
	
	@SuppressWarnings("unchecked")
	public void merge(N node, Object obj, List<N> path, int index) {
		Object oldValue = node.value;
		if ( oldValue.equals(obj) ) return;
		if ( oldValue.getClass() != obj.getClass() ) {
			throw new AssertionError(oldValue.getClass() + " : " + obj.getClass());
		}
		if ( node.children != null ) {
			path.add(node);
			List<N> insert = null;
//			List<Node> remove = null;
			int newLen = Node.extractChildCount(obj);
			int oldLen = node.children.size();
			for (int i = 0, inserted = 0; i < newLen; i++) {
				Object child = Node.generateChild(obj, i);
				int oldIndex = findChild(node.children, child);
				if ( oldIndex == -1 ) {
					if ( insert == null ) insert = new ArrayList<>();
					insert.add(newNode(node, child));
					inserted++;
				} else if ( oldIndex < i - inserted ) {
					if ( insert == null ) insert = new ArrayList<>();
//					if ( remove == null ) remove = new ArrayList<>();
					N cn = (N) node.children.remove(oldIndex);
					cn.value = child;
					insert.add(cn);
					remove(cn, path, oldIndex);
					oldLen--;
				} else {
					merge((N) node.children.get(oldIndex), child, path, oldIndex);
				}
			}
			for (int i = 0; i < oldLen; i++) {
				Object child = node.children.get(i).value;
				int newIndex = findChild(obj, newLen, child);
				if ( newIndex == -1 ) {
					N cn = (N) node.children.remove(i);
					remove(cn, path, i);
//					if ( remove == null ) remove = new ArrayList<>();
//					remove.add(cn);
					oldLen--;
					i--;
				}
			}
//			if ( remove != null ) {
//				for (Node rem : remove) {
//					fireRemoved(event(rem, path));
//				}
//			}
			if ( insert != null ) {
				for (N ins : insert) {
					int i = Node.generateIndexOfChild(obj, ins.value);
					node.children.add(i, ins);
					insert(ins, path, i);
				}
			}
			path.remove(path.size() - 1);
		}
		node.value = obj;
		change(node, path, index);
	}
	
	protected abstract N newNode(N parent, Object child);
	
	protected abstract void remove(N removedNode, List<N> path, int childIndex);
	
	protected abstract void insert(N removedNode, List<N> path, int childIndex);
	
	protected abstract void change(N removedNode, List<N> path, int childIndex);
	
	public static int findChild(List<Node> values, Object child) {
		int childCount = values.size();
		for (int i = 0; i < childCount; i++) {
			Object otherChild = values.get(i).value;
			if ( equivalent(child, otherChild) ) {
				return i;
			}
		}
		return -1;
	}
	
	public static int findChild(Object parent, int childCount, Object child) {
		for (int i = 0; i < childCount; i++) {
			Object otherChild = Node.generateChild(parent, i);
			if ( equivalent(child, otherChild) ) {
				return i;
			}
		}
		return -1;
	}
	
	public static boolean equivalent(Object ao, Object bo) {
		if ( ao instanceof PJPThread ) {
			return ( (PJPThread) ao ).id == ( (PJPThread) bo ).id;
		} else if ( ao instanceof PJPExecutingStack ) {
			if ( bo instanceof PJPExecutingStack ) return true;
			if ( bo instanceof ExecutingNode ) return false;
			if ( bo instanceof PJPStackFrame ) return false;
			throw new AssertionError(bo.getClass());
		} else if ( ao instanceof ExecutingNode ) {
			if ( bo instanceof PJPStackFrame ) return false;
			if ( bo instanceof PJPExecutingStack ) return false;
			return ( (ExecutingNode) ao ).shown.sameMethod(( (ExecutingNode) bo ).shown);
		} else if ( ao instanceof PJPStackFrame ) {
			if ( bo instanceof ExecutingNode ) return false;
			if ( bo instanceof PJPExecutingStack ) return false;
			return ( (PJPStackFrame) ao ).sameMethod((PJPStackFrame) bo);
		} else {
			throw new AssertionError(ao.getClass().getName());
		}
	}
	
	
}
