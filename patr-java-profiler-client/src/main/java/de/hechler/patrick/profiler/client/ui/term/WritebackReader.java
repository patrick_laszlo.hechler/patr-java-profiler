// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.term;

import java.io.IOException;
import java.io.Reader;

public class WritebackReader extends Reader {
	
	private final Reader in;
	private char[]       buf;
	private int          pos;
	private int          size;
	
	public WritebackReader(Reader in) {
		this.in = in;
		this.buf = new char[128];
	}
	
	public void writeBufStart(char[] cbuf, int off, int len) throws IllegalStateException {
		checkArgs(cbuf, off, len);
		char[] b = this.buf;
		if ( b == null ) {
			throw new IllegalStateException("closed");
		}
		int s = this.size;
		int p = this.pos;
		if ( b.length - s >= len ) {
			if ( p < len ) {
				if ( s != 0 ) {
					System.arraycopy(b, p, b, len, s);
				}
				p = len;
			}
			System.arraycopy(cbuf, off, b, p - len, len);
			this.pos = p - len;
			this.size = s + len;
		} else {
			char[] nb = new char[b.length << 1];
			System.arraycopy(cbuf, off, nb, 0, len);
			System.arraycopy(b, p, nb, s, s);
			this.buf = nb;
			this.size = s + len;
			if ( p != 0 ) this.pos = 0;
		}
	}
	
	public void writeBufEnd(char[] cbuf, int off, int len) throws IllegalStateException {
		checkArgs(cbuf, off, len);
		char[] b = this.buf;
		if ( b == null ) {
			throw new IllegalStateException("closed");
		}
		int s = this.size;
		int p = this.pos;
		if ( b.length - s >= len ) {
			if ( b.length - s - p < len ) {
				System.arraycopy(b, p, b, 0, s);
				this.pos = p = 0;
			}
			System.arraycopy(cbuf, off, b, p + s, len);
			this.size += len;
		} else {
			char[] nb = new char[b.length << 1];
			System.arraycopy(b, p, nb, 0, s);
			System.arraycopy(cbuf, off, b, s, len);
			this.buf = nb;
			this.size = s + len;
			if ( p != 0 ) this.pos = 0;
		}
	}
	
	@Override
	public int read() throws IOException {
		int s = this.size;
		if ( s == 0 ) {
			return this.in.read();
		}
		int p = this.pos;
		int result = this.buf[p];
		if ( s - 1 == 0 ) {
			this.size = this.pos = 0;
		} else {
			this.size = s - 1;
			this.pos = p + 1;
		}
		return result;
	}
	
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		int s = this.size;
		if ( s == 0 ) {
			return this.in.read(cbuf, off, len);
		}
		int p = this.pos;
		if ( s >= len || !this.in.ready() ) {
			int cpy = Math.min(s, len);
			System.arraycopy(this.buf, p, cbuf, off, cpy);
			if ( s <= len ) {
				this.size = this.pos = 0;
			} else {
				this.size = s - cpy;
				this.pos = p + cpy;
			}
			return cpy;
		} // check otherwise done by System.arraycopy or in.read
		checkArgs(cbuf, off, len);
		System.arraycopy(this.buf, p, cbuf, off, s);
		int noff = off + s;
		int nlen = len - s;
		this.pos = this.size = 0;
		return s + this.in.read(cbuf, noff, nlen);
	}
	
	private static void checkArgs(char[] cbuf, int off, int len) {
		if ( off < 0 || len < 0 || cbuf.length - off < len ) {
			throw new IllegalArgumentException("off=" + off + ", len=" + len + ", cbuf.length=" + cbuf.length);
		}
	}
	
	@Override
	public boolean ready() throws IOException {
		return this.size != 0 || this.in.ready();
	}
	
	@Override
	public void close() throws IOException {
		this.in.close();
		this.buf = null;
		this.size = 0;
	}
	
}
