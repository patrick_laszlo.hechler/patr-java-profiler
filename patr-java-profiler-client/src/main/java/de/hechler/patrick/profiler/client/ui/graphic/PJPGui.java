// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.graphic;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.InputEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.WindowConstants;

import de.hechler.patrick.profiler.client.Version;
import de.hechler.patrick.profiler.client.ui.PJPUserInterface;
import de.hechler.patrick.profiler.imp.PJPSnapshot;


public class PJPGui implements PJPUserInterface {
	
	public final PJPTreeModel model;
	
	private String      remote;
	private JFrame      frame;
	private JScrollPane sp;
	private JTree       tree;
	
	public PJPGui() {
		model = new PJPTreeModel();
	}
	
	
	@Override
	public void close() {
		frame.dispose();
	}
	
	@Override
	public void state(int state) {
		switch ( state ) {
		case STATE_RUNNING:
			model.rootSuffix(null);
			break;
		case STATE_CONNECTION_LOST:
			model.rootSuffix(" (connection lost)");
			frame.setTitle(remote + " (connection lost)");
			break;
		case STATE_FINAL_SNAPSHOT:
			model.rootSuffix(" (final state)");
			frame.setTitle(remote + " (final state)");
			break;
		case STATE_TERMINATED_NO_SN:
			model.rootSuffix(" (terminated)");
			frame.setTitle(remote + " (terminated)");
			break;
		default:
			throw new IllegalArgumentException("unknown state: " + state);
		}
	}
	
	@Override
	public void update(PJPSnapshot show) {
		model.show(show);
	}
	
	@Override
	public void error(Throwable t) {
		model.rootSuffix(" (error: " + t.getMessage() + ')');
		JOptionPane.showMessageDialog(frame, t.toString(), t.getClass().getSimpleName(), JOptionPane.ERROR_MESSAGE);
	}
	
	public PJPGui makeVisible() {
		frame.enableInputMethods(false);
		frame.getGraphicsConfiguration().getDevice().setFullScreenWindow(frame);
//		frame.setVisible(true); 
		return this;
	}
	
	public PJPGui init(String remote, Version remotePjpVersion, int remoteJvmVersion) {
		return init(remote + " (patrjprof " + remotePjpVersion + ", jvm " + remoteJvmVersion + ')');
	}
	
	public PJPGui init(String remote) {
		if ( frame != null ) {
			throw new IllegalStateException("already initilized");
		}
		this.remote = remote;
		frame = new JFrame("PatrJProf Client: " + remote);
		tree = new JTree(model);
		sp = new JScrollPane(tree);
		sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		Font f = new Font("sans-serif", Font.PLAIN, 20);
		tree.setFont(f);
		tree.putClientProperty("JTree.lineStyle", "Angled");// for java look and feel this is the default
		tree.setCellRenderer(new PJPTreeCellRenderer());
		FontMetrics m = tree.getFontMetrics(f);
		final int h = m.getHeight();
		frame.setContentPane(sp);
		frame.setLocationByPlatform(true);
		frame.pack();
		
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		sp.setWheelScrollingEnabled(false);
		sp.addMouseWheelListener(e -> {// scroll line wise
			int value = (int) ( h * e.getPreciseWheelRotation() );
			JScrollBar sb;
			if ( ( e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK ) != 0 ) {
				sb = sp.getHorizontalScrollBar();
			} else {
				sb = sp.getVerticalScrollBar();
			}
			sb.setValue(sb.getValue() + value);
		});
		return this;
	}
	
}
