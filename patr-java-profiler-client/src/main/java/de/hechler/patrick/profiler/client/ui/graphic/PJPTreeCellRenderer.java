// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.client.ui.graphic;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.IOError;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

import de.hechler.patrick.profiler.client.ui.utils.ExecutingNode;
import de.hechler.patrick.profiler.client.ui.utils.Node;
import de.hechler.patrick.profiler.imp.PJPExecutingStack;
import de.hechler.patrick.profiler.imp.PJPSnapshot;
import de.hechler.patrick.profiler.imp.PJPStackFrame;
import de.hechler.patrick.profiler.imp.PJPThread;


public class PJPTreeCellRenderer extends DefaultTreeCellRenderer implements TreeCellRenderer {
	
	private static final long serialVersionUID = 1L;
	
	private static final Icon SNAPSHOT;
	private static final Icon THREAD;
	private static final Icon STACK_EXECUTING;
	private static final Icon FRAME_EXECUTING;
	private static final Icon FRAME_FINISHED;
	private static final Icon LEAF_FINISHED;
	private static final Icon LEAF_EXECUTING;
	
	static {
		try {
			SNAPSHOT = icon("/de/hechler/patrick/profiler/client/img/snapshot.png");
			THREAD = icon("/de/hechler/patrick/profiler/client/img/thread.png");
			STACK_EXECUTING = icon("/de/hechler/patrick/profiler/client/img/stack-exe.png");
			FRAME_EXECUTING = icon("/de/hechler/patrick/profiler/client/img/frame-exe.png");
			FRAME_FINISHED = icon("/de/hechler/patrick/profiler/client/img/frame.png");
			LEAF_EXECUTING = icon("/de/hechler/patrick/profiler/client/img/leaf-exe.png");
			LEAF_FINISHED = icon("/de/hechler/patrick/profiler/client/img/leaf.png");
		} catch ( IOException e ) {
			throw new IOError(e);
		}
	}
	
	private static ImageIcon icon(String res) throws IOException {
		URL url = PJPTreeCellRenderer.class.getResource(res);
		BufferedImage img = ImageIO.read(url);
		ImageIcon ii = new ImageIcon(img);
		return ii;
	}
	
	private Object value;
	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row,
		boolean hasFocus) {
		this.value = ( (Node) value ).value();
		return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
	}
	
	@Override
	public Icon getLeafIcon() {
		Object v = value;
		if ( v instanceof PJPStackFrame ) return LEAF_FINISHED;
		if ( v instanceof ExecutingNode ) return LEAF_EXECUTING;
		return getClosedIcon(); //XXX this should not be needed!
	}
	
	@Override
	public Icon getOpenIcon() {
		Object v = value;
		if ( v instanceof PJPStackFrame ) return FRAME_FINISHED;
		if ( v instanceof ExecutingNode ) return FRAME_EXECUTING;
		if ( v instanceof PJPExecutingStack ) return STACK_EXECUTING;
		if ( v instanceof PJPThread ) return THREAD;
		if ( v instanceof PJPSnapshot ) return SNAPSHOT;
		throw new AssertionError(v == null ? "null" : v.getClass());
	}
	
	@Override
	public Icon getClosedIcon() {
		return getOpenIcon();
	}
	
}
