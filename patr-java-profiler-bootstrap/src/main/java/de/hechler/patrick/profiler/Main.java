// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler;

import java.io.IOException;
import java.io.InputStream;

/**
 * this class can be used by the command line user to get some help
 */
public class Main {
	
	private Main() {}
	
	/**
	 * only to tell users what they should do
	 * 
	 * @param args what the user wants to know
	 */
	public static void main(String[] args) {
		if ( args.length == 0 ) {
			args = new String[]{ "help" };
		}
		for (int i = 0; i < args.length; i++) {
			if ( "help".equalsIgnoreCase(args[i]) ) {
				bootstrapHelp(true);
			} else if ( "bootstrap-help".equalsIgnoreCase(args[i]) ) {
				bootstrapHelp(false);
			} else if ( "license".equalsIgnoreCase(args[i]) ) {
				echoResource("/de/hechler/patrick/profiler/bootstrap/LICENSE");
			} else if ( "version".equalsIgnoreCase(args[i]) ) {
				System.out.print("Patr-Java-Profiler Bootstrap: ");
				echoResource("/de/hechler/patrick/profiler/bootstrap/VERSION");
				System.out.println();
			} else {
				System.out.println("Patr Java Profiler Bootstrap: unknown option: '" + args[i] + '\'');
				bootstrapHelp(true);
				System.exit(1);
			}
		}
	}
	
	private static void echoResource(String res) {
		try ( InputStream in = Main.class.getResourceAsStream(res) ) {
			byte[] buf = new byte[1024];
			while ( true ) {
				int r = in.read(buf, 0, 1024);
				if ( r == -1 ) break;
				System.out.write(buf, 0, r);
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	
	private static void bootstrapHelp(boolean completeHelp) {
		if ( completeHelp ) {
			System.out.println("Patr Java Profiler Help:");
			System.out.println("Patr Java Profiler Agent Help:");
			System.out.println("  pass the jar as a java agent (-javaagent:<JAR_FILE>[=<OPTIONS>])");
			System.out.println("  for more information use");
			System.out.println("  java -javaagent:<JAR_FILE>=help");
		}
		if ( true ) {
			System.out.println("Patr Java Profiler Bootstrap Help:");
			System.out.println("  pass the jar to the bootstrap class loader (-Xbootclasspath/a:<JAR_FILE>)");
			System.out.println("  arguments:");
			System.out.println("    bootstrap-help");
		}
		if ( completeHelp ) {
			System.out.println("      print only the bootstraps help message to sdtout");
		} else {
			System.out.println("      print this message to sdtout");
		}
		if ( true ) {
			System.out.println("    help");
		}
		if ( completeHelp ) {
			System.out.println("      print this message to sdtout");
		} else {
			System.out.println("      print the complete help message to sdtout");
		}
		if ( true ) {
			System.out.println("      when there are no arguments this is also done");
			System.out.println("    license");
			System.out.println("      print the license to sdtout");
		}
	}
	
}
