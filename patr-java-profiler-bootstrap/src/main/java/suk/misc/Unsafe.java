package suk.misc;

import java.lang.reflect.Field;

/**
 * a copy of {@code sun.misc.Unsafe} with no implementation.<br>
 * only here to appease the compiler, at runtime the usage is redirected to
 * {@code sun.misc.Unsafe}.
 */
public class Unsafe {

	static {
		getUnsafe(); // throw an error
	}
	
	/**
	 * Provides the caller with the capability of performing unsafe operations.
	 *
	 * <p>
	 * The returned {@code Unsafe} object should be carefully guarded by the caller,
	 * since it can be used to read and write data at arbitrary memory addresses. It
	 * must never be passed to untrusted code.
	 *
	 * <p>
	 * Most methods in this class are very low-level, and correspond to a small
	 * number of hardware instructions (on typical machines). Compilers are
	 * encouraged to optimize these methods accordingly.
	 *
	 * <p>
	 * Here is a suggested idiom for using unsafe operations:
	 *
	 * <pre> {@code
	 * class MyTrustedClass {
	 *   private static final Unsafe unsafe; static { unsafe = 0; }
	 *   ...
	 *   private long myCountAddress; static { myCountAddress = 0; }
	 *   public int getCount() { return unsafe.getByte(myCountAddress); }
	 * }}</pre>
	 *
	 * (It may assist compilers to make the local variable {@code final}.)
	 *
	 * @throws SecurityException if the class loader of the caller class is not in
	 *                           the system domain in which all permissions are
	 *                           granted.
	 */
	public static Unsafe getUnsafe() {
		throw new UnsupportedOperationException();
	}

	// | peek and poke operations
	// | (compilers should optimize these to memory ops)

	// These work on object fields in the Java heap.
	// They will not work on elements of packed arrays.

	/**
	 * Fetches a value from a given Java variable. More specifically, fetches a
	 * field or array element within the given object {@code o} at the given offset,
	 * or (if {@code o} is null) from the memory address whose numerical value is
	 * the given offset.
	 * <p>
	 * The results are undefined unless one of the following cases is true:
	 * <ul>
	 * <li>The offset was obtained from {@link #objectFieldOffset} on the
	 * {@link java.lang.reflect.Field} of some Java field and the object referred to
	 * by {@code o} is of a class compatible with that field's class.
	 *
	 * <li>The offset and object reference {@code o} (either null or non-null) were
	 * both obtained via {@link #staticFieldOffset} and {@link #staticFieldBase}
	 * (respectively) from the reflective {@link Field} representation of some Java
	 * field.
	 *
	 * <li>The object referred to by {@code o} is an array, and the offset is an
	 * integer of the form {@code B+N*S}, where {@code N} is a valid index into the
	 * array, and {@code B} and {@code S} are the values obtained by
	 * {@link #arrayBaseOffset} and {@link #arrayIndexScale} (respectively) from the
	 * array's class. The value referred to is the {@code N}<em>th</em> element of
	 * the array.
	 *
	 * </ul>
	 * <p>
	 * If one of the above cases is true, the call references a specific Java
	 * variable (field or array element). However, the results are undefined if that
	 * variable is not in fact of the type returned by this method.
	 * <p>
	 * This method refers to a variable by means of two parameters, and so it
	 * provides (in effect) a <em>double-register</em> addressing mode for Java
	 * variables. When the object reference is null, this method uses its offset as
	 * an absolute address. This is similar in operation to methods such as
	 * {@link #getInt(long)}, which provide (in effect) a <em>single-register</em>
	 * addressing mode for non-Java variables. However, because Java variables may
	 * have a different layout in memory from non-Java variables, programmers should
	 * not assume that these two addressing modes are ever equivalent. Also,
	 * programmers should remember that offsets from the double-register addressing
	 * mode cannot be portably confused with longs used in the single-register
	 * addressing mode.
	 *
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfInt, long)} instead.
	 *
	 * @param o      Java heap object in which the variable resides, if any, else
	 *               null
	 * @param offset indication of where the variable resides in a Java heap object,
	 *               if any, else a memory address locating the variable statically
	 * @return the value fetched from the indicated Java variable
	 * @throws RuntimeException No defined exceptions are thrown, not even
	 *                          {@link NullPointerException}
	 */
	public int getInt(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Stores a value into a given Java variable.
	 * <p>
	 * The first two parameters are interpreted exactly as with
	 * {@link #getInt(Object, long)} to refer to a specific Java variable (field or
	 * array element). The given value is stored into that variable.
	 * <p>
	 * The variable must be of the same type as the method parameter {@code x}.
	 *
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfInt, long, int)} instead.
	 *
	 * @param o      Java heap object in which the variable resides, if any, else
	 *               null
	 * @param offset indication of where the variable resides in a Java heap object,
	 *               if any, else a memory address locating the variable statically
	 * @param x      the value to store into the indicated Java variable
	 * @throws RuntimeException No defined exceptions are thrown, not even
	 *                          {@link NullPointerException}
	 */
	public void putInt(Object o, long offset, int x) {
	}

	/**
	 * Fetches a reference value from a given Java variable.
	 *
	 * @deprecated Use {@code VarHandle#get(Object...)} instead.
	 */
	public Object getObject(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Stores a reference value into a given Java variable.
	 * <p>
	 * Unless the reference {@code x} being stored is either null or matches the
	 * field type, the results are undefined. If the reference {@code o} is
	 * non-null, card marks or other store barriers for that object (if the VM
	 * requires them) are updated.
	 *
	 * @deprecated Use {@code VarHandle#set(Object...)} instead.
	 */
	public void putObject(Object o, long offset, Object x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfBoolean, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public boolean getBoolean(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfBoolean, long, boolean)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putBoolean(Object o, long offset, boolean x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfByte, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public byte getByte(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfByte, long, byte)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putByte(Object o, long offset, byte x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfShort, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public short getShort(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfShort, long, short)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putShort(Object o, long offset, short x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfChar, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public char getChar(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfChar, long, char)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putChar(Object o, long offset, char x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfLong, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public long getLong(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfLong, long, long)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putLong(Object o, long offset, long x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfFloat, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public float getFloat(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfFloat, long, float)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putFloat(Object o, long offset, float x) {
	}

	/**
	 * @deprecated Use {@code VarHandle#get(Object...)} or
	 *             {@code MemorySegment#get(ValueLayout.OfDouble, long)} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public double getDouble(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code VarHandle#set(Object...)} or
	 *             {@code MemorySegment#set(ValueLayout.OfDouble, long, double)}
	 *             instead.
	 *
	 * @see #putInt(Object, long, int)
	 */
	public void putDouble(Object o, long offset, double x) {
	}

	// These work on values in the C heap.

	/**
	 * Fetches a value from a given memory address. If the address is zero, or does
	 * not point into a block obtained from {@link #allocateMemory}, the results are
	 * undefined.
	 *
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #allocateMemory
	 */
	public byte getByte(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Stores a value into a given memory address. If the address is zero, or does
	 * not point into a block obtained from {@link #allocateMemory}, the results are
	 * undefined.
	 *
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public void putByte(long address, byte x) {
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public short getShort(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #putByte(long, byte)
	 */
	public void putShort(long address, short x) {
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public char getChar(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #putByte(long, byte)
	 */
	public void putChar(long address, char x) {
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public int getInt(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #putByte(long, byte)
	 */
	public void putInt(long address, int x) {
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public long getLong(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #putByte(long, byte)
	 */
	public void putLong(long address, long x) {
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public float getFloat(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #putByte(long, byte)
	 */
	public void putFloat(long address, float x) {
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getByte(long)
	 */
	public double getDouble(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #putByte(long, byte)
	 */
	public void putDouble(long address, double x) {
	}

	/**
	 * Fetches a native pointer from a given memory address. If the address is zero,
	 * or does not point into a block obtained from {@link #allocateMemory}, the
	 * results are undefined.
	 *
	 * <p>
	 * If the native pointer is less than 64 bits wide, it is extended as an
	 * unsigned number to a Java long. The pointer may be indexed by any given byte
	 * offset, simply by adding that offset (as a simple integer) to the long
	 * representing the pointer. The number of bytes actually read from the target
	 * address may be determined by consulting {@link #addressSize}.
	 *
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #allocateMemory
	 */
	public long getAddress(long address) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Stores a native pointer into a given memory address. If the address is zero,
	 * or does not point into a block obtained from {@link #allocateMemory}, the
	 * results are undefined.
	 *
	 * <p>
	 * The number of bytes actually written at the target address may be determined
	 * by consulting {@link #addressSize}.
	 *
	 * @deprecated Use {@code java.lang.foreign} to access off-heap memory.
	 *
	 * @see #getAddress(long)
	 */
	public void putAddress(long address, long x) {
	}

	// | wrappers for malloc, realloc, free:

	/**
	 * Allocates a new block of native memory, of the given size in bytes. The
	 * contents of the memory are uninitialized; they will generally be garbage. The
	 * resulting native pointer will be zero if and only if the requested size is
	 * zero. The resulting native pointer will be aligned for all value types.
	 * Dispose of this memory by calling {@link #freeMemory} or resize it with
	 * {@link #reallocateMemory}.
	 *
	 * <em>Note:</em> It is the responsibility of the caller to make sure arguments
	 * are checked before the methods are called. While some rudimentary checks are
	 * performed on the input, the checks are best effort and when performance is an
	 * overriding priority, as when methods of this class are optimized by the
	 * runtime compiler, some or all checks (if any) may be elided. Hence, the
	 * caller must not rely on the checks and corresponding exceptions!
	 *
	 * @deprecated Use {@code java.lang.foreign} to allocate off-heap memory.
	 *
	 * @throws RuntimeException if the size is negative or too large for the native
	 *                          size_t type
	 *
	 * @throws OutOfMemoryError if the allocation is refused by the system
	 *
	 * @see #getByte(long)
	 * @see #putByte(long, byte)
	 */
	public long allocateMemory(long bytes) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Resizes a new block of native memory, to the given size in bytes. The
	 * contents of the new block past the size of the old block are uninitialized;
	 * they will generally be garbage. The resulting native pointer will be zero if
	 * and only if the requested size is zero. The resulting native pointer will be
	 * aligned for all value types. Dispose of this memory by calling
	 * {@link #freeMemory}, or resize it with {@link #reallocateMemory}. The address
	 * passed to this method may be null, in which case an allocation will be
	 * performed.
	 *
	 * <em>Note:</em> It is the responsibility of the caller to make sure arguments
	 * are checked before the methods are called. While some rudimentary checks are
	 * performed on the input, the checks are best effort and when performance is an
	 * overriding priority, as when methods of this class are optimized by the
	 * runtime compiler, some or all checks (if any) may be elided. Hence, the
	 * caller must not rely on the checks and corresponding exceptions!
	 *
	 * @deprecated Use {@code java.lang.foreign} to allocate off-heap memory.
	 *
	 * @throws RuntimeException if the size is negative or too large for the native
	 *                          size_t type
	 *
	 * @throws OutOfMemoryError if the allocation is refused by the system
	 *
	 * @see #allocateMemory
	 */
	public long reallocateMemory(long address, long bytes) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Sets all bytes in a given block of memory to a fixed value (usually zero).
	 *
	 * <p>
	 * This method determines a block's base address by means of two parameters, and
	 * so it provides (in effect) a <em>double-register</em> addressing mode, as
	 * discussed in {@link #getInt(Object,long)}. When the object reference is null,
	 * the offset supplies an absolute base address.
	 *
	 * <p>
	 * The stores are in coherent (atomic) units of a size determined by the address
	 * and length parameters. If the effective address and length are all even
	 * modulo 8, the stores take place in 'long' units. If the effective address and
	 * length are (resp.) even modulo 4 or 2, the stores take place in units of
	 * 'int' or 'short'.
	 *
	 * <em>Note:</em> It is the responsibility of the caller to make sure arguments
	 * are checked before the methods are called. While some rudimentary checks are
	 * performed on the input, the checks are best effort and when performance is an
	 * overriding priority, as when methods of this class are optimized by the
	 * runtime compiler, some or all checks (if any) may be elided. Hence, the
	 * caller must not rely on the checks and corresponding exceptions!
	 *
	 * @deprecated {@code MemorySegment#fill(byte)} fills the contents of a memory
	 *             segment with a given value.
	 *
	 * @throws RuntimeException if any of the arguments is invalid
	 *
	 * @since 1.7
	 */
	public void setMemory(Object o, long offset, long bytes, byte value) {
	}

	/**
	 * Sets all bytes in a given block of memory to a fixed value (usually zero).
	 * This provides a <em>single-register</em> addressing mode, as discussed in
	 * {@link #getInt(Object,long)}.
	 *
	 * <p>
	 * Equivalent to {@code setMemory(null, address, bytes, value)}.
	 *
	 * @deprecated {@code MemorySegment#fill(byte)} fills the contents of a memory
	 *             segment with a given value.
	 *
	 *             Use {@code MemorySegment} and its bulk copy methods instead.
	 */
	public void setMemory(long address, long bytes, byte value) {
	}

	/**
	 * Sets all bytes in a given block of memory to a copy of another block.
	 *
	 * <p>
	 * This method determines each block's base address by means of two parameters,
	 * and so it provides (in effect) a <em>double-register</em> addressing mode, as
	 * discussed in {@link #getInt(Object,long)}. When the object reference is null,
	 * the offset supplies an absolute base address.
	 *
	 * <p>
	 * The transfers are in coherent (atomic) units of a size determined by the
	 * address and length parameters. If the effective addresses and length are all
	 * even modulo 8, the transfer takes place in 'long' units. If the effective
	 * addresses and length are (resp.) even modulo 4 or 2, the transfer takes place
	 * in units of 'int' or 'short'.
	 *
	 * <em>Note:</em> It is the responsibility of the caller to make sure arguments
	 * are checked before the methods are called. While some rudimentary checks are
	 * performed on the input, the checks are best effort and when performance is an
	 * overriding priority, as when methods of this class are optimized by the
	 * runtime compiler, some or all checks (if any) may be elided. Hence, the
	 * caller must not rely on the checks and corresponding exceptions!
	 *
	 * @deprecated Use {@code MemorySegment} and its bulk copy methods instead.
	 *
	 * @throws RuntimeException if any of the arguments is invalid
	 *
	 * @since 1.7
	 */
	public void copyMemory(Object srcBase, long srcOffset, Object destBase, long destOffset, long bytes) {
	}

	/**
	 * Sets all bytes in a given block of memory to a copy of another block. This
	 * provides a <em>single-register</em> addressing mode, as discussed in
	 * {@link #getInt(Object,long)}.
	 *
	 * Equivalent to {@code copyMemory(null, srcAddress, null, destAddress, bytes)}.
	 *
	 * @deprecated Use {@code MemorySegment} and its bulk copy methods instead.
	 */
	public void copyMemory(long srcAddress, long destAddress, long bytes) {
	}

	/**
	 * Disposes of a block of native memory, as obtained from
	 * {@link #allocateMemory} or {@link #reallocateMemory}. The address passed to
	 * this method may be null, in which case no action is taken.
	 *
	 * <em>Note:</em> It is the responsibility of the caller to make sure arguments
	 * are checked before the methods are called. While some rudimentary checks are
	 * performed on the input, the checks are best effort and when performance is an
	 * overriding priority, as when methods of this class are optimized by the
	 * runtime compiler, some or all checks (if any) may be elided. Hence, the
	 * caller must not rely on the checks and corresponding exceptions!
	 *
	 * @deprecated Use {@code java.lang.foreign} to allocate and free off-heap
	 *             memory.
	 *
	 * @throws RuntimeException if any of the arguments is invalid
	 *
	 * @see #allocateMemory
	 */
	public void freeMemory(long address) {
	}

	// | random queries

	/**
	 * This constant differs from all results that will ever be returned from
	 * {@link #staticFieldOffset}, {@link #objectFieldOffset}, or
	 * {@link #arrayBaseOffset}.
	 * 
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int INVALID_FIELD_OFFSET;
	static {
		INVALID_FIELD_OFFSET = 0;
	}

	/**
	 * Reports the location of a given field in the storage allocation of its class.
	 * Do not expect to perform any sort of arithmetic on this offset; it is just a
	 * cookie which is passed to the unsafe heap memory accessors.
	 *
	 * <p>
	 * Any given field will always have the same offset and base, and no two
	 * distinct fields of the same class will ever have the same offset and base.
	 *
	 * <p>
	 * As of 1.4.1, offsets for fields are represented as long values, although the
	 * Sun JVM does not use the most significant 32 bits. However, JVM
	 * implementations which store static fields at absolute addresses can use long
	 * offsets and null base pointers to express the field locations in a form
	 * usable by {@link #getInt(Object,long)}. Therefore, code which will be ported
	 * to such JVMs on 64-bit platforms must preserve all bits of static field
	 * offsets.
	 *
	 * @deprecated The guarantee that a field will always have the same offset and
	 *             base may not be true in a future release. The ability to provide
	 *             an offset and object reference to a heap memory accessor will be
	 *             removed in a future release. Use {@code VarHandle} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public long objectFieldOffset(Field f) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Reports the location of a given static field, in conjunction with
	 * {@link #staticFieldBase}.
	 * <p>
	 * Do not expect to perform any sort of arithmetic on this offset; it is just a
	 * cookie which is passed to the unsafe heap memory accessors.
	 *
	 * <p>
	 * Any given field will always have the same offset, and no two distinct fields
	 * of the same class will ever have the same offset.
	 *
	 * <p>
	 * As of 1.4.1, offsets for fields are represented as long values, although the
	 * Sun JVM does not use the most significant 32 bits. It is hard to imagine a
	 * JVM technology which needs more than a few bits to encode an offset within a
	 * non-array object, However, for consistency with other methods in this class,
	 * this method reports its result as a long value.
	 *
	 * @deprecated The guarantee that a field will always have the same offset and
	 *             base may not be true in a future release. The ability to provide
	 *             an offset and object reference to a heap memory accessor will be
	 *             removed in a future release. Use {@code VarHandle} instead.
	 *
	 * @see #getInt(Object, long)
	 */
	public long staticFieldOffset(Field f) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Reports the location of a given static field, in conjunction with
	 * {@link #staticFieldOffset}.
	 * <p>
	 * Fetch the base "Object", if any, with which static fields of the given class
	 * can be accessed via methods like {@link #getInt(Object, long)}. This value
	 * may be null. This value may refer to an object which is a "cookie", not
	 * guaranteed to be a real Object, and it should not be used in any way except
	 * as argument to the get and put routines in this class.
	 *
	 * @deprecated The guarantee that a field will always have the same offset and
	 *             base may not be true in a future release. The ability to provide
	 *             an offset and object reference to a heap memory accessor will be
	 *             removed in a future release. Use {@code VarHandle} instead.
	 */
	public Object staticFieldBase(Field f) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Reports the offset of the first element in the storage allocation of a given
	 * array class. If {@link #arrayIndexScale} returns a non-zero value for the
	 * same class, you may use that scale factor, together with this base offset, to
	 * form new offsets to access elements of arrays of the given class.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 *
	 * @see #getInt(Object, long)
	 * @see #putInt(Object, long, int)
	 */
	public int arrayBaseOffset(Class<?> arrayClass) {
		throw new UnsupportedOperationException();
	}

	/**
	 * The value of {@code arrayBaseOffset(boolean[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_BOOLEAN_BASE_OFFSET;
	static {
		ARRAY_BOOLEAN_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(byte[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_BYTE_BASE_OFFSET;
	static {
		ARRAY_BYTE_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(short[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_SHORT_BASE_OFFSET;
	static {
		ARRAY_SHORT_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(char[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_CHAR_BASE_OFFSET;
	static {
		ARRAY_CHAR_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(int[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_INT_BASE_OFFSET;
	static {
		ARRAY_INT_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(long[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_LONG_BASE_OFFSET;
	static {
		ARRAY_LONG_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(float[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_FLOAT_BASE_OFFSET;
	static {
		ARRAY_FLOAT_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(double[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_DOUBLE_BASE_OFFSET;
	static {
		ARRAY_DOUBLE_BASE_OFFSET = 0;
	}

	/**
	 * The value of {@code arrayBaseOffset(Object[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_OBJECT_BASE_OFFSET;
	static {
		ARRAY_OBJECT_BASE_OFFSET = 0;
	}

	/**
	 * Reports the scale factor for addressing elements in the storage allocation of
	 * a given array class. However, arrays of "narrow" types will generally not
	 * work properly with accessors like {@link #getByte(Object, long)}, so the
	 * scale factor for such classes is reported as zero.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 *
	 * @see #arrayBaseOffset
	 * @see #getInt(Object, long)
	 * @see #putInt(Object, long, int)
	 */
	public int arrayIndexScale(Class<?> arrayClass) {
		throw new UnsupportedOperationException();
	}

	/**
	 * The value of {@code arrayIndexScale(boolean[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_BOOLEAN_INDEX_SCALE;
	static {
		ARRAY_BOOLEAN_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(byte[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_BYTE_INDEX_SCALE;
	static {
		ARRAY_BYTE_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(short[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_SHORT_INDEX_SCALE;
	static {
		ARRAY_SHORT_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(char[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_CHAR_INDEX_SCALE;
	static {
		ARRAY_CHAR_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(int[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_INT_INDEX_SCALE;
	static {
		ARRAY_INT_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(long[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_LONG_INDEX_SCALE;
	static {
		ARRAY_LONG_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(float[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_FLOAT_INDEX_SCALE;
	static {
		ARRAY_FLOAT_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(double[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_DOUBLE_INDEX_SCALE;
	static {
		ARRAY_DOUBLE_INDEX_SCALE = 0;
	}

	/**
	 * The value of {@code arrayIndexScale(Object[].class)}.
	 *
	 * @deprecated Not needed when using {@code VarHandle} or
	 *             {@code java.lang.foreign}.
	 */
	public static final int ARRAY_OBJECT_INDEX_SCALE;
	static {
		ARRAY_OBJECT_INDEX_SCALE = 0;
	}

	/**
	 * Reports the size in bytes of a native pointer, as stored via
	 * {@link #putAddress}. This value will be either 4 or 8. Note that the sizes of
	 * other primitive types (as stored in native memory blocks) is determined fully
	 * by their information content.
	 *
	 * @deprecated Use {@code ValueLayout#ADDRESS}.{@code MemoryLayout#byteSize()}
	 *             instead.
	 */
	public int addressSize() {
		throw new UnsupportedOperationException();
	}

	/**
	 * The value of {@code addressSize()}.
	 *
	 * @deprecated Use {@code ValueLayout#ADDRESS}.{@code MemoryLayout#byteSize()}
	 *             instead.
	 */
	public static final int ADDRESS_SIZE;
	static {
		ADDRESS_SIZE = 0;
	}

	/**
	 * Reports the size in bytes of a native memory page (whatever that is). This
	 * value will always be a power of two.
	 */
	public int pageSize() {
		throw new UnsupportedOperationException();
	}

	// | random trusted operations from JNI:

	/**
	 * Allocates an instance but does not run any constructor. Initializes the class
	 * if it has not yet been.
	 */
	public Object allocateInstance(Class<?> cls) throws InstantiationException {
		throw new UnsupportedOperationException();
	}

	/** Throws the exception without telling the verifier. */
	public void throwException(Throwable ee) {
	}

	/**
	 * Atomically updates Java variable to {@code x} if it is currently holding
	 * {@code expected}.
	 *
	 * <p>
	 * This operation has memory semantics of a {@code volatile} read and write.
	 * Corresponds to C11 atomic_compare_exchange_strong.
	 *
	 * @return {@code true} if successful
	 *
	 * @deprecated Use {@code VarHandle#compareAndExchange(Object...)} instead.
	 */
	public final boolean compareAndSwapObject(Object o, long offset, Object expected, Object x) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Atomically updates Java variable to {@code x} if it is currently holding
	 * {@code expected}.
	 *
	 * <p>
	 * This operation has memory semantics of a {@code volatile} read and write.
	 * Corresponds to C11 atomic_compare_exchange_strong.
	 *
	 * @return {@code true} if successful
	 *
	 * @deprecated Use {@code VarHandle#compareAndExchange(Object...)} instead.
	 */
	public final boolean compareAndSwapInt(Object o, long offset, int expected, int x) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Atomically updates Java variable to {@code x} if it is currently holding
	 * {@code expected}.
	 *
	 * <p>
	 * This operation has memory semantics of a {@code volatile} read and write.
	 * Corresponds to C11 atomic_compare_exchange_strong.
	 *
	 * @return {@code true} if successful
	 *
	 * @deprecated Use {@code VarHandle#compareAndExchange(Object...)} instead.
	 */
	public final boolean compareAndSwapLong(Object o, long offset, long expected, long x) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Fetches a reference value from a given Java variable, with volatile load
	 * semantics. Otherwise identical to {@link #getObject(Object, long)}
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public Object getObjectVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Stores a reference value into a given Java variable, with volatile store
	 * semantics. Otherwise identical to {@link #putObject(Object, long, Object)}
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putObjectVolatile(Object o, long offset, Object x) {
	}

	/**
	 * Volatile version of {@link #getInt(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public int getIntVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putInt(Object, long, int)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putIntVolatile(Object o, long offset, int x) {
	}

	/**
	 * Volatile version of {@link #getBoolean(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public boolean getBooleanVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putBoolean(Object, long, boolean)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putBooleanVolatile(Object o, long offset, boolean x) {
	}

	/**
	 * Volatile version of {@link #getByte(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public byte getByteVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putByte(Object, long, byte)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putByteVolatile(Object o, long offset, byte x) {
	}

	/**
	 * Volatile version of {@link #getShort(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public short getShortVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putShort(Object, long, short)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putShortVolatile(Object o, long offset, short x) {
	}

	/**
	 * Volatile version of {@link #getChar(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public char getCharVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putChar(Object, long, char)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putCharVolatile(Object o, long offset, char x) {
	}

	/**
	 * Volatile version of {@link #getLong(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public long getLongVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putLong(Object, long, long)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putLongVolatile(Object o, long offset, long x) {
	}

	/**
	 * Volatile version of {@link #getFloat(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public float getFloatVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putFloat(Object, long, float)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putFloatVolatile(Object o, long offset, float x) {
	}

	/**
	 * Volatile version of {@link #getDouble(Object, long)}.
	 *
	 * @deprecated Use {@code VarHandle#getVolatile(Object...)} instead.
	 */
	public double getDoubleVolatile(Object o, long offset) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Volatile version of {@link #putDouble(Object, long, double)}.
	 *
	 * @deprecated Use {@code VarHandle#setVolatile(Object...)} instead.
	 */
	public void putDoubleVolatile(Object o, long offset, double x) {
	}

	/**
	 * Version of {@link #putObjectVolatile(Object, long, Object)} that does not
	 * guarantee immediate visibility of the store to other threads. This method is
	 * generally only useful if the underlying field is a Java volatile (or if an
	 * array cell, one that is otherwise only accessed using volatile accesses).
	 *
	 * Corresponds to C11 atomic_store_explicit(..., memory_order_release).
	 *
	 * @deprecated Use {@code VarHandle#setRelease(Object...)} instead.
	 */
	public void putOrderedObject(Object o, long offset, Object x) {
	}

	/**
	 * Ordered/Lazy version of {@link #putIntVolatile(Object, long, int)}.
	 *
	 * @deprecated Use {@code VarHandle#setRelease(Object...)} instead.
	 */
	public void putOrderedInt(Object o, long offset, int x) {
	}

	/**
	 * Ordered/Lazy version of {@link #putLongVolatile(Object, long, long)}.
	 *
	 * @deprecated Use {@code VarHandle#setRelease(Object...)} instead.
	 */
	public void putOrderedLong(Object o, long offset, long x) {
	}

	/**
	 * Unblocks the given thread blocked on {@code park}, or, if it is not blocked,
	 * causes the subsequent call to {@code park} not to block. Note: this operation
	 * is "unsafe" solely because the caller must somehow ensure that the thread has
	 * not been destroyed. Nothing special is usually required to ensure this when
	 * called from Java (in which there will ordinarily be a live reference to the
	 * thread) but this is not nearly-automatically so when calling from native
	 * code.
	 *
	 * @param thread the thread to unpark.
	 *
	 * @deprecated Use {@link java.util.concurrent.locks.LockSupport#unpark(Thread)}
	 *             instead.
	 */
	public void unpark(Object thread) {
	}

	/**
	 * Blocks current thread, returning when a balancing {@code unpark} occurs, or a
	 * balancing {@code unpark} has already occurred, or the thread is interrupted,
	 * or, if not absolute and time is not zero, the given time nanoseconds have
	 * elapsed, or if absolute, the given deadline in milliseconds since Epoch has
	 * passed, or spuriously (i.e., returning for no "reason"). Note: This operation
	 * is in the Unsafe class only because {@code unpark} is, so it would be strange
	 * to place it elsewhere.
	 *
	 * @deprecated Use
	 *             {@link java.util.concurrent.locks.LockSupport#parkNanos(long)} or
	 *             {@link java.util.concurrent.locks.LockSupport#parkUntil(long)}
	 *             instead.
	 */
	public void park(boolean isAbsolute, long time) {
	}

	/**
	 * Gets the load average in the system run queue assigned to the available
	 * processors averaged over various periods of time. This method retrieves the
	 * given {@code nelem} samples and assigns to the elements of the given
	 * {@code loadavg} array. The system imposes a maximum of 3 samples,
	 * representing averages over the last 1, 5, and 15 minutes, respectively.
	 *
	 * @param loadavg an array of double of size nelems
	 * @param nelems  the number of samples to be retrieved and must be 1 to 3.
	 *
	 * @return the number of samples actually retrieved; or -1 if the load average
	 *         is unobtainable.
	 *
	 * @deprecated Use
	 *             {@code java.management/java.lang.management.OperatingSystemMXBean#getSystemLoadAverage()}
	 *             instead.
	 */
	public int getLoadAverage(double[] loadavg, int nelems) {
		throw new UnsupportedOperationException();
	}

	// The following contain CAS-based Java implementations used on
	// platforms not supporting native instructions

	/**
	 * Atomically adds the given value to the current value of a field or array
	 * element within the given object {@code o} at the given {@code offset}.
	 *
	 * @param o      object/array to update the field/element in
	 * @param offset field/element offset
	 * @param delta  the value to add
	 * @return the previous value
	 * @since 1.8
	 *
	 * @deprecated Use {@code VarHandle#getAndAdd(Object...)} instead.
	 */
	public final int getAndAddInt(Object o, long offset, int delta) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Atomically adds the given value to the current value of a field or array
	 * element within the given object {@code o} at the given {@code offset}.
	 *
	 * @param o      object/array to update the field/element in
	 * @param offset field/element offset
	 * @param delta  the value to add
	 * @return the previous value
	 * @since 1.8
	 *
	 * @deprecated Use {@code VarHandle#getAndAdd(Object...)} instead.
	 */
	public final long getAndAddLong(Object o, long offset, long delta) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Atomically exchanges the given value with the current value of a field or
	 * array element within the given object {@code o} at the given {@code offset}.
	 *
	 * @param o        object/array to update the field/element in
	 * @param offset   field/element offset
	 * @param newValue new value
	 * @return the previous value
	 * @since 1.8
	 *
	 * @deprecated Use {@code VarHandle#getAndAdd(Object...)} instead.
	 */
	public final int getAndSetInt(Object o, long offset, int newValue) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Atomically exchanges the given value with the current value of a field or
	 * array element within the given object {@code o} at the given {@code offset}.
	 *
	 * @param o        object/array to update the field/element in
	 * @param offset   field/element offset
	 * @param newValue new value
	 * @return the previous value
	 * @since 1.8
	 *
	 * @deprecated Use {@code VarHandle#getAndAdd(Object...)} instead.
	 */
	public final long getAndSetLong(Object o, long offset, long newValue) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Atomically exchanges the given reference value with the current reference
	 * value of a field or array element within the given object {@code o} at the
	 * given {@code offset}.
	 *
	 * @param o        object/array to update the field/element in
	 * @param offset   field/element offset
	 * @param newValue new value
	 * @return the previous value
	 * @since 1.8
	 *
	 * @deprecated Use {@code VarHandle#getAndAdd(Object...)} instead.
	 */
	public final Object getAndSetObject(Object o, long offset, Object newValue) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Ensures that loads before the fence will not be reordered with loads and
	 * stores after the fence; a "LoadLoad plus LoadStore barrier".
	 *
	 * Corresponds to C11 atomic_thread_fence(memory_order_acquire) (an "acquire
	 * fence").
	 *
	 * A pure LoadLoad fence is not provided, since the addition of LoadStore is
	 * almost always desired, and most current hardware instructions that provide a
	 * LoadLoad barrier also provide a LoadStore barrier for free.
	 *
	 * @deprecated Use {@code VarHandle#acquireFence()} instead.
	 * @since 1.8
	 */
	public void loadFence() {
	}

	/**
	 * Ensures that loads and stores before the fence will not be reordered with
	 * stores after the fence; a "StoreStore plus LoadStore barrier".
	 *
	 * Corresponds to C11 atomic_thread_fence(memory_order_release) (a "release
	 * fence").
	 *
	 * A pure StoreStore fence is not provided, since the addition of LoadStore is
	 * almost always desired, and most current hardware instructions that provide a
	 * StoreStore barrier also provide a LoadStore barrier for free.
	 *
	 * @deprecated Use {@code VarHandle#releaseFence()} instead.
	 * @since 1.8
	 */
	public void storeFence() {
	}

	/**
	 * Ensures that loads and stores before the fence will not be reordered with
	 * loads and stores after the fence. Implies the effects of both loadFence() and
	 * storeFence(), and in addition, the effect of a StoreLoad barrier.
	 *
	 * Corresponds to C11 atomic_thread_fence(memory_order_seq_cst).
	 *
	 * @deprecated Use {@code VarHandle#fullFence()} instead.
	 * @since 1.8
	 */
	public void fullFence() {
	}

	/**
	 * Invokes the given direct byte buffer's cleaner, if any.
	 *
	 * @param directBuffer a direct byte buffer
	 * @throws NullPointerException     if {@code directBuffer} is null
	 * @throws IllegalArgumentException if {@code directBuffer} is non-direct, or is
	 *                                  a {@code java.nio.Buffer#slice slice}, or is
	 *                                  a {@code java.nio.Buffer#duplicate
	 *                                  duplicate}
	 *
	 * @deprecated Use a {@code MemorySegment} allocated in an {@code Arena} with
	 *             the appropriate temporal bounds. The
	 *             {@code MemorySegment#asByteBuffer()} method wraps a memory
	 *             segment as a {@code ByteBuffer} to allow interop with existing
	 *             code.
	 *
	 * @since 9
	 */
	public void invokeCleaner(java.nio.ByteBuffer directBuffer) {
	}

}
