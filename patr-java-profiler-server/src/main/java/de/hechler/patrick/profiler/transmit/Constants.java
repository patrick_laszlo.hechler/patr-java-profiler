// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.transmit;

import java.io.IOException;
import java.io.InputStream;

/**
 * If you see this class in a project other than the <i>Patr-Java-Profiler: Server</i> it is a copy of the original file
 * and any changes made may be overwritten in build time
 * <p>
 * this class declares constants used for client/server communication and some helper methods
 * <p>
 * all constants here have a suffix of the form {@code _<N>}<br>
 * {@code N} is the length in bytes of the constant<br>
 * numbers with more than one byte are stored in little-endian byte order<br>
 * <p>
 * If a {@link String} is send it is always encoded in {@code UTF-8} (if not otherwise specified)<br>
 * all constants with a suffix of the form {@code _STR} will transport a string (usually in the form of a stream)
 * <p>
 * sometimes a {@code stream} is send:
 * <ol>
 * <li>{@code int32} : {@code length} (will never be negative)
 * <ul>
 * <li>{@code length == 0} : {@code EOF}</li>
 * <li>{@code length != 0} : stream continues
 * <ol>
 * <li>{@code int8[length]} : additional data of the stream</li>
 * </ol>
 * </li>
 * </ul>
 * </li>
 * </ol>
 */
public class Constants {
	
	private Constants() {}
	
	// server responses
	/**
	 * response of the server to {@link #HELLO_4}<br>
	 * any other response means (most likely) that the server is not a <i>Patr-Java-Profiler: Server</i> and the
	 * connection should be closed
	 */
	public static final byte HELLO_RESPONSE_1 = 0x62;
	
	// client commands
	// - connection stuff
	/**
	 * send by the client directly after connection to the server, the server responses with {@link #HELLO_RESPONSE_1}
	 * <p>
	 * after this is done the client can use all commands except for the {@link #HELLO_4} command.<br>
	 * before this is done the client can only use the {@link #HELLO_4} command.<br>
	 * If too much time passes until the server receives the {@link #HELLO_4} command it may decide to close the
	 * connection
	 */
	public static final int  HELLO_4 = 0x01B5349B;
	/**
	 * informs the server that the connection should be closed<br>
	 * there is no response, the server will just close the connection
	 */
	public static final byte BYE_1   = 0x14;
	/**
	 * informs the server that the program should start<br>
	 * the server respond with {@link #START_1} and start the program if it wasn't already started<br>
	 * if the program is already running this command has no effect (the server will still respond with
	 * {@link #START_1})<br>
	 * this can be used to avoid race conditions when the profiled program is fast
	 */
	public static final byte START_1 = 0x3F;
	// - static information
	/**
	 * sends the version as:
	 * <ol>
	 * <li>int32: major version</li>
	 * <li>int32: minor version</li>
	 * <li>int32: bug-fix version</li>
	 * <li>int8: type
	 * <ul>
	 * <li>0: release</li>
	 * <li>1: snapshot</li>
	 * <li>2: feature branch
	 * <ol>
	 * <li>stream: branch description/name</li>
	 * </ol>
	 * </li>
	 * </ul>
	 * </li>
	 * </ol>
	 */
	public static final byte VERSION_1         = (byte) 0xA1;
	/**
	 * sends the version as stream
	 */
	public static final byte VERSION_STR_1     = (byte) 0x7F;
	/**
	 * sends the JVM version as a single int32
	 */
	public static final byte JVM_VERSION_1     = (byte) 0x6B;
	/**
	 * sends the JVM version as stream
	 */
	public static final byte JVM_VERSION_STR_1 = (byte) 0x41;
	/**
	 * sends the process id and start time:
	 * <ol>
	 * <li>{@code int64} : {@code process id}</li>
	 * <li>{@code int64} : {@code epoch second}</li>
	 * <li>{@code int32} : {@code additional nano-seconds}</li>
	 * </ol>
	 */
	public static final byte PID_1             = (byte) 0xC2;
	// - information from the profiler
	/**
	 * sends a report of the current state from the profiler using its exporter<br>
	 * before the report is send the server will send {@link #SEND_REPORT_1}
	 */
	public static final byte REPORT_1 = (byte) 0x65;
	
	// notifications send from the server
	// - reports
	/**
	 * send when the server is about send a report<br>
	 * this is may be because the client requested it ({@link #REPORT_1} was send) or the server is about to
	 * terminate<br>
	 * after this the server will send a report as if the client did send a {@link #REPORT_1}<br>
	 * after the report the connection will be closed by the server
	 * <p>
	 * if the server did send a {@link #SEND_REPORT_1} without first receiving a {@link #REPORT_1} it has to either send followed
	 * by a {@link #QUIT_1} or disconnection
	 */
	public static final byte SEND_REPORT_1 = (byte) 0xBD;
	// - exit
	/**
	 * send when the server is about to terminate<br>
	 * after this the connection will be closed by the server
	 */
	public static final byte QUIT_1 = (byte) 0x12;
	// TODO add a constant to get the path of the finish report file
	
	/**
	 * extracts the version of the {@link System#getProperties() java.version} system property
	 * 
	 * @return the version of the JVM
	 */
	public static int jvmVersion() {
		String v = System.getProperty("java.version");
		int i = v.indexOf('.');
		if ( i == -1 ) return Integer.parseInt(v);
		if ( i == 0 ) return 0;
		if ( i == 1 && v.charAt(0) == '1' ) { // for example 1.8
			int e = v.indexOf('.', i + 1);
			if ( e == -1 ) e = v.length();
			if ( i + 1 != e ) {
				return Integer.parseInt(v.substring(i + 1, e));
			}
		}
		return Integer.parseInt(v.substring(0, i));
	}
	
	/**
	 * writes the given resource to {@link System#out std-out}
	 * @param res the resource to echo
	 */
	public static void echoResource(String res) {
		try ( InputStream in = Constants.class.getResourceAsStream(res) ) {
			byte[] buf = new byte[1024];
			while ( true ) {
				int r = in.read(buf, 0, 1024);
				if ( r == -1 ) break;
				System.out.write(buf, 0, r);
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	
}
