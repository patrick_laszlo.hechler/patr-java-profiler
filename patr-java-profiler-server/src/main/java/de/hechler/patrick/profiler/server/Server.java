// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.server;

import static de.hechler.patrick.profiler.transmit.Constants.BYE_1;
import static de.hechler.patrick.profiler.transmit.Constants.HELLO_4;
import static de.hechler.patrick.profiler.transmit.Constants.HELLO_RESPONSE_1;
import static de.hechler.patrick.profiler.transmit.Constants.JVM_VERSION_1;
import static de.hechler.patrick.profiler.transmit.Constants.JVM_VERSION_STR_1;
import static de.hechler.patrick.profiler.transmit.Constants.PID_1;
import static de.hechler.patrick.profiler.transmit.Constants.QUIT_1;
import static de.hechler.patrick.profiler.transmit.Constants.REPORT_1;
import static de.hechler.patrick.profiler.transmit.Constants.SEND_REPORT_1;
import static de.hechler.patrick.profiler.transmit.Constants.START_1;
import static de.hechler.patrick.profiler.transmit.Constants.VERSION_1;
import static de.hechler.patrick.profiler.transmit.Constants.VERSION_STR_1;
import static de.hechler.patrick.profiler.transmit.Constants.echoResource;
import static de.hechler.patrick.profiler.transmit.Constants.jvmVersion;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.StreamCorruptedException;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.ProtocolFamily;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.StandardProtocolFamily;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import de.hechler.patrick.profiler.PatrProfiler;
import de.hechler.patrick.profiler.agent.ServerInterface;

/**
 * this class is used by the <i>Patr-Java-Profiler: Agent</i> to allow other
 * programs to retrieve information of the profiled program while it still
 * executes
 */
@SuppressWarnings("cast")
public class Server implements Runnable, ServerInterface {

	private static final String RES_LICENSE = "/de/hechler/patrick/profiler/server/LICENSE";
	private static final String RES_VERSION = "/de/hechler/patrick/profiler/server/VERSION";

	@SuppressWarnings("unused")
	private Object transformer;
	@SuppressWarnings("unused")
	private Instrumentation inst;

	private Thread thread;

	private int port = -1;
	private String socket;
	private int socketOff;
	private int socketEnd;
	private int waitUntilConnect;
	private int multipleClients;
	private boolean printClientArgs;

	private Map<Thread, Closeable> openThreads;

	private ServerSocketChannel ssok;
	private long pid;
	private Instant startTime;

	/**
	 * called by the <i>Patr-Java-Profiler: Agent</i> to create a new Server 
	 */
	public Server() {
	}

	private String opt;

	@Override
	public void initArgs(String args) {
		opt = args;
	}

	@Override
	public void parseArg(int off, int end) {
		acceptArg(opt, off, end);
	}
	
	/**
	 * accept the given option
	 * <p>
	 * this method is invoked very early from the Agent(start of pre-main, before
	 * transformer is registered)
	 * 
	 * @param opt the option (and garbage)
	 * @param off the start offset of the option
	 * @param end the end of the option
	 */
	public void acceptArg(String opt, int off, int end) {
		if (isArg(off, end, opt, "help")) {
			System.out.println("Patr Java Profiler Agent Daemon Help:");
			System.out.println("  Usage:");
			String prefix = opt.substring(opt.lastIndexOf(off, ',') + 1, off);
			System.out.println(prefix + "<OPTION>");
			System.out.println("    use help to get a general help for the agent (not " + prefix + "help)");
			System.out.println("    note that this server only accepts one connection at a time by default");
			System.out.println("      use the multi-client option if needed");
			prefix = "    " + prefix;
			System.out.println("  Options:");
			System.out.println(prefix + "help");
			System.out.println("      print this message and halt");
			System.out.println(prefix + "version");
			System.out.println("      print version information and halt");
			System.out.println(prefix + "license");
			System.out.println("      print the license to stdout and halt");
			System.out.println(prefix + "port=<PORT>");
			System.out.println("      create a tcp server and listen on PORT");
			System.out.println(prefix + "socket=<SOCKET>");
			System.out.println("      create SOCKET as a socket file and listen on it");
			System.out.println("      only available if JVM version >= 16");
			System.out.println(prefix + "wait=[0|1|2]");
			System.out.println("      wait until a connection is established");
			System.out.println("      if the value is omitted 2 is used");
			System.out.println("      0: do not wait");
			System.out.println("      1: wait until the first connection is established");
			System.out.println("      2: wait until a connection sends the start command");
			System.out.println(prefix + "wait2=[0|1|2]");
			System.out.println("      like wait but if wait is already specified this is ignored");
			System.out.println("      note that a later wait will still fail");
			System.out.println(prefix + "multi-client=[0|1]");
			System.out.println("      allow multiple clients to connect to this server at the same time");
			System.out.println("      if the value is omitted 1 is used");
			System.out.println("      0: do not allow multiple clients");
			System.out.println("      1: allow multiple clients");
			System.out.println(prefix + "print-client-args");
			System.out.println("      print a line containing an argument which can be passed to the client");
			System.out.println("      which then will (if it runs on the same machine) connect to this server");
			halt(1);
		} else if (isArg(off, end, opt, "version")) {
			System.out.print("Patr-Java-Profiler Transmit: ");
			echoResource(RES_VERSION);
			System.out.println();
			halt(1);
		} else if (isArg(off, end, opt, "license")) {
			echoResource(RES_LICENSE);
			halt(1);
		} else if (isAssignableArg(off, end, opt, "port", '=')) {
			if (port != -1) {
				System.err.println("Patr Java Profiler Agent Daemon: illegal options: port specified multiple times! '"
						+ opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			if (socket != null) {
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal options: both socket and port specified, you have to decide! '"
								+ opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			off += "port=".length();
			if (end <= off) {
				port = 0;
			} else {
				int p = 0;
				for (int i = off; i < end; i++) {
					int d = opt.charAt(i) - '0';
					if (d < 0 || d >= 10) {
						System.err.println(
								"Patr Java Profiler Agent Daemon: illegal port: port must be a valid decimal number (without any sign)! '"
										+ opt.substring(off, end) + "', options '" + opt + '\'');
						System.err.println("use help to get a list of options");
						halt(2);
					}
					p = (p * 10) + d;
					if (p > 0xFFFF) {
						System.err.println("Patr Java Profiler Agent Daemon: illegal port: port must be below 2^16! '"
								+ opt.substring(off, end) + "', options '" + opt + '\'');
						System.err.println("use help to get a list of options");
						halt(2);
					}
				}
				port = p;
			}
		} else if (isAssignableArg(off, end, opt, "socket", '=')) {
			if (jvmVersion() < 16) {
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal option: socket requires a JVM with version >= 16, did you mean port? '"
								+ opt.substring(off, end) + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			if (port != -1) {
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal options: both socket and port specified, you have to decide! '"
								+ opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			if (socket != null) {
				System.err
						.println("Patr Java Profiler Agent Daemon: illegal options: socket specified multiple times! '"
								+ opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			off += "socket=".length();
			socket = opt;
			if (off < end) {
				socketOff = off;
				socketEnd = end;
			}
		} else if (isAssignableArg(off, end, opt, "wait", '=') || isAssignableArg(off, end, opt, "wait2", '=')) {
			if (waitUntilConnect != 0) {
				if (opt.startsWith("wait2", off))
					return;
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal options: wait=[0|1|2] specified multiple times! '"
								+ opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			off = opt.indexOf('=', off);
			if (off == -1 || off >= end) {
				waitUntilConnect = 2;
			} else if (end - off != 2) {
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal wait specification: only 0, 1 and 2 is permitted, got: '"
								+ opt.substring(off + 1, end) + "' options: '" + opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			} else {
				switch (opt.charAt(off + 1)) {
				case '0':
					waitUntilConnect = -1;
					break;
				case '1':
					waitUntilConnect = 1;
					break;
				case '2':
					waitUntilConnect = 2;
					break;
				default:
					System.err.println(
							"Patr Java Profiler Agent Daemon: illegal wait specification: only 0, 1 and 2 is permitted, got: '"
									+ opt.substring(off + 1, end) + "' options: '" + opt + '\'');
					System.err.println("use help to get a list of options");
					halt(2);
				}
			}
		} else if (isAssignableArg(off, end, opt, "multi-client", '=')) {
			if (multipleClients != 0) {
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal options: multi-client=[0|1] specified multiple times! '"
								+ opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			}
			off += "multi-client=".length();
			if (off >= end) {
				multipleClients = 1;
			} else if (end - off != 1) {
				System.err.println(
						"Patr Java Profiler Agent Daemon: illegal multi-client specification: only 0 and 1 is permitted, got: '"
								+ opt.substring(off, end) + "' options: '" + opt + '\'');
				System.err.println("use help to get a list of options");
				halt(2);
			} else {
				switch (opt.charAt(off)) {
				case '0':
					multipleClients = -1;
					break;
				case '1':
					multipleClients = 1;
					break;
				default:
					System.err.println(
							"Patr Java Profiler Agent Daemon: illegal multi-client specification: only 0 and 1 is permitted, got: '"
									+ opt.substring(off, end) + "' options: '" + opt + '\'');
					System.err.println("use help to get a list of options");
					halt(2);
				}
			}
		} else if (isArg(off, end, opt, "print-client-args")) {
			printClientArgs = true;
		} else {
			System.err.println("Patr Java Profiler Agent Daemon: unknown option: '" + opt.substring(off, end) + '\'');
			System.err.println("use help to get a list of options");
			halt(2);
		}
	}

	private static void halt(int status) {
		System.err.close();// force flush
		System.out.close();
		Runtime.getRuntime().halt(status);
	}

	private static boolean isAssignableArg(int off, int end, String args, String refArg, char assign) {
		int len = end - off;
		int alen = refArg.length();
		if (len < alen)
			return false;
		if (!args.startsWith(refArg, off))
			return false;
		if (len == alen)
			return true;
		return args.charAt(off + alen) == assign;
	}

	private static boolean isArg(int off, int end, String args, String refArg) {
		if (end - off != refArg.length())
			return false;
		return args.startsWith(refArg, off);
	}


	@Override
	public void start(Object ct, Instrumentation inst) {
		this.transformer = ct;
		this.inst = inst;
		start();
	}
	
	/**
	 * start the server in a new thread
	 * <p>
	 * this method is invoked very early from the Agent (end of pre-main, after
	 * transformer is registered and some more setup is done)
	 */
	public void start() {
		// some logic of acceptArg may need do be done here, because the transformer is
		// now registered
		try {
			if (jvmVersion() >= 9) {
				// java.lang.ProcessHandle : since 9
				Class<?> cls = Class.forName("java.lang.ProcessHandle");
				Method current = cls.getMethod("current");
				Object me = current.invoke(null);
				Method getPid = cls.getMethod("pid");
				pid = (long) getPid.invoke(me);
				Method getInfo = cls.getMethod("info");
				Object myInfo = getInfo.invoke(me);
				Class<?> infoCls;
				try {
					infoCls = Class.forName("java.lang.ProcessHandle$Info");
				} catch (@SuppressWarnings("unused") ClassNotFoundException e) {
					infoCls = null;
				}
				Method getStart;
				if (infoCls != null && infoCls.isInstance(myInfo)) {
					getStart = infoCls.getMethod("startInstant");
				} else {
					getStart = cls.getMethod("startInstant");
				}
				startTime = (Instant) ((Optional<?>) getStart.invoke(myInfo)).orElseGet(() -> {
					// they check anyway so might as well add a warning
					System.err.println("[PatrJProf-Server]: start time of current process not known");
					return null;
				});
			}
			if (socket != null) {// version>=16 ==> process id is set
				// java.net.UnixDomainSocketAddress : since 16
				Class<?> cls = Class.forName("java.net.UnixDomainSocketAddress");
				Method of = cls.getMethod("of", String.class);
				String s;
				if (socketEnd == 0) {
					String tmp = System.getProperty("java.io.tmpdir");
					String fs = System.getProperty("file.separator");// replace the path separator
					s = tmp + fs + "patrjprof-" + startTime.toString().replace(':', '-') + "-" + pid + ".sok";
				} else {
					s = socket.substring(socketOff, socketEnd);
				}
				socket = s;
				SocketAddress sa = (SocketAddress) of.invoke(null, s);
				StandardProtocolFamily spf = StandardProtocolFamily.valueOf("UNIX");
				Method sscOpen = ServerSocketChannel.class.getMethod("open", ProtocolFamily.class);
				ssok = (ServerSocketChannel) sscOpen.invoke(null, spf);
				try {
					ssok.bind(sa, 2);
				} catch (IOException e) {
					System.err.println("[PatrJProf-Server]: Failed to start the server on socket " + s);
					e.printStackTrace();
					halt(2);
				}
			} else {
				int p = port;
				if (p == -1)
					p = 0;
				try {
					ssok = ServerSocketChannel.open().bind(new InetSocketAddress(p));
				} catch (IOException e) {
					System.err.println("[PatrJProf-Server]: Failed to start the server on port " + p);
					e.printStackTrace();
					halt(2);
				}
			}
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
				| InvocationTargetException e) {
			e.printStackTrace();
			halt(2);
		}
		thread = new Thread(this, "PatrJProf-Server");
		thread.setDaemon(true);
		if (multipleClients > 0) {
			openThreads = new ConcurrentHashMap<>();
			openThreads.put(thread, ssok);
		}
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				if (multipleClients > 0) {
					openThreads.keySet().forEach(Thread::interrupt);
				}
				if (thread.isAlive()) {
					thread.interrupt();
					thread.join(500L);
					if (thread.isAlive()) {
						thread.interrupt();
						thread.join(125L);
						if (thread.isAlive()) {
							thread.interrupt();
							thread.join(50L);
						}
					}
				}
				if (multipleClients > 0) {
					Thread.sleep(50L);
					for (Closeable c : openThreads.values()) {
						try {
							c.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				return;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}));
		if (waitUntilConnect > 0) {// avoid unnecessary synchronization
			synchronized (this) {
				thread.start();
				try {
					do {
						wait();
					} while (waitUntilConnect > 0);
				} catch (InterruptedException e) {
					e.printStackTrace();
					halt(2);
				}
			}
		} else {
			thread.start();
		}
	}

	/**
	 * runs the server until the current thread is interrupted
	 */
	@Override
	public void run() {
		ServerSocketChannel ser = ssok;
		try {
			if (printClientArgs) {
				SocketAddress la = ser.getLocalAddress();
				if (la instanceof InetSocketAddress) {
					System.out.println("--port=" + ((InetSocketAddress) la).getPort());
				} else if (la.getClass().getName().equals("java.net.UnixDomainSocketAddress")) {
					try {
						Path path = (Path) la.getClass().getMethod("getPath").invoke(la);
						System.out.println("--socket=" + path);
					} catch (NoSuchMethodException | SecurityException | IllegalAccessException
							| InvocationTargetException e) {
						System.out.println("--socket=" + la);
						e.printStackTrace();
					}
				} else {
					throw new IllegalArgumentException(la == null ? "null" : la.getClass().getName());
				}
			}
			System.out.println("[PatrJProf-Server]: run server on socket " + ser.getLocalAddress());
			System.out.flush();
			ser.configureBlocking(true);
			while (true) {
				SocketChannel sok = ser.accept();
				if (multipleClients > 0) {
					SocketAddress ra = sok.getRemoteAddress();
					synchronized (this) {
						Thread t = new Thread(() -> {
							try {
								handle(sok);
							} catch (IOException | InterruptedException e) {
								if (e instanceof InterruptedIOException | e instanceof ClosedByInterruptException) {
									//
								} else {
									System.err.println(
											"[PatrJProf-Server]: error on connection " + ra + " : " + e.toString());
								}
							} finally {
								try {
									sok.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
								synchronized (Server.this) {
									openThreads.remove(Thread.currentThread());
								}
							}
						});
						openThreads.put(t, sok);
						t.setDaemon(true);
						t.setName("PatrJProfDaemon-" + ra);
						t.start();
					}
					continue;
				}
				try {
					handle(sok);
				} catch (IOException e) {
					if (e instanceof InterruptedIOException | e instanceof ClosedByInterruptException) {
						throw e;
					} else {
						System.err.println("[PatrJProf-Server]: error on connection " + sok.getRemoteAddress() + " : "
								+ e.toString());
					}
				} finally {
					sok.close();
				}
			}
		} catch (@SuppressWarnings("unused") ClosedByInterruptException | InterruptedIOException
				| InterruptedException e) {
			// nothing to be done here, we should just terminate
		} catch (IOException e) {
			System.err.println("PatrJavaProfiler Agent Daemon: failed: deamon is no longer available");
			e.printStackTrace();
		} finally {
			if (socket != null) {
				new File(socket).delete();
			}
			try {
				ser.close();
			} catch (IOException e) {
				System.err.println("[PatrJProf-Server]: failed to close the server socket");
				e.printStackTrace();
			}
		}
	}

	private void startProgram(int value) {
		if (waitUntilConnect == value) {
			synchronized (this) {
				if (waitUntilConnect != value) {
					throw new AssertionError();
				}
				waitUntilConnect = -2;
				notifyAll();
			}
		}
	}

	private void handle(SocketChannel sok) throws IOException, InterruptedException {
		ByteBuffer b = ByteBuffer.allocateDirect(8192);
		b.order(ByteOrder.LITTLE_ENDIAN);
		((Buffer) b).limit(4);
		sok.configureBlocking(false);
		long start = System.nanoTime();
		for (int r = sok.read(b); b.hasRemaining(); r = sok.read(b)) {
			if (r == -1) {
				System.err.println("[PatrJProf-Server]: disconnect before greet: " + sok.getRemoteAddress());
				return;
			}
			if (System.nanoTime() - start > 2_500000000L) {
				throw new SocketTimeoutException("no connection after 2.5 seconds");
			}
			Thread.sleep(10L);
		}
		if (b.getInt(0) != HELLO_4) {
			System.err.println("[PatrJProf-Server]: invalid greet from " + sok.getRemoteAddress());
			return;
		}
		// after the connect there is no timeout,
		// the client is allowed to send as few request as it wishes
		((Buffer) b).position(0);
		((Buffer) b).limit(1);
		b.put(0, HELLO_RESPONSE_1);
		Runnable exp = new PatrProfiler.Exporter(sok, b);
		startProgram(1);
		blockingSend(b, sok);
		try {
			while (true) {
				((Buffer) b).position(0);
				((Buffer) b).limit(1);
				if (blockingRead(sok, b))
					break;
				switch (b.get(0)) {
				case BYE_1:
					return;
				case VERSION_1:
					sendVersion(sok, b);
					break;
				case VERSION_STR_1:
					sendVersionStr(sok, b);
					break;
				case JVM_VERSION_1:
					sendJvmVersion(sok, b);
					break;
				case JVM_VERSION_STR_1:
					sendJvmVersionStr(sok, b);
					break;
				case PID_1:
					sendPID(sok, b);
					break;
				case START_1:
					sendStart(sok, b);
					break;
				case REPORT_1:
					((Buffer) b).position(0);
					((Buffer) b).limit(b.capacity());
					b.put(SEND_REPORT_1);
					exp.run();
					break;
				default:
					System.err.println("[PatrJProf-Server]: invalid command from " + sok.getRemoteAddress() //
							+ " : " + b.get(0) + " : 0x" + Integer.toHexString(0xFF & b.get(0)));
					return;
				}
			}
		} catch (InterruptedException | InterruptedIOException ie) {
			finish(sok, b, exp);
			throw ie;
		}
	}

	private static void finish(SocketChannel sc, ByteBuffer b, Runnable exp) throws IOException, InterruptedException {
		if (sc.isOpen()) {
			System.out.println("finish now");
			((Buffer) b).position(0);
			((Buffer) b).limit(b.capacity());
			b.put(SEND_REPORT_1);
			exp.run();
			((Buffer) b).position(0);
			((Buffer) b).limit(1);
			b.put(0, QUIT_1);
			blockingSend(b, sc);
			Thread.sleep(1000L);
		} else {
			System.err.println("skip finish");
		}
	}

	private void sendStart(SocketChannel sok, ByteBuffer b) throws IOException {
		startProgram(2);
		((Buffer) b).limit(2);
		b.put(1, START_1);
		blockingSend(b, sok);
	}

	private void sendPID(SocketChannel sok, ByteBuffer b) throws IOException {
		((Buffer) b).position(0);
		((Buffer) b).limit(20);
		if (startTime == null) {
			b.putLong(0, 0);
			b.putLong(8, 0);
			b.putInt(16, 0);
		} else {
			b.putLong(0, pid);
			b.putLong(8, startTime.getEpochSecond());
			b.putInt(16, startTime.getNano());
		}
		blockingSend(b, sok);
	}

	private static void sendJvmVersionStr(SocketChannel sok, ByteBuffer b) throws IOException {
		byte[] v = System.getProperty("java.version").getBytes(StandardCharsets.UTF_8);
		blockingSendStream(b, sok, v);
	}

	private static void sendJvmVersion(SocketChannel sok, ByteBuffer b) throws IOException {
		((Buffer) b).limit(4);
		b.putInt(0, jvmVersion());
		((Buffer) b).position(0);
		blockingSend(b, sok);
	}

	private void sendVersionStr(SocketChannel sok, ByteBuffer b) throws IOException {
		try (InputStream in = getClass().getResourceAsStream(RES_VERSION)) {
			blockingSendStream(in, b, sok, new byte[8]);
		}
	}

	private void sendVersion(SocketChannel sok, ByteBuffer b) throws IOException, StreamCorruptedException {
		try (InputStream in = getClass().getResourceAsStream(RES_VERSION)) {
			byte[] buf = new byte[8];
			CharsetDecoder d = StandardCharsets.UTF_8.newDecoder();
			d.onMalformedInput(CodingErrorAction.REPORT);
			d.onUnmappableCharacter(CodingErrorAction.REPORT);
			CharBuffer cb = CharBuffer.allocate(8);
			while (blockingRead(in, b, 0, buf)) {
				cb = decode(b, d, cb, false, "my own version");
			}
			cb = decode(b, d, cb, true, "my own version");
			cb.flip();
			int v0 = parseNum(cb, '.', false, "major-version");
			int v1 = parseNum(cb, '.', false, "minor-version");
			int v2 = parseNum(cb, '-', true, "bugfix-version");
			((Buffer) b).limit(13);
			b.putInt(0, v0);
			b.putInt(4, v1);
			b.putInt(8, v2);
			if (cb.remaining() > "-SNAPSHOT".length()) {
				// all branch versions must have the "-SNAPSHOT" suffix
				cb.limit(cb.limit() - "-SNAPSHOT".length());
				b.put(12, (byte) 2);
				CharsetEncoder e = StandardCharsets.UTF_8.newEncoder();
				((Buffer) b).position(17);
				((Buffer) b).limit(b.capacity());
				int sendpos = 13;
				while (true) {
					CoderResult cres = e.encode(cb, b, true);
					if (cres.isError()) {
						System.err
								.println("[PatrJProf-Server]: failed to encode my version ofter decoding it: " + cres);
						halt(4);
					}
					int len = b.position() - (sendpos + 4);
					if (cres.isOverflow()) {
						((Buffer) b).flip();
						b.putInt(sendpos, len);
						blockingSend(b, sok);
						((Buffer) b).position(4);
						((Buffer) b).limit(b.capacity());
						sendpos = 4;
						continue;
					}
					if (cres.isUnderflow()) {
						boolean bool = b.remaining() >= 4;
						if (bool) {
							b.putInt(0);
						}
						((Buffer) b).flip();
						b.putInt(sendpos, len);
						blockingSend(b, sok);
						if (!bool) {
							((Buffer) b).position(0);
							((Buffer) b).limit(4);
							b.putInt(0, 0);
							blockingSend(b, sok);
						}
						break;
					}
				}
			} else {
				b.position(0);
				b.put(12, cb.hasRemaining() ? (byte) 1 : (byte) 0);
				blockingSend(b, sok);
			}
		}
	}

	private static int parseNum(CharBuffer cb, char end, boolean allowEOF, String name)
			throws StreamCorruptedException {
		int pos = cb.position();
		int limit = cb.limit();
		int result = 0;
		while (true) {
			if (limit == pos) {
				if (allowEOF) {
					break;
				}
				throw new StreamCorruptedException(
						"expected the " + name + " number to end with '" + end + "', but got EOF");
			}
			int d = cb.get(pos++) - '0';
			if (d < 0 || d >= 10) {
				if (d + '0' == end) {
					break;
				}
				throw new StreamCorruptedException("expected the " + name + " number to end with '" + end
						+ "', but got '" + (char) (d + '0') + "'");
			}
			long newresult = (result * 10L) + d;
			result = (int) newresult;
			if (result != newresult) {
				throw new StreamCorruptedException("expected the " + name + " number to be not greather than "
						+ Integer.MAX_VALUE + " but got (at least) " + newresult);
			}
		}
		cb.position(pos);
		return result;
	}

	private static CharBuffer decode(ByteBuffer b, CharsetDecoder d, CharBuffer cb, boolean end, String name) {
		CharBuffer cbuf = cb;
		while (true) {
			CoderResult res = d.decode(b, cbuf, end);
			if (res.isError()) {
				System.err.println("[PatrJProf-Server]: failed to read " + name + ": " + res);
				halt(4);
			}
			if (res.isOverflow()) {
				CharBuffer tmp = CharBuffer.allocate(cbuf.capacity() * 2);
				((Buffer) cbuf).flip();
				tmp.put(cbuf);
				cbuf = tmp;
				continue;
			}
			if (res.isUnderflow()) {
				return cbuf;
			}
		}
	}

	private static void blockingSendStream(ByteBuffer b, SocketChannel sok, byte[] data) throws IOException {
		int capacity = b.capacity();
		int off = 0;
		((Buffer) b).limit(capacity);
		capacity -= 4;
		for (int end = data.length - capacity; off < end; off += capacity) {
			((Buffer) b).position(4);
			b.putInt(0, capacity);
			b.put(data, off, capacity);
			((Buffer) b).position(4);
			blockingSend(b, sok);
		}
		int remain = data.length - off;
		b.putInt(0, remain);
		((Buffer) b).position(4);
		((Buffer) b).limit(remain + 4);
		b.put(data, off, remain);
		if (capacity - 4 >= remain) {
			((Buffer) b).limit(remain + 8);
			b.putInt(0);
		} else {
			((Buffer) b).position(0);
			blockingSend(b, sok);
			b.putInt(0, 0);
			((Buffer) b).limit(4);
		}
		((Buffer) b).position(0);
		blockingSend(b, sok);
	}

	private static void blockingSendStream(InputStream in, ByteBuffer b, SocketChannel sok, byte[] buf)
			throws IOException {
		((Buffer) b).limit(b.capacity());
		((Buffer) b).position(4);
		while (true) {
			int i = in.read(buf);
			if (i == -1) {
				if (b.remaining() > 4) {
					b.putInt(0, b.position() - 4);
					b.putInt(0);
					((Buffer) b).flip();
					blockingSend(b, sok);
					return;
				}
			} else if (b.remaining() >= i) {
				b.put(buf, 0, i);
				continue;
			}
			b.putInt(0, b.position() - 4);
			((Buffer) b).flip();
			blockingSend(b, sok);
		}
	}

	private static void blockingSend(ByteBuffer b, SocketChannel sok) throws IOException {
		while (b.hasRemaining()) {
			sok.write(b);
		}
	}

	private static boolean blockingRead(SocketChannel sok, ByteBuffer b) throws IOException, InterruptedException {
		for (int r = sok.read(b);; r = sok.read(b)) {
			if (r == -1) {
				System.err.println("[PatrJProf-Server]: " + sok.getRemoteAddress() + " disconnected");
				return true;
			} else if (r == 0) {
				if (b.hasRemaining()) {
					Thread.sleep(10);
					continue;
				}
				throw new AssertionError("nothing read, but there also is nothing available");
			} else {
				return false;
			}
		}
	}

	private static boolean blockingRead(InputStream in, ByteBuffer b, int initpos, byte[] buf) throws IOException {
		((Buffer) b).position(initpos);
		((Buffer) b).limit(b.capacity());
		for (int r = in.read(buf);; r = in.read(buf, 0, Math.min(buf.length, b.remaining()))) {
			if (r <= -1) {
				((Buffer) b).flip();
				return r == 0;
			}
			b.put(buf, 0, r);
		}
	}

}
