// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.test;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * this class's main method executes the tests
 */
public class PHPTestMain {

	private PHPTestMain() {
	}

	/**
	 * the value of a regular expression
	 */
	private static final String P_CONTENT = //
			/*	*/"((?<GENERATOR>[a-zA-Z]+) generator"//
					+ "|(?<MICROCHIP>[a-zA-Z]+)-compatible microchip)";

	@SuppressWarnings("unused")
	static void enter(long time, String a, String b, String c) {
	}

	@SuppressWarnings("unused")
	static void exit(long time, long start) {
	}

	void sample() {
		long start;
		enter(start = System.nanoTime(), "de/hechler/patrick/PHPTestMain", "sample", "()V");
		try {
			sample0();
			exit(System.nanoTime(), start);
			return;
		} catch (Throwable t) {
			exit(System.nanoTime(), start);
			throw t;
		}
	}

	void sample0() {

	}

	/**
	 * execute the tests
	 * 
	 * @param args empty or the amount of test calls
	 */
	public static void main(String[] args) {
		System.out.println("START");
		((Buffer) ByteBuffer.allocate(10)).limit(1);
		Pattern.compile(P_CONTENT);
		new Scanner(System.in).close();
		long start = System.currentTimeMillis();
		int remain = 1010101;
		if (args.length != 0) {
			try {
				remain = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println("Usage: PHPTestMain [TEST-CALL-COUNT]");
				System.exit(1);
			}
		}
		remain -= cal(1, remain);
		remain -= cal(100, remain);
		remain -= cal(10000, remain);
		remain -= cal(1000000, remain);
		if (remain > 0) {
			final int cnt = remain;
			new Thread(() -> {
				cal(cnt, cnt);
				noteFinish(start);
			}).start();
		} else {
			noteFinish(start);
		}
	}

	private static int cal(int count, int maxCount) {
		if (maxCount < count) {
			count = maxCount;
		}
		if (count <= 0) {
			return 0;
		}
		System.out.println("PIPTestMain: " + count + "*cal SimpleClass.methodName()");
		for (int cnt = 0; cnt < count; cnt++) {
			SimpleClass.methodName();
		}
		System.out.println("PIPTestMain: " + count + "*ret SimpleClass.methodName()");
		return count;
	}

	private static void noteFinish(long start) {
		long totalTime = System.currentTimeMillis() - start;
		System.out.println("needed a total of " + totalTime + " millis");
		if ((totalTime /= 1000) != 0) {
			System.out.println("                  " + totalTime + " seconds");
		}
		if ((totalTime /= 60) != 0) {
			System.out.println("                  " + totalTime + " minutes");
		}
		if ((totalTime /= 60) != 0) {
			System.out.println("                  " + totalTime + " hours");
		}
	}

}
