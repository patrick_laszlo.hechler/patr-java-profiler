// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.test;

import java.io.PrintStream;

/**
 * a simple class used to test the PatrJavaProfiler
 */
public class SimpleClass {
	
	private SimpleClass(PrintStream out, String arg) {
		out.print(arg);
	}
	
	/**
	 * this method executes some other methods and lets (indirectly) print 'hello world\n'
	 */
	static void methodName() {
		helperMethodName();
		otherHelperMethodName("hello");
		helperMethodName();
		otherHelperMethodName(" world\n");
		helperMethodName();
	}
	
	private static void helperMethodName() {}
	
	private static void otherHelperMethodName(String printme) {
		new SimpleClass(System.out, printme);
	}
	
}
