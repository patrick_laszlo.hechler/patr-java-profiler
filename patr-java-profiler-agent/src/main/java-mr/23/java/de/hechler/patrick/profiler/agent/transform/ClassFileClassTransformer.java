// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import static de.hechler.patrick.profiler.agent.CTHelp.*;
import static de.hechler.patrick.profiler.agent.CTHelp.AGENT_METHOD_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.AGENT_METHOD_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.CALLER_SENSITIVE0_ANNOT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.CALLER_SENSITIVE1_ANNOT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.INIT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.INTRINSIC_CANDIDATE_ANNOT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.POLYMORPHIC_SIGNATURE_ANNOT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_DIRECT_EXIT_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_DIRECT_EXIT_INIT_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_DIRECT_EXIT_INIT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_DIRECT_EXIT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_ENTER_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_ENTER_INIT_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_ENTER_INIT_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_ENTER_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PPROF_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.PREFIX_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SUK_MISC_UNSAFE_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SUN_MISC_UNSAFE_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SUN_MISC_UNSAFE_GET_UNSAFE_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SUN_MISC_UNSAFE_GET_UNSAFE_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SUN_MISC_UNSAFE_THE_UNSAFE_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SYSTEM_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SYSTEM_NANO_TIME_NAME_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.SYSTEM_TIME_DESC_STR;
import static de.hechler.patrick.profiler.agent.CTHelp.THROWABLE_DESC_STR;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_ERR_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_OUT_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysew;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysw;

import java.lang.classfile.AccessFlags;
import java.lang.classfile.Annotation;
import java.lang.classfile.Attribute;
import java.lang.classfile.ClassBuilder;
import java.lang.classfile.ClassFile;
import java.lang.classfile.ClassModel;
import java.lang.classfile.CodeBuilder;
import java.lang.classfile.CodeElement;
import java.lang.classfile.FieldElement;
import java.lang.classfile.FieldModel;
import java.lang.classfile.Instruction;
import java.lang.classfile.Label;
import java.lang.classfile.MethodElement;
import java.lang.classfile.MethodModel;
import java.lang.classfile.Opcode;
import java.lang.classfile.PseudoInstruction;
import java.lang.classfile.TypeKind;
import java.lang.classfile.attribute.AnnotationDefaultAttribute;
import java.lang.classfile.attribute.CodeAttribute;
import java.lang.classfile.attribute.ConstantValueAttribute;
import java.lang.classfile.attribute.DeprecatedAttribute;
import java.lang.classfile.attribute.ExceptionsAttribute;
import java.lang.classfile.attribute.InnerClassInfo;
import java.lang.classfile.attribute.InnerClassesAttribute;
import java.lang.classfile.attribute.MethodParametersAttribute;
import java.lang.classfile.attribute.RuntimeVisibleAnnotationsAttribute;
import java.lang.classfile.attribute.RuntimeVisibleParameterAnnotationsAttribute;
import java.lang.classfile.attribute.RuntimeVisibleTypeAnnotationsAttribute;
import java.lang.classfile.attribute.SignatureAttribute;
import java.lang.classfile.attribute.StackMapFrameInfo;
import java.lang.classfile.attribute.StackMapFrameInfo.ObjectVerificationTypeInfo;
import java.lang.classfile.attribute.StackMapFrameInfo.SimpleVerificationTypeInfo;
import java.lang.classfile.attribute.StackMapFrameInfo.VerificationTypeInfo;
import java.lang.classfile.attribute.StackMapTableAttribute;
import java.lang.classfile.attribute.SyntheticAttribute;
import java.lang.classfile.constantpool.ClassEntry;
import java.lang.classfile.constantpool.Utf8Entry;
import java.lang.classfile.instruction.ArrayLoadInstruction;
import java.lang.classfile.instruction.ArrayStoreInstruction;
import java.lang.classfile.instruction.BranchInstruction;
import java.lang.classfile.instruction.ConstantInstruction;
import java.lang.classfile.instruction.ConvertInstruction;
import java.lang.classfile.instruction.DiscontinuedInstruction;
import java.lang.classfile.instruction.DiscontinuedInstruction.JsrInstruction;
import java.lang.classfile.instruction.ExceptionCatch;
import java.lang.classfile.instruction.FieldInstruction;
import java.lang.classfile.instruction.IncrementInstruction;
import java.lang.classfile.instruction.InvokeDynamicInstruction;
import java.lang.classfile.instruction.InvokeInstruction;
import java.lang.classfile.instruction.LabelTarget;
import java.lang.classfile.instruction.LoadInstruction;
import java.lang.classfile.instruction.LookupSwitchInstruction;
import java.lang.classfile.instruction.MonitorInstruction;
import java.lang.classfile.instruction.NewMultiArrayInstruction;
import java.lang.classfile.instruction.NewObjectInstruction;
import java.lang.classfile.instruction.NewPrimitiveArrayInstruction;
import java.lang.classfile.instruction.NewReferenceArrayInstruction;
import java.lang.classfile.instruction.NopInstruction;
import java.lang.classfile.instruction.OperatorInstruction;
import java.lang.classfile.instruction.ReturnInstruction;
import java.lang.classfile.instruction.StackInstruction;
import java.lang.classfile.instruction.StoreInstruction;
import java.lang.classfile.instruction.SwitchCase;
import java.lang.classfile.instruction.TableSwitchInstruction;
import java.lang.classfile.instruction.ThrowInstruction;
import java.lang.classfile.instruction.TypeCheckInstruction;
import java.lang.constant.ClassDesc;
import java.lang.constant.MethodTypeDesc;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.AccessFlag;
import java.security.ProtectionDomain;
import java.util.List;
import java.util.Optional;
import java.util.function.ToIntBiFunction;

import de.hechler.patrick.profiler.agent.ClassBuffer;
import de.hechler.patrick.profiler.agent.ClassFileClassBuffer;
import de.hechler.patrick.profiler.agent.PProfilerAgent;

/**
 * the java23 implementation of the {@code classfile-api} class transformer<br>
 * does not use the java version suffix, because the class is build with
 * {@code --enable-preview}
 */
public class ClassFileClassTransformer extends AbstractClassTransformer<ClassFileClassBuffer, Module> {

	/** transform {@code <clinit>} methods */
	protected int doClinit;
	/** transform {@code <init>} methods */
	protected int doInit;
	/** use my transformer if the class file version is known */
	protected int swapToMy;
	/** don't use the safe exit variant everywhere */
	protected int allowUnsafeConstructor;
	/** prefer to wrap methods */
	protected int preferToWrap;
	/** allow to inline methods */
	protected int allowToInline;

	protected boolean doClinit() {
		return isSet(this.doClinit, true);
	}

	protected boolean doInit() {
		return isSet(this.doInit, true);
	}

	protected boolean swapToMy() {
		return isSet(this.swapToMy, false);
	}

	protected boolean allowUnsafeConstructor() {
		// note that if swapToMy is set this can happen, even if set to false!
		return isSet(this.allowUnsafeConstructor, true);
	}

	protected boolean preferToWrap() {
		return isSet(this.preferToWrap, false);
	}

	protected boolean allowToInline() {
		return isSet(this.allowToInline, true);
	}

	protected boolean swapToMy(ClassFileClassBuffer cls) {
		return swapToMy() && cls.model().majorVersion() <= ClassBuffer.MAX_VERSION;
	}

	@Override
	public void parseOption(String args, int off, int endOff) {
		if (PProfilerAgent.isAssignableArg(off, endOff, args, "doClinit", '=')) {
			doClinit = parseBooleanOption(doClinit, args, off, endOff, "doClinit");
		} else if (PProfilerAgent.isAssignableArg(off, endOff, args, "doInit", '=')) {
			doInit = parseBooleanOption(doInit, args, off, endOff, "doInit");
		} else if (PProfilerAgent.isAssignableArg(off, endOff, args, "swapToMy", '=')) {
			swapToMy = parseBooleanOption(swapToMy, args, off, endOff, "swapToMy");
		} else if (PProfilerAgent.isAssignableArg(off, endOff, args, "allowUnsafeConstructor", '=')) {
			allowUnsafeConstructor = parseBooleanOption(allowUnsafeConstructor, args, off, endOff,
					"allowUnsafeConstructor");
		} else if (PProfilerAgent.isAssignableArg(off, endOff, args, "preferToWrap", '=')) {
			preferToWrap = parseBooleanOption(preferToWrap, args, off, endOff, "preferToWrap");
		} else if (PProfilerAgent.isAssignableArg(off, endOff, args, "allowToInline", '=')) {
			allowToInline = parseBooleanOption(allowToInline, args, off, endOff, "allowToInline");
		} else {
			super.parseOption(args, off, endOff);
		}
	}

	@Override
	public void init() {
		if (!preferToWrap() && !allowToInline()) {
			throw new IllegalStateException("not preferToWrap and not allowToInline");
		}
		super.init();
	}

	private static final ClassDesc SYSTEM_CLASS = ClassDesc.ofDescriptor(SYSTEM_DESC_STR);
	private static final MethodTypeDesc SYSTEM_NANO_TIME_DESC = MethodTypeDesc.ofDescriptor(SYSTEM_TIME_DESC_STR);

	private static final ClassDesc PATR_PROFILER_CLASS = ClassDesc.ofDescriptor(PPROF_DESC_STR);
	private static final MethodTypeDesc PPROF_ENTER_DESC = MethodTypeDesc.ofDescriptor(PPROF_ENTER_DESC_STR);
	private static final MethodTypeDesc PPROF_DIRECT_EXIT_DESC = MethodTypeDesc
			.ofDescriptor(PPROF_DIRECT_EXIT_DESC_STR);
	private static final MethodTypeDesc PPROF_ENTER_INIT_DESC = MethodTypeDesc.ofDescriptor(PPROF_ENTER_INIT_DESC_STR);
	private static final MethodTypeDesc PPROF_DIRECT_EXIT_INIT_DESC = MethodTypeDesc
			.ofDescriptor(PPROF_DIRECT_EXIT_INIT_DESC_STR);
	private static final MethodTypeDesc PPROF_GEN_ID_DESC = MethodTypeDesc.ofDescriptor(PPROF_GEN_ID_DESC_STR);

	private static final ClassDesc SUN_MISC_UNSAFE_CLASS = ClassDesc.ofDescriptor(SUN_MISC_UNSAFE_DESC_STR);
	private static VerificationTypeInfo SUN_MISC_UNSAFE_VERIFY = ObjectVerificationTypeInfo.of(SUN_MISC_UNSAFE_CLASS);

	private static final ClassDesc THROWABLE_CLASS = ClassDesc.ofDescriptor(THROWABLE_DESC_STR);

	@Override
	public byte[] transform(Module module, ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		return super.transformCaller(module, loader, className, classBeingRedefined, protectionDomain, classfileBuffer);
	}

	@Override
	protected ClassFileClassBuffer createHelper(Module module, ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer)
			throws IllegalClassFormatException {
		return new ClassFileClassBuffer(module, loader, classfileBuffer);
	}

	@Override
	protected byte[] transformOtherOrBootstrap(ClassFileClassBuffer cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		ClassModel model = cls.initModel();
		if (swapToMy(cls)) {
			ClassFileClassBuffer h = MyWrapImpl8.initHelper(clsName, cls);
			if (h == null) {
				return null;
			}
			return MyWrapImpl8.transformOtherOrBootstrap(this, h, clsName, classBeingRedefined);
		}
		if (model.isModuleInfo()) {
			return null;
		}
		if (PPROF_NAME_STR.equals(clsName)) {
			return transformMyInternBootstrap(cls, clsName, classBeingRedefined);
		}
		for (Attribute<?> attr : model.attributes()) {
			if (attr instanceof InnerClassesAttribute i) {
				ClassDesc thisClass = model.thisClass().asSymbol();
				for (InnerClassInfo inf : i.classes()) {
					if (inf.innerClass().asSymbol().equals(thisClass)) {
						Optional<ClassEntry> outerClass = inf.outerClass();
						if (outerClass.isPresent() && PPROF_NAME_STR.equals(outerClass.get().asInternalName())) {
							return transformMyInternBootstrap(cls, clsName, classBeingRedefined);
						}
						break;
					}
				}
				break; // there can only be one inner classes attribute
			}
		}
		if (model.methods().isEmpty()) {
			return null;
		}
		return transformOther(cls, clsName, classBeingRedefined);
	}

	@Override
	protected byte[] transformOther(ClassFileClassBuffer cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
//		if (
//		// clsName.equals("org/eclipse/jdt/launching/internal/javaagent/")
//		// ||
//				clsName.equals("sun/security/util/ManifestDigester")
//				||
//				clsName.equals("sun/security/util/ManifestDigester$Position")
//				||
//				clsName.equals("java/lang/Module$ReflectionData")
//				) {
//			return null;
//		}
//		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
//			sysw("OTHER: " + clsName + "\n");
//		}
		if (swapToMy(cls)) {
			if (doInit() || doClinit()) {
				cls.buffer(ClassFile.of().transform(cls.model(), (ct, ce) -> {
					if (ce instanceof MethodModel mm) {
						String mname = mm.methodName().stringValue();
						if (INIT_NAME_STR.equals(mname) && doInit()
								|| CLASS_INIT_NAME_STR.equals(mname) && doClinit()) {
							transformJava(cls, clsName, classBeingRedefined, ct, mm);
							return;
						}
					}
					ct.with(ce);
				}));
			}
			ClassFileClassBuffer h = MyWrapImpl8.initHelper(clsName, cls);
			if (h == null) {
				return null;
			}
			return MyWrapImpl8.transformOther(h, clsName, classBeingRedefined);
		}
		return ClassFile.of().transform(cls.model(), (ct, ce) -> {
			if (ce instanceof MethodModel mm) {
				AccessFlags flags = mm.flags();
				if (!flags.has(AccessFlag.ABSTRACT)) {
					if (flags.has(AccessFlag.NATIVE)) {
						if (!transformWrap(cls, clsName, classBeingRedefined, ct, mm)) {
							ct.with(mm);
						}
						return;
					} else {
						transformJava(cls, clsName, classBeingRedefined, ct, mm);
					}
					return;
				}
			}
			ct.with(ce);
		});
	}

	private void transformJava(ClassFileClassBuffer cls, String clsName, Class<?> classBeingRedefined, ClassBuilder ct,
			MethodModel mm) {
		if (skip(mm)) {
			ct.with(mm);
			return;
		}
		if (preferToWrap()) {
			if (transformWrap(cls, clsName, classBeingRedefined, ct, mm)) {
				return;
			}
			if (!allowToInline()) {
				ct.with(mm);
				return;
			}
		}
		ct.transformMethod(mm, (mt, me) -> {
			if (me instanceof CodeAttribute ca) {
				mt.withCode(cb -> {
					String mname = mm.methodName().stringValue();
					if (INIT_NAME_STR.equals(mname)) {
						transformInit(cls, clsName, classBeingRedefined, mm, ca, cb);
						return;
					}
					MethodTypeDesc mtype = mm.methodTypeSymbol();
					int myLocal = mm.code().get().maxLocals();
					enter(clsName, cb, mname, mtype.descriptorString(), myLocal);
					Label profileStart = cb.newBoundLabel();
					for (CodeElement ce : ca.elements()) {
						if (ce instanceof Instruction i) {
							switch (i) {
							case ReturnInstruction ri -> {
								exit(cb, myLocal);
								cb.with(ri);
							}
							default -> cb.with(i);
							}
						} else if (ce instanceof PseudoInstruction
								|| ce instanceof RuntimeVisibleTypeAnnotationsAttribute) {
							cb.with(ce);
						}
					}
					Label profileEnd = cb.newBoundLabel();
					Label handlerStart = profileEnd;
					exit(cb, myLocal);
					Label handlerEnd = cb.newBoundLabel();
					cb.athrow();
					cb.exceptionCatchAll(profileStart, profileEnd, handlerStart);
					cb.exceptionCatchAll(handlerStart, handlerEnd, handlerStart);
				});
			} else {
				mt.with(me);
			}
		});
	}

	private boolean skip(MethodModel mm) {
		String mname = mm.methodName().stringValue();
		return INIT_NAME_STR.equals(mname) && !doInit() || CLASS_INIT_NAME_STR.equals(mname) && !doClinit();
	}

	private static void enterUnsafe(String clsName, CodeBuilder cb, String mname, String mtypeDesc) {
		time(cb);
		cb.loadConstant(clsName);
		cb.loadConstant(mname);
		cb.loadConstant(mtypeDesc);
		cb.invokestatic(PATR_PROFILER_CLASS, PPROF_ENTER_NAME_STR, PPROF_ENTER_DESC, false);
	}

	private static void exitUnsafe(CodeBuilder cb) {
		time(cb);
		cb.invokestatic(PATR_PROFILER_CLASS, PPROF_DIRECT_EXIT_NAME_STR, PPROF_DIRECT_EXIT_DESC, false);
	}

	private void enter(String clsName, CodeBuilder cb, String mname, String mtypeDesc, int local) {
		if (allowUnsafeConstructor()) {
			enterUnsafe(clsName, cb, mname, mtypeDesc);
			return;
		}
		enterInit(clsName, cb, mname, mtypeDesc, local);
	}

	private void exit(CodeBuilder cb, int local) {
		if (allowUnsafeConstructor()) {
			exitUnsafe(cb);
			return;
		}
		failExitInit(cb, local);
	}

	private static void enterInit(String clsName, CodeBuilder cb, String mname, String mtypeDesc, int local) {
		time(cb);
		cb.invokestatic(PATR_PROFILER_CLASS, PPROF_GEN_ID_NAME_STR, PPROF_GEN_ID_DESC, false);
		cb.dup();
		cb.storeLocal(TypeKind.IntType, local);
		cb.loadConstant(clsName);
		cb.loadConstant(mname);
		cb.loadConstant(mtypeDesc);
		cb.invokestatic(PATR_PROFILER_CLASS, PPROF_ENTER_INIT_NAME_STR, PPROF_ENTER_INIT_DESC, false);
	}

	private static void failExitInit(CodeBuilder cb, int local) {
		time(cb);
		cb.loadLocal(TypeKind.IntType, local);
		cb.invokestatic(PATR_PROFILER_CLASS, PPROF_DIRECT_EXIT_INIT_NAME_STR, PPROF_DIRECT_EXIT_INIT_DESC, false);
	}

	private static void time(CodeBuilder cb) {
		cb.invokestatic(SYSTEM_CLASS, SYSTEM_NANO_TIME_NAME_STR, SYSTEM_NANO_TIME_DESC, false);
	}

	public static String tos(boolean[] b) {
		char[] result = new char[b.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = b[i] ? 'X' : '_';
		}
		return new String(result);
	}

	private void transformInit(ClassFileClassBuffer cls, String clsName, Class<?> classBeingRedefined, MethodModel mm,
			CodeAttribute ca, CodeBuilder cb) {
		boolean initilized = false;
		boolean[] stack = new boolean[mm.code().get().maxStack()];
		boolean[] vars = new boolean[mm.code().get().maxLocals()];
		enterInit(clsName, cb, mm.methodName().stringValue(), mm.methodType().stringValue(), vars.length);
		Label profileStart = cb.newBoundLabel();
		Label handlerSuccessStart = cb.newLabel();
		Label handlerFailureStart = cb.newLabel();
		Label handlerMidStart = null; // cb.newLabel(); // TODO remove this line
		int stackSize = 0;
		List<StackMapFrameInfo> infs = null;
		Iterable<CodeElement> elements = ca.elements();
		ToIntBiFunction<Label, ToIntBiFunction<Label, ?>> other = null;
		for (CodeElement ce : elements) {
			switch (ce) {
			case StackMapTableAttribute sm -> {
				infs = sm.entries();
			}
			case ExceptionCatch sm -> {
				final ToIntBiFunction<Label, ToIntBiFunction<Label, ?>> oother = other;
				other = (lbl, o) -> {
					if (lbl.equals(sm.handler())) {
						@SuppressWarnings("unchecked")
						ToIntBiFunction<Label, ToIntBiFunction<Label, ?>> oo = (ToIntBiFunction<Label, ToIntBiFunction<Label, ?>>) o;
						// avoid stack overflow
						int s = sm.tryStart().equals(lbl) ? -1 : oo.applyAsInt(sm.tryStart(), oo);
						if (s == -1 && !sm.tryEnd().equals(lbl)) {
							s = oo.applyAsInt(sm.tryEnd(), oo);
						}
						if (s != -1) {
							for (int i = 0; i < s; i++) {
								stack[i] = false;
							}
							return 1;
						}
					}
					if (oother != null) {
						return oother.applyAsInt(lbl, o);
					}
					return -1;
				};
			}
			case RuntimeVisibleTypeAnnotationsAttribute r -> cb.with(ce);
			default -> {
			}
			}
		}
		vars[0] = true;
		for (CodeElement ce : elements) {
			switch (ce) {
			case Instruction inst -> {
				switch (inst) {
				case ThrowInstruction a -> {
					for (int i = 0; i < stackSize; i++) {
						stack[i] = false;
					}
					stackSize = -1;
				}
				case ReturnInstruction ri -> {
					exit(cb, vars.length);
					for (int i = 0; i < stackSize; i++) {
						stack[i] = false;
					}
					stackSize = -1;
				}
				case ArrayLoadInstruction a -> stackSize += a.typeKind().slotSize() - 2;
				case ArrayStoreInstruction a -> {
					stack[stackSize - 1] = false;
					stackSize -= a.typeKind().slotSize() + 2;
				}
				case BranchInstruction a -> {
					switch (a.opcode()) {
					case GOTO, GOTO_W:
						break;
					case IFEQ, IFNE, IFLT, IFGE, IFGT, IFLE, IFNULL, IFNONNULL:
						stack[--stackSize] = false;
						break;
					case IF_ACMPEQ, IF_ACMPNE, IF_ICMPEQ, IF_ICMPGE, IF_ICMPGT, IF_ICMPLE, IF_ICMPLT, IF_ICMPNE:
						stack[stackSize - 1] = false;
						stack[stackSize - 2] = false;
						stackSize -= 2;
						break;
					// $CASES-OMITTED$
					default:
						if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
							sysew("UNKNOWN BRANCH OPCODE, EXTENSION NEEDED!");
						}
					}
					other = branch(stack, initilized ? null : stack.clone(), vars, initilized ? null : vars.clone(),
							stackSize, other, a.target());
					if (a.opcode() == Opcode.GOTO || a.opcode() == Opcode.GOTO_W) {
						for (int i = 0; i < stackSize; i++) {
							stack[i] = false;
						}
						for (int i = 0; i < vars.length; i++) {
							vars[i] = false;
						}
						stackSize = -1;
					}
				}
				case ConstantInstruction a -> stackSize += a.typeKind().slotSize();
				case ConvertInstruction c -> stackSize += c.toType().slotSize() - c.fromType().slotSize();
				case DiscontinuedInstruction a -> {
					switch (a.opcode().kind()) {
					case DISCONTINUED_JSR:
						if (a instanceof JsrInstruction jsr) {
							other = branch(stack, initilized ? null : stack.clone(), vars,
									initilized ? null : vars.clone(), stackSize, other, jsr.target());
						}
						//$FALL-THROUGH$
					case DISCONTINUED_RET:
						for (int i = 0; i < stackSize; i++) {
							stack[i] = false;
						}
						stackSize = -1;
						if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
							sysew("HEY, I NEED A STACK MAP TABLE!");
						}
						break;
					// $CASES-OMITTED$
					default:
						if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
							sysew("UNKNOWN DISCONTINUED OPCODE, EXTENSION NEEDED!");
						}
					}
				}
				case FieldInstruction a -> {
					int ss = TypeKind.from(a.field().typeSymbol()).slotSize();
					switch (a.opcode()) {
					case GETSTATIC -> stackSize += ss;
					case GETFIELD -> stackSize += ss - 1;
					case PUTSTATIC -> {
						stackSize -= ss;
						stack[stackSize] = false;
					}
					case PUTFIELD -> {
						stack[stackSize - 1] = false;
						stackSize -= ss + 1;
						stack[stackSize] = false;
					}
					// $CASES-OMITTED$
					default -> {
						throw new AssertionError("UNKNOWN FIELD ACCESS OPCODE, EXTENSION NEEDED!");
					}
					}
				}
				case InvokeDynamicInstruction a -> {
					MethodTypeDesc sym = a.typeSymbol();
					for (int i = 0, len = sym.parameterCount(); i < len; i++) {
						stackSize -= TypeKind.from(sym.parameterType(i)).slotSize();
						stack[stackSize] = false;
					}
					stackSize += TypeKind.from(sym.returnType()).slotSize();
				}
				case InvokeInstruction a -> {
					boolean initilizer = false, instance;
					switch (a.opcode()) {
					case INVOKESPECIAL -> {
						instance = true;
						if (!initilized && INIT_NAME_STR.equals(a.name().stringValue())) {
							initilizer = true;
						}
					}
					case INVOKEVIRTUAL, INVOKEINTERFACE -> {
						initilizer = false;
						instance = true;
					}
					case INVOKESTATIC -> {
						initilizer = false;
						instance = false;
					}
					// $CASES-OMITTED$
					default -> {
						if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
							sysew("UNKNOWN INVOKE OPCODE, EXTENSION NEEDED!");
						}
						initilizer = false;
						instance = true; // hopefully, may lead to an error
					}
					}
					MethodTypeDesc sym = a.typeSymbol();
					for (int i = sym.parameterCount(); --i >= 0;) {
						stackSize -= TypeKind.from(sym.parameterType(i)).slotSize();
						stack[stackSize] = false;
					}
					if (instance) {
						if (initilizer && !stack[stackSize - 1]) { // Initialize some other object
							initilizer = false;
						}
						stack[--stackSize] = false;
					}
					stackSize += TypeKind.from(sym.returnType()).slotSize();
					if (initilizer) {
						initilized = true;
						clear(vars, stack, stackSize);
						for (int i = 0; i < vars.length; i++) {
							vars[i] = false;
						}
						for (int i = 0; i < stackSize; i++) {
							stack[i] = false;
						}
						Label swap = cb.newBoundLabel();
						cb.with(inst);
						// TODO create swap here!!!
						cb.exceptionCatchAll(profileStart, swap, handlerFailureStart);
						Label oldSwap = swap;
						swap = cb.newBoundLabel();
						if (handlerMidStart != null) {
							cb.exceptionCatchAll(oldSwap, swap, handlerMidStart);
						}
						profileStart = swap;
						continue; // don't add the instruction twice!
					}
				}
				case LoadInstruction a -> {
					stack[stackSize] = vars[a.slot()];
					stackSize += a.typeKind().slotSize();
				}
				case StoreInstruction a -> {
					stackSize -= a.typeKind().slotSize();
					vars[a.slot()] = stack[stackSize];
					stack[stackSize] = false;
				}
				case IncrementInstruction a -> {
				}
				case LookupSwitchInstruction a -> {
					stackSize--;
					boolean[] varsCpy = initilized ? null : vars.clone();
					boolean[] stackCpy = initilized ? null : stack.clone();
					other = branch(stack, stackCpy, vars, varsCpy, stackSize, other, a.defaultTarget());
					for (SwitchCase sc : a.cases()) {
						other = branch(stack, stackCpy, vars, varsCpy, stackSize, other, sc.target());
					}
					stackSize = clear(vars, stack, stackSize);
				}
				case TableSwitchInstruction a -> {
					stackSize--;
					boolean[] varsCpy = initilized ? null : vars.clone();
					boolean[] stackCpy = initilized ? null : stack.clone();
					other = branch(stack, stackCpy, vars, varsCpy, stackSize, other, a.defaultTarget());
					for (SwitchCase sc : a.cases()) {
						other = branch(stack, stackCpy, vars, varsCpy, stackSize, other, sc.target());
					}
					stackSize = clear(vars, stack, stackSize);
				}
				case MonitorInstruction a -> {
					// TODO probably also time the monitor-enter/-exit instructions
				}
				case NewMultiArrayInstruction a -> stackSize -= a.dimensions() - 1;
				case NewObjectInstruction a -> stackSize++;
				case NewPrimitiveArrayInstruction a -> {
				}
				case NewReferenceArrayInstruction a -> {
				}
				case NopInstruction a -> {
				}
				case OperatorInstruction a -> {
					switch (a.opcode()) {
					case IUSHR, LUSHR, ISHL, LSHL, ISHR, LSHR -> stackSize--;
					case IADD, LADD, FADD, DADD, ISUB, LSUB, FSUB, DSUB, IMUL, LMUL, FMUL, DMUL, IDIV, LDIV, FDIV, DDIV,
							IREM, LREM, FREM, DREM, IAND, LAND, IOR, LOR, IXOR, LXOR ->
						stackSize -= a.typeKind().slotSize();
					case INEG, DNEG, LNEG, FNEG, ARRAYLENGTH -> {
					}
					case LCMP, FCMPL, FCMPG, DCMPL, DCMPG -> stackSize -= a.typeKind().slotSize() * 2 - 1;
					// $CASES-OMITTED$
					default -> {
						throw new AssertionError("UNKNOWN OPERATOR OPCODE, EXTENSION NEEDED!");
					}
					}
				}
				case StackInstruction a -> {
					switch (a.opcode()) {
					case POP -> stack[--stackSize] = false;
					case POP2 -> {
						stack[stackSize - 1] = false;
						stack[stackSize -= 2] = false;
					}
					case DUP -> {
						stack[stackSize] = stack[stackSize - 1];
						stackSize++;
					}
					case DUP_X1 -> {
						stack[stackSize] = stack[stackSize - 1];
						stack[stackSize - 1] = stack[stackSize - 2];
						stack[stackSize - 2] = stack[stackSize];
						stackSize++;
					}
					case DUP_X2 -> {
						stack[stackSize] = stack[stackSize - 1];
						stack[stackSize - 1] = stack[stackSize - 2];
						stack[stackSize - 2] = stack[stackSize - 3];
						stack[stackSize - 3] = stack[stackSize];
						stackSize++;
					}
					case DUP2 -> {
						stack[stackSize + 1] = stack[stackSize - 1];
						stack[stackSize] = stack[stackSize - 2];
						stackSize += 2;
					}
					case DUP2_X1 -> {
						stack[stackSize + 1] = stack[stackSize - 1];
						stack[stackSize] = stack[stackSize - 2];
						stack[stackSize - 1] = stack[stackSize - 3];
						stack[stackSize - 2] = stack[stackSize + 1];
						stack[stackSize - 3] = stack[stackSize];
						stackSize += 2;
					}
					case DUP2_X2 -> {
						stack[stackSize + 1] = stack[stackSize - 1];
						stack[stackSize] = stack[stackSize - 2];
						stack[stackSize - 1] = stack[stackSize - 3];
						stack[stackSize - 2] = stack[stackSize - 4];
						stack[stackSize - 3] = stack[stackSize + 1];
						stack[stackSize - 4] = stack[stackSize];
						stackSize += 2;
					}
					case SWAP -> {
						boolean b = stack[stackSize - 1];
						stack[stackSize - 1] = stack[stackSize - 2];
						stack[stackSize - 2] = b;
					}
					// $CASES-OMITTED$
					default -> throw new AssertionError("UNKNOWN STAK OPCODE, EXTENSION NEEDED!");
					}
				}
				case TypeCheckInstruction a -> {
					switch (a.opcode()) {
					case Opcode.INSTANCEOF -> stack[stackSize - 1] = false;
					case Opcode.CHECKCAST -> {
					}
					// $CASES-OMITTED$
					default -> {
						if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
							sysew("UNKNOWN TYPE CHECK OPCODE, EXTENSION NEEDED!");
						}
					}
					}
				}
				default -> throw new AssertionError("UNKNOWN INSTRUCTION TYPE, EXTENSION NEEDED!");
				}
				cb.with(inst);
			}
			case LabelTarget l -> {
				Label lbl = l.label();
				boolean done = false;
				boolean oldInitilized = initilized;
				int oldStackSize = stackSize;
				if (infs != null) {
					for (StackMapFrameInfo smi : infs) { // remember index and just add one?
						if (smi.target().equals(lbl)) {
							initilized = true;
							int i = 0;
							for (VerificationTypeInfo vti : smi.locals()) {
								if (vti.tag() == SimpleVerificationTypeInfo.ITEM_UNINITIALIZED_THIS.tag()) {
									initilized = false;
									vars[i++] = true;
								} else {
									vars[i++] = false;
									if (vti.tag() == SimpleVerificationTypeInfo.ITEM_DOUBLE.tag()
											|| vti.tag() == SimpleVerificationTypeInfo.ITEM_LONG.tag()) {
										vars[i++] = false;
									}
								}
							}
							stackSize = 0;
							for (VerificationTypeInfo vti : smi.locals()) {
								if (vti.tag() == SimpleVerificationTypeInfo.ITEM_UNINITIALIZED_THIS.tag()) {
									initilized = false;
									stack[stackSize++] = true;
								} else {
									stack[stackSize++] = false;
									if (vti.tag() == SimpleVerificationTypeInfo.ITEM_DOUBLE.tag()
											|| vti.tag() == SimpleVerificationTypeInfo.ITEM_LONG.tag()) {
										vars[stackSize++] = false;
									}
								}
							}
							break;
						}
					}
				}
				if (!done && other != null) {
					try {
						int s = other.applyAsInt(lbl, other);
						if (s != -1) {
							stackSize = s;
							initilized = true;
							for (int i = 0; i < vars.length; i++) {
								if (vars[i]) {
									initilized = false;
									break;
								}
							}
							if (initilized) {
								for (int i = 0; i < stackSize; i++) {
									if (stack[i]) {
										initilized = false;
										break;
									}
								}
							}
						} else if (stackSize != -1) {
							// only add if not yet added
							other = branch(stack, initilized ? null : stack.clone(), vars,
									initilized ? null : vars.clone(), stackSize, other, lbl);
						}
					} catch (StackOverflowError s) {
						throw s;
					}
				}
				if (oldInitilized != initilized) {
					if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
						if (oldStackSize != -1) {
							throw new AssertionError(oldStackSize);
						}
					}
					Label swap = cb.newBoundLabel();
					cb.exceptionCatchAll(profileStart, swap, oldInitilized ? handlerSuccessStart : handlerFailureStart);
					profileStart = swap;
				}
				cb.with(ce);
			}
			case PseudoInstruction p -> cb.with(ce);
			default -> {
			}
			}
		}
		Label handler0Start = initilized ? handlerSuccessStart : handlerFailureStart;
		Label handler1Start = initilized ? handlerFailureStart : handlerSuccessStart;
		Label handler2Start = handlerMidStart;
		cb.labelBinding(handler0Start);
		if (initilized && handler2Start == null) {
			exit(cb, vars.length);
		} else {
			failExitInit(cb, vars.length);
		}
		Label handler0End = cb.newBoundLabel();
		cb.athrow();
		cb.labelBinding(handler1Start);
		if (initilized && handler2Start == null) {
			failExitInit(cb, vars.length);
		} else {
			exit(cb, vars.length);
		}
		Label handler1End = cb.newBoundLabel();
		cb.athrow();
		cb.exceptionCatchAll(profileStart, handler0End, handler0Start);
		cb.exceptionCatchAll(handler1Start, handler1End, handler1Start);
		if (handler2Start != null) {
			cb.labelBinding(handler2Start);
			exit(cb, vars.length);
			cb.athrow();
//			cb.exceptionCatchAll(profileStart, handler0End, handler0Start); // XXX does not work, just hope that no second exception is thrown
		}
		// XXX seems to be ignored by the builder:
//		if (infs == null) {
//			infs = List.of();
//		}
//		StackMapFrameInfo[] ninfs = new StackMapFrameInfo[infs.size() + 2];
//		for (int i = 0, len = infs.size(); i < len; i++) {
//			StackMapFrameInfo inf = infs.get(i);
//			ninfs[i] = StackMapFrameInfo.of(inf.target(), inf.locals(), inf.stack());
//		}
//		MethodTypeDesc mtype = mm.methodTypeSymbol();
//		VerificationTypeInfo[] locals = new VerificationTypeInfo[1 
//		                                                         + mtype.parameterCount()
//		                                                         ];
//		for (int i = 1; i < locals.length; i++) {
//			ClassDesc pt = mtype.parameterType(i - 1);
//			if (pt.isPrimitive()) {
//				TypeKind tk = TypeKind.from(pt);
//				switch (tk) {
//				case BooleanType, ByteType, CharType, IntType, ShortType ->
//					locals[i] = SimpleVerificationTypeInfo.ITEM_INTEGER;
//				case DoubleType -> locals[i] = SimpleVerificationTypeInfo.ITEM_DOUBLE;
//				case FloatType -> locals[i] = SimpleVerificationTypeInfo.ITEM_FLOAT;
//				case LongType -> locals[i] = SimpleVerificationTypeInfo.ITEM_LONG;
//				case ReferenceType, VoidType -> throw new AssertionError(tk + " : " + pt);
//				default -> throw new AssertionError(tk + " : " + pt);
//				}
//			} else {
//				locals[i] = ObjectVerificationTypeInfo.of(pt);
//			}
//		}
//		locals[0] = initilized ? ObjectVerificationTypeInfo.of(cls.model().thisClass())
//				: SimpleVerificationTypeInfo.ITEM_UNINITIALIZED_THIS;
//		List<VerificationTypeInfo> stackVTI = List.of(ObjectVerificationTypeInfo.of(THROWABLE_CLASS));
//		ninfs[ninfs.length - 2] = StackMapFrameInfo.of(handler0Start, List.of(locals), stackVTI);
//		locals[0] = initilized ? SimpleVerificationTypeInfo.ITEM_UNINITIALIZED_THIS
//				: ObjectVerificationTypeInfo.of(cls.model().thisClass());
//		ninfs[ninfs.length - 1] = StackMapFrameInfo.of(handler1Start, List.of(locals), stackVTI);
//		cb.with(StackMapTableAttribute.of(List.of(ninfs))); 
	}

	private static int clear(boolean[] vars, boolean[] stack, int stackSize) {
		for (int i = 0; i < vars.length; i++) {
			vars[i] = false;
		}
		for (int i = 0; i < stackSize; i++) {
			stack[i] = false;
		}
		stackSize = -1;
		return stackSize;
	}

	private static ToIntBiFunction<Label, ToIntBiFunction<Label, ?>> branch(boolean[] stack, boolean[] stackCpy,
			boolean[] vars, boolean[] varsCpy, int stackSize, ToIntBiFunction<Label, ToIntBiFunction<Label, ?>> other,
			Label target) {
		return (lbl, o) -> {
			if (lbl.equals(target)) {
				for (int i = 0; i < vars.length; i++) {
					vars[i] = varsCpy == null ? false : varsCpy[i];
				}
				for (int i = 0; i < stackSize; i++) {
					stack[i] = stackCpy == null ? false : stackCpy[i];
				}
				if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
					for (int i = stackSize; i < stack.length; i++) {
						if (stack[i]) {
							throw new AssertionError();
						}
					}
				}
				return stackSize;
			}
			if (other == null) {
				return -1;
			}
			return other.applyAsInt(lbl, o);
		};
	}

	private boolean transformWrap(ClassFileClassBuffer cls, String clsName, Class<?> classBeingRedefined,
			ClassBuilder ct, MethodModel mm) {
		if (classBeingRedefined != null) {
			return false;
		}
		for (MethodElement me : mm.elements()) {
			if (me instanceof RuntimeVisibleAnnotationsAttribute annots) {
				for (Annotation annot : annots.annotations()) {
					String clsNam = annot.className().stringValue();
					if (CALLER_SENSITIVE0_ANNOT_NAME_STR.equals(clsNam)
							|| CALLER_SENSITIVE1_ANNOT_NAME_STR.equals(clsNam)
							|| POLYMORPHIC_SIGNATURE_ANNOT_NAME_STR.equals(clsNam)
							|| INTRINSIC_CANDIDATE_ANNOT_NAME_STR.equals(clsNam)) {
						return false; // don't wrap those!
					}
				}
			}
		}
		final int flags = mm.flags().flagsMask();
		String mname = mm.methodName().stringValue();
		if (mname.charAt(0) == '<') {
			return false;
		}
		String prefixedName = PREFIX_STR + mname;
		final int synFlags = flags | AccessFlag.SYNTHETIC.mask(); // | AccessFlag.BRIDGE.mask(); // ?
		ct.withMethod(prefixedName, mm.methodTypeSymbol(), synFlags, mb -> {
			for (MethodElement me : mm.elements()) {
				if (me instanceof ExceptionsAttribute || me instanceof MethodParametersAttribute
						|| me instanceof SignatureAttribute || me instanceof SyntheticAttribute
						|| me instanceof CodeAttribute) {
					mb.with(me);
				} else if (me instanceof AccessFlags) {
					mb.with(AccessFlags.ofClass(synFlags));
				}
			}
		});
		ClassEntry thisClass = cls.model().thisClass();
		ct.withMethod(mname, mm.methodTypeSymbol(), flags, mb -> {
			for (MethodElement me : mm.elements()) {
				if (me instanceof AccessFlags || me instanceof ExceptionsAttribute
						|| me instanceof MethodParametersAttribute || me instanceof SignatureAttribute
						|| me instanceof SyntheticAttribute || me instanceof RuntimeVisibleAnnotationsAttribute
						|| me instanceof RuntimeVisibleParameterAnnotationsAttribute
						|| me instanceof RuntimeVisibleTypeAnnotationsAttribute
						|| me instanceof AnnotationDefaultAttribute || me instanceof DeprecatedAttribute) {
					mb.with(me);
				}
			}
			mb.withCode(c -> {
				MethodTypeDesc mtype = mm.methodTypeSymbol();
				int myLocal = 0;
				for (int i = 0, l = mtype.parameterCount(); i < l; i++) {
					myLocal += TypeKind.from(mtype.parameterType(i)).slotSize();
				}
				if (!mm.flags().has(AccessFlag.STATIC)) {
					myLocal += TypeKind.ReferenceType.slotSize();
				}
				enter(clsName, c, mname, mtype.descriptorString(), myLocal);
				Label profileStart = c.newBoundLabel();
				int vi = 0;
				if ((flags & AccessFlag.STATIC.mask()) == 0) {
					c.aload(0);
					vi = 1;
				}
				for (int i = 0, len = mtype.parameterCount(); i < len; i++) {
					ClassDesc param = mtype.parameterType(i);
					c.loadLocal(TypeKind.from(param), vi);
					vi += TypeKind.from(param).slotSize();
				}
				if (myLocal != vi) {
					throw new AssertionError();
				}
				Opcode op = (flags & AccessFlag.STATIC.mask()) == 0 ? Opcode.INVOKESPECIAL : Opcode.INVOKESTATIC;
				c.invoke(op, thisClass.asSymbol(), prefixedName, mtype, cls.model().flags().has(AccessFlag.INTERFACE));
				exit(c, myLocal);
				ClassDesc r = mtype.returnType();
				c.return_(TypeKind.from(r));
				Label handlerStart = c.newBoundLabel();
				exit(c, myLocal);
				Label profileEnd = c.newBoundLabel();
				c.athrow();
				c.exceptionCatchAll(profileStart, profileEnd, handlerStart);
			});
		});
		return true;
	}

	@Override
	protected byte[] transformMyInternBootstrap(ClassFileClassBuffer cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			System.out.println("BOOTSTRAP: " + clsName + "\n");
		}
		return ClassFile.of().transform(cls.model(), (b, e) -> {
			if (e instanceof MethodModel m) {
				MethodTypeDesc mtype = m.methodTypeSymbol();
				if (SUK_MISC_UNSAFE_DESC_STR.equals(mtype.returnType().descriptorString())) {
					mtype = MethodTypeDesc.of(SUN_MISC_UNSAFE_CLASS, mtype.parameterArray());
				}
				b.withMethod(m.methodName().stringValue(), mtype, m.flags().flagsMask(), mt -> {
					for (MethodElement me : m.elements()) {
						if (me instanceof CodeAttribute ca) {
							mt.transformCode(ca, (t, ce) -> {
								if (ce instanceof FieldInstruction ci
										&& SUK_MISC_UNSAFE_DESC_STR.equals(ci.type().stringValue())) {
									t.fieldAccess(ci.opcode(), ci.owner().asSymbol(), ci.name().stringValue(),
											SUN_MISC_UNSAFE_CLASS);
									return;
								} else if (ce instanceof InvokeInstruction ii) {
									MethodTypeDesc itype = ii.typeSymbol();
									ClassDesc own = ii.owner().asSymbol();
									if (SUK_MISC_UNSAFE_DESC_STR.equals(itype.returnType().descriptorString())
											|| SUK_MISC_UNSAFE_DESC_STR.equals(own.descriptorString())) {
										if (SUK_MISC_UNSAFE_DESC_STR.equals(own.descriptorString())) {
											own = SUN_MISC_UNSAFE_CLASS;
										}
										if (SUK_MISC_UNSAFE_DESC_STR.equals(itype.returnType().descriptorString())) {
											itype = MethodTypeDesc.of(SUN_MISC_UNSAFE_CLASS, itype.parameterArray());
										}
										t.invoke(ii.opcode(), own, ii.name().stringValue(), itype, ii.isInterface());
										return;
									}
								} else if (ce instanceof StackMapTableAttribute smt) {
									List<StackMapFrameInfo> smte = smt.entries();
									StackMapFrameInfo[] nsmt = null;
									for (int i = 0; i < smte.size(); i++) {
										StackMapFrameInfo smi = smte.get(i);
										List<VerificationTypeInfo> olocals = smi.locals();
										VerificationTypeInfo[] locals = nsmt == null ? null
												: new VerificationTypeInfo[olocals.size()];
										for (int ii = 0; ii < olocals.size(); ii++) {
											VerificationTypeInfo vti = olocals.get(ii);
											if (vti instanceof ObjectVerificationTypeInfo o && SUK_MISC_UNSAFE_DESC_STR
													.equals(o.classSymbol().descriptorString())) {
												if (locals == null) {
													nsmt = new StackMapFrameInfo[smte.size()];
													for (int si = 0; si < i; si++) {
														nsmt[si] = smte.get(si);
													}
													locals = new VerificationTypeInfo[olocals.size()];
													for (int si = 0; si < ii; si++) {
														locals[si] = olocals.get(si);
													}
												}
												locals[ii] = SUN_MISC_UNSAFE_VERIFY;
											} else {
												locals[ii] = vti;
											}
										}
										List<VerificationTypeInfo> ostack = smi.stack();
										VerificationTypeInfo[] stack = nsmt == null ? null
												: new VerificationTypeInfo[olocals.size()];
										for (int ii = 0; ii < ostack.size(); ii++) {
											VerificationTypeInfo vti = ostack.get(ii);
											if (vti instanceof ObjectVerificationTypeInfo o && SUK_MISC_UNSAFE_DESC_STR
													.equals(o.classSymbol().descriptorString())) {
												if (stack == null) {
													nsmt = new StackMapFrameInfo[smte.size()];
													for (int si = 0; si < i; si++) {
														nsmt[si] = smte.get(si);
													}
													locals = olocals.toArray(new VerificationTypeInfo[olocals.size()]);
													stack = new VerificationTypeInfo[ostack.size()];
													for (int si = 0; si < ii; si++) {
														stack[si] = ostack.get(si);
													}
												}
												stack[ii] = SUN_MISC_UNSAFE_VERIFY;
											} else {
												stack[ii] = vti;
											}
										}
										if (nsmt != null) {
											nsmt[i] = StackMapFrameInfo.of(smi.target(), List.of(locals),
													List.of(stack));
										}
									}
									if (nsmt != null) {
										t.with(StackMapTableAttribute.of(List.of(nsmt)));
										return;
									}
								}
								t.with(ce);
							});
						} else {
							mt.with(me);
						}
					}
				});
				return;
			}
			if (e instanceof FieldModel f && SUK_MISC_UNSAFE_DESC_STR.equals(f.fieldType().stringValue())) {
				b.withField(f.fieldName().stringValue(), SUN_MISC_UNSAFE_CLASS, fb -> {
					fb.withFlags(f.flags().flagsMask());
					for (FieldElement fe : f.elementList()) {
						if (fe instanceof DeprecatedAttribute || fe instanceof RuntimeVisibleAnnotationsAttribute
								|| fe instanceof RuntimeVisibleTypeAnnotationsAttribute
								|| fe instanceof SyntheticAttribute) {
							fb.with(fe);
						}
					}
				});
				return;
			}
			b.with(e);
		});
	}

	@Override
	protected byte[] transformSunMiscUnsafe(ClassFileClassBuffer cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		if (!replaceGetUnsafe()) {
			return null;
		}
		return ClassFile.of().transform(cls.initModel(), (ct, ce) -> {
			if (ce instanceof MethodModel mm) {
				Utf8Entry mName = mm.methodName();
				if (SUN_MISC_UNSAFE_GET_UNSAFE_NAME_STR.equals(mName.stringValue())) {
					Utf8Entry mType = mm.methodType();
					if (SUN_MISC_UNSAFE_GET_UNSAFE_DESC_STR.equals(mType.stringValue())) {
						ct.withMethodBody(mName, mType, AccessFlag.PUBLIC.mask() | AccessFlag.STATIC.mask(), c -> {
							ClassDesc sunMiscUnsafe = cls.model().thisClass().asSymbol();
							c.putfield(sunMiscUnsafe, SUN_MISC_UNSAFE_THE_UNSAFE_STR, sunMiscUnsafe);
							c.areturn();
						});
						return;
					}
				}
			}
			ct.with(ce);
		});
	}

	@Override
	@SuppressWarnings("unused")
	protected byte[] transformPProfilerAgent(ClassFileClassBuffer cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		String oldName = PProfilerAgent.name();
		int i = oldName.indexOf('[');
		if (i < 0) {
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("PProfilerAgent.name() does not contains a \'[\'\n");
			}
			return null;
		}
		if (i >= oldName.length() - 1) {
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("how often do you want to modify PProfilerAgent?\n");
			}
			return null;
		}
		final String newName;
		if (i >= oldName.length() - 3) {
			newName = oldName.substring(0, i + 1).concat(oldName.substring(i + 2));
		} else {
			newName = oldName.substring(0, i + 1).concat(oldName.substring(i + 3));
		}
		return ClassFile.of().transform(cls.initModel(), (ct, ce) -> {
			if (ce instanceof FieldModel fm && AGENT_FIELD_NAME_STR.equals(fm.fieldName().stringValue())) {
				ct.transformField(fm, (ft, fe) -> {
					if (fe instanceof ConstantValueAttribute c) {
						ft.with(ConstantValueAttribute.of(oldName));
						return;
					}
					ft.with(fe);
				});
				return;
			} else if (ce instanceof MethodModel mm) {
				Utf8Entry mName = mm.methodName();
				if (AGENT_METHOD_NAME_STR.equals(mName.stringValue())) {
					Utf8Entry mType = mm.methodType();
					if (AGENT_METHOD_DESC_STR.equals(mType.stringValue())) {
						ct.withMethodBody(mName, mType, mm.flags().flagsMask(), c -> {
							c.loadConstant(newName);
							c.areturn();
						});
						return;
					}
				}
			}
			ct.with(ce);
		});
	}

}
