// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import java.lang.classfile.ClassFile;
import java.lang.classfile.ClassModel;

/**
 * the java23 implementation of the {@code classfile-api} class transformer<br>
 * does not use the java version suffix, because the class is build with {@code --enable-preview}
 */
public class ClassFileClassBuffer extends ClassBuffer {

	private Module module;
	private ClassLoader loader;
	private ClassModel model;

	public ClassFileClassBuffer(Module module, ClassLoader loader, byte[] buffer) {
		super(buffer);
		this.module = module;
		this.loader = loader;
	}

	public ClassLoader loader() {
		return this.loader;
	}

	public Module module() {
		return this.module;
	}

	public ClassModel initModel() {
		return this.model = ClassFile.of().parse(this.buffer());
	}

	public ClassModel model() {
		return this.model;
	}

}
