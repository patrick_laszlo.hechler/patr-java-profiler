// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import java.lang.instrument.Instrumentation;

import de.hechler.patrick.profiler.agent.transform.CTs;
import de.hechler.patrick.profiler.agent.transform.CTs23;

/**
 * the java23 {@link PProfilerAgent} wrapper, only works if preview features are
 * enabled.
 *
 * @see #premain(String, Instrumentation)
 */
public class PJPAgentJCF {

	private PJPAgentJCF() {
	}

	/**
	 * invoked by the java9 {@link PJPAgent#premain(String, Instrumentation)
	 * premain} if the JVM version is 23.<br>
	 * if preview features are enabled, the new Class-File-API is used. if not this
	 * method fails and the java9 premain will fall back to the old ClassTransformer
	 *
	 * @param agentArgs the arguments to be parsed
	 * @param inst      the {@link Instrumentation}
	 */
	public static void premain(String agentArgs, Instrumentation inst) {
		CTs.init(new CTs23());
		PProfilerAgent.premain(agentArgs, inst);
	}

}
