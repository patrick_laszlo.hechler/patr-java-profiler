// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

public class WorstClassTransformer extends ClassFileClassTransformer {

	@Override
	protected boolean swapToMy() {
		return isSet(this.swapToMy, true);
	}
	
}
