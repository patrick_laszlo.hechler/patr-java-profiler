// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

public class WrapClassTransformer23 extends WorstClassTransformer {
	
	public WrapClassTransformer23() {
		super();
		super.allowToInline = -1;
		super.preferToWrap = 1;
	}
	
}
