// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import java.util.Arrays;

import de.hechler.patrick.profiler.agent.PProfilerAgent;

public class CTs23 extends CTs9 {

	@Override
	public String[] allImpl() {
		String[] all = super.allImpl();
		all = Arrays.copyOf(all, all.length + 2);
		all[all.length - 2] = "classfile-api";
		all[all.length - 1] = "worst";
		return all;
	}

	@Override
	public String descImpl(String name) {
		switch (name) {
		case "classfile-api":
			return "uses the classfile api to transform classes";
		case "worst":
			return "uses the 'best' available transformation method for each class/method";
		default:
			return super.descImpl(name);
		}
	}

	@Override
	public AbstractClassTransformer<?, ?> getImpl(String name, int off, int end) {
		if (PProfilerAgent.isArg(off, end, name, "classfile-api")) {
			return new ClassFileClassTransformer();
		}
		if (PProfilerAgent.isArg(off, end, name, "worst")) {
			return new WorstClassTransformer();
		}
		if (PProfilerAgent.isArg(off, end, name, "default")) {
			return new DefaultClassTransformer23();
		}
		if (PProfilerAgent.isArg(off, end, name, "wrap")) {
			return new WrapClassTransformer23();
		}
		return super.getImpl(name, off, end);
	}

	@Override
	public String nameImpl(AbstractClassTransformer<?, ?> ct) {
		switch (ct) {
		case WrapClassTransformer23 c:
			return "wrap";
		case WorstClassTransformer c:
			return "worst";
		case ClassFileClassTransformer c:
			return "classfile-api";
		default:
			return super.nameImpl(ct);
		}
	}

}
