// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

public class InlineClassTransformer extends WorstClassTransformer {
	
	public InlineClassTransformer() {
		super();
		super.allowToInline = 1;
		super.preferToWrap = -1;
	}
	
}
