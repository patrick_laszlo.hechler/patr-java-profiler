// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class MyWrapClassTransformer9 extends MyWrapClassTransformer8 {

	@Override
	public byte[] transform(Module module, ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		return transformCaller(module.isNamed() ? module.getName() : null, loader, className, classBeingRedefined, protectionDomain, classfileBuffer);
	}

}
