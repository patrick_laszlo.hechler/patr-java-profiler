// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import java.util.Arrays;

import de.hechler.patrick.profiler.agent.PProfilerAgent;

public class CTs9 extends CTs8 {

	@Override
	public String[] allImpl() {
		String[] all = super.allImpl();
		all = Arrays.copyOf(all, all.length + 1);
		all[all.length - 1] = "wrap-impl9";
		return all;
	}

	@Override
	public String descImpl(String name) {
		switch (name) {
		case "my-wrap8":
			return "same as wrap-impl, but is not module aware";
		case "my-wrap9":
			return "same as wrap-impl";
		default:
			return super.descImpl(name);
		}
	}

	@Override
	public AbstractClassTransformer<?, ?> getImpl(String name, int off, int end) {
		if (PProfilerAgent.isArg(off, end, name, "my-wrap9")) {
			return new MyWrapClassTransformer9();
		}
		return super.getImpl(name, off, end);
	}
	
	@Override
	public String nameImpl(AbstractClassTransformer<?, ?> ct) {
		if (ct instanceof MyWrapClassTransformer9) {
			return "my-wrap";
		}
		if (ct instanceof MyWrapClassTransformer8) {
			return "my-wrap8";
		}
		return super.nameImpl(ct);
	}
	
}
