// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import java.lang.instrument.Instrumentation;

/**
 * overwritten by the java23 impl and only used if java version is 23.<br>
 * this allows to compile with preview features, even if the JVM does not
 * support them
 */
public class PJPAgentJFC {

	private PJPAgentJFC() {
	}

	/**
	 * overwritten by the java23 impl and only used if java version is 23.<br>
	 * this allows to compile with preview features, even if the JVM does not
	 * support them
	 */
	public static void premain(String agentArgs, Instrumentation inst) {
		throw new AssertionError();
	}

}
