// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

/**
 * used to represent a complete {@link ModificationList}
 */
public class ModificationListStart extends ModificationList {
	
	/**
	 * the last entry in this list
	 */
	public ModificationList end;
	
	/**
	 * the change of the overall class file size done by the complete list
	 */
	public int sizeMod;
	
	/**
	 * create a new {@link ModificationListStart}
	 */
	public ModificationListStart() {
		super();
	}
	
	@Override
	public String toString() {
		if ( end == null && next == null ) {
			return "nothing";
		}
		return super.toString();
	}
	
}
