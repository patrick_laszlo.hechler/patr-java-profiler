// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import static de.hechler.patrick.profiler.agent.ClassBuffer.*;

/**
 * this class represents an Attribute in a class file of a method, field or whatever
 */
public class Attribute {
	
	public int offset;
	public int end;
	
	/**
	 * creates a new {@link Attribute}
	 * <p>note that the initial attribute is invalid, until {@code offset} and {@code end} is set.<br>
	 * {@link #isName(ClassBuffer, int)} can be used as soon as {@code offset} is set ({@code end} is ignored)
	 */
	public Attribute() {}
	
	/**
	 * checks if this attribute has the given name
	 * 
	 * @param cf
	 *                    the class file
	 * @param nameCPE
	 *                    the constant pool index of the name
	 * 					
	 * @return <code>true</code> if this attribute has the given name and <code>false</code> if not
	 */
	public boolean isName(ClassBuffer cf, int nameCPE) {
		byte[] d = cf.buffer();
		int myNameCPE = getUShort(d, offset);
		return myNameCPE == nameCPE;
	}
	
}
