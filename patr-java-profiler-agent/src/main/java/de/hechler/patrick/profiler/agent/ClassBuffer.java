// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import static de.hechler.patrick.profiler.agent.PProfilerAgent.LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_ERR_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_OUT_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysew;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysw;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.lang.instrument.IllegalClassFormatException;

/**
 * this class wraps around the binary data of an class file and allows their
 * modification
 */
public class ClassBuffer {
	
	public static final byte[] CODE_ATTR_NAME = CTHelp.getBytes(CTHelp.CODE_ATTR_STR);
	
	private byte[]        data;
	private ConstantPool  constPool = new ConstantPool();
	private int           fieldsEnd;
	private MethodList    mets;
	private int           methodsEnd;
	private AttributeList attrs;
	
	/**
	 * the earliest class file version that is supported.<br>
	 * class files with a version below this value will trigger an error
	 */
	public static final int MIN_VERSION             = 45;
	/**
	 * the class file version which introduced the stack map
	 */
	public static final int STACK_TABLE_MIN_VERSION = 50;
	/**
	 * the maximum supported version.<br>
	 * class files with a newer version will trigger a warning (and potential errors
	 * if new features are used)
	 */
	public static final int MAX_VERSION             = 67;
	
	public static final int OFF_MAGIC    = 0;
	public static final int OFF_VERSION  = OFF_MAGIC + 4 + 2;
	public static final int OFF_CP_LENP1 = OFF_VERSION + 2;
	public static final int OFF_CP_DATA  = OFF_CP_LENP1 + 2;
	
	/**
	 * create a new {@link ClassBuffer} from the given class file
	 * 
	 * @param data the class files binary data
	 */
	public ClassBuffer(byte[] data) {
		this.data = data;
	}
	
	/**
	 * returns the binary data of the class file
	 * <p>
	 * note that a modifiable array is returned:<br>
	 * after modifications that change the structure of the class file (for example
	 * resize a UTF entry in the constant pool or modify a method) the methods
	 * {@link #readCP()} and {@link #readMethods()} should be called to refresh the
	 * cache, before the instance is used again
	 * 
	 * @return the binary data of the class file
	 * 
	 * @see #data(byte[])
	 */
	public byte[] buffer() {
		return data;
	}
	
	/**
	 * set the class file buffer and reset all cached values.
	 * 
	 * @param data the new class file buffer
	 */
	public void buffer(byte[] data) {
		this.data = data;
		this.constPool.offsets = null;
		this.constPool.end = 0;
		this.fieldsEnd = 0;
		this.mets = null;
		this.methodsEnd = 0;
		this.attrs = null;

	}
	
	/**
	 * sets the binary data of the class file<br>
	 * the methods {@link #readCP()} and {@link #readMethods()} should be called to
	 * refresh the cache, before the instance is used again
	 * 
	 * @param data the new binary data of this class file
	 * 
	 * @see #buffer()
	 */
	public void data(byte[] data) {
		this.data = data;
	}
	
	/**
	 * returns the constant pool of this {@link ClassBuffer}
	 * 
	 * @return the constant pool of this {@link ClassBuffer}
	 */
	public ConstantPool constPool() {
		return constPool;
	}
	
	/**
	 * returns the fields end of this {@link ClassBuffer}
	 * 
	 * @return the fields end of this {@link ClassBuffer}
	 */
	public int fieldsEnd() {
		return fieldsEnd;
	}
	
	/**
	 * returns the method list of this {@link ClassBuffer}
	 * 
	 * @return the method list of this {@link ClassBuffer}
	 */
	public MethodList methods() {
		return mets;
	}
	
	/**
	 * returns the method end of this {@link ClassBuffer}
	 * 
	 * @return the method end of this {@link ClassBuffer}
	 */
	public int methodsEnd() {
		return methodsEnd;
	}
	
	/**
	 * returns the (class) attribute list of this {@link ClassBuffer}
	 * 
	 * @return the (class) attribute list of this {@link ClassBuffer}
	 */
	public AttributeList attributes() {
		return attrs;
	}
	
	// here are only the flags that (may) be needed
	
	/** the access flag for {@code interface} class files */
	public static final int CLS_ACC_INTERFACE  = 0x0200;
	/** the access flag for {@code @interface} class files */
	public static final int CLS_ACC_ANNOTATION = 0x2000;
	/** the access flag for {@code enum} class files */
	public static final int CLS_ACC_ENUM       = 0x4000;
	/** the access flag for {@code module} class files */
	public static final int CLS_ACC_MODULE     = 0x8000;
	
	/** the access flag for {@code abstract} methods */
	public static final int MET_ACC_PUBLIC    = 0x0001;
	/** the access flag for {@code abstract} methods */
	public static final int MET_ACC_PRIVATE   = 0x0002;
	/** the access flag for {@code abstract} methods */
	public static final int MET_ACC_PROTECTED = 0x0004;
	/** the access flag for {@code static} methods */
	public static final int MET_ACC_STATIC    = 0x0008;
	/** the access flag for {@code bridge} methods */
	public static final int MET_ACC_BRIDGE    = 0x0040;
	/** the access flag for {@code native} methods */
	public static final int MET_ACC_NATIVE    = 0x0100;
	/** the access flag for {@code abstract} methods */
	public static final int MET_ACC_ABSTRACT  = 0x0400;
	/** the access flag for {@code synthetic} methods */
	public static final int MET_ACC_SYNTHETIC = 0x1000;
	
	/**
	 * returns the access flags of the class file
	 * <p>
	 * this method <b>MUST NOT</b> be called before {@link #readCP()} was called
	 * 
	 * @return the access flags of the class file
	 */
	public int accessFlags() {
		return getUShort(data, constPool.end);
	}
	
	/**
	 * returns the name of this class in modified-UTF-8 format
	 * 
	 * @return the name of this class in modified-UTF-8 format
	 */
	public byte[] name() {
		byte[] d = data;
		ConstantPool cp = constPool;
		int[] offs = cp.offsets;
		int off = offs[getUShort(d, cp.end + 2) - 1];
		off = offs[getUShort(d, off + 1) - 1];
		int len = getUShort(d, off + 1);
		byte[] name = new byte[len];
		System.arraycopy(d, off + 3, name, 0, len);
		return name;
	}
	
	/**
	 * checks if the version of the class file is supported
	 * 
	 * @throws IllegalClassFormatException if the class file does not start with the
	 *                                     magic or if the class file is really old
	 */
	public void checkVersion() throws IllegalClassFormatException {
		byte[] d = data;
		int magic = getInt(d, OFF_MAGIC);
		if (magic != 0xCAFEBABE) {
			throw new IllegalClassFormatException("invalid magic");
		}
		int v = getUShort(d, OFF_VERSION);
		if (v < MIN_VERSION) {
			throw new IllegalClassFormatException("version is too low");
		}
		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			if (v > MAX_VERSION) {
				sysew("[de.hechler.patrick.profiler.ClassFile]: WARN: class file version above max version: " + v);
			}
		}
	}
	
	/**
	 * returns the version of the class file
	 * 
	 * @return the version of the class file
	 */
	public int version() {
		return getUShort(data, OFF_VERSION);
	}
	
	/**
	 * update the constant pool
	 * 
	 * @throws IllegalClassFormatException if the class file is invalid (there is no
	 *                                     guarantee that an invalid class file will
	 *                                     trigger an exception)
	 */
	public void readCP() throws IllegalClassFormatException {
		checkVersion();
		byte[] d = data;
		ConstantPool cp = constPool;
		cp.update(d);
	}
	
	/**
	 * transforms the class file so that the it can be profiled
	 * 
	 * @throws IllegalClassFormatException if the class file is invalid (there is no
	 *                                     guarantee that an invalid class file will
	 *                                     trigger an exception)
	 * 									
	 * @return the {@link MethodList} containing all methods of this class
	 */
	public MethodList readMethods() throws IllegalClassFormatException {
		byte[] d = data;
		int off = skipInterfacesAndFields();
		int mcnt = getUShort(d, off);
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			logMethodCount(mcnt);
		}
		off += 2;
		if (mcnt == 0) {
			methodsEnd = off;
			mets = null;
			return null;
		}
		MethodList metL = null;
		while (--mcnt >= 0) {
			Method met = readMethod(d, off);
			off = met.end;
			MethodList nMetL = new MethodList();
			nMetL.met = met;
			if (metL == null) {
				mets = nMetL;
			} else {
				metL.next = nMetL;
			}
			metL = nMetL;
		}
		methodsEnd = off;
		return mets;
	}
	
	/**
	 * parses a method
	 * 
	 * @param md   the method data
	 * @param moff the offset of the method
	 * 
	 * @return the parsed method
	 */
	public Method readMethod(byte[] md, int moff) {
		ConstantPool cp = constPool;
		byte[] d = data;
		Method met = new Method();
		met.offset = moff;
		int acnt = getUShort(md, moff + 6);
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			int metAccFlags = met.accessFlags(md);
			sysw("  the method ");
			if (( metAccFlags & MET_ACC_NATIVE ) != 0) {
				sysw("native ");
			}
			if (( metAccFlags & MET_ACC_SYNTHETIC ) != 0) {
				sysw("synthetic ");
			}
			if (( metAccFlags & MET_ACC_BRIDGE ) != 0) {
				sysw("bridge ");
			}
			int metNameOff = met.nameOff(cp, md, moff);
			int metNameLen = getUShort(d, metNameOff + 1);
			sysw(d, metNameOff + 3, metNameLen);
			int metDescOff = met.descriptorOff(cp, md, moff);
			int metDescLen = getUShort(d, metDescOff + 1);
			sysw(d, metDescOff + 3, metDescLen);
			sysw(" has ");
			sysw(String.valueOf(acnt));
			sysw(" attributs\n");
		}
		moff += 8;
		AttributeList lastAttrL = null;
		while (--acnt >= 0) {
			Attribute attr = new Attribute();
			attr.offset = moff;
			AttributeList nAttrL = new AttributeList();
			nAttrL.attr = attr;
			int attrNameOff = cp.offsets[getUShort(md, moff) - 1];
			if (LOG_LEVEL > MIN_OUT_LOG_LEVEL) {
				int attrNameLen = getUShort(d, attrNameOff + 1);
				sysw("    the attribute ");
				sysw(md, attrNameOff + 3, attrNameLen);
				sysw(" has a length of ");
				sysw(String.valueOf(getInt(md, moff + 2)));
				sysw(" bytes\n");
				if (isModUTF8(md, "Code".getBytes(UTF_8), attrNameOff)) {
					sysw("      max_stack=");
					sysw(String.valueOf(getUShort(md, moff + 6)));
					sysw("\n      max_locals=");
					sysw(String.valueOf(getUShort(md, moff + 8)));
					sysw("\n      code_length=");
					int codeLength = getInt(md, moff + 10);
					sysw(String.valueOf(codeLength));
					sysw("\n        ");
					for (int pc = 0; pc < codeLength; pc++) {
						String str = Integer.toHexString(0xFF & md[moff + 14 + pc]).toUpperCase();
						if (pc != 0) sysw(" ");
						if (str.length() == 1) sysw("0");
						sysw(str);
					}
					sysw("\n      attributes_count=");
					sysw(String.valueOf(getUShort(md, moff + 14 + codeLength)));
					sysw("\n");
				}
			}
			moff = skipAttribute(md, moff);
			attr.end = moff;
			if (lastAttrL == null) {
				met.attributes = nAttrL;
			} else {
				lastAttrL.next = nAttrL;
			}
			lastAttrL = nAttrL;
		}
		met.end = moff;
		return met;
	}
	
	/**
	 * parses a method attribute
	 * 
	 * @param ad   the attribute data
	 * @param aoff the offset of the attribute in the {@code ad} array
	 * 
	 * @return the parsed attribute
	 */
	public Attribute readMethodAttr(byte[] ad, int aoff) {
		byte[] d = data;
		ConstantPool cp = constPool;
		Attribute attr = new Attribute();
		attr.offset = aoff;
		int attrNameOff = cp.offsets[getUShort(ad, aoff) - 1];
		if (LOG_LEVEL > MIN_OUT_LOG_LEVEL) {
			int attrNameLen = getUShort(d, attrNameOff + 1);
			sysw("    the attribute ");
			sysw(d, attrNameOff + 3, attrNameLen);
			sysw(" has a length of ");
			sysw(String.valueOf(getInt(ad, aoff + 2)));
			sysw(" bytes\n");
			if (isModUTF8(d, "Code".getBytes(UTF_8), attrNameOff)) {
				sysw("      max_stack=");
				sysw(String.valueOf(getUShort(ad, aoff + 6)));
				sysw("\n      max_locals=");
				sysw(String.valueOf(getUShort(ad, aoff + 8)));
				sysw("\n      code_length=");
				int codeLength = getInt(ad, aoff + 10);
				sysw(String.valueOf(codeLength));
				sysw("\n        ");
				writeHex(ad, aoff + 14, codeLength);
				sysw("\n      attributes_count=");
				sysw(String.valueOf(getUShort(ad, aoff + 14 + codeLength)));
				sysw("\n");
			}
		}
		aoff = skipAttribute(ad, aoff);
		attr.end = aoff;
		return attr;
	}
	
	public static void writeHex(byte[] arr, int off, int length) {
		for (int i = 0; i < length; i++) {
			String str = Integer.toHexString(0xFF & arr[off + i]).toUpperCase();
			if (i != 0) sysw(" ");
			if (str.length() == 1) sysw("0");
			sysw(str);
		}
	}
	
	/**
	 * skips the interfaces and fields of this class file
	 * 
	 * @return the end offset of the fields
	 */
	public int skipInterfacesAndFields() {
		int off = 10 + constPool.end;
		byte[] d = data;
		off += getUShort(d, off - 4) * 2;
		int cnt = getUShort(d, off - 2);
		while (--cnt >= 0) {
			int acnt = getUShort(d, off + 6);
			off += 8;
			while (--acnt >= 0) {
				off = skipAttribute(d, off);
			}
		}
		fieldsEnd = off;
		return off;
	}
	
	/**
	 * reads and returns the attributes of the class file
	 * 
	 * @return the attributes of the class file
	 * 
	 * @throws IllegalClassFormatException if an error is detected
	 */
	public AttributeList readAttributes() throws IllegalClassFormatException {
		int off = methodsEnd;
		byte[] d = data;
		int acnt = getUShort(d, off);
		off += 2;
		AttributeList lastAL = null;
		if (acnt == 0) {
			attrs = null;
			checkEnd(d, off, acnt);
			return null;
		}
		for (int a = 0; a < acnt; a++) {
			Attribute attr = new Attribute();
			AttributeList nextAL = new AttributeList();
			nextAL.attr = attr;
			if (lastAL == null) {
				attrs = nextAL;
			} else {
				lastAL.next = nextAL;
			}
			lastAL = nextAL;
			attr.offset = off;
			off = skipAttribute(d, off);
			attr.end = off;
		}
		checkEnd(d, off, acnt);
		return attrs;
	}
	
	private static void checkEnd(byte[] d, int off, int acnt) throws IllegalClassFormatException {
		if (d.length != off) {
			throw new IllegalClassFormatException(
					"garbage at end of file: " + off + " : " + d.length + " (after " + acnt + " attributes)");
		}
	}
	
	private void logMethodCount(int mcnt) throws IllegalClassFormatException {
		ConstantPool cp = constPool;
		byte[] d = data;
		int clsNameOff = cp.offsets[getUShort(d, cp.end + 2) - 1];
		if (d[clsNameOff] != ConstantPool.CONSTANT_CLASS) {
			throw new IllegalClassFormatException("class name string is no CLASS entry");
		}
		clsNameOff = cp.offsets[getUShort(d, clsNameOff + 1) - 1];
		if (d[clsNameOff] != ConstantPool.CONSTANT_UTF8) {
			throw new IllegalClassFormatException("class name string is no UTF8 entry");
		}
		int clsNameLen = getUShort(d, clsNameOff + 1);
		sysw("the class ");
		sysw(d, clsNameOff + 3, clsNameLen);
		sysw(" has ");
		sysw(String.valueOf(mcnt));
		sysw(" methods\n");
	}
	
	/**
	 * returns the end offset of the attribute at the given offset
	 * 
	 * @param d   the class file
	 * @param off the offset of the attribute
	 * 
	 * @return end offset of the attribute
	 */
	public static int skipAttribute(byte[] d, int off) {
		return off + 6 + getInt(d, off + 2);
	}
	
	/**
	 * get an unsigned short
	 * 
	 * @param data the data
	 * @param off  the offset
	 * 
	 * @return the unsigned short stored in the data array at the given offset
	 */
	public static int getUShort(byte[] data, int off) {
		int restult = ( 0xFF & data[off] ) << 8;
		restult |= ( 0xFF & data[off + 1] );
		return restult;
	}
	
	/**
	 * sets an 16-bit-integer at the given offset
	 * 
	 * @param data the data
	 * @param off  the offset
	 * @param val  the value
	 */
	public static void putUShort(byte[] data, int off, int val) {
		data[off] = (byte) ( val >>> 8 );
		data[off + 1] = (byte) ( val );
	}
	
	/**
	 * increment the short value at the given offset by one
	 * 
	 * @param data the data
	 * @param off  the offset
	 * 
	 * @return the new value at the given offset
	 */
	public static int incUShort(byte[] data, int off) {
		int val = ++data[off + 1];
		if (val == 0) {
			val = ++data[off];
			if (val == 0) {
				throw new IllegalStateException("16-bit aritmethic overflow!");
			}
			data[off + 1] = 0;
			return val << 8;
		} else {
			data[off + 1] = (byte) val;
			return ( 0xFF & val ) | ( ( 0xFF & data[off] ) << 8 );
		}
	}
	
	/**
	 * get an integer
	 * 
	 * @param data the data
	 * @param off  the offset
	 * 
	 * @return the integer stored in the data array at the given offset
	 */
	public static int getInt(byte[] data, int off) {
		int restult = ( 0xFF & data[off] ) << 24;
		restult |= ( 0xFF & data[off + 1] ) << 16;
		restult |= ( 0xFF & data[off + 2] ) << 8;
		restult |= ( 0xFF & data[off + 3] );
		return restult;
	}
	
	/**
	 * sets an integer at the given offset
	 * 
	 * @param data the data
	 * @param off  the offset
	 * @param val  the value
	 */
	public static void putInt(byte[] data, int off, int val) {
		data[off] = (byte) ( val >>> 24 );
		data[off + 1] = (byte) ( val >>> 16 );
		data[off + 2] = (byte) ( val >>> 8 );
		data[off + 3] = (byte) ( val );
	}
	
	/**
	 * get an unsigned integer
	 * 
	 * @param data the data
	 * @param off  the offset
	 * 
	 * @return the unsigned integer stored in the data array at the given offset
	 */
	public static long getUInt(byte[] data, int off) {
		long restult = ( 0xFFL & data[off] ) << 24;
		restult |= ( 0xFFL & data[off + 1] ) << 16;
		restult |= ( 0xFFL & data[off + 2] ) << 8;
		restult |= ( 0xFFL & data[off + 3] );
		return restult;
	}
	
	/**
	 * get an 64-bit-integer
	 * 
	 * @param data the data
	 * @param off  the offset
	 * 
	 * @return the 64-bit-integer stored in the data array at the given offset
	 */
	public static long getLong(byte[] data, int off) {
		long restult = ( 0xFFL & data[off] ) << 56;
		restult |= ( 0xFFL & data[off + 1] ) << 48;
		restult |= ( 0xFFL & data[off + 2] ) << 40;
		restult |= ( 0xFFL & data[off + 3] ) << 32;
		restult |= ( 0xFFL & data[off + 4] ) << 24;
		restult |= ( 0xFFL & data[off + 5] ) << 16;
		restult |= ( 0xFFL & data[off + 6] ) << 8;
		restult |= ( 0xFFL & data[off + 7] );
		return restult;
	}
	
	/**
	 * sets an 64-bit-integer at the given offset
	 * 
	 * @param data the data
	 * @param off  the offset
	 * @param val  the value
	 */
	public static void putLong(byte[] data, int off, long val) {
		data[off] = (byte) ( val >>> 56 );
		data[off + 1] = (byte) ( val >>> 48 );
		data[off + 2] = (byte) ( val >>> 40 );
		data[off + 3] = (byte) ( val >> 32 );
		data[off + 4] = (byte) ( val >>> 24 );
		data[off + 5] = (byte) ( val >>> 16 );
		data[off + 6] = (byte) ( val >>> 8 );
		data[off + 7] = (byte) ( val );
	}
	
	/**
	 * checks whether the given Modified-UTF8 array stores the same value as the
	 * CP-Entry at the given offset
	 * 
	 * @param data    the class file
	 * @param modUtf8 the UTF data array
	 * @param off     the offset of the CP-Entry in the class file
	 * 
	 * @return <code>true</code> if the given Modified-UTF8 array stores the same
	 *         value as the CP-Entry at the given offset and <code>false</code> if
	 *         they diffe
	 */
	public static boolean isModUTF8(byte[] data, byte[] modUtf8, int off) {
		int len;
		len = getUShort(data, off + 1);
		if (len != modUtf8.length) return false;
		off += 3;
		while (--len >= 0) {
			if (data[off + len] != modUtf8[len]) return false;
		}
		return true;
	}
	
}
