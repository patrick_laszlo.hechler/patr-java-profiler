// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

public abstract class CTs {

	private static CTs instance;

	public CTs() {
	}

	public static void init(CTs instance) {
		init(instance, false);
	}

	public static void init(CTs instance, boolean force) {
		if (CTs.instance != null && !force) {
			throw new AssertionError();
		}
		CTs.instance = instance;
	}

	public abstract String[] allImpl();

	public static String[] all() {
		return instance.allImpl();
	}

	public abstract String descImpl(String name);

	public static String desc(String name) {
		return instance.descImpl(name);
	}

	public abstract AbstractClassTransformer<?, ?> getImpl(String name, int off, int end);

	public static AbstractClassTransformer<?, ?> get(String name, int off, int end) {
		return instance.getImpl(name, off, end);
	}

	public abstract String nameImpl(AbstractClassTransformer<?, ?> ct);

	public static String name(AbstractClassTransformer<?, ?> ct) {
		return instance.nameImpl(ct);
	}

}
