// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import java.lang.instrument.IllegalClassFormatException;

public class CTHelp {

	public static final String THREAD_CUR_THREAD_NAME_STR = "currentThread";
	public static final String THREAD_CUR_THREAD_DESC_STR = "()Ljava/lang/Thread;";
	public static final String THREAD_NAME_STR = "java/lang/Thread";

	public static final String SYSTEM_CURRENT_TIME_NAME_STR = "currentTimeMillis";
	public static final String SYSTEM_NANO_TIME_NAME_STR = "nanoTime";
	public static final String SYSTEM_NAME_STR = "java/lang/System";
	public static final String SYSTEM_DESC_STR = "L" + SYSTEM_NAME_STR + ";";
	public static final String SYSTEM_TIME_DESC_STR = "()J";

	public static final String INIT_NAME_STR = "<init>";
	public static final String CLASS_INIT_NAME_STR = "<clinit>";

	public static final String PPROF_NAME_STR = "de/hechler/patrick/profiler/PatrProfiler";
	public static final String PPROF_DESC_STR = "L" + PPROF_NAME_STR + ";";
	public static final String PPROF_ENTER_NAME_STR = "enterMethod";
	public static final String PPROF_ENTER_DESC_STR = "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
	public static final String PPROF_DIRECT_EXIT_EVEN_ERR_NAME_STR = "exitMethodEvenError";
	public static final String PPROF_DIRECT_EXIT_NAME_STR = "exitMethod";
	public static final String PPROF_DIRECT_EXIT_DESC_STR = "(J)V";
	public static final String PPROF_ENTER_INIT_NAME_STR = "enterInitMethod";
	public static final String PPROF_ENTER_INIT_DESC_STR = "(JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V";
	public static final String PPROF_DIRECT_EXIT_INIT_NAME_STR = "abnormalExitInitMethod";
	public static final String PPROF_DIRECT_EXIT_INIT_DESC_STR = "(JI)V";
	public static final String PPROF_GEN_ID_NAME_STR = "genInstanceID";
	public static final String PPROF_GEN_ID_DESC_STR = "()I";

	public static final String PREFIX_STR = "$$PatrJavaProfiler$$";

	public static final String SUK_MISC_UNSAFE_NAME_STR = "suk/misc/Unsafe";
	public static final String SUK_MISC_UNSAFE_DESC_STR = "L" + SUK_MISC_UNSAFE_NAME_STR + ";";
	public static final String SUK_MISC_UNSAFE_GET_UNSAFE_DESC_STR = "()Lsuk/misc/Unsafe;";

	public static final String SUN_MISC_UNSAFE_THE_UNSAFE_STR = "theUnsafe";
	public static final String SUN_MISC_UNSAFE_NAME_STR = "sun/misc/Unsafe";
	public static final String SUN_MISC_UNSAFE_DESC_STR = "L" + SUN_MISC_UNSAFE_NAME_STR + ";";
	public static final String SUN_MISC_UNSAFE_GET_UNSAFE_DESC_STR = "()Lsun/misc/Unsafe;";
	public static final String SUN_MISC_UNSAFE_GET_UNSAFE_NAME_STR = "getUnsafe";

	public static final String CALLER_SENSITIVE0_ANNOT_NAME_STR = "Lsun/reflect/CallerSensitive;";
	public static final String CALLER_SENSITIVE1_ANNOT_NAME_STR = "Ljdk/internal/reflect/CallerSensitive;";
	public static final String POLYMORPHIC_SIGNATURE_ANNOT_NAME_STR = "Ljava/lang/invoke/MethodHandle$PolymorphicSignature;";
	public static final String INTRINSIC_CANDIDATE_ANNOT_NAME_STR = "Ljdk/internal/vm/annotation/IntrinsicCandidate;";

	public static final String THROWABLE_NAME_STR = "java/lang/Throwable";
	public static final String THROWABLE_DESC_STR = "L" + THREAD_NAME_STR + ";";

	public static final String INNER_CLASSES_ATTR_STR = "InnerClasses";
	public static final String CODE_ATTR_STR = "Code";
	public static final String EXCEPTIONS_ATTR_STR = "Exceptions";
	public static final String METHOD_PARAMETERS_ATTR_STR = "MethodParameters";
	public static final String SYNTHETIC_ATTR_STR = "Synthetic";
	public static final String SIGNATURE_ATTR_STR = "Signature";
	public static final String RUNTIME_VISIBLE_ANNOTATIONS_ATTR_STR = "RuntimeVisibleAnnotations";
	public static final String STACK_MAP_TABLE_ATTR_STR = "StackMapTable";

	public static final String AGENT_FIELD_NAME_STR = "P_PROFILER_AGENT_NAME";
	public static final String AGENT_METHOD_NAME_STR = "name";
	public static final String AGENT_METHOD_DESC_STR = "()Ljava/lang/String;";

	public static byte[] getBytes(String str) {
		// see DataOutputStream.writeUTF
		int strlen = str.length();
		int utflen = 0;
		for (int i = 0; i < strlen; i++) {
			int c = str.charAt(i);
			if ((c >= 0x0001) && (c <= 0x007F)) {
				utflen++;
			} else if (c > 0x07FF) {
				utflen += 3;
			} else {
				utflen += 2;
			}
		}
		byte[] bytearr = new byte[utflen];
		int i = 0, c, count = 0;
		for (i = 0; i < strlen; i++) {
			c = str.charAt(i);
			if (!((c >= 0x0001) && (c <= 0x007F)))
				break;
			bytearr[count++] = (byte) c;
		}

		for (; i < strlen; i++) {
			c = str.charAt(i);
			if ((c >= 0x0001) && (c <= 0x007F)) {
				bytearr[count++] = (byte) c;

			} else if (c > 0x07FF) {
				bytearr[count++] = (byte) (0xE0 | ((c >> 12) & 0x0F));
				bytearr[count++] = (byte) (0x80 | ((c >> 6) & 0x3F));
				bytearr[count++] = (byte) (0x80 | ((c >> 0) & 0x3F));
			} else {
				bytearr[count++] = (byte) (0xC0 | ((c >> 6) & 0x1F));
				bytearr[count++] = (byte) (0x80 | ((c >> 0) & 0x3F));
			}
		}
		return bytearr;
	}

	public static String getString(byte[] bytearr) throws IllegalClassFormatException {
		return getString(bytearr, 0, bytearr.length);
	}

	public static String getString(byte[] bytearr, int off, int utflen) throws IllegalClassFormatException {
		// see DataInputStream.readUTF
		int char2, char3;
		int count = 0;
		int chararr_count = 0;

		char[] chararr = new char[utflen];

		while (count < utflen) {
			int c = bytearr[off + count] & 0xff;
			if (c > 127) {
				break;
			}
			count++;
			chararr[chararr_count++] = (char) c;
		}

		while (count < utflen) {
			int c = bytearr[off + count] & 0xff;
			switch (c >> 4) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				/* 0xxxxxxx */
				count++;
				chararr[chararr_count++] = (char) c;
				break;
			case 12:
			case 13:
				/* 110x xxxx, 10xx xxxx */
				count += 2;
				if (count > utflen) {
					throw new IllegalClassFormatException("malformed input: partial character at end");
				}
				char2 = bytearr[off + count - 1];
				if ((char2 & 0xC0) != 0x80) {
					throw new IllegalClassFormatException("malformed input around byte " + count);
				}
				chararr[chararr_count++] = (char) (((c & 0x1F) << 6) | (char2 & 0x3F));
				break;
			case 14:
				/* 1110 xxxx, 10xx xxxx, 10xx xxxx */
				count += 3;
				if (count > utflen) {
					throw new IllegalClassFormatException("malformed input: partial character at end");
				}
				char2 = bytearr[off + count - 2];
				char3 = bytearr[off + count - 1];
				if (((char2 & 0xC0) != 0x80) || ((char3 & 0xC0) != 0x80)) {
					throw new IllegalClassFormatException("malformed input around byte " + (count - 1));
				}
				chararr[chararr_count++] = (char) (((c & 0x0F) << 12) | ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0));
				break;
			default:
				/* 10xx xxxx or 1111 xxxx */
				throw new IllegalClassFormatException("malformed input around byte " + count);
			}
		}
		// The number of chars produced may be less than utflen
		return new String(chararr, 0, chararr_count);
	}

}
