// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

/**
 * represents a list of attributes
 */
public class AttributeList {
	
	public Attribute     attr;
	public AttributeList next;
	
	/**
	 * creates a new attribute list
	 * <p>note that the initial attribute list is invalid, until an {@link #attr} is set to a non <code>null</code>
	 * value
	 */
	public AttributeList() {}
	
	/**
	 * returns the first attribute list that holds an attribute with the given name
	 * 
	 * @param cf
	 *                 the class file
	 * @param nameCPE
	 *                 the constant pool entry index of the name
	 * 				
	 * @return the first attribute list that holds an attribute with the given name
	 */
	public AttributeList get(ClassBuffer cf, int nameCPE) {
		Attribute a = attr;
		if ( a.isName(cf, nameCPE) ) {
			return this;
		}
		AttributeList n = next;
		if ( n == null ) return null;
		return n.get(cf, nameCPE);
	}
	
}
