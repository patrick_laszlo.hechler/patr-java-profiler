// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import static de.hechler.patrick.profiler.agent.ClassBuffer.OFF_CP_DATA;
import static de.hechler.patrick.profiler.agent.ClassBuffer.OFF_CP_LENP1;
import static de.hechler.patrick.profiler.agent.ClassBuffer.getUShort;
import static de.hechler.patrick.profiler.agent.ClassBuffer.putUShort;

import java.lang.instrument.IllegalClassFormatException;

/**
 * a constant pool instance allows to read/modify the constant pool segment of a class file
 */
public class ConstantPool {
	
	public int[] offsets;
	public int   end;
	
	/**
	 * creates a new {@link ConstantPool} instance
	 */
	public ConstantPool() {}
	
	/**
	 * update the constant pool to match the given class file
	 * 
	 * @param data the class file
	 * 
	 * @throws IllegalClassFormatException if an error is detected
	 */
	public void update(byte[] data) throws IllegalClassFormatException {
		int count = getUShort(data, OFF_CP_LENP1) - 1;
		int[] offs = new int[count];
		int off = OFF_CP_DATA;
		for (int i = 0; i < count; i++) {
			offs[i] = off;
			if ( isTwoCPEntriesTag(data[off]) ) {
				if ( ++i == count ) {
					throw new IllegalClassFormatException(
						"the second half of a twoCPEntries constant is outside of the valid range");
				}
				offs[i] = -1;
			}
			off = skipCPEntry(data, off);
		}
		offsets = offs;
		end = off;
	}
	
	/**
	 * find the given constant pool entry
	 * 
	 * @param data  the class file
	 * @param entry the constant pool entry
	 * 
	 * @return the index of the given constant pool entry or {@code 0}
	 */
	public int findCPEntry(byte[] data, byte[] entry) {
		int[] offs = offsets;
		for (int i = offs.length; --i >= 0;) {
			int off = offs[i];
			if ( off == -1 ) continue;
			if ( isEntry(data, off, entry) ) return i + 1;
		}
		return 0;
	}
	
	/**
	 * like {@link #findCPEntry(byte[], byte[])}, but returns the bitwise not of the successful result and still returns
	 * {@code 0} on failure
	 * 
	 * @param data  the class file
	 * @param entry the constant pool entry
	 * 
	 * @return the bitwise not index of the given constant pool entry or {@code 0}
	 */
	public int findCPEntryTilde(byte[] data, byte[] entry) {
		int cpi = findCPEntry(data, entry);
		if ( cpi != 0 ) return ~cpi;
		return 0;
	}
	
	private static boolean isEntry(byte[] data, int off, byte[] entry) {
		for (int i = 0, len = entry.length; i < len; i++) {
			if ( data[off + i] != entry[i] ) return false;
		}
		return true;
	}
	
	/**
	 * find the given constant pool entry
	 * 
	 * @param data    the class file
	 * @param buf     the buffer to fill with the constant pool entry
	 * @param cpEntry the content of the cp-entry referenced by the buffer
	 * 
	 * @return the index of the given constant pool entry or {@code 0} if {@code cpEntry} was not found or the negative
	 *             index of {@code cpEntry}
	 */
	public int findRefCPEntry(byte[] data, byte[] buf, byte[] cpEntry) {
		int nameCPE = findCPEntry(data, cpEntry);
		if ( nameCPE == 0 ) return 0;
		putUShort(buf, 1, nameCPE);
		int utfRefCPE = findCPEntry(data, buf);
		if ( utfRefCPE == 0 ) return ~nameCPE;
		return utfRefCPE;
	}
	
	/**
	 * find the given constant pool entry
	 * 
	 * @param data     the class file
	 * @param buf      the buffer to fill with the constant pool entry
	 * @param cpEntry0 the content of the first cp-entry referenced by the buffer
	 * @param cpEntry1 the content of the second cp-entry referenced by the buffer
	 * 
	 * @return the index of the given constant pool entry or {@code ~( name0CPE | ( name1CPE << 16 ) )}
	 */
	public int find2RefsCPEntry(byte[] data, byte[] buf, byte[] cpEntry0, byte[] cpEntry1) {
		int name0CPE = findCPEntry(data, cpEntry0);
		int name1CPE = findCPEntry(data, cpEntry1);
		putUShort(buf, 1, name0CPE);
		putUShort(buf, 3, name1CPE);
		if ( name0CPE == 0 || name1CPE == 0 ) return ~( name0CPE | ( name1CPE << 16 ) );
		int utfRefCPE = findCPEntry(data, buf);
		if ( utfRefCPE == 0 ) return ~( name0CPE | ( name1CPE << 16 ) );
		return utfRefCPE;
	}
	
	/** CONSTANT_Utf8 tag */
	public static final int CONSTANT_UTF8                = 1;
	/** CONSTANT_Integer tag */
	public static final int CONSTANT_INTEGER             = 3;
	/** CONSTANT_Float tag */
	public static final int CONSTANT_FLOAT               = 4;
	/** CONSTANT_Long tag */
	public static final int CONSTANT_LONG                = 5;
	/** CONSTANT_Double tag */
	public static final int CONSTANT_DOUBLE              = 6;
	/** CONSTANT_Class tag */
	public static final int CONSTANT_CLASS               = 7;
	/** CONSTANT_String tag */
	public static final int CONSTANT_STRING              = 8;
	/** CONSTANT_Fieldref tag */
	public static final int CONSTANT_FIELDREF            = 9;
	/** CONSTANT_Methodref tag */
	public static final int CONSTANT_METHODREF           = 10;
	/** CONSTANT_InterfaceMethodref tag */
	public static final int CONSTANT_INTERFACE_METHODREF = 11;
	/** CONSTANT_NameAndType tag */
	public static final int CONSTANT_NAME_AND_TYPE       = 12;
	/** CONSTANT_MethodHandle tag */
	public static final int CONSTANT_METHOD_HANDLE       = 15;
	/** CONSTANT_MethodType tag */
	public static final int CONSTANT_METHOD_TYPE         = 16;
	/** CONSTANT_Dynamic tag */
	public static final int CONSTANT_DYNAMIC             = 17;
	/** CONSTANT_InvokeDynamic tag */
	public static final int CONSTANT_INVOKE_DYNAMIC      = 18;
	/** CONSTANT_Module tag */
	public static final int CONSTANT_MODULE              = 19;
	/** CONSTANT_Package tag */
	public static final int CONSTANT_PACKAGE             = 20;
	
	private static boolean isTwoCPEntriesTag(int val) {
		return val == CONSTANT_LONG || val == CONSTANT_DOUBLE;
	}
	
	private static int skipCPEntry(byte[] data, int off) throws IllegalClassFormatException {
		switch ( data[off] ) {
		case CONSTANT_CLASS:
		case CONSTANT_STRING:
		case CONSTANT_METHOD_TYPE:
		case CONSTANT_MODULE:
		case CONSTANT_PACKAGE:
			return off + 3;
		case CONSTANT_METHOD_HANDLE:
			return off + 4;
		case CONSTANT_FIELDREF:
		case CONSTANT_METHODREF:
		case CONSTANT_INTERFACE_METHODREF:
		case CONSTANT_INTEGER:
		case CONSTANT_FLOAT:
		case CONSTANT_NAME_AND_TYPE:
		case CONSTANT_DYNAMIC:
		case CONSTANT_INVOKE_DYNAMIC:
			return off + 5;
		case CONSTANT_DOUBLE:
		case CONSTANT_LONG:
			return off + 9;
		case CONSTANT_UTF8:
			// getInt is not needed, since the entire class file fits in an Integer indexable byte array
			int len = getUShort(data, off + 1);
			for (int i = len; --i >= 0;) {
				if ( data[off + 3 + i] == 0 ) {
					throw new IllegalClassFormatException("zero byte inside of Modified-UTF8 segment");
				}
			}
			return off + 3 + len;
		default:
			throw new IllegalClassFormatException("unknown constant pool tag: " + data[off]);
		}
	}
	
}
