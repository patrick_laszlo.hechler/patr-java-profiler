// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

/**
 * a {@link ModificationList} is used to save modifications and then apply then at once
 */
public class ModificationList {
	
//	String debug;
	/**
	 * the start offset of this modification
	 */
	public int    offset;
	/**
	 * the length of the original data which is replaced/removed by this modification
	 */
	public int    length;
	/**
	 * the replacement data or <code>null</code> if this modification just removes
	 */
	public byte[] replacement;
	
	/**
	 * the next modificaion
	 */
	public ModificationList next;
	
	/**
	 * create a new {@link ModificationList}
	 */
	public ModificationList() {}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		append(sb);
		return sb.toString();
	}
	
	private void append(StringBuilder sb) {
//		if ( debug != null ) {
//			sb.append(debug);
//		}
		if ( replacement == null ) {
			sb.append("remove at ").append(offset).append(' ').append(length).append(" bytes");
		} else {
			if ( length == 0 ) {
				sb.append("insert at ").append(offset).append(' ');
			} else {
				sb.append("replace at ").append(offset).append(' ').append(length).append(" bytes with ");
			}
			append(sb, replacement);
		}
		if ( next != null ) {
			sb.append('\n');
			next.append(sb);
		}
	}
	
	private static void append(StringBuilder sb, byte[] data) {
		sb.append(data.length).append(" bytes: [");
		for (int i = 0; i < data.length; i++) {
			if ( i != 0 ) sb.append(' ');
			String str = Integer.toHexString(0xFF & data[i]).toUpperCase();
			if ( str.length() == 1 ) sb.append('0');
			sb.append(str);
		}
		sb.append(']');
	}
	
}
