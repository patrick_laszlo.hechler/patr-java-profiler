// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import static de.hechler.patrick.profiler.agent.PProfilerAgent.LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_ERR_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_FERR_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_OUT_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysw;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.ProtectionDomain;

import de.hechler.patrick.profiler.agent.PProfilerAgent;

public abstract class AbstractClassTransformer<H, M> implements ClassFileTransformer {

	public void init() {
		try {
////		Class.forName("java.lang.WeakPairMap$Pair$Weak", true, PProfilerAgent.class.getClassLoader());
			Class.forName("sun.misc.Unsafe", true, PProfilerAgent.class.getClassLoader());
		} catch (@SuppressWarnings("unused") ClassNotFoundException e) {
		}
	}

	/**
	 * should sun.misc.Unsafe.getUnsafe() replace in order to simply return
	 * theUnsafe?
	 * <ul>
	 * <li>{@code 0}: default value</li>
	 * <li>{@code 1}: yes</li>
	 * <li>{@code -1}: no</li>
	 * </ul>
	 */
	protected int replaceGetUnsafe;

	protected boolean replaceGetUnsafe() {
		return isSet(this.replaceGetUnsafe, false);
	}

	/**
	 * checks if the given boolean property is set to <code>true</code>
	 * 
	 * @param value the property
	 * @param def   the default value
	 * @return <code>true</code> if {@code value > 0} or if
	 *         {@code value == 0 && def} and <code>false</code> if {@code value < 0}
	 *         or if {@code value == 0 && !def}
	 */
	protected boolean isSet(int value, boolean def) {
		if (value == 0) {
			return def;
		}
		return value > 0;
	}

	public void parseOption(String args, int off, int endOff) {
		String name = "replaceGetUnsafe";
		if (PProfilerAgent.isAssignableArg(off, endOff, args, name, '=')) {
			replaceGetUnsafe = parseBooleanOption(replaceGetUnsafe, args, off, endOff, name);
		} else if (PProfilerAgent.isArg(off, endOff, args, "help")) {
			help(args.substring(args.lastIndexOf(',', off) + 1, off));
		} else {
			System.err.println("unknown option: '" + args.substring(args.lastIndexOf(',', off), endOff) + "' (args='"
					+ args + "')");
			PProfilerAgent.halt(1);
		}
	}

	protected int parseBooleanOption(int old, String args, int off, int endOff, String name) {
		if (old != 0) {
			System.err.println("replaceGetUnsafe option used multiple times");
			PProfilerAgent.halt(1);
		}
		switch (endOff - off - name.length()) {
		case 0:
		case 1:
			return 1;
		case 2:
			char c = args.charAt(endOff - 1);
			if (c == '0') {
				return -1;
			}
			if (c == '1') {
				return 1;
			}
			//$FALL-THROUGH$
		default:
			System.err.println(
					"invalid value for " + name + ": '" + args.substring(off, endOff) + "' (args='" + args + "')");
			PProfilerAgent.halt(1);
			throw null;
		}
	}

	public String name() {
		return ClassTransformers.name(this);
	}

	protected void help(String prefix) {
		System.out.println("Class Transformer Help: Type " + name());
		printUsage(prefix);
		PProfilerAgent.halt(1);
	}

	protected void printUsage(String prefix) {
		System.out.println("  " + prefix + "help");
		System.out.println("    print this message and terminate");
		System.out.println("  " + prefix + "replaceGetUnsafe[=01]");
		System.out.println("    replace the sun.misc.Unsafe.getUnsafe() implementation");
		System.out.println("    so that it just returns theUnsafe");
	}

	protected abstract H createHelper(M module, ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		return transformCaller(null, loader, className, classBeingRedefined, protectionDomain, classfileBuffer);
	}

	@SuppressWarnings("unused")
	public byte[] transformCaller(M module, ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		try {
			H cls = createHelper(module, loader, className, classBeingRedefined, protectionDomain, classfileBuffer);
			if (cls == null) {
				return null;
			}
			if (PProfilerAgent.class == classBeingRedefined) {
				return transformPProfilerAgent(cls, classBeingRedefined);
			} else if ("sun/misc/Unsafe".equals(className)) {
				return transformSunMiscUnsafe(cls, classBeingRedefined);
			} else if ("jdk/internal/misc/Unsafe".equals(className)) {
				return transformJdkInternalMiscUnsafe(cls, classBeingRedefined);
			}
//			if (
////			!"java/util/regex/Pattern$BitClass".equals(className) 
////			&&
////			!"java/util/regex/Pattern$BmpCharPredicate".equals(className)
////			&&
//			
//			!"de/hechler/patrick/profiler/test/PHPTestMain".equals(className)
//				&& !"de/hechler/patrick/profiler/test/SimpleClass".equals(className)
////				&& !"java/util/IdentityHashMap$IdentityHashMapIterator".equals(className)
////				&& !"java/util/IdentityHashMap$KeyIterator".equals(className)
////				&& !"java/lang/ApplicationShutdownHooks".equals(className)
////				&& !"java/lang/ApplicationShutdownHooks$1".equals(className)
////				&& !"java/lang/Shutdown".equals(className)
////			&&false
//			
//			
//					!"de/hechler/patrick/profiler/test/PHPTestMain".equals(className)
//			) {
//				sysew("skip ");
//				sysew(className);
//				sysew("\n");
//				return null;
//			}
			byte[] result = transformOtherOrBootstrap(cls, className, classBeingRedefined);
//			if ("org/eclipse/jdt/launching/internal/javaagent/Premain".equals(className)) {
//				try {
//					Files.write(Paths.get("Premain.class"), result);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//			if ("de/hechler/patrick/profiler/test/PHPTestMain".equals(className)) {
//				try {
//					Files.write(Paths.get("PHPTestMain.class"), result);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				try {
					String modName = module == null ? "" : module.toString();
					if (modName.startsWith("unnamed")) {
						modName = "unnamed";
					} else if (modName.startsWith("module ")) {
						modName = modName.substring("module ".length());
					}
					Path clsPath = Paths.get("testout/MODIFIED/" + modName + "/" + className + ".class");
					Files.createDirectories(clsPath.getParent());
					if (result != null) {
						Files.write(clsPath, result);
					}
					clsPath = Paths.get("testout/ORIG/" + modName + "/" + className + ".class");
					Files.createDirectories(clsPath.getParent());
					Files.write(clsPath, classfileBuffer);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return result;
		} catch (Throwable t) {
			if (LOG_LEVEL >= MIN_FERR_LOG_LEVEL) {// only occurrence of MIN_FERR_LOG_LEVEL
				System.err.print("error while transforming ");
				System.err.print(className);
				System.err.print('\n');
				t.printStackTrace();
				if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
					Runtime.getRuntime().halt(16);
				}
			}
			throw t;
		}
	}

	protected abstract byte[] transformOtherOrBootstrap(H cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException;

	protected abstract byte[] transformOther(H cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException;

	protected abstract byte[] transformMyInternBootstrap(H cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException;

	@SuppressWarnings("unused")
	protected byte[] transformJdkInternalMiscUnsafe(H cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			sysw("skip the internal Unsafe");
		}
		return null;
	}

	protected abstract byte[] transformSunMiscUnsafe(H cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException;

	protected abstract byte[] transformPProfilerAgent(H cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException;

}
