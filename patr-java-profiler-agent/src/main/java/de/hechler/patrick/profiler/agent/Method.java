// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import static de.hechler.patrick.profiler.agent.ClassBuffer.*;

/**
 * this class is used to represent a method in the class file
 */
public class Method {
	
	public int           offset;
	public int           end;
	public AttributeList attributes;
	
	/**
	 * create a new method
	 */
	public Method() {}
	
	/**
	 * returns the access flags of the method
	 * 
	 * @param d the class file
	 * 
	 * @return the access flags of the method
	 */
	public int accessFlags(byte[] d) {
		return getUShort(d, offset);
	}
	
	/**
	 * returns the position in the class file of the constant pool entry holding the methods name
	 * 
	 * @param cf the class file
	 * 
	 * @return the position in the class file of the constant pool entry holding the methods name
	 */
	public int nameOff(ClassBuffer cf) {
		return cf.constPool().offsets[getUShort(cf.buffer(), offset + 2) - 1];
	}
	
	/**
	 * returns the position in the class file of the constant pool entry holding the methods name
	 * 
	 * @param cp   the constant pool
	 * @param md   the methods byte code
	 * @param moff the offsewt of the method in the {@code md} array
	 * 
	 * @return the position in the class file of the constant pool entry holding the methods name
	 */
	public int nameOff(ConstantPool cp, byte[] md, int moff) {
		return cp.offsets[getUShort(md, moff + 2) - 1];
	}
	
	/**
	 * returns the position in the class file of the constant pool entry holding the methods name
	 * 
	 * @param cf the class file
	 * 
	 * @return the position in the class file of the constant pool entry holding the methods name
	 */
	public int descriptorOff(ClassBuffer cf) {
		return cf.constPool().offsets[getUShort(cf.buffer(), offset + 4) - 1];
	}
	
	/**
	 * returns the position in the class file of the constant pool entry holding the methods name
	 * 
	 * @param cp   the constant pool
	 * @param md   the methods byte code
	 * @param moff the offsewt of the method in the {@code md} array
	 * 
	 * @return the position in the class file of the constant pool entry holding the methods name
	 */
	public int descriptorOff(ConstantPool cp, byte[] md, int moff) {
		return cp.offsets[getUShort(md, moff + 4) - 1];
	}
	
	/**
	 * checks if this method has the given name and descriptor
	 * 
	 * @param cf         the class file
	 * @param name       the name
	 * @param descriptor the descriptor
	 * 
	 * @return <code>true</code> if this method has the given name and descriptor and <code>false</code> if not
	 */
	public boolean isNameAndDescriptor(ClassBuffer cf, byte[] name, byte[] descriptor) {
		byte[] d = cf.buffer();
		return isModUTF8(d, name, nameOff(cf)) && isModUTF8(d, descriptor, descriptorOff(cf));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Method [offset=");
		builder.append(offset);
		builder.append(", end=");
		builder.append(end);
		builder.append("]");
		return builder.toString();
	}
	
}
