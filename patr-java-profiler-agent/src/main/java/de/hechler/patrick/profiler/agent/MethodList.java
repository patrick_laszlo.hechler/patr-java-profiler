// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

/**
 * a list of methods
 */
public class MethodList {
	
	public Method     met;
	public MethodList next;
	
	/**
	 * create a new method list
	 */
	public MethodList() {}
	
	/**
	 * returns the specified method
	 * 
	 * @param cf         the class file
	 * @param name       the methods name
	 * @param descriptor the methods descriptor
	 * 
	 * @return the specified method
	 */
	public Method get(ClassBuffer cf, byte[] name, byte[] descriptor) {
		if ( met.isNameAndDescriptor(cf, name, descriptor) ) {
			return met;
		}
		if ( next == null ) return null;
		return next.get(cf, name, descriptor);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MethodList: {");
		for (MethodList ml = this; ml != null; ml = ml.next) {
			sb.append("\n  ").append(ml.met);
		}
		return sb.append("\n}").toString();
	}
	
}
