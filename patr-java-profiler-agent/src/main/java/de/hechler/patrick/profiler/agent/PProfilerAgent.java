// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.io.InputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.function.BiFunction;

import de.hechler.patrick.profiler.agent.transform.AbstractClassTransformer;
import de.hechler.patrick.profiler.agent.transform.ClassTransformers;
import de.hechler.patrick.profiler.agent.transform.DefaultClassTransformer;
import de.hechler.patrick.profiler.agent.transform.MyWrapClassTransformer;

/**
 * the agent class which registers a {@link ClassFileTransformer} to profile
 * classes
 */
@SuppressWarnings("unused")
public class PProfilerAgent {

	static final String PREFIX = CTHelp.PREFIX_STR;

	/**
	 * the return value of {@link #name()} until this class is modified, which
	 * should happen even before the <code>main(String[])</code> method is called
	 * 
	 * @see #name()
	 */
	public static final String P_PROFILER_AGENT_NAME = "PProfilerAgent [unmodified]";

	private PProfilerAgent() {
	}

	static class Options {

		AbstractClassTransformer<?, ?> transform;
		ServerInterface start;

	}

	/**
	 * the agents pre-main method called when the agent is registered using the
	 * {@code -javaagent} command line argument
	 * 
	 * @param agentArgs the arguments to be parsed
	 * @param inst      the {@link Instrumentation}
	 */
	static void premain(String agentArgs, Instrumentation inst) {
		try {
			AbstractClassTransformer<?, ?> ct = null;
			ServerInterface ad = null;
			if (agentArgs != null) {
				Object opts = parseArgs(agentArgs, inst);
				if (opts instanceof ClassFileTransformer) {
					ct = (AbstractClassTransformer<?, ?>) opts;
				} else if (opts instanceof ServerInterface) {
					ad = (ServerInterface) opts;
				} else if (opts instanceof Options) {
					ct = ((Options) opts).transform;
					ad = ((Options) opts).start;
				}
			}
			if (ct == null) {
				ct = new DefaultClassTransformer();
			}
			if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
				sysew("");// ensure all the basic classes for System.write and String.getBytes are loaded
			}
			byte[] nl = LOG_LEVEL >= MIN_OUT_LOG_LEVEL ? new byte[] { '\n' } : null;
			if (!inst.isRetransformClassesSupported()) {
				throw new IllegalStateException("ERROR: retransform is not supported");
			}
			ct.init();
			inst.addTransformer(ct, true);
			inst.setNativeMethodPrefix(ct, PREFIX);
//		try {
//			Class.forName("de.hechler.patrick.profiler.PatrProfiler", true, PProfilerAgent.class.getClassLoader());
//		} catch ( ClassNotFoundException e ) {
//			throw new NoClassDefFoundError(e.toString());
//		}
			int cnt = 0;
			int ncnt = 0;
			for (Class<?> cls : inst.getAllLoadedClasses()) {
				if (!cls.isInterface() && !cls.isAnnotation() && !cls.isArray() && !cls.isPrimitive()
//				&& "\0jdk/internal/misc/Unsafe".equals(cls.getName()) 
				) {
					if (inst.isModifiableClass(cls)) {
						cnt++;
					} else {
						if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
							ncnt++;
							sysw("no retransform of ");
							sysw(String.valueOf(cls));
							sysw(nl);
						}
					}
				}
			}
			Class<?>[] retransform = new Class[cnt];
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("retransform ");
				sysw(String.valueOf(cnt));
				sysw(" classes\n" //
						+ "no retransform of ");
				sysw(String.valueOf(ncnt));
				sysw(" classes\n");
			}
			for (Class<?> cls : inst.getAllLoadedClasses()) {
				if (!cls.isInterface() && !cls.isAnnotation() && inst.isModifiableClass(cls)) {
					retransform[--cnt] = cls;
					if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
						sysw(String.valueOf(cls));
						sysw(nl);
					}
				}
			}
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				if (cnt != 0) {
					sysw("ERROR: ");
					sysw(String.valueOf(cnt));
					sysw(nl);
				}
			}
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("bevore retransform: ");
				sysw(String.valueOf(name()));
				sysw(" : ");
				sysw(P_PROFILER_AGENT_NAME);
				sysw(nl);
			}
//			inst.retransformClasses(retransform);//TODO add again
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("after retransform: ");
				sysw(String.valueOf(name()));
				sysw(" (static final field): ");
				sysw(P_PROFILER_AGENT_NAME);
				sysw(nl);
				sysw("SUCCESSFULLY RETRANSFORMED ");
				sysw(String.valueOf(retransform.length));
				sysw(" CLASSES\n");
			}
			if (ad != null) {
				ad.start(ct, inst);
				// start the daemon after all initializations are done to avoid race conditions
			}
		} catch (Throwable e) {
			e.printStackTrace();
			Runtime.getRuntime().halt(2);
		}
	}

	// in case of error use halt, because exit may not yet work
	private static Object parseArgs(String agentArgs, Instrumentation inst) {
		ServerInterface daemon = null;
		AbstractClassTransformer<?, ?> newCt = null;
//		Method daemonParseArg = null;
		for (int off = 0, end; off < agentArgs.length(); off = end + 1) {
			end = agentArgs.indexOf(',', off);
			if (end == -1)
				end = agentArgs.length();
			if (end == off)
				continue;// ignore empty options (,,)
			if (isArg(off, end, agentArgs, "help")) {
				agentHelp(true);
				halt(1);
			} else if (isArg(off, end, agentArgs, "agent-help")) {
				agentHelp(false);
				halt(1);
			} else if (isArg(off, end, agentArgs, "bootstrap-help")) {
				try {
					Class<?> cls = Class.forName("de.hechler.patrick.profiler.Main", true,
							PProfilerAgent.class.getClassLoader());
					Method main = cls.getMethod("main", String[].class);
					main.invoke(null, (Object) new String[] { "bootstrap-help" });
				} catch (Throwable t) {
					System.err.println("it seems that the bootstrap library is missing or corrupt");
					System.err.println("maybe you should check the versions or try the 'help' option");
					t.printStackTrace();
					halt(2);
				}
				halt(1);
			} else if (isArg(off, end, agentArgs, "license")) {
				echoResource("/de/hechler/patrick/profiler/agent/LICENSE");
				halt(1);
			} else if (isArg(off, end, agentArgs, "version")) {
				System.out.print("Patr-Java-Profiler Agent: ");
				echoResource("/de/hechler/patrick/profiler/agent/VERSION");
				System.out.println();
				halt(1);
			} else if (isAssignableArg(off, end, agentArgs, "transformer", '=')) {
				if (newCt != null) {
					System.err.println("the transformer is already set!");
					System.err.println("arguments: '" + agentArgs + "'");
					halt(1);
				}
				off += "transformer.".length();
				if (end <= off) {
					newCt = ClassTransformers.get();
				} else if (isArg(off, end, agentArgs, "list")) {
					System.out.println("(built-in) transformers:");
					ClassTransformers.listTransformers(System.out, "    ");
					halt(1);
				} else {
					newCt = ClassTransformers.get(agentArgs, off, end);
				} // first check for transformer=<name> and then for transformer.<option>
			} else if (isAssignableArg(off, end, agentArgs, "transformer", '.')) {
				if (newCt == null) {
					newCt = ClassTransformers.get();
				}
				newCt.parseOption(agentArgs, off + "transformer.".length(), end);
			} else if (isAssignableArg(off, end, agentArgs, "daemon", '.')) {
				if (daemon == null) {
					try {
						Class<?> cls = Class.forName("de.hechler.patrick.profiler.server.Server", true,
								PProfilerAgent.class.getClassLoader());
						Constructor<?> c = cls.getConstructor();
						daemon = (ServerInterface) c.newInstance();
						daemon.initArgs(agentArgs);
					} catch (Throwable t) {
						System.err.println("it seems that the transmit library is missing or corrupt");
						System.err.println("maybe you should check the versions or try the 'daemon.help' option");
						t.printStackTrace();
						halt(2);
					}
				}
				off += "daemon.".length();
				if (end > off) {
					try {
						daemon.parseArg(off, end);
					} catch (Throwable t) {
						System.err.println("it seems that the transmit library is corrupt");
						System.err.println("maybe you should check the versions or try the 'help' option");
						t.printStackTrace();
						halt(2);
					}
				}
			} else {
				System.err.println("Patr Java Profiler Agent: unknown option: '" + agentArgs.substring(off, end)
						+ "' (options: '" + agentArgs + "')");
				System.err.println("use help to get a list of options");
				halt(2);
			}
		}
		if (newCt != null) {
			if (daemon != null) {
				Options o = new Options();
				o.start = daemon;
				o.transform = newCt;
				return o;
			}
			return newCt;
		}
		return daemon;
	}

	public static void halt(int status) {
		System.err.close();
		System.out.close();
		Runtime.getRuntime().halt(status);
	}

	public static void echoResource(String res) {
		try (InputStream in = PProfilerAgent.class.getResourceAsStream(res)) {
			byte[] buf = new byte[1024];
			while (true) {
				int r = in.read(buf, 0, 1024);
				if (r == -1)
					break;
				System.out.write(buf, 0, r);
			}
		} catch (IOException e) {
			e.printStackTrace();
			Runtime.getRuntime().halt(2);
		}
	}

	public static boolean isAssignableArg(int off, int end, String args, String refArg, char assign) {
		int len = end - off;
		int alen = refArg.length();
		if (len < alen)
			return false;
		if (!args.startsWith(refArg, off))
			return false;
		if (len == alen)
			return true;
		return args.charAt(off + alen) == assign;
	}

	public static boolean isArg(int off, int end, String args, String refArg) {
		if (end - off != refArg.length())
			return false;
		return args.startsWith(refArg, off);
	}

	private static void agentHelp(boolean completeHelp) {
		if (completeHelp) {
			System.out.println("Patr Java Profiler Help:");
		}
		if (true) {
			System.out.println("Patr Java Profiler Agent Help:");
			System.out.println("  pass the jar as a java agent (-javaagent:<JAR_FILE>[=<OPTIONS>])");
			System.out.println("  seperate multiple options with a comma ','");
			System.out.println("  options:");
			System.out.println("    agent-help");
		}
		if (completeHelp) {
			System.out.println("      print only the agents help message to sdtout and terminate");
		} else {
			System.out.println("      print this message to sdtout and terminate");
		}
		if (true) {
			System.out.println("    bootstrap-help");
			System.out.println("      print the bootstrap-help message of the bootstrap library and terminate");
			System.out.println("      this option fails if the bootstrap library is missing");
			System.out.println("    help");
		}
		if (completeHelp) {
			System.out.println("      print this message to sdtout and halt imidatly");
		} else {
			System.out.println("      print the complete help message to sdtout and terminate");
		}
		if (true) {
			System.out.println("    daemon[.[OPTION]]");
			System.out.println("      pass OPTION to the daemon");
			System.out.println("      if no daemon is present no daemon will be used");
			System.out.println("      use daemon[.] to load (and start) the daemon with default options");
			System.out.println("      use daemon.help to show a list of available options");
			System.out.println("      only available when the transmit library is present");
			System.out.println("    daemon.help");
			System.out.println("      show a list of available daemon options");
			System.out.println("      only available when the transmit library is present");
			System.out.println("    transformer=list");
			System.out.println("      list all built-in transformers");
			System.out.println("    transformer=<name>");
			System.out.println("      use the transformer <name> instead of 'default'");
			System.out.println("      must be used before any transformer.<option>");
			System.out.println("    transformer.<option>");
			System.out.println("      pass <option> to transformer");
			System.out.println("      try transformer.help to get a help");
			System.out.println("      if no transformer has been set previusly the default transformer is used");
			System.out.println("    license");
			System.out.println("      print the license to sdtout and terminate");
			System.out.println("    version");
			System.out.println("      print version information to sdtout and terminate");
		}
		if (completeHelp) {
			System.out.println("Patr Java Profiler Bootstrap Help:");
			System.out.println("  pass the jar to the bootstrap class loader (-Xbootclasspath/a:<JAR_FILE>)");
			System.out.println("  for more information use");
			System.out.println("  java -cp <JAR_FILE> de.hechler.patrick.profiler.Main help");
			System.out.println("  or");
			System.out.println("  java -Xbootclasspath/a:<JAR_FILE> de.hechler.patrick.profiler.Main help");
		}
	}

	/**
	 * returns {@code "PProfilerAgent [unmodified]"} if this class was not modified
	 * by its own byte code transformer<br>
	 * otherwise {@code "PProfilerAgent [modified]"} will be returned (the
	 * {@code 'un'} after the {@code '['} will be removed)
	 * <p>
	 * after two modifications the result should be
	 * {@code "PProfilerAgent [dified]"}
	 * 
	 * @return {@code "PProfilerAgent [unmodified]"} if this class was not modified
	 *         by its own byte code transformer, otherwise
	 *         {@code "PProfilerAgent [modified]"}
	 */
	public static String name() {
		// the compiler will optimize this to a constant pool access
		// thus the real value of the static variable does not matter
		// the value of the constant pool entry matters and this value
		// can simple be modified by the ClassTransformer
		// (the ClassTransformer could also modify the method content
		// to instead use the constant pool entry if there is a
		// compiler that implements 'static final String' accesses
		// with a real field access)
		return P_PROFILER_AGENT_NAME;
	}

	// print can (during pre-main) fail with a circular class load exception
	// also it is more easy to configure the logging this way:
	/**
	 * <pre>
	 *  0: none
	 *  1: sysew --&gt; stdout
	 *  2: sysew --&gt; stderr
	 *  3: sysew --&gt; stdout and sysw --&gt; stdout
	 *  4: sysew --&gt; stderr and sysw --&gt; stdout
	 *  5: sysew --&gt; stderr and sysw --&gt; stderr
	 *  5+: same as 5
	 *  0-: less then none (see {@link #MIN_FERR_LOG_LEVEL})
	 * </pre>
	 * 
	 * @see #MIN_ERR_LOG_LEVEL
	 * @see #MIN_OUT_LOG_LEVEL
	 * @see #sysew(byte[])
	 * @see #sysew(String)
	 * @see #sysew(byte[], int, int)
	 * @see #sysw(byte[])
	 * @see #sysw(String)
	 * @see #sysw(byte[], int, int)
	 */
	public static final int LOG_LEVEL = 0;
	/**
	 * the minimum log level for
	 * {@link MyWrapClassTransformer#transform(ClassLoader, String, Class, java.security.ProtectionDomain, byte[])
	 * ClassTranformer.transform} to fail when an error occurs,<br>
	 * if {@link #LOG_LEVEL} is below this value all errors are silently ignored.
	 * <p>
	 * note that if {@link #LOG_LEVEL} is below {@link #MIN_OUT_LOG_LEVEL} or
	 * {@link #MIN_ERR_LOG_LEVEL} there is also less checking done, thus some errors
	 * may not be found, even if {@link #LOG_LEVEL} is not below this value.
	 */
	public static final int MIN_FERR_LOG_LEVEL = 0;
	/**
	 * the minimum log level for sysew to have an effect
	 * 
	 * @see #LOG_LEVEL
	 * @see #sysew(byte[])
	 * @see #sysew(String)
	 * @see #sysew(byte[], int, int)
	 */
	public static final int MIN_ERR_LOG_LEVEL = 1;
	/**
	 * the minimum log level for sysw to have an effect
	 * 
	 * @see #LOG_LEVEL
	 * @see #sysw(byte[])
	 * @see #sysw(String)
	 * @see #sysw(byte[], int, int)
	 */
	public static final int MIN_OUT_LOG_LEVEL = 3;

	/**
	 * write the given array to {@link System#out}
	 * 
	 * @param arr the data to write to {@link System#out}
	 * 
	 * @see #LOG_LEVEL
	 * @see #MIN_OUT_LOG_LEVEL
	 */
	public static void sysw(byte[] arr) {
		if (LOG_LEVEL < 3)
			return;
		if (LOG_LEVEL < 5) {
			System.out.write(arr, 0, arr.length);
		} else {
			System.err.write(arr, 0, arr.length);
		}
	}

	/**
	 * write the given segment of the array to {@link System#out}
	 * 
	 * @param arr the array containing the data to write to {@link System#out}
	 * @param off the offset
	 * @param len the length
	 * 
	 * @see #LOG_LEVEL
	 * @see #MIN_OUT_LOG_LEVEL
	 */
	public static void sysw(byte[] arr, int off, int len) {
		if (LOG_LEVEL < 3)
			return;
		if (LOG_LEVEL < 5) {
			System.out.write(arr, off, len);
		} else {
			System.err.write(arr, off, len);
		}
	}

	/**
	 * write the given string to {@link System#out}
	 * 
	 * @param str the string to write to {@link System#out}
	 * 
	 * @see #LOG_LEVEL
	 * @see #MIN_OUT_LOG_LEVEL
	 */
	public static void sysw(String str) {
		if (LOG_LEVEL < 3)
			return;
		byte[] bs = str.getBytes(UTF_8);
		if (LOG_LEVEL < 5) {
			System.out.write(bs, 0, bs.length);
		} else {
			System.err.write(bs, 0, bs.length);
		}
	}

	/**
	 * write the given array to {@link System#err}
	 * 
	 * @param arr the data to write to {@link System#err}
	 * 
	 * @see #LOG_LEVEL
	 * @see #MIN_ERR_LOG_LEVEL
	 */
	public static void sysew(byte[] arr) {
		if (LOG_LEVEL < 1)
			return;
		if (LOG_LEVEL < 2 || (LOG_LEVEL > 2 && LOG_LEVEL < 4)) {
			System.err.write(arr, 0, arr.length);
		} else {
			System.err.write(arr, 0, arr.length);
		}
	}

	/**
	 * write the given segment of the array to {@link System#err}
	 * 
	 * @param arr the array containing the data to write to {@link System#err}
	 * @param off the offset
	 * @param len the length
	 * 
	 * @see #LOG_LEVEL
	 * @see #MIN_ERR_LOG_LEVEL
	 */
	public static void sysew(byte[] arr, int off, int len) {
		if (LOG_LEVEL < 1)
			return;
		if (LOG_LEVEL < 2 || (LOG_LEVEL > 2 && LOG_LEVEL < 4)) {
			System.out.write(arr, off, len);
		} else {
			System.err.write(arr, off, len);
		}
	}

	/**
	 * write the given string to {@link System#err}
	 * 
	 * @param str the string to write to {@link System#err}
	 * 
	 * @see #LOG_LEVEL
	 * @see #MIN_ERR_LOG_LEVEL
	 */
	public static void sysew(String str) {
		if (LOG_LEVEL < 1)
			return;
		byte[] bs = str.getBytes(UTF_8);
		if (LOG_LEVEL < 2 || (LOG_LEVEL > 2 && LOG_LEVEL < 4)) {
			System.out.write(bs, 0, bs.length);
		} else {
			System.err.write(bs, 0, bs.length);
		}
	}

}
