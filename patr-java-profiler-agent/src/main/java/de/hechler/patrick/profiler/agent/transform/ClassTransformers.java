// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import java.io.PrintStream;
import java.lang.reflect.Constructor;

public class ClassTransformers {

	public static void listTransformers(PrintStream out, String prefix) {
		for (String tn : CTs.all()) {
			out.println(prefix + tn);
			out.println(prefix + "  " + CTs.desc(tn));
		}
	}

	public static AbstractClassTransformer<?, ?> get() {
		return get("default", 0, "default".length());
	}

	public static AbstractClassTransformer<?, ?> get(String name, int off, int end) {
		AbstractClassTransformer<?, ?> ct = CTs.get(name, off, end);
		if (ct != null) {
			return ct;
		}
		int csi = 0;
		char[] cs = new char[(end - off)];// max & expected size
		boolean makeUpper = true;
		for (int typei = 0; typei < (end - off); typei++) {
			if (name.charAt(off + typei) < 'a' || name.charAt(off + typei) > 'z') {
				if (name.charAt(off + typei) != '-') {
					return finishGet(name, off, end, name.substring(off, end));
				}
				typei++;
				if (makeUpper) {
					throw new IllegalArgumentException("Invalid Transformer type: dash behind dash! type: '"
							+ name.substring(off, off + (end - off)) + "'");
				}
				makeUpper = true;
				continue;
			} else if (name.charAt(off + typei) == '.') {
				return finishGet(name, off, end, name.substring(off, end));
			}
			cs[csi++] = name.charAt(off + typei);
			if (makeUpper) {
				cs[csi - 1] += 'A' - 'a';
				makeUpper = false;
			}
		}
		if (makeUpper) {
			throw new IllegalArgumentException(
					"Invalid Transformer type: space at end of type: '" + name.substring(off, off + (end - off)) + "'");
		}
		String clsName = "de.hechler.patrick.profiler.agent.transform.".concat(new String(cs, 0, csi))
				.concat("ClassTranformer");
		return finishGet(name, off, end, clsName);
	}

	private static AbstractClassTransformer<?, ?> finishGet(String name, int off, int end, String clsName) {
		try {
			Class<?> cls = Class.forName(clsName);
			Constructor<?> con = cls.getConstructor();
			return (AbstractClassTransformer<?, ?>) con.newInstance();
		} catch (Exception e) {
			throw new IllegalArgumentException("Transformer Type invalid or failed to initilize! transformer: '"
					+ name.substring(off, off + (end - off)) + "', msg: '" + e.getMessage() + "'");
		}
	}

	public static String name(AbstractClassTransformer<?, ?> ct) {
		String name = CTs.name(ct);
		if (name != null) {
			return name;
		}
		String sm = ct.getClass().getSimpleName();
		if (sm.isEmpty() || !sm.endsWith("ClassTranformer")
				|| !ct.getClass().getName().startsWith("de.hechler.patrick.profiler.agent.transform.")) {
			return ct.getClass().getName();
		}
		StringBuilder sb = new StringBuilder(sm.length());
		sb.append(Character.toLowerCase(sm.charAt(0)));
		for (int i = 1; i < sm.length(); i++) {
			char c = sm.charAt(i);
			if (c >= 'A' && c <= 'Z') {
				sb.append('-');
				sb.append(c - 'A' + 'a');
			} else if (c >= 'A' && c <= 'Z') {
				sb.append(c);
			} else {
				return ct.getClass().getName();
			}
		}
		return sb.toString();
	}

}
