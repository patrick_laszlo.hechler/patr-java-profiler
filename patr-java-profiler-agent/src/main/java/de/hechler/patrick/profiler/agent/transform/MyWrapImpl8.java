// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import static de.hechler.patrick.profiler.agent.CTHelp.*;
import static de.hechler.patrick.profiler.agent.ClassBuffer.CLS_ACC_ANNOTATION;
import static de.hechler.patrick.profiler.agent.ClassBuffer.CLS_ACC_ENUM;
import static de.hechler.patrick.profiler.agent.ClassBuffer.CLS_ACC_INTERFACE;
import static de.hechler.patrick.profiler.agent.ClassBuffer.CLS_ACC_MODULE;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_ABSTRACT;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_BRIDGE;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_NATIVE;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_PRIVATE;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_PROTECTED;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_PUBLIC;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_STATIC;
import static de.hechler.patrick.profiler.agent.ClassBuffer.MET_ACC_SYNTHETIC;
import static de.hechler.patrick.profiler.agent.ClassBuffer.OFF_CP_LENP1;
import static de.hechler.patrick.profiler.agent.ClassBuffer.OFF_VERSION;
import static de.hechler.patrick.profiler.agent.ClassBuffer.STACK_TABLE_MIN_VERSION;
import static de.hechler.patrick.profiler.agent.ClassBuffer.getUShort;
import static de.hechler.patrick.profiler.agent.ClassBuffer.incUShort;
import static de.hechler.patrick.profiler.agent.ClassBuffer.isModUTF8;
import static de.hechler.patrick.profiler.agent.ClassBuffer.putInt;
import static de.hechler.patrick.profiler.agent.ClassBuffer.putUShort;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_ERR_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_FERR_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.MIN_OUT_LOG_LEVEL;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysew;
import static de.hechler.patrick.profiler.agent.PProfilerAgent.sysw;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.ProtectionDomain;
import java.util.Arrays;

import de.hechler.patrick.profiler.agent.Attribute;
import de.hechler.patrick.profiler.agent.AttributeList;
import de.hechler.patrick.profiler.agent.ClassBuffer;
import de.hechler.patrick.profiler.agent.ConstantPool;
import de.hechler.patrick.profiler.agent.Method;
import de.hechler.patrick.profiler.agent.MethodList;
import de.hechler.patrick.profiler.agent.ModificationList;
import de.hechler.patrick.profiler.agent.ModificationListStart;
import de.hechler.patrick.profiler.agent.PProfilerAgent;

/**
 * this class is used to transform classes so that they can be profiled by the
 * {@link PProfilerAgent}
 */
@SuppressWarnings("unused")
public class MyWrapImpl8 {

	private static final byte[] THREAD_CUR_THREAD_NAME = getBytes(THREAD_CUR_THREAD_NAME_STR);
	private static final byte[] THREAD_CUR_THREAD_DESC = getBytes(THREAD_CUR_THREAD_DESC_STR);
	private static final byte[] THREAD_NAME = getBytes(THREAD_NAME_STR);

	private static final byte[] SYSTEM_CURRENT_TIME_NAME = getBytes(SYSTEM_CURRENT_TIME_NAME_STR);
	private static final byte[] SYSTEM_NANO_TIME_NAME = getBytes(SYSTEM_NANO_TIME_NAME_STR);
	private static final byte[] SYSTEM_NAME = getBytes(SYSTEM_NAME_STR);
	private static final byte[] SYSTEM_TIME_DESC = getBytes(SYSTEM_TIME_DESC_STR);

	private static final byte[] INIT_NAME = getBytes(INIT_NAME_STR);
	private static final byte[] CLASS_INIT_NAME = getBytes(CLASS_INIT_NAME_STR);

	private static final byte[] PPROF_NAME = getBytes(PPROF_NAME_STR);
	private static final byte[] PPROF_ENTER_NAME = getBytes(PPROF_ENTER_NAME_STR);
	private static final byte[] PPROF_ENTER_DESC = getBytes(PPROF_ENTER_DESC_STR);
	private static final byte[] PPROF_DIRECT_EXIT_NAME = getBytes(PPROF_DIRECT_EXIT_EVEN_ERR_NAME_STR);
	private static final byte[] PPROF_DIRECT_EXIT_DESC = getBytes(PPROF_DIRECT_EXIT_DESC_STR);
	private static final byte[] PREFIX = getBytes(PREFIX_STR);

	private static final byte[] CALLER_SENSITIVE0_ANNOT_NAME = getBytes(CALLER_SENSITIVE0_ANNOT_NAME_STR);
	private static final byte[] CALLER_SENSITIVE1_ANNOT_NAME = getBytes(CALLER_SENSITIVE1_ANNOT_NAME_STR);
	private static final byte[] POLYMORPHIC_SIGNATURE_ANNOT_NAME = getBytes(POLYMORPHIC_SIGNATURE_ANNOT_NAME_STR);
	private static final byte[] INTRINSIC_CANDIDATE_ANNOT_NAME = getBytes(INTRINSIC_CANDIDATE_ANNOT_NAME_STR);

	private static final byte[] SUN_MISC_UNSAFE_THE_UNSAFE = getBytes(SUN_MISC_UNSAFE_THE_UNSAFE_STR);
	private static final byte[] SUN_MISC_UNSAFE_GET_UNSAFE_NAME = getBytes(SUN_MISC_UNSAFE_GET_UNSAFE_NAME_STR);
	private static final byte[] SUN_MISC_UNSAFE_GET_UNSAFE_DESC = getBytes(SUN_MISC_UNSAFE_GET_UNSAFE_DESC_STR);

	private static final byte[] SUK_MISC_UNSAFE_CPE;
	private static final byte[] SUK_MISC_UNSAFE_GET_UNSAFE_CPE;
	private static final byte[] SYSTEM_TIME_DESC_UTF_CPE;
	private static final byte[] SYSTEM_NANO_TIME_NAME_UTF_CPE;
	private static final byte[] SYSTEM_NAME_UTF_CPE;
	private static final byte[] PPROF_ENTER_NAME_CPE;
	private static final byte[] PPROF_ENTER_DESC_CPE;
	private static final byte[] PPROF_DIRECT_EXIT_NAME_CPE;
	private static final byte[] PPROF_DIRECT_EXIT_DESC_CPE;
	private static final byte[] PPROF_NAME_CPE;
	private static final byte[] INNER_CLASSES_ATTR_CPE;
	private static final byte[] CODE_ATTR_CPE;
	private static final byte[] EXCEPTIONS_ATTR_CPE;
	private static final byte[] MET_PARAMS_ATTR_CPE;
	private static final byte[] SYNTHETIC_ATTR_CPE;
	private static final byte[] SIGNATURE_ATTR_CPE;
	private static final byte[] RUNTIME_ANNOTATIONS_ATTR_CPE;
	private static final byte[] STACK_MAP_TABLE_ATTR_CPE;
	private static final byte[] THROWABLE_NAME_CPE;

	static {
		SUK_MISC_UNSAFE_CPE = utfEntryBytes(getBytes(SUK_MISC_UNSAFE_NAME_STR));
		SUK_MISC_UNSAFE_GET_UNSAFE_CPE = utfEntryBytes(getBytes(SUK_MISC_UNSAFE_GET_UNSAFE_DESC_STR));
		SYSTEM_NAME_UTF_CPE = utfEntryBytes(SYSTEM_NAME);
		SYSTEM_TIME_DESC_UTF_CPE = utfEntryBytes(SYSTEM_TIME_DESC);
		SYSTEM_NANO_TIME_NAME_UTF_CPE = utfEntryBytes(SYSTEM_NANO_TIME_NAME);
		PPROF_ENTER_NAME_CPE = utfEntryBytes(PPROF_ENTER_NAME);
		PPROF_ENTER_DESC_CPE = utfEntryBytes(PPROF_ENTER_DESC);
		PPROF_DIRECT_EXIT_NAME_CPE = utfEntryBytes(PPROF_DIRECT_EXIT_NAME);
		PPROF_DIRECT_EXIT_DESC_CPE = utfEntryBytes(PPROF_DIRECT_EXIT_DESC);
		PPROF_NAME_CPE = utfEntryBytes(PPROF_NAME);
		INNER_CLASSES_ATTR_CPE = utfEntryBytes(getBytes(INNER_CLASSES_ATTR_STR));
		CODE_ATTR_CPE = utfEntryBytes(ClassBuffer.CODE_ATTR_NAME);
		EXCEPTIONS_ATTR_CPE = utfEntryBytes(getBytes(EXCEPTIONS_ATTR_STR));
		MET_PARAMS_ATTR_CPE = utfEntryBytes(getBytes(METHOD_PARAMETERS_ATTR_STR));
		SYNTHETIC_ATTR_CPE = utfEntryBytes(getBytes(SYNTHETIC_ATTR_STR));
		SIGNATURE_ATTR_CPE = utfEntryBytes(getBytes(SIGNATURE_ATTR_STR));
		RUNTIME_ANNOTATIONS_ATTR_CPE = utfEntryBytes(getBytes(RUNTIME_VISIBLE_ANNOTATIONS_ATTR_STR));
		STACK_MAP_TABLE_ATTR_CPE = utfEntryBytes(getBytes(STACK_MAP_TABLE_ATTR_STR));
		THROWABLE_NAME_CPE = utfEntryBytes(getBytes(THROWABLE_NAME_STR));
	}

	private static byte[] utfEntryBytes(byte[] t) {
		byte[] res = new byte[3 + t.length];
		res[0] = ConstantPool.CONSTANT_UTF8;
		putUShort(res, 1, t.length);
		System.arraycopy(t, 0, res, 3, t.length);
		return res;
	}

	private MyWrapImpl8() {
	}

	public static ClassBuffer createHelper(String module, ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer)
			throws IllegalClassFormatException {
		ClassBuffer cls = new ClassBuffer(classfileBuffer);
		return initHelper(className, cls);
	}

	public static <C extends ClassBuffer> C initHelper(String className, C cls) throws IllegalClassFormatException {
		cls.readCP();
		if ((cls.accessFlags() & (CLS_ACC_ANNOTATION | CLS_ACC_MODULE)) != 0) {
			return null;
		}
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			sysw(PProfilerAgent.name());
			sysw(": transform ");
			if ((cls.accessFlags() & CLS_ACC_INTERFACE) != 0) {
				sysw("interface ");
			} else if ((cls.accessFlags() & CLS_ACC_ENUM) != 0) {
				sysw("enum ");
			} else {
				sysw("class ");
			}
			sysw(className);
			sysw("\n");
		}
		MethodList mets = cls.readMethods();
		if (mets == null)
			return null;
		return cls;
	}

	public static <C extends ClassBuffer> byte[] transformOtherOrBootstrap(AbstractClassTransformer<C, ?> me, C cls,
			String className, Class<?> classBeingRedefined) throws IllegalClassFormatException {
		byte[] d = cls.buffer();
		AttributeList attrs = cls.readAttributes();
		ConstantPool cp = cls.constPool();
		int classEntryIndex = getUShort(d, cp.end + 2);
		if (attrs != null) {
			int innerClss = cp.findCPEntry(d, INNER_CLASSES_ATTR_CPE);
			if (innerClss != 0) {
				AttributeList attr = attrs.get(cls, innerClss);
				if (attr != null) {
					int attrOff = attr.attr.offset;
					int numCls = getUShort(d, attrOff + 6);
					for (attrOff += 8; --numCls >= 0 && attrOff + 8 <= attr.attr.end; attrOff += 8) {
						int innerClsEntry = getUShort(d, attrOff);
						if (innerClsEntry == classEntryIndex) {
							int oce = getUShort(d, attrOff + 2);
							if (oce == 0) {
								break;
							}
							classEntryIndex = oce;
							break;
						}
					}
				}
			}
		}
		int classEntryOffset = cp.offsets[classEntryIndex - 1];
		int classUTFOffset = cp.offsets[getUShort(d, classEntryOffset + 1) - 1];
		if (isModUTF8(d, PPROF_NAME, classUTFOffset)) {
			return me.transformMyInternBootstrap(cls, className, classBeingRedefined);
		}
		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			sysew("profile ");
			sysew(className);
			sysew(" : ");
			sysew(String.valueOf(d.length));
			sysew("\n");
		}
		return me.transformOther(cls, className, classBeingRedefined);
	}

	public static final class CPIndices {

		int curMetNameStr;
		int curMetDescStr;

		int myClassStr;
		int systemClass;
		int systemNanoMethodRef;
		int pprofClass;
		int pprofEnterMethodRef;
//		int pprofExitMethodRef;
		int pprofDirectExitMethodRef;

		int throwableClass;
		int stackMapTableAttr;
		// various method attributes of different importance
		int runtimeAnnotsAttr; // look, special handling
		int codeAttr; // move to wrapped, expect at most one
		int exceptionsAttr; // copy to wrapped
		int metParamsAttr; // copy to wrapped
		int syntheticAttr; // copy to wrapped
		int signatureAttr; // copy to wrapped

	}

	public static byte[] transformOther(ClassBuffer cls, String className, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			sysw("transform some other class\n");
		}
		ModificationListStart cpMods = new ModificationListStart();
		ModificationListStart mods = new ModificationListStart();
		byte[] d = cls.buffer();
		byte[] unmodified = LOG_LEVEL >= MIN_ERR_LOG_LEVEL ? d.clone() : null;
		ConstantPool cp = cls.constPool();
		CPIndices cpis = new CPIndices();
		cpis.runtimeAnnotsAttr = cp.findCPEntry(d, RUNTIME_ANNOTATIONS_ATTR_CPE);
		cpis.codeAttr = cp.findCPEntry(d, CODE_ATTR_CPE);
		cpis.exceptionsAttr = cp.findCPEntry(d, EXCEPTIONS_ATTR_CPE);
		cpis.metParamsAttr = cp.findCPEntry(d, MET_PARAMS_ATTR_CPE);
		cpis.syntheticAttr = cp.findCPEntry(d, SYNTHETIC_ATTR_CPE);
		cpis.signatureAttr = cp.findCPEntry(d, SIGNATURE_ATTR_CPE);
		if (cls.version() >= ClassBuffer.STACK_TABLE_MIN_VERSION) {
			cpis.stackMapTableAttr = cp.findCPEntry(d, STACK_MAP_TABLE_ATTR_CPE);
			cpis.throwableClass = cp.findCPEntryTilde(d, THROWABLE_NAME_CPE);
		}
		byte[] buf = new byte[3];
		buf[0] = ConstantPool.CONSTANT_CLASS;
		cpis.systemClass = cp.findRefCPEntry(d, buf, SYSTEM_NAME_UTF_CPE);
		cpis.pprofClass = cp.findRefCPEntry(d, buf, PPROF_NAME_CPE);
		boolean unwrappableMethod = false;
		for (MethodList mets = cls.methods(); mets != null; mets = mets.next) {
			Method met = mets.met;
			int acc = met.accessFlags(d);
			if ((acc & MET_ACC_SYNTHETIC) != 0)
				continue;
			if (classBeingRedefined != null) {
				if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
					if (!unwrappableMethod) {
						sysw(String.valueOf(classBeingRedefined));
						sysw(" has methods which can not be profiled\n");
						unwrappableMethod = true;
					}
				}
				continue;
			}
			int originalNameOff = cp.offsets[getUShort(d, met.offset + 2) - 1];
			if ((acc & MET_ACC_ABSTRACT) != 0) {
				continue;
			}
			if ((acc & MET_ACC_NATIVE) != 0) {
				int clsCPI = getUShort(d, cp.end + 2);
				int descCPI = getUShort(d, met.offset + 4);
				int descOff = cp.offsets[descCPI - 1];
				if (isModUTF8(d, SYSTEM_TIME_DESC, descOff)
						&& (isModUTF8(d, SYSTEM_CURRENT_TIME_NAME, originalNameOff)
								|| isModUTF8(d, SYSTEM_NANO_TIME_NAME, originalNameOff))
						&& isModUTF8(d, SYSTEM_NAME, cp.offsets[clsCPI - 1])
						|| isModUTF8(d, THREAD_CUR_THREAD_DESC, descOff)
								&& isModUTF8(d, THREAD_CUR_THREAD_NAME, originalNameOff)
								&& isModUTF8(d, THREAD_NAME, cp.offsets[clsCPI - 1])) {
					if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
						// should never hapen since System and Thread are already loaded
						// (maybe System is not loaded when LOG_LEVEL is lower, but LOG_LEVEL is set at
						// compile time)
						sysew("the methods System.nanoTime/currentTimeMillis and Thread.currentThread can not be profiled\n");
					}
					continue;
				}
			} else if (isModUTF8(d, CLASS_INIT_NAME, originalNameOff) || isModUTF8(d, INIT_NAME, originalNameOff)) {
				continue;// $$PatrJavaProfiler$$<init> is not a valid method name
			}
			wrapMethod(cls, cpMods, mods, d, cp, cpis, met);
		}
		if (mods.end != null) {
			registerModification(cpMods, mods);// append mods to cpMods
		}
		byte[] newData = applyModifications(cpMods, d);
		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			Path pMod = Paths.get("testout/patrjprof", className + ".mod.class");
			Path pMHx = Paths.get("testout/patrjprof", className + ".mod.class.hex");
			Path pOrg = Paths.get("testout/patrjprof", className + ".class");
			try {
				Files.createDirectories(pOrg.getParent());
				try (OutputStream out = Files.newOutputStream(pOrg)) {
					out.write(unmodified);
				}
				try (OutputStream out = Files.newOutputStream(pMod)) {
					out.write(newData);
				}
				try (BufferedWriter out = Files.newBufferedWriter(pMHx, StandardCharsets.UTF_8)) {
					for (int i = 0; i < newData.length; i += 8) {
						for (int ii = 0; ii < 8 && i + ii < newData.length; ii++) {
							if (ii != 0)
								out.write(' ');
							String str = Integer.toHexString(0xFF & newData[i + ii]).toUpperCase();
							if (str.length() == 1)
								out.write('0');
							out.write(str);
						}
						out.newLine();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			cls.data(unmodified);
			cls.readCP();
			cls.readMethods();
			cls.readAttributes();
			ClassBuffer ncls = new ClassBuffer(newData);
			ncls.readCP();
			ncls.readMethods();
			int methodEnd = cls.methodsEnd();
			int nMethodEnd = ncls.methodsEnd();
			byte[] afterMethods = Arrays.copyOfRange(unmodified, methodEnd, unmodified.length);
			byte[] nAfterMethods = Arrays.copyOfRange(newData, nMethodEnd, newData.length);
			if (!Arrays.equals(afterMethods, nAfterMethods)) {
				System.err.println(Arrays.toString(afterMethods));
				System.err.println(Arrays.toString(nAfterMethods));
				System.err.println(Arrays
						.toString(Arrays.copyOfRange(nAfterMethods, 0, nAfterMethods.length - afterMethods.length)));
				System.err.println(nAfterMethods.length - afterMethods.length);
				System.err.println(Arrays.toString(Arrays.copyOfRange(nAfterMethods,
						nAfterMethods.length - afterMethods.length, nAfterMethods.length)));
				throw new AssertionError("ERROR");
			}
			ncls.readAttributes();
			MethodList nmets = ncls.methods();
			for (MethodList mets = cls.methods(); mets != null; mets = mets.next) {
				int nameOff = mets.met.nameOff(cls);
				int descOff = mets.met.descriptorOff(cls);
				int nameLen = getUShort(unmodified, nameOff + 1);
				int descLen = getUShort(unmodified, descOff + 1);
				byte[] name = new byte[nameLen];
				byte[] desc = new byte[descLen];
				System.arraycopy(unmodified, nameOff + 3, name, 0, nameLen);
				System.arraycopy(unmodified, descOff + 3, desc, 0, descLen);
				Method nmet = nmets.get(ncls, name, desc);
				if (nmet == null) {
					throw new AssertionError("I removed the method " + getString(name) + getString(desc));
				}
			}
		}
		return newData;
	}

	private static void wrapMethod(ClassBuffer cls, ModificationListStart cpMods, ModificationListStart mods, byte[] d,
			ConstantPool cp, CPIndices cpis, Method met) throws AssertionError {
		if (cpis.codeAttr == 0) {
			if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
				sysew("THE CLASS FILE DOES NOT HAVE A SINGLE Code ATTRIBUTE (");
				sysew(cls.name());
				sysew(")\n");
			}
			cpis.codeAttr = addCPE(cpMods, d, cp, CODE_ATTR_CPE);
		}
		final int macc = getUShort(d, met.offset);
		putUShort(d, met.offset, macc & ~MET_ACC_NATIVE);
		int oldCpLenP1 = getUShort(d, OFF_CP_LENP1);
		// add a methodref to the newly created method in the constant pool
		ModificationList newMod = new ModificationList();
		putUShort(d, OFF_CP_LENP1, oldCpLenP1 + 3);
		int originalNameCPI = getUShort(d, met.offset + 2);
		int originalNameOff = cp.offsets[originalNameCPI - 1];
		int originalNameLen = getUShort(d, originalNameOff + 1);
		int descCPI = getUShort(d, met.offset + 4);
		byte[] prefix;
//		if ( ( acc & MET_ACC_STATIC ) != 0 || ( cls.accessFlags() & CLS_ACC_INTERFACE ) == 0 ) {
		prefix = PREFIX;
//		} else {
//			prefix = INTERFACE_PREFIX;
//		}
		byte[] newModData = new byte[13 + prefix.length + originalNameLen];
		int clsCPI = getUShort(d, cp.end + 2);
		int newMethodrefCPI = oldCpLenP1;
		int newNameCPI = oldCpLenP1 + 2;
		if ((cls.accessFlags() & CLS_ACC_INTERFACE) == 0) {
			newModData[0] = ConstantPool.CONSTANT_METHODREF;
		} else {
			newModData[0] = ConstantPool.CONSTANT_INTERFACE_METHODREF;
		}
		putUShort(newModData, 1, clsCPI);
		putUShort(newModData, 3, oldCpLenP1 + 1);
		newModData[5] = ConstantPool.CONSTANT_NAME_AND_TYPE;
		putUShort(newModData, 6, newNameCPI);
		putUShort(newModData, 8, descCPI);
		newModData[10] = ConstantPool.CONSTANT_UTF8;
		putUShort(newModData, 11, prefix.length + originalNameLen);
		System.arraycopy(prefix, 0, newModData, 13, prefix.length);
		System.arraycopy(d, originalNameOff + 3, newModData, 13 + prefix.length, originalNameLen);
		newMod.offset = cp.end;
		newMod.replacement = newModData;
		// newMod.debug = "constant pool methodref for new method ("
//					+ new String(newModData, 13, prefix.length + originalNameLen, UTF_8) + "): ";
		registerModification(cpMods, newMod);
		// add the Code attribute
		newMod = new ModificationList();
		int attributeCopyCount = 0;
		ModificationListStart copyToWrappedAttrs = new ModificationListStart();
		if (met.attributes != null) {
			if (cpis.runtimeAnnotsAttr != 0) {
				AttributeList attr = met.attributes.get(cls, cpis.runtimeAnnotsAttr);
				if (attr != null) {
					int off = attr.attr.offset;
					int numAnnots = getUShort(d, off + 6);
					off += 8;
					while (--numAnnots >= 0) {
						int typeOff = cp.offsets[getUShort(d, off) - 1];
						if (isModUTF8(d, CALLER_SENSITIVE0_ANNOT_NAME, typeOff)
								|| isModUTF8(d, CALLER_SENSITIVE1_ANNOT_NAME, typeOff)
								|| isModUTF8(d, POLYMORPHIC_SIGNATURE_ANNOT_NAME, typeOff)
								|| isModUTF8(d, INTRINSIC_CANDIDATE_ANNOT_NAME, typeOff)) {
							if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
								sysew("skip caller sensitive method or a with a polymorphic signature: ");
								int name = met.nameOff(cls);
								sysew(d, name + 3, getUShort(d, name + 1));
								sysew("\n");
							}
							return;
						}
						off = skipAnnotation(d, off);
					}
				}
			}
			AttributeList attr = met.attributes.get(cls, cpis.codeAttr);
			if (attr != null) {
				newMod.offset = attr.attr.offset;
				newMod.length = attr.attr.end - attr.attr.offset;
				attributeCopyCount = registerCopyAttrModification(d, copyToWrappedAttrs, attributeCopyCount, attr.attr);
			} else {// for example native methods usually don't have a Code attribute
				newMod.offset = met.end;
				incUShort(d, met.offset + 6);
			}
			attributeCopyCount = copyAttrToWrappedIfExists(cls, d, met, attributeCopyCount, copyToWrappedAttrs,
					cpis.exceptionsAttr);
			attributeCopyCount = copyAttrToWrappedIfExists(cls, d, met, attributeCopyCount, copyToWrappedAttrs,
					cpis.metParamsAttr);
			attributeCopyCount = copyAttrToWrappedIfExists(cls, d, met, attributeCopyCount, copyToWrappedAttrs,
					cpis.signatureAttr);
			attributeCopyCount = copyAttrToWrappedIfExists(cls, d, met, attributeCopyCount, copyToWrappedAttrs,
					cpis.syntheticAttr);
		} else {
			// for example native methods usually don't have any attributes if they don't
			// have annotations/fancy stuff
			newMod.offset = met.end;
			incUShort(d, met.offset + 6);
		}
		initClsAndMetStrCPIs(cpMods, d, cp, cpis, originalNameCPI, descCPI);
		initAttrCPIs(cpMods, d, cp, cpis);
		initSystemCPIs(cpMods, d, cp, cpis);
		initPProfCPIs(cpMods, d, cp, cpis);
		long stackSizeLocalVarsCommandSize = countStackSizeLocalVarsCommandSize(cls, descCPI, macc, 0, 4);
		int commandSize = (int) (stackSizeLocalVarsCommandSize >>> 32);
		sysw("orig:commandSize=");
		sysw(Integer.toString(commandSize));
		commandSize += 6;// call nanoTime; {dup2}; call enterMethod (without 3*ldc,{1*lstore})
		sysw("\nnano;enter;-3*LDCW-1*LS:commandSize=");
		sysw(Integer.toString(commandSize));
		commandSize += 6;// call nanoTime, exitMethod {without 1*lload}
		sysw("\nnano;exit;-1*LL:commandSize=");
		sysw(Integer.toString(commandSize));
		commandSize += 7;// call nanoTime, exitMethod; athorw {without 1*lload}
		sysw("\nnano;exit;athrow;1*-LL:commandSize=");
		sysw(Integer.toString(commandSize));
		commandSize += ldcLen(cpis.myClassStr);
		commandSize += ldcLen(cpis.curMetNameStr);
		commandSize += ldcLen(cpis.curMetDescStr);
		sysw("\n[0-3]*LDCW:commandSize=");
		sysw(Integer.toString(commandSize));
		int stackSize = (0xFF & (int) (stackSizeLocalVarsCommandSize >>> 16));
		sysw("\nstackSize=");
		sysw(Integer.toString(stackSize));
		int paramSize = (0xFF & (int) (stackSizeLocalVarsCommandSize >>> 24));
		sysw("\nparamSize=");
		sysw(Integer.toString(paramSize));
		if (stackSize < 5)
			stackSize = 5;
		sysw("\nstackSize=");
		sysw(Integer.toString(stackSize));
		int localVars = (0xFFFF & (int) stackSizeLocalVarsCommandSize);
		sysw("\nlocalVars=");
		sysw(Integer.toString(localVars));
//		int loadStoreOffAdd = loadStoreOffAdd(localVars - 2);
//		sysw("\nloadStoreOffAdd=");
//		sysw(Integer.toString(loadStoreOffAdd));
//		commandSize += loadStoreOffAdd * 3;
		sysw("\n3*l(laod/store):commandSize=");
		sysw(Integer.toString(commandSize));
//		int appendFramePosition = 4 + loadStoreOffAdd;
//		sysw("\nappendFramePosition=");
//		sysw(Integer.toString(appendFramePosition));
		int relativeHandlerPosition = commandSize - 7; // commandSize - loadStoreOffAdd - 7;
		sysw("\nrelativeHandlerPosition=");
		sysw(Integer.toString(relativeHandlerPosition));
		sysw("\n");
		int afterSize = 10;
		if (cls.version() >= ClassBuffer.STACK_TABLE_MIN_VERSION) {
			afterSize += 14;
			if (needExtendedEqL1S(relativeHandlerPosition, 0)) {
				afterSize += 2;
			}
		}
		newModData = new byte[14 + commandSize + afterSize];
		putUShort(newModData, 0, cpis.codeAttr);
		putInt(newModData, 2, newModData.length - 6);
		putUShort(newModData, 6, stackSize);
		putUShort(newModData, 8, localVars);
		putInt(newModData, 10, commandSize);
		int descOffset = cls.constPool().offsets[descCPI - 1];
		int descRem = getUShort(d, descOffset + 1);
		int nmdOff = 14;
		descOffset += 4;// 3 for the UTF cp-entry and 1 for the initial '('
		int lv = 0;

		newModData[nmdOff] = (byte) 0xB8;// invokestatic
		putUShort(newModData, nmdOff + 1, cpis.systemNanoMethodRef);
//		newModData[nmdOff + 3] = (byte) 0x5C;// dup2
//		if ( nmdOff + 4 - 14 != appendFramePosition ) {
//			throw new AssertionError(( nmdOff + 4 - 14 ) + " : " + appendFramePosition);
//		}
		nmdOff += 3; // loadStoreLocalVar(newModData, nmdOff + 4, localVars - 2, 0x3F, 0x37);//
						// lstore
		sysw("nmdOff-14=");
		sysw(Integer.toString(nmdOff - 14));
		sysw(", invoke nano; dup2; lstore done\n");
		nmdOff = insertLDC(cpis.myClassStr, newModData, nmdOff);
		sysw("nmdOff-14=");
		sysw(Integer.toString(nmdOff - 14));
		sysw(", LDC className done\n");
		nmdOff = insertLDC(cpis.curMetNameStr, newModData, nmdOff);
		sysw("nmdOff-14=");
		sysw(Integer.toString(nmdOff - 14));
		sysw(", LDC methodName done\n");
		nmdOff = insertLDC(cpis.curMetDescStr, newModData, nmdOff);
		sysw("nmdOff-14=");
		sysw(Integer.toString(nmdOff - 14));
		sysw(", LDC methodDesc done\n");
		newModData[nmdOff] = (byte) 0xB8;// invokestatic
		putUShort(newModData, nmdOff + 1, cpis.pprofEnterMethodRef);
		nmdOff += 3;
		sysw("nmdOff-14=");
		sysw(Integer.toString(nmdOff - 14));
		sysw(", invoke enter done\n");
		if (nmdOff != (14 + 6 //
				+ (needWideLdc(cpis.myClassStr) ? 3 : 2) //
				+ (needWideLdc(cpis.curMetNameStr) ? 3 : 2) //
				+ (needWideLdc(cpis.curMetDescStr) ? 3 : 2))) {
			throw new AssertionError(nmdOff - 14 + " : " + (6//
					+ (needWideLdc(cpis.myClassStr) ? 3 : 2) //
					+ (needWideLdc(cpis.curMetNameStr) ? 3 : 2) //
					+ (needWideLdc(cpis.curMetDescStr) ? 3 : 2)));
		}

		int relativeHandlerStartCatch = nmdOff - 14;

		if ((macc & MET_ACC_STATIC) == 0) {
			sysw("nmdOff-14=");
			sysw(String.valueOf(nmdOff - 14));
			sysw("\n");
			nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x2A, 0x19);// aload
			lv++;
		}
		loop: for (;; descOffset++, descRem--) {
			sysw("nmdOff-14=");
			sysw(String.valueOf(nmdOff - 14));
			sysw(", descRem=");
			sysw(String.valueOf(descRem));
			sysw(", d[descOff]=");
			sysw(String.valueOf((char) d[descOffset]));
			sysw("\n");
			int nlv;
			switch (d[descOffset]) {
			case '[': {
				int acnl = arrayClassNameLength(d, descOffset);
				descOffset += acnl;
				descRem -= acnl;
				nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x2A, 0x19);// aload
				nlv = lv + 1;
				break;
			}
			case 'L': {
				do {
					descRem--;
				} while (d[++descOffset] != ';');
				nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x2A, 0x19);// aload
				nlv = lv + 1;
				break;
			}
			case 'D': {
				nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x26, 0x18);// dload
				nlv = lv + 2;
				break;
			}
			case 'F': {
				nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x22, 0x17);// fload
				nlv = lv + 1;
				break;
			}
			case 'B':
			case 'C':
			case 'Z':
			case 'S':
			case 'I': {
				nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x1A, 0x15);// iload
				nlv = lv + 1;
				break;
			}
			case 'J': {
				nmdOff = loadStoreLocalVar(newModData, nmdOff, lv, 0x1E, 0x16);// lload
				nlv = lv + 2;
				break;
			}
			case ')':
				break loop;
			default:
				throw new AssertionError((char) d[descOffset] + " : " + descRem);
			}
			lv = nlv;
		}
		if ((macc & MET_ACC_STATIC) != 0) {
			newModData[nmdOff] = (byte) 0xB8;// invokestatic
		} else // if ((cls.accessFlags() & CLS_ACC_INTERFACE) != 0)
		{
			newModData[nmdOff] = (byte) 0xB7;// invokespecial
//		} else {
//			newModData[nmdOff] = (byte) 0xB6;// invokevirtual
		}
		putUShort(newModData, nmdOff + 1, newMethodrefCPI);
		nmdOff += 3;
		sysw("nmdOff-14=");
		sysw(String.valueOf(nmdOff - 14));
		sysw(", invoke-wrapped done");
		sysw("\n");

		newModData[nmdOff] = (byte) 0xB8;// invokestatic
		putUShort(newModData, nmdOff + 1, cpis.systemNanoMethodRef);
		nmdOff += 3; // loadStoreLocalVar(newModData, nmdOff + 3, localVars - 2, 0x1E, 0x16);// lload

		sysw("nmdOff-14=");
		sysw(String.valueOf(nmdOff - 14));
		sysw(", invoke exits nanoTime done");
		sysw("\n");

		int relativeHandlerEndCatch = nmdOff - 14;
		newModData[nmdOff] = (byte) 0xB8;// invokestatic
		putUShort(newModData, nmdOff + 1, cpis.pprofDirectExitMethodRef);

		sysw("nmdOff-14+3=");
		sysw(String.valueOf(nmdOff - 14 + 3));
		sysw(", invoke exit done");
		sysw("\n");

		switch (d[descOffset + 1]) {
		case '[':
		case 'L':
			newModData[nmdOff + 3] = (byte) 0xB0;// areturn
			break;
		case 'D':
			newModData[nmdOff + 3] = (byte) 0xAF;// dreturn
			break;
		case 'F':
			newModData[nmdOff + 3] = (byte) 0xAE;// freturn
			break;
		case 'B':
		case 'C':
		case 'Z':
		case 'S':
		case 'I':
			newModData[nmdOff + 3] = (byte) 0xAC;// ireturn
			break;
		case 'J':
			newModData[nmdOff + 3] = (byte) 0xAD;// lreturn
			break;
		case 'V':
			newModData[nmdOff + 3] = (byte) 0xB1;// return
			break;
		default:
			throw new AssertionError((char) d[descOffset]);
		}

		sysw("nmdOff-14+4=");
		sysw(String.valueOf(nmdOff - 14 + 4));
		sysw(", return done");
		sysw("\n");

		if (nmdOff + 4 - 14 != relativeHandlerPosition) {
			sysw("code until now: ");
			ClassBuffer.writeHex(newModData, 14, nmdOff - 14 + 4);
			sysw("\n  nmdOff-14+4=");
			sysw(String.valueOf(nmdOff - 14 + 4));
			sysw("\n  systemNanoMethodRef=");
			sysw(Integer.toHexString(cpis.systemNanoMethodRef));
			sysw("\n  pprofEnterMethodRef=");
			sysw(Integer.toHexString(cpis.pprofEnterMethodRef));
			sysw("\n  pprofDirectExitMethodRef=");
			sysw(Integer.toHexString(cpis.pprofDirectExitMethodRef));
			sysw("\n  myClassStr=");
			sysw(Integer.toHexString(cpis.myClassStr));
			sysw("\n  curMetNameStr=");
			sysw(Integer.toHexString(cpis.curMetNameStr));
			sysw("\n  curMetDescStr=");
			sysw(Integer.toHexString(cpis.curMetDescStr));
			sysw("\n");
			throw new AssertionError((nmdOff - 14 + 4) + " : " + relativeHandlerPosition);
		}
		newModData[nmdOff + 4] = (byte) 0xB8;// invokestatic
		putUShort(newModData, nmdOff + 5, cpis.systemNanoMethodRef);
		nmdOff += 7; // loadStoreLocalVar(newModData, nmdOff + 7, localVars - 2, 0x1E, 0x16);// lload
		newModData[nmdOff] = (byte) 0xB8;// invokestatic
		putUShort(newModData, nmdOff + 1, cpis.pprofDirectExitMethodRef);
		newModData[nmdOff + 3] = (byte) 0xBF;// athrow

		if (nmdOff + 4 - 14 != relativeHandlerPosition + 7) {
			sysw("code until now: ");
			ClassBuffer.writeHex(newModData, 14, nmdOff - 14 + 4);
			sysw("\n  nmdOff-14+4=");
			sysw(String.valueOf(nmdOff - 14 + 4));
			sysw("\n  systemNanoMethodRef=");
			sysw(Integer.toHexString(cpis.systemNanoMethodRef));
			sysw("\n  pprofEnterMethodRef=");
			sysw(Integer.toHexString(cpis.pprofEnterMethodRef));
			sysw("\n  pprofDirectExitMethodRef=");
			sysw(Integer.toHexString(cpis.pprofDirectExitMethodRef));
			sysw("\n  myClassStr=");
			sysw(Integer.toHexString(cpis.myClassStr));
			sysw("\n  curMetNameStr=");
			sysw(Integer.toHexString(cpis.curMetNameStr));
			sysw("\n  curMetDescStr=");
			sysw(Integer.toHexString(cpis.curMetDescStr));
			sysw("\n");
			throw new AssertionError((nmdOff + 4 - 14) + " : " + (relativeHandlerPosition + 7));
		}

		if (nmdOff - 10 != commandSize) {
			throw new AssertionError((nmdOff + 4 - 14) + " : " + commandSize);
		}

		putUShort(newModData, nmdOff + 4, 1);// exception_table_length
		putUShort(newModData, nmdOff + 6, relativeHandlerStartCatch);// exception_table_entry : start_pc
		putUShort(newModData, nmdOff + 8, relativeHandlerEndCatch);// exception_table_entry : end_pc
		putUShort(newModData, nmdOff + 10, relativeHandlerPosition);// exception_table_entry : handler_pc
		putUShort(newModData, nmdOff + 12, 0);// exception_table_entry : catch_type

		nmdOff += 14;
		if (nmdOff != commandSize + 14 + 10) {
			throw new AssertionError(nmdOff + " : " + (commandSize + 14 + 10));
		}

		if (cls.version() >= ClassBuffer.STACK_TABLE_MIN_VERSION) {
			putUShort(newModData, nmdOff, 1);// attributes_count : 1 (StackMapTable)
			putUShort(newModData, nmdOff + 2, cpis.stackMapTableAttr);// attribute_name_index
			putInt(newModData, nmdOff + 4, newModData.length - nmdOff - 8);// attribute_length
			putUShort(newModData, nmdOff + 8, 1);// number_of_entries
			if (needExtendedEqL1S(relativeHandlerPosition, 0)) {
				newModData[nmdOff + 10] = (byte) 247; // frame_type = same_locals_1_stack_item_frame_extended [247]
				putUShort(newModData, nmdOff + 11, relativeHandlerPosition);// offset_delta
				nmdOff += 13;
			} else {
				// same_locals_1_stack_item_frame
				newModData[nmdOff + 10] = (byte) (64 + relativeHandlerPosition);
				// frame_type = same_locals_1_stack_item_frame [64..127]
				nmdOff += 11;
			}
			newModData[nmdOff] = (byte) 7; // Object_variable_info : tag = ITEM_Object
			putUShort(newModData, nmdOff + 1, cpis.throwableClass);// Object_variable_info : cpool_index
			nmdOff += 3;
		} else {
			nmdOff += 2;
//			putUShort(newModData, nmdOff - 2, 0);// attributes_count // zero init
		}
		if (nmdOff != newModData.length) {
			throw new AssertionError(nmdOff + " : " + newModData.length);
		}

		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			Attribute ra;
			try {
				ra = cls.readMethodAttr(newModData, 0);
			} catch (Throwable e) {
				sysew("THIS IS A BUG: result of readMethod is not as expected! expected: ");
				sysew(String.valueOf(newModData.length));
				sysew(" actual: ");
				sysew(e.toString());
				sysew("\n");
				throw e;
			}
			if (ra.end != newModData.length) {
				sysew("THIS IS A BUG: result of readMethod is not as expected! expected: ");
				sysew(String.valueOf(newModData.length));
				sysew(" actual: ");
				sysew(String.valueOf(ra.end));
				sysew("\n");
				throw new AssertionError("BUG in parseCodeAttr");
			}
		}
		// putUShort(newModData, newModData.length-4, 0);//zero init
//				putUShort(newModData, newModData.length-2, 0);//zero init
		newMod.replacement = newModData;
		// newMod.debug =
//					"Code attribute for " + new String(d, originalNameOff + 3, originalNameLen, UTF_8) + ": ";
		registerModification(mods, newMod);
		// add native method with prefix
		newMod = new ModificationList();
		incUShort(d, cls.fieldsEnd());// increment method count
		newMod.offset = met.end;
		// if met.start, the modification must be instered before the Code attribute
		// modification
		newModData = new byte[8];
		putUShort(newModData, 0, //
				(macc & ~(MET_ACC_PROTECTED | MET_ACC_PUBLIC))//
						| (MET_ACC_SYNTHETIC | MET_ACC_BRIDGE | MET_ACC_PRIVATE));
		// is this a bridge method?
		putUShort(newModData, 2, newNameCPI);
		putUShort(newModData, 4, descCPI);
		putUShort(newModData, 6, attributeCopyCount);
		newMod.replacement = newModData;
		// newMod.debug = "native method " + prefix
//					+ new String(d, originalNameOff + 3, originalNameLen, UTF_8) + ": ";
		newMod.replacement = applyModifications(copyToWrappedAttrs, newModData);
		registerModification(mods, newMod);
		// END
	}

	private static boolean needExtendedEqL1S(int relativeHandlerPosition, int appendFramePosition) {
		return relativeHandlerPosition - appendFramePosition > 127 - 63;
	}

	private static int skipAnnotation(byte[] d, int off) throws ClassFormatError {
		int numEVPs = getUShort(d, off + 2);
		return skipAnnotationElementValuePairs(d, off + 4, numEVPs);
	}

	private static int skipAnnotationElementValuePairs(byte[] d, int off, int numEVPs) throws ClassFormatError {
		return skipAnnotationElementValueOptNameImpl(d, off, numEVPs, 2);
	}

	private static int skipAnnotationElementValues(byte[] d, int off, int numEVPs) throws ClassFormatError {
		return skipAnnotationElementValueOptNameImpl(d, off, numEVPs, 0);
	}

	private static int skipAnnotationElementValueOptNameImpl(byte[] d, int off, int numEVPs, int startSize)
			throws ClassFormatError {
		while (--numEVPs >= 0) {
			off += startSize;
			switch (d[off]) {
			case 'B':
			case 'C':
			case 'D':
			case 'F':
			case 'I':
			case 'J':
			case 'S':
			case 'Z':
			case 's':
			case 'c':
				off += 3;
				break;
			case 'e':
				off += 5;
				break;
			case '@':
				off = skipAnnotation(d, off + 1);
				break;
			case '[':
				int numValues = getUShort(d, off + 1);
				off = skipAnnotationElementValues(d, off + 3, numValues);
				break;
			default:
				throw new ClassFormatError("invalid annotation value tag: '" + d[off + 2] + "'");
			}
		}
		return off;
	}

	private static int copyAttrToWrappedIfExists(ClassBuffer cls, byte[] d, Method met, int copyCount,
			ModificationListStart copyToWrappedAttrs, int cpi) {
		if (cpi == 0)
			return copyCount;
		AttributeList attr = met.attributes;
		while (true) {
			attr = attr.get(cls, cpi);
			if (attr == null)
				return copyCount;
			copyCount = registerCopyAttrModification(d, copyToWrappedAttrs, copyCount, attr.attr);
			attr = attr.next;
			if (attr == null)
				return copyCount;
		}
	}

	private static int registerCopyAttrModification(byte[] d, ModificationListStart copyToWrappedAttrs, int copyCount,
			Attribute attr) {
		int off = attr.offset;
		byte[] data;
		try {
			data = new byte[attr.end - off];
		} catch (Throwable t) {
			if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
				System.err.write('n');
				System.err.write('c');
				System.err.write('c');
				System.err.write(':');
				System.err.write(' ');
				int cc = copyCount + 1;
				while (cc != 0) {// print reversed numbers but use less memory
					System.err.write('0' + (cc % 10));
					cc /= 10;
				}
				System.err.write('\n');
				System.err.write('a');
				System.err.write('l');
				System.err.write('o');
				System.err.write('c');
				System.err.write('S');
				System.err.write('i');
				System.err.write('z');
				System.err.write('e');
				System.err.write(':');
				System.err.write(' ');
				int len = attr.end - off;
				while (len != 0) {
					System.err.write('0' + (len % 10));
					len /= 10;
				}
				System.err.write(' ');
				System.err.write('b');
				System.err.write('y');
				System.err.write('t');
				System.err.write('e');
				System.err.write('s');
				System.err.write('\n');
			}
			throw t;
		}
		System.arraycopy(d, off, data, 0, data.length);
		ModificationList mod = new ModificationList();
		mod.offset = 8;
		mod.replacement = data;
		registerModification(copyToWrappedAttrs, mod);
		return copyCount + 1;
	}

	private static boolean needWideLdc(int cpi) {
		return (cpi & 0xFF) != cpi;
	}

	private static int ldcLen(int cpi) {
		if (needWideLdc(cpi))
			return 3;
		return 2;
	}

	private static int insertLDC(int cpi, byte[] newModData, int nmdOff) {
		if (needWideLdc(cpi)) {
			newModData[nmdOff] = (byte) 0x13;// ldc_w
			putUShort(newModData, nmdOff + 1, cpi);
			return nmdOff + 3;
		} else {
			newModData[nmdOff] = (byte) 0x12;// ldc
			newModData[nmdOff + 1] = (byte) cpi;
			return nmdOff + 2;
		}
	}

//	private static int ensureCPEntry(ModificationListStart cpMods, byte[] d, ConstantPool cp, byte[] cpe, CPIndices cpis) {
//		Integer i = cpis.other.get(cpe);
//		if ( i != null ) return i.intValue();
//		int cpi = ensureCPEntry0(cpMods, d, cp, cpe);
//		cpis.other.put(cpe, Integer.valueOf(cpi));
//		return cpi;
//	}

	private static int ensureCPEntry0(ModificationListStart cpMods, byte[] d, ConstantPool cp, byte[] cpe) {
		int cpi = cp.findCPEntry(d, cpe);
		if (cpi == 0) {
			cpi = addCPE(cpMods, d, cp, cpe);
		}
		return cpi;
	}

	private static void initClsAndMetStrCPIs(ModificationListStart cpMods, byte[] d, ConstantPool cp, CPIndices cpis,
			int nameCPI, int descCPI) {
		if (cpis.myClassStr == 0) {
			byte[] buf = new byte[3];
			buf[0] = ConstantPool.CONSTANT_STRING;
			int off = cp.offsets[getUShort(d, cp.end + 2) - 1];
			putUShort(buf, 1, getUShort(d, off + 1));
			cpis.myClassStr = ensureCPEntry0(cpMods, d, cp, buf);
		}
		byte[] buf = new byte[3];
		buf[0] = ConstantPool.CONSTANT_STRING;
		putUShort(buf, 1, nameCPI);
		cpis.curMetNameStr = ensureCPEntry0(cpMods, d, cp, buf);
		buf = new byte[3];
		buf[0] = ConstantPool.CONSTANT_STRING;
		putUShort(buf, 1, descCPI);
		cpis.curMetDescStr = ensureCPEntry0(cpMods, d, cp, buf);
	}

	private static void initPProfCPIs(ModificationListStart cpMods, byte[] d, ConstantPool cp, CPIndices cpis) {
		if (cpis.pprofClass == 0) {
			cpis.pprofClass = ~addCPE(cpMods, d, cp, PPROF_NAME_CPE);
		}
		if (cpis.pprofClass < 0) {
			byte[] buf = new byte[3];
			buf[0] = ConstantPool.CONSTANT_CLASS;
			putUShort(buf, 1, ~cpis.pprofClass);
			cpis.pprofClass = addCPE(cpMods, d, cp, buf);
		}
		if (cpis.pprofEnterMethodRef == 0) {
			cpis.pprofEnterMethodRef = initMethodRef(cpMods, d, cp, PPROF_ENTER_NAME_CPE, PPROF_ENTER_DESC_CPE,
					cpis.pprofClass);
		}
		if (cpis.pprofDirectExitMethodRef == 0) {
			cpis.pprofDirectExitMethodRef = initMethodRef(cpMods, d, cp, PPROF_DIRECT_EXIT_NAME_CPE,
					PPROF_DIRECT_EXIT_DESC_CPE, cpis.pprofClass);
		}
	}

	private static void initAttrCPIs(ModificationListStart cpMods, byte[] d, ConstantPool cp, CPIndices cpis) {
		if (getUShort(d, OFF_VERSION) >= STACK_TABLE_MIN_VERSION) {
			if (cpis.stackMapTableAttr == 0) {
				cpis.stackMapTableAttr = addCPE(cpMods, d, cp, STACK_MAP_TABLE_ATTR_CPE);
			}
			if (cpis.throwableClass == 0) {
				cpis.throwableClass = ~addCPE(cpMods, d, cp, THROWABLE_NAME_CPE);
			}
			if (cpis.throwableClass < 0) {
				byte[] buf = new byte[3];
				buf[0] = ConstantPool.CONSTANT_CLASS;
				putUShort(buf, 1, ~cpis.throwableClass);
				cpis.throwableClass = addCPE(cpMods, d, cp, buf);
			}
		}
	}

	private static void initSystemCPIs(ModificationListStart cpMods, byte[] d, ConstantPool cp, CPIndices cpis) {
		if (cpis.systemClass == 0) {
			cpis.systemClass = ~addCPE(cpMods, d, cp, SYSTEM_NAME_UTF_CPE);
		}
		if (cpis.systemClass < 0) {
			byte[] buf = new byte[3];
			buf[0] = ConstantPool.CONSTANT_CLASS;
			putUShort(buf, 1, ~cpis.systemClass);
			cpis.systemClass = addCPE(cpMods, d, cp, buf);
		}
		if (cpis.systemNanoMethodRef == 0) {
			cpis.systemNanoMethodRef = initMethodRef(cpMods, d, cp, SYSTEM_NANO_TIME_NAME_UTF_CPE,
					SYSTEM_TIME_DESC_UTF_CPE, cpis.systemClass);
		}
	}

	private static int initMethodRef(ModificationListStart cpMods, byte[] d, ConstantPool cp, byte[] methodNameUtfCpe,
			byte[] descUtfCpe, int classCPI) {
		byte[] buf = new byte[5];
		buf[0] = ConstantPool.CONSTANT_NAME_AND_TYPE;
		int natcpi = cp.find2RefsCPEntry(d, buf, methodNameUtfCpe, descUtfCpe);
		if (natcpi < 0) {
			natcpi = ~natcpi;
			int nanoTimeCPI = natcpi & 0xFFFF;
			if (nanoTimeCPI == 0) {
				nanoTimeCPI = addCPE(cpMods, d, cp, methodNameUtfCpe);
			}
			int timeDescCPI = (natcpi >>> 16) & 0xFFFF;
			if (timeDescCPI == 0) {
				timeDescCPI = addCPE(cpMods, d, cp, descUtfCpe);
			}
			putUShort(buf, 1, nanoTimeCPI);
			putUShort(buf, 3, timeDescCPI);
			natcpi = addCPE(cpMods, d, cp, buf);
			buf = new byte[5];
		}
		buf[0] = ConstantPool.CONSTANT_METHODREF;
		putUShort(buf, 1, classCPI);
		putUShort(buf, 3, natcpi);
		int cpi = cp.findCPEntry(d, buf);
		if (cpi != 0)
			return cpi;
		return addCPE(cpMods, d, cp, buf);
	}

	private static int addCPE(ModificationListStart cpMods, byte[] d, ConstantPool cp, byte[] cpe) {
		int index = incUShort(d, OFF_CP_LENP1) - 1;
		ModificationList mod = new ModificationList();
		mod.offset = cp.end;
		mod.replacement = cpe;
//		codeMod.debug = "constant pool entry: ";
		registerModification(cpMods, mod);
		return index;
	}

	private static byte[] applyModifications(ModificationListStart mods, byte[] oldData) {
		if (mods.end == null) {
			return oldData;
		}
		byte[] newData = new byte[oldData.length + mods.sizeMod];
		ModificationList ml = mods;
		int addOff = 0;
		int dataOff = 0;
		do {
			if (ml.offset != dataOff) {
				System.arraycopy(oldData, dataOff, newData, dataOff + addOff, ml.offset - dataOff);
			}
			dataOff = ml.offset;
			byte[] repl = ml.replacement;
			if (repl != null) {
				System.arraycopy(repl, 0, newData, ml.offset + addOff, repl.length);
				addOff += repl.length;
			}
			if (ml.length != 0) {
				addOff -= ml.length;
				dataOff += ml.length;
			}
			ml = ml.next;
		} while (ml != null);
		System.arraycopy(oldData, dataOff, newData, dataOff + addOff, oldData.length - dataOff);
		if (oldData.length + addOff != newData.length) {
			throw new AssertionError();
		}
		return newData;
	}

	private static int loadStoreLocalVar(byte[] d, int off, int localVar, int smallLoadOpcode, int longLoadOpcode) {
		int result;
		if (localVar < 4) {
			d[off] = (byte) (smallLoadOpcode + localVar);
			result = off + 1;
		} else if ((localVar & 0xFF) == localVar) {
			d[off] = (byte) longLoadOpcode;
			d[off + 1] = (byte) localVar;
			result = off + 2;
		} else {
			d[off] = (byte) 0xC4;
			d[off + 1] = (byte) longLoadOpcode;
			putUShort(d, off + 2, localVar);
			result = off + 4;
		}
		if (result != off + loadStoreOffAdd(localVar)) {
			throw new AssertionError(
					off + "ls" + localVar + " :: " + result + " : " + (off + loadStoreOffAdd(localVar)));
		}
		return result;
	}

	private static long countStackSizeLocalVarsCommandSize(ClassBuffer cls, int descCPE, int accessFlags, int localVars,
			int resultAdditionalStackSize) {
		byte[] d = cls.buffer();
		int offset = cls.constPool().offsets[descCPE - 1];
		offset += 4;
		int resLV = 0;
		int resCS = 0;
		if ((accessFlags & MET_ACC_STATIC) == 0) {
			resLV++;
			resCS++;
		}
		loop: for (;; offset++) {
			int nresLV;
			switch (d[offset]) {
			case '[': {
				offset += arrayClassNameLength(d, offset);
				nresLV = resLV + 1;
				break;
			}
			case 'L': {
				while (d[++offset] != ';')
					;
				nresLV = resLV + 1;
				break;
			}
			case 'B':
			case 'C':
			case 'F':
			case 'I':
			case 'S':
			case 'Z':
				nresLV = resLV + 1;
				break;
			case 'D':
			case 'J':
				nresLV = resLV + 2;
				break;
			case ')':
				break loop;
			default:
				throw new AssertionError((char) d[offset]);
			}
			resCS += loadStoreOffAdd(resLV);
			resLV = nresLV;
		}
		int resPS = resLV - localVars;
		int resSS = resLV - localVars;
		switch (d[offset + 1]) {
		case 'V':
			break;
		case '[':
		case 'L':
		case 'B':
		case 'C':
		case 'F':
		case 'I':
		case 'S':
		case 'Z': {
			if (resSS < 1 + resultAdditionalStackSize)
				resSS = 1 + resultAdditionalStackSize;
			break;
		}
		case 'D':
		case 'J': {
			if (resSS < 2 + resultAdditionalStackSize)
				resSS = 2 + resultAdditionalStackSize;
			break;
		}
		default:
			throw new AssertionError((char) d[offset + 1]);
		}
		resCS += 4;// invoke(static|virtual) indexUShort, [.]return
		return 0xFFFFL & resLV | ((0xFFL & resSS) << 16) | ((0xFFL & resPS) << 24) | ((0xFFFFFFFFL & resCS) << 32);
	}

	private static int loadStoreOffAdd(int localVar) {
		if (localVar < 4)
			return 1;
		else if ((localVar & 0xFF) == localVar)
			return 2;
		else
			return 4;
	}

	private static int arrayClassNameLength(byte[] d, int offset) throws AssertionError {
		int len = 0;
		while (d[offset + ++len] == '[')
			;
		switch (d[offset + len]) {
		case 'L': {
			while (d[offset + ++len] != ';')
				;
			break;
		}
		case 'B':
		case 'C':
		case 'F':
		case 'I':
		case 'S':
		case 'Z':
		case 'D':
		case 'J':
			break;
		default:
			throw new AssertionError((char) d[offset + len]);
		}
		return len;
	}

	private static void registerModification(ModificationListStart mods, ModificationList newMod) {
		ModificationList end = newMod;
		if (newMod instanceof ModificationListStart) {
			ModificationListStart newModStart = (ModificationListStart) newMod;
			if (newModStart.end != null) {
				end = newModStart.end;
			} else {
				throw new AssertionError("empty modification passed to registerModification as newMod");
			}
			mods.sizeMod += newModStart.sizeMod;
		} else {
			mods.sizeMod -= newMod.length;
			if (newMod.replacement != null) {
				mods.sizeMod += newMod.replacement.length;
			}
		}
		ModificationList lastEnd = mods.end;
		if (lastEnd == null) {
			mods.offset = newMod.offset;
			mods.length = newMod.length;
			mods.replacement = newMod.replacement;
			end = mods;
			if (newMod instanceof ModificationListStart) {
				ModificationListStart mls = (ModificationListStart) newMod;
				if (mls.next != null) {
					end = mls.end;
					mods.next = mls.next;
				}
			}
		} else {
			lastEnd.next = newMod;
		}
		mods.end = end;
	}

	public static byte[] transformMyInternBootstrap(ClassBuffer cls, String className, Class<?> classBeingRedefined) {
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			sysw("TRANSFORM MY INTERN CLASS ");
			sysw(className);
			sysw(" (it is PatrProfiler or a nested class)\n");
		}
		byte[] d = cls.buffer();
		ConstantPool cp = cls.constPool();
		int i = cp.findCPEntry(d, SUK_MISC_UNSAFE_CPE);
		int oi = cp.findCPEntry(d, SUK_MISC_UNSAFE_GET_UNSAFE_CPE);
		if (i != 0) {
			final int kOffset = 3 + 3 - 1;
			if (d[cp.offsets[i - 1] + kOffset] != 'k') {
				sysw("SUK_MISC_UNSAFE=[");
				byte[] buf = new byte[8];
				for (i = 0; i < SUK_MISC_UNSAFE_CPE.length; i++) {
					int off = 0;
					if (i != 0) {
						buf[0] = ',';
						off = 1;
					}
					if (i >= 10) {
						buf[off] = (byte) ('0' + (i / 10));
						buf[off + 1] = (byte) ('0' + (i % 10));
						off += 2;
					} else {
						buf[off++] = (byte) ('0' + i);
					}
					buf[off++] = '=';
					if (!Character.isISOControl(SUK_MISC_UNSAFE_CPE[i])) {
						buf[off] = '\'';
						buf[off + 1] = SUK_MISC_UNSAFE_CPE[i];
						buf[off + 2] = '\'';
						buf[off + 3] = ':';
						off += 4;
					}
					sysw(buf, 0, off);
					ClassBuffer.writeHex(SUK_MISC_UNSAFE_CPE, i, 1);
				}
				sysw("]");
				throw new AssertionError();
			}
			d[cp.offsets[i - 1] + kOffset] = 'n';
		}
		if (oi != 0) {
			final int kOffset = 3 + 2 + 1 + 3 - 1;
			if (d[cp.offsets[oi - 1] + kOffset] != 'k') {
				throw new AssertionError();
			}
			d[cp.offsets[oi - 1] + kOffset] = 'n';
		}
		if (i != 0) {
			return d;
		}
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			sysew("skip my intern class (no Unsafe access)\n");
		}
		return null;
	}

	public static byte[] transformSunMiscUnsafe(ClassBuffer cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		MethodList mets = cls.methods();
		ConstantPool cp = cls.constPool();
		byte[] d = cls.buffer();
		int[] offs = cp.offsets;
		for (int cpi = 0; cpi < offs.length; cpi++) {
			int off = offs[cpi];
			if (off == -1)
				continue;
			if (d[off] != ConstantPool.CONSTANT_FIELDREF)
				continue;
			if (getUShort(d, off + 1) != getUShort(d, cp.end + 2))
				continue;
			int nameAndTypeOffset = offs[getUShort(d, off + 3) - 1];
			int nameOffset = offs[getUShort(d, nameAndTypeOffset + 1) - 1];
			if (!isModUTF8(d, SUN_MISC_UNSAFE_THE_UNSAFE, nameOffset))
				continue;

			Method getUnsafe = mets.get(cls, SUN_MISC_UNSAFE_GET_UNSAFE_NAME, SUN_MISC_UNSAFE_GET_UNSAFE_DESC);
			if (getUnsafe == null) {
				sysew("skip sun.misc.Unsafe (getUnsafe()Lsun/misc/Unsafe; not found)\n");
				return null;
			}
			Attribute code = getUnsafe.attributes.get(cls, cp.findCPEntry(d, CODE_ATTR_CPE)).attr;

			ModificationListStart mod = new ModificationListStart();
			off = code.offset + 2;
			mod.offset = off;
			mod.length = code.end - mod.offset;
			mod.sizeMod = 20 - mod.length;
			byte[] repl = new byte[20];
			mod.replacement = repl;
			putInt(repl, 0, 16 /* attribute_length */);
			putUShort(repl, 4, 1 /* max_stack */);
			putUShort(repl, 6, 0 /* max_locals */);
			putInt(repl, 8, 4 /* code_length */);
			repl[12] = (byte) 0xB2; // code: getstatic
			putUShort(repl, 13, cpi + 1 /* code: getstatic: indexbyte[12] */);
			repl[15] = (byte) 0xB0; // code: areturn
			putUShort(repl, 16, 0 /* exception_table_length */);
			putUShort(repl, 18, 0 /* attributes_count */);
			repl = applyModifications(mod, d);
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("skip sun.misc.Unsafe (thats a lie, getUnsafe() now just returns theUnsafe)\n");
			}
			if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
				cls.data(repl);
				cls.readCP();
				cls.readMethods();
				cls.readAttributes();
			}
			return repl;
		}
		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			sysew("skip the Unsafe (theUnsafe not found)\n");
		}
		return null;
	}

	public static byte[] transformPProfilerAgent(ClassBuffer cls, Class<?> classBeingRedefined) {
		ConstantPool cp = cls.constPool();
		byte[] d = cls.buffer();
		if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
			sysw("TRANSFORM PProfilerAgent\n");
		}
		byte[] toStringConstant = getBytes(PProfilerAgent.name());
		int[] offs = cp.offsets;
		for (int i = 0; i < offs.length; i++) {
			int off = offs[i];
			if (off == -1)
				continue;
			if (d[off] != ConstantPool.CONSTANT_UTF8)
				continue;
			if (!isModUTF8(d, toStringConstant, off))
				continue;
			if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
				sysw("MODIFY PProfilerAgent\n");
			}
			int remOff = PProfilerAgent.name().indexOf('[');
			if (remOff == -1) {
				if (LOG_LEVEL >= MIN_OUT_LOG_LEVEL) {
					sysw("PProfilerAgent.name() does not contains a \'[\'\n");
				}
				return null;
			}
			int removeSize = Math.min(2, toStringConstant.length - remOff - 1);
			byte[] newClassFile = new byte[d.length - removeSize];
			int removeOffset = off + remOff + 4;
			int continueOffset = removeOffset + 2;
			int strStart = off + 3;
			System.arraycopy(d, 0, newClassFile, 0, off + 1);
			putUShort(newClassFile, off + 1, getUShort(d, off + 1) - removeSize);
			System.arraycopy(d, strStart, newClassFile, strStart, removeOffset - strStart);
			System.arraycopy(d, continueOffset, newClassFile, removeOffset, d.length - continueOffset);
			return newClassFile;
		}
		if (LOG_LEVEL >= MIN_ERR_LOG_LEVEL) {
			sysew("I could not find the method PProfilerAgent.name()\n");
		}
		return null;
	}

}
