// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import de.hechler.patrick.profiler.agent.ClassBuffer;
import de.hechler.patrick.profiler.agent.PProfilerAgent;

/**
 * this class is used to transform classes so that they can be profiled by the
 * {@link PProfilerAgent}
 */
public class MyWrapClassTransformer8 extends AbstractClassTransformer<ClassBuffer, String> {

	@Override
	protected ClassBuffer createHelper(String module, ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer)
			throws IllegalClassFormatException {
		return MyWrapImpl8.createHelper(module, loader, className, classBeingRedefined, protectionDomain, classfileBuffer);
	}

	@Override
	protected byte[] transformOtherOrBootstrap(ClassBuffer cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		return MyWrapImpl8.transformOtherOrBootstrap(this, cls, clsName, classBeingRedefined);
	}

	@Override
	protected byte[] transformOther(ClassBuffer cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		return MyWrapImpl8.transformOther(cls, clsName, classBeingRedefined);
	}

	@Override
	protected byte[] transformMyInternBootstrap(ClassBuffer cls, String clsName, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		return MyWrapImpl8.transformMyInternBootstrap(cls, clsName, classBeingRedefined);
	}

	@Override
	protected byte[] transformSunMiscUnsafe(ClassBuffer cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		return MyWrapImpl8.transformSunMiscUnsafe(cls, classBeingRedefined);
	}

	@Override
	protected byte[] transformPProfilerAgent(ClassBuffer cls, Class<?> classBeingRedefined)
			throws IllegalClassFormatException {
		return MyWrapImpl8.transformPProfilerAgent(cls, classBeingRedefined);
	}


}
