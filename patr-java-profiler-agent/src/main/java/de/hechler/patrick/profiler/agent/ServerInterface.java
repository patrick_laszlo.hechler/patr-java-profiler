// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import java.lang.instrument.Instrumentation;

public interface ServerInterface {

	void initArgs(String args);
	
	void parseArg(int off, int end);

	void start(Object ct, Instrumentation inst);
	
}
