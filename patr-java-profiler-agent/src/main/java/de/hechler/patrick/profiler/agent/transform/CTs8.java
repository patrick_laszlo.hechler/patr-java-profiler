// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent.transform;

import de.hechler.patrick.profiler.agent.PProfilerAgent;

public class CTs8 extends CTs {

	@Override
	public String[] allImpl() {
		return new String[] { "default", "wrap", "my-wrap", "my-wrap8" };
	}

	@Override
	public String descImpl(String name) {
		switch (name) {
		case "default":
			return "the default class file transformer, always present";
		case "wrap":
			return "wraps all methods, is not able to transform <init>/<clinit>";
		case "my-wrap":
			return "wrap implementation that does not use any API";
		case "my-wrap8":
			return "same as wrap-impl";
		default:
			return "unknown transformer: '" + name + '\'';
		}
	}

	@Override
	public AbstractClassTransformer<?, ?> getImpl(String name, int off, int end) {
		if (PProfilerAgent.isArg(off, end, name, "default")) {
			return new DefaultClassTransformer();
		}
		if (PProfilerAgent.isArg(off, end, name, "wrap")) {
			return new WrapClassTransformer();
		}
		if (PProfilerAgent.isArg(off, end, name, "my-wrap")) {
			return new MyWrapClassTransformer();
		}
		if (PProfilerAgent.isArg(off, end, name, "my-wrap8")) {
			return new MyWrapClassTransformer8();
		}
		return null;
	}

	@Override
	public String nameImpl(AbstractClassTransformer<?, ?> ct) {
		if (ct instanceof MyWrapClassTransformer8) {
			return "my-wrap8";
		}
		return null;
	}

}
