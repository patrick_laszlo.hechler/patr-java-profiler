// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.profiler.agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

import de.hechler.patrick.profiler.agent.transform.CTs;
import de.hechler.patrick.profiler.agent.transform.CTs8;

/**
 * the real agent class which registers a {@link ClassFileTransformer} to profile classes<br>
 * this class is only a little wrapper around {@link PProfilerAgent} to make the usage of Multi-Release easier
 */
public class PJPAgent {
	
	private PJPAgent() {}
	
	/**
	 * the agents pre-main method called when the agent is registered using the {@code -javaagent} command line argument
	 * 
	 * @param agentArgs the arguments to be parsed
	 * @param inst      the {@link Instrumentation}
	 */
	public static void premain(String agentArgs, Instrumentation inst) {
		CTs.init(new CTs8());
		PProfilerAgent.premain(agentArgs, inst);
	}
	
}
