Patr-Java-Profiler
==================

An Open source  java profiler completely written in java.    
The profiler consist of:
+ [java agent](patr-java-profiler-agent/README.md)
    + contains the agent, which transforms classes so that they use the bootstrap library
+ [bootstrap library](patr-java-profiler-bootstrap/README.md)
    + contains the bootstrap, which does the actual profiling
+ [server library](patr-java-profiler-server/README.md)
    + contains the server daemon, which allows clients to show the user a live view
+ [client program](patr-java-profiler-client/README.md)
    + contains the client, which can interpret the output file of the profiler and connect to the server
+ [test project](patr-java-profiler-test/README.md)
    + Sample/Dummy Project used to test the profiler

Sections:
+ [Install](#install)
    + install the profiler or look at the source
+ [Usage](#usage)
    + Use the profiler
+ [Troubleshooting](#troubleshooting)
    + fix the profiler

# Install

+ See the [Source](#source-code)
+ Install [pre-build jars](#install-pre-build-jars)
+ Use/Install with a [Maven](#use-with-maven)
+ Install with [pacman](#install-with-pacman)

## Source Code
The *Patr-Java-Profiler* sources can be found on the *latest* (or *latest-snapshot*) branch:
+ The *latest* branch contains the latest non-snapshot version.
    + the latest non-snapshot version should always work
+ The *latest-snapshot* branch contains the latest version
    + note that the *latest-snapshot* branch can be in an unusable state.
    + note that the *latest-snapshot* branch does not need to contain a snapshot version.
        + *latest-snapshot* holds the newest version there currently is, that may also be a non-snapshot version.

### Build from Source

#### Build manually

+ Each project has the source files located at `src/main/java`
+ resource files are located at `src/main/resources`
+ Additional Multi-Release source files may be located in `src/main/java-mr/<version>/java`
    + they depend on the Non-Multi-Release sources and the Multi-Release sources with lower versions
    + they should be compiled to `META-INF/versions/<version>`


The source files need to be compiled to `.class` files.    
The resource files need to be copied to the (same) class path.

Note that the agent sub-project has an important MANIFEST file inside of its resource folder. Some tools ignore it and create their own MANIFEST file in the class path, make sure this does not happen if you are using a tool.

additional the `VERSION` file has to be copied to each class path at the location `de/hechler/patrick/profiler/`*&lt;sub-project-name&gt;*.    
*sub-project-name* is the name of the sub project without the leading '`patr-java-profiler-`'.    
The *client* sub-project also needs the `de/hechler/patrick/profiler/transmit/Constants.java` file from the *server* sub-project to be copied to its own source folder.

#### Build with Patr-Java-Build
*Patr-Java-Build* can be used to build this project:    
+ you can get a prebuilt *Patr-Java-Build* jar from the [nexus](https://nexuspat.hechler.de/#browse/browse:maven-releases:de%2Fhechler%2Fpatrick%2Fbuild%2Fpatr-java-build) repository
+ alternativly you can build *Patr-Java-Build* with maven (`mvn package/compile`) or (if you already have built it once) with itself.    

Assertions:
+ the sub-projects assume that their location is a direct sub-folder of their parent project and that the root project contains a `VERSION` file.
+ The client sub-project also assumes the presence of the server sub-project as its sibling and copies the `Constants.java` file from it.
+ These expectations should all be satisfied in the git repository.

simply use `java -jar patr-java-build.jar package`.
It is important that you are currently in the projects root directory of the project.
+ this command can be used to build either the *Patr-Java-Profiler* or the *Patr-Java-Build* project.

#### Build with maven
Multi-Release is not supported when building with maven.    
to get Multi-Release functionality build with `patr-java-build`.

Assertions:
+ the sub-projects assume that their location is a direct sub-folder of their parent project and that the root project contains a `VERSION` file.
+ The client sub-project also assumes the presence of the server sub-project as its sibling and copies the `Constants.java` file from it.
+ These expectations should all be satisfied in the git repository.

To build the command `mvn package` can be used (inside of the root-project).    
Execute the command inside of a sub-project to only build the sub-project.

## Install Pre-Build jars

For pre-build jars use the [Nexus Repository](https://nexuspat.hechler.de/#browse/browse):
+ [releases](https://nexuspat.hechler.de/#browse/browse:maven-releases:de%2Fhechler%2Fpatrick%2Fprofiler)
+ [snapshots](https://nexuspat.hechler.de/#browse/browse:maven-snapshots:de%2Fhechler%2Fpatrick%2Fprofiler)

You need a some jar files (replace `$VERSION` with the version):
+ `patr-java-profiler-agent-$VERSION-jar-with-dependencies.jar`
    + located at `de/hechler/patrick/profiler/patr-java-profiler-agent/$VERSION/`
    + needed to run the profiler
+ `patr-java-profiler-bootstrap-$VERSION.jar`
    + located at `de/hechler/patrick/profiler/patr-java-profiler-bootstrap/$VERSION/`
    + needed to run the profiler
+ `patr-java-profiler-server-$VERSION.jar`
    + located at `de/hechler/patrick/profiler/patr-java-profiler-server/$VERSION/`
        + (as soon as there is a snapshot/release with the server)
    + needed if the profiler should allow clients to retrieve results before the program has finished
+ `patr-java-profiler-client-$VERSION.jar`
    + located at `de/hechler/patrick/profiler/patr-java-profiler-client/$VERSION/`
        + (as soon as there is a snapshot/release with the client)
    + needed to read/interpret the result of the profiler or to connect to a running profiler with server

## Install with pacman

If you use `pacman` as package manager you can install the *Patr-Java-Profiler* with it:
1. `git clone` *&lt;patrjprof-package&gt;*
    + `https://aur.archlinux.org/patrjprof.git`
    + `https://aur.archlinux.org/patrjprof-git.git`
        + may contain new features, but may be unstable
2. run `makepkg`
    + see the arch linux wiki for [makepkg](wiki.archlinux.org/title/Makepkg#Usage) for details
3. run `pacman -U` *&lt;package&gt;*
    + the package should be named something like `patrjprof-VERSION-any.pkg.tar.zst`
    + see the arch linux wiki for [pacman](wiki.archlinux.org/title/Pacman#Additional_commands)
4. [start the profiler](#start-profiler)

# Usage
To use the profiler with a Maven Project see the [Use with Maven](#use-with-maven) section.

## Start Profiler
You can use the `patr-java-prof.sh` script (if you have a shell):
+ do not execute `java` directly, but instead the script
    + use the same arguments you whould normally pass to `java` when you don't want to profile
        + with the `--help` argument the scripts output will differ to the output java
        + the script may also support some additional arguments like `--agent <OPTS>`, use `--help` for an overview
        + by default the script will start the program and connect a client to it
+ If you installed the [patrjprof package with pacman](#install-with-pacman):
    + you can also use `/usr/bin/patrjprof`

Alternativly you can do what the script whould try and do the following:
1. add the following the the JVM arguments and start the application
    + `-javaagent:`/path/to/patr-java-profiler-agent-jar-with-dependencies.jar
        + to pass options to the agent append `=be_nice,be_polite` to the end
        + If you installed the [patrjprof package with pacman](#install-with-pacman):
            + you can use `-javaagent:/usr/share/java/patrjprof/patr-java-profiler-agent.jar`
    + `-Xbootclasspath/a:`/path/to/patr-java-profiler-bootstrap.jar
        + If you installed the [patrjprof package with pacman](#install-with-pacman):
            + you can use `-javaagent:/usr/share/java/patrjprof/patr-java-profiler-bootstrap.jar`
    + `-Xbootclasspath/a:`/path/to/patr-java-profiler-server.jar
        + If you installed the [patrjprof package with pacman](#install-with-pacman):
            + you can use `-javaagent:/usr/share/java/patrjprof/patr-java-profiler-server.jar`
        + only needed when a live view of the profiled application is needed
        + use `daemon.help` as agent option for information
2. if needed: start a client
    + `java -cp` /path/to/patr-java-profiler-client.jar `de.hechler.patrick.profiler.client.Client` *&lt;client-arguments&gt;*
        + If you installed the [patrjprof package with pacman](#install-with-pacman):
            + you can use `-javaagent:/usr/share/java/patrjprof/patr-java-profiler-client.jar`
        + use `--help` for more information
        + use `--file` *&lt;ouput-file&gt;* to view the [output file](#final-result) generated when the profiled program terminates
        + note that the profiled program has to be started with server to retrieve a live view
            + use `--port` *&lt;port&gt;* to connect to a profiler running at *port*
            + use `--socket` *&lt;socket-file&gt;* to connect to a profiler using the *socket-file*
                + only works if the JVM version is at least 16

## Final Result
When the program terminates (this will need more time than usual) the profiler will store its result in a file.    
If the file already existed it will be overwritten. If the program does not have the permission to do so (or the file is a directory) a stack trace may be printed, but nothing else will happen.

+ See [File Location](#file-location) for the location of the file
+ See [File Content](#file-content) for an explanation of the files content
+ See [Missing File](#missing-file) for a set of exception when the file will **not** be generated

### File Location
If the Property `pat.jprof.finish-file` is sets, its value will be used otherwise the file will be `./patr-java-profiler-output.data`.
+ Set this property using `-Dpat.jprof.finish-file=testout/patrjprof.data`.
    + Alternatively the java program can set/modify it using `System.setProperty` (the final value of the property will be used).
+ If the value of the property is `none` no file will be generated

### File Content

The content of the file can be interpreted using the *client* sub-project using the `--file` argument.

### Missing File

The file will not or only partly be generated if:
+ the program receives a `SIGKILL`.
+ the program calls `Runtime.getRuntime().halt(exitNum)`.
+ a Shutdown Hook (see `Runtime.getRuntime().addShutdownHook(Thread)`) calls `Runtime.getRuntime().exit(exitNum)` with a non-zero exit status.
+ the JVM crashes.
+ the program never calls any profiled code.
+ the value of the property `pat.jprof.finish-file` is `none`.
    + see [File Location](#file-location) for more information

## Use with Maven
If you use maven, you can add the following to your pom:    
These snippets assume, that `${patrjprof.version}` is set to the version to install (for snapshot versions append `-SNAPSHOT` to the version string).    
To look which versions are available you can look at the [nexus repository](#install-pre-build-jars)

This snippet adds the Maven Repository for the *Patr-Java-Profiler* project.     
If you only want to use releases (or only snapshots), you need to add only the `nexuspatreleases` (or only `nexuspatsnapshots`)    
In the `repositories` section:
```
<repository>
    <id>nexuspatreleases</id>
    <url>https://nexuspat.hechler.de/repository/maven-releases/</url>
</repository>
<repository>
    <id>nexuspatsnapshots</id>
    <url>https://nexuspat.hechler.de/repository/maven-snapshots/</url>
</repository>
```

This snippet declares two dependencies, one for the agent and and one for the bootstrap library.     
In the `dependencies` section:
```
<dependency>
    <groupId>de.hechler.patrick.profiler</groupId>
    <artifactId>patr-java-profiler-agent</artifactId>
    <version>${patrjprof.version}</version>
    <scope>test</scope> <!-- If you want these dependencies to appear in non-test build remove this line -->
</dependency>
<dependency>
    <groupId>de.hechler.patrick.profiler</groupId>
    <artifactId>patr-java-profiler-bootstrap</artifactId>
    <version>${patrjprof.version}</version>
    <scope>test</scope> <!-- If you want these dependencies to appear in non-test build remove this line -->
</dependency>    
 ```

### Execute profiler as maven goal
You can add the following snippets, so that you can profile your project with `mvn exec:exec`:     
Note that `exec:java` does not work, because the *Patr-Java-Profiler* is added to the JVM with command line arguments.    
If your Maven repositories are **not** stored in `~/.m2/repository` you need to modify the `plugin`&gt;`configuration`&gt;`commandlineArgs` section of this snippet.    
In the `build`&gt;`plugins` section:
 ```
<plugin>
    <groupId>org.codehaus.mojo</groupId>
    <artifactId>exec-maven-plugin</artifactId>
    <version>3.2.0</version>
    <configuration>
        <executable>${java.home}/bin/java</executable>
        <commandlineArgs>
            -javaagent:${user.home}/.m2/repository/de/hechler/patrick/profiler/patr-java-profiler-agent/${patrjprof.version}/patr-java-profiler-agent-${patrjprof.version}-jar-with-dependencies.jar <!-- do not remove all non-comment spaces between the arguments -->
            -Xbootclasspath/a:${user.home}/.m2/repository/de/hechler/patrick/profiler/patr-java-profiler-bootstrap/${patrjprof.version}/patr-java-profiler-bootstrap-${patrjprof.version}.jar
        </commandlineArgs>
        <mainClass>de.hechler.patrick.profiler.test.TestClassFile</mainClass>
    </configuration>
</plugin>
 ```

# Troubleshooting

If you found a bug, please create a new [issue](https://git.rwth-aachen.de/patrick_laszlo.hechler/patr-java-profiler/-/issues)

known problems:
+ NoSuchMethodDefError: `ByteBuffer.position/limit()ByteBuffer`
    + this error can occur when the compile java version is above the runtime version
    + workaround:
        + try to re-build the server and client jar with java8
        + try to run the program with java17
    + fix:
        + before calling `ByteBuffer.position/limit` cast to `Buffer`
    + reason:
        + these methods return the `Buffer` object (like `new StringBuilder().append("abc").append("cde").toString();`)
        + in java8 the ByteBuffer class did not overwrite `position()` and `limit()`
            + thus the methods return an object of type `Buffer`
        + in newer java versions (at least in java17) `ByteBuffer` overwrites those methods
            + the implementations `ByteBuffer` call the super methods, return themself and have `ByteBuffer` as return type
        + when the code is compiled with a new java version the compiler usually uses the library of the new java version
            + the compiler sees the method `ByteBuffer.position() -> return ByteBuffer` in its library
            + note that a method call in the java bytecode is implemented using a `invoke*` (here `invokevirtual`) instruction
            + the `invoke*` instructions use method references, which have method descriptors
                + the method descriptor of `Buffer.limit(int) -> return Buffer` is `(I)Ljava/nio/Buffer;`
                + the compiler used `ByteBuffer.limit(int) -> return ByteBuffer` and with the method descriptor `(I)Ljava/nio/ByteBuffer;`
                + now the jvm wants to execute the `invokevirtual` instruction:
                    + methodreference: class: `ByteBuffer`, name: `limit`, descriptor: `(I)Ljava/nio/ByteBuffer;`
                    + so the jvm searches for the referenced method, but is not able to find it, thus the error
