#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later

set -e

function follow_links {
  FL_PATH="$1"
  while true ; do
    if test -L "$FL_PATH"; then
      LINK="$(readlink -n "$FL_PATH")"
      if [[ "$LINK" =~ '^/.*$'  ]] ; then
        FL_PATH="$(dirname $FL_PATH)/$LINK"
      else
        FL_PATH="$LINK"
      fi
    elif test ! -e "$FL_PATH"; then
      echo "  is a link" >&2
      return 1
    else
      break
    fi
  done
  echo -n "$FL_PATH"
}

function parent_dir_follow_links {
  DIR="$(follow_links "$1")"
  DIR="$(dirname "$DIR")"
  DIR="$(follow_links "$DIR")" # this is not really needed
  echo -n "$DIR"
}

if test " $WD"               = " "; then WD="$(parent_dir_follow_links "$0")"                                       ; fi

# base settings
if test " $VERSION"          = " "; then VERSION="$(cat $WD/VERSION)"                                               ; fi

if test " $BOOTSTRAP_FOLDER" = " "; then BOOTSTRAP_FOLDER="$WD/patr-java-profiler-bootstrap/target"                 ; fi
if test " $AGENT_FOLDER"     = " "; then AGENT_FOLDER="$WD/patr-java-profiler-agent/target"                         ; fi
if test " $SERVER_FOLDER"    = " "; then SERVER_FOLDER="$WD/patr-java-profiler-server/target"                       ; fi
if test " $CLIENT_FOLDER"    = " "; then CLIENT_FOLDER="$WD/patr-java-profiler-client/target"                       ; fi

if test " $BOOTSTRAP_FNAME"   = " "; then BOOTSTRAP_FNAME="patr-java-profiler-bootstrap-$VERSION.jar"               ; fi
if test " $AGENT_FNAME"       = " "; then AGENT_FNAME="patr-java-profiler-agent-$VERSION-jar-with-dependencies.jar" ; fi
if test " $SERVER_FNAME"      = " "; then SERVER_FNAME="patr-java-profiler-server-$VERSION.jar"                     ; fi
if test " $CLIENT_FNAME"      = " "; then CLIENT_FNAME="patr-java-profiler-client-$VERSION.jar"                     ; fi

# used values
if test " $BOOTSTRAP_JAR" = " "; then
  BOOTSTRAP_JAR="$BOOTSTRAP_FOLDER/$BOOTSTRAP_FNAME"
fi
if test " $AGENT_JAR" = " "; then 
  AGENT_JAR="$AGENT_FOLDER/$AGENT_FNAME"
fi
if test " $SERVER_JAR" = " "; then 
  SERVER_JAR="$SERVER_FOLDER/$SERVER_FNAME"
fi
if test " $CLIENT_JAR" = " "; then 
  CLIENT_JAR="$CLIENT_FOLDER/$CLIENT_FNAME"
fi

source "$WD"/patr-java-prof-help.sh "$@"
