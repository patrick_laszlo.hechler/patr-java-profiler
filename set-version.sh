#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later

set -e

VERSION="$1"

function usage {
  echo "Usage: $(basename $0) [snap] <NEW_VERSION>" >&2
  echo "       $(basename $0) [branch] <NEW_VERSION> <NEW_BRANCH>" >&2
  exit 1
}

if test "$VERSION" = "snap"; then
  if test $# != 2; then
    usage
  fi
  VERSION="$2-SNAPSHOT"
elif test "$VERSION" = "branch"; then
  if test $# != 3; then
    usage
  fi
  VERSION="$2-$3-SNAPSHOT"
elif test $# != 1; then
  usage
fi

if test "$(echo -n "$VERSION" | wc -l)" != 0; then
  echo "multi line version"
  exit 1
elif test "$(echo -n "$VERSION" | wc -w)" != 1; then
  echo "multi word (or empty) version"
  exit 1
fi


function follow_links {
  FL_PATH="$1"
  while true ; do
    if test -L "$FL_PATH"; then
      LINK="$(readlink -n "$FL_PATH")"
      if [[ "$LINK" =~ '^/.*$'  ]] ; then
        FL_PATH="$(dirname $FL_PATH)/$LINK"
      else
        FL_PATH="$LINK"
      fi
    elif test ! -e "$FL_PATH"; then
      echo "  is a link" >&2
      return 1
    else
      break
    fi
  done
  echo -n "$FL_PATH"
}

function parent_dir_follow_links {
  DIR="$(follow_links "$1")"
  DIR="$(dirname "$DIR")"
  DIR="$(follow_links "$DIR")"
  echo -n "$DIR"
}

WD="$(parent_dir_follow_links "$0")"

cd "$WD"

echo "set version to $VERSION"
echo "set version in these files:"
echo "  VERSION"
echo -n $VERSION > VERSION

for DIR in ./ */; do
  POM="${DIR}pom.xml"
  if test -f "$POM"; then
    echo "  $POM"
    sed -E 's#^(\t\t?<version>)[a-zA-Z0-9._-]+(</version>)#\1'$VERSION'\2#' --in-place "$POM"
  else
    echo "the file $POM does not exist" >&2
    exit 1
  fi
  PJB="${DIR}pjb.json"
  if test -f "$PJB"; then
    echo "  $PJB"
    sed -E 's#^(  "version": ")[a-zA-Z0-9._-]+(",)#\1'$VERSION'\2#' --in-place "$PJB"
  else
    echo "the file $PJB does not exist" >&2
    exit 1
  fi
done
